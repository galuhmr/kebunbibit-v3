<link href="<?php echo base_url(); ?>public/css/sidebar-menu.css" rel="stylesheet">
<div style="padding: 25px; margin-top: 27px;" class="container white-bg">
   <div class="row">
    <div class="col-lg-2">
        <div class="span3">
            <section>
                <ul class="sidebar-menu">
                    <h3 class="kb-title">
                        kategori
                    </h3>
                    <?php foreach ($categories as $id => $category): ?>
                                    <li>
                                        <a class="li-cat" 
                                           href="<?php echo base_url(); ?>index.php/product/<?php echo $category->id_category . '/' . $category->link_rewrite?>">
                                           <?php echo $category->name ?>
                                        </a>
                           </li>
                    <?php endforeach; ?>
                </ul>
            </section> 
        </div> 
    </div>
      <div class="col-lg-1">
         <ul class="image-preview-small">
            <?php foreach($images as $image): ?>
            <li>
               <img class="img-small-click" src="<?php echo get_product_image($image); ?>">
            </li>
            <?php endforeach; ?>
         </ul>
      </div>
      <div class="col-lg-4">
         <img class="product-img" id="zoom-img" src="<?php echo get_product_image($images[0]); ?>">
      </div>
      <div class="col-lg-5 pull-right">
         <div id="product-detail-title"><?php echo $product->name; ?></div>
         <div id="product-detail-description">
            <?php echo empty($product->description) ? "<i style='color: #999;'>Tidak ada deskripsi untuk produk ini</i>" : $product->description; ?>
         </div>
         <div  id="product-detail-price">
            <?php echo format_to_rupiah($product->price); ?>
         </div>
         <div style="margin-top: 1em;">
            <?php if(!$is_mine): ?>
            <a style="text-align: center;" 
               class="kb-button kb-deal <?php $is_login ? "" : "show-login"; ?>" 
               href="<?php echo $url_deal; ?>">
            <i class="fa fa-angle-double-down"></i> Tawar Dulu
            </a>
            <span style="display: none;" class="c-product_formatted_price">
            <?php echo format_to_rupiah($product->price); ?>
            </span>
            <span style="display: none;" class="c-seller_name">
            <?php echo $product->firstname . " " . $product->lastname; ?>
            </span>
            <input type="hidden" class="c-id_seller" value="<?php echo $product->seller_id; ?>">
            <input type="hidden" class="c-product_id" value="<?php echo $product->product_id; ?>">
            <input type="hidden" class="c-product_name" value="<?php echo $product->name; ?>">
            <input type="hidden" class="c-product_price" value="<?php echo $product->price; ?>">
            <?php if($is_login): ?>
            <button class="kb-button kb-cart"><i class="fa fa-shopping-cart"></i> Beli Langsung</button>
            <?php else: ?>
            <button class="kb-button show-login" style="background-color: #7ac144;">
            <i class="fa fa-shopping-cart"></i> Beli
            </button>
            <?php endif; ?>   
            <?php else: ?>
            <input type="hidden" class="c-id_seller-edit" value="<?php echo $product->seller_id; ?>">
            <input type="hidden" class="c-product_id-edit" value="<?php echo $product->product_id; ?>">
            <input type="hidden" class="c-product_name-edit" value="<?php echo $product->name; ?>">
            <input type="hidden" class="c-product_price-edit" value="<?php echo $product->price; ?> ">
            <button style="background-color: #b9b9b9;" class="kb-button kb-update">
            <i class="fa fa-pencil-square-o"></i> Ubah
            </button>
            <button style="background-color: #d9534f; color: #FFF !important;" class="kb-button kb-delete">
            <i class="fa fa-trash-o"></i> Hapus
            </button>
            <?php endif; ?>
         </div>
      </div>
   </div>
</div>

<div style='display: none; position: absolute; z-index: 2; text-align: center;' class='update-product option-detail'>
   <div style='float: left'>
      <input type='text' class='edit-product-price' value="<?php echo $product->price + 0; ?>">
      <textarea class='edit-desc'><?php echo $product->description; ?></textarea>
      <button class='kb-button apply-edit'>Lanjutkan</button>
      <button class='kb-button cancel-edit'>Batal</button>
   </div>
</div>
<div style='display: none; text-align: center;
            height: 110px !important; top: 17em !important;' class='delete-product option-detail'>
   <p>
      Anda yakin ingin menghapus produk ini?
   </p>
   <button class='kb-button yes-delete'>Ya</button>
   <button class='kb-button no-delete'>Tidak</button>
</div>
<div style='display: none; text-align: center;' class='validate-product option-detail'>
   <div style='display: none;' class='progressbar'>
      <img src="<?php echo image_url(); ?>public/img/loading.gif">
      <p style='margin-top: 26px;font-family: 'Source Sans Pro', sans-serif;'>
         Tunggu Sebentar
      </p> 
   </div>
   <div style='display: none;' class='message'>
      <div style='display: none;' class='error'>
         <div class='alert alert-danger'>
            Harga minimum 10,000
         </div>
         <button class='kb-button try-again-edit'>Coba Lagi</button>
      </div>
      <div style='display: none;' class='success'>
         <div class='alert alert-success'></div>
         <button class='kb-button done-edit'>Selesai</button>
      </div>
   </div>
</div>
<div id="Z4ARdvivjhAjpMhnEUFF"></div>