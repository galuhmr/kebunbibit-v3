<?php
  defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container">
  <ol class="breadcrumb">
    <li><a href="#">Halaman Depan</a></li>
    <li><a href="#">Akun Saya</a></li>
    <li class="active">Transaksi</li>
  </ol>

  <div class="row">
      <div class="col-lg-3">
         <div class="panel panel-default">
            <div class="panel-heading panel-heading-kb">
               <h3 class="panel-title">Menu Saya</h3>
            </div>
            <div class="panel-body">
               <ul id="account-menu">
                  <?php $menus = account_menu(); ?>
                  <?php foreach($menus as $menu): ?>  
                    <li>
                       <a href="<?= $menu['link']; ?>"><?= $menu['name']; ?></a>
                    </li>
                  <?php endforeach; ?>
               </ul>
            </div><!-- .panel-body -->
         </div><!-- .panel -->
      </div><!-- .col-lg-3 -->
    <div class="col-lg-9">
      <div class="panel panel-default">
        <div class="panel-heading panel-heading-kb">
          <h3 class="panel-title">Transaksi</h3>
        </div>
        <div class="panel-body">
           <div class="clearfix">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#tab_a" data-toggle="tab">Pembelian</a></li>
              <li><a href="#tab_b" data-toggle="tab">Penjualan (3)</a></li>
              <li><a href="#tab_c" data-toggle="tab">Detail Jual</a></li>
            </ul>
            <br>
            <div class="tab-content">
                   <div class="tab-pane active" id="tab_a">
                        <table class="table table-bordered clearfix">
                          <thead>
                            <th>Label</th>
                            <th>Detail Transaksi</th>
                            <th>Penjual</th>
                            <th>Status Terakhir</th>
                            <th>Opsi</th>
                          </thead>
                          <tbody>
                            <tr>
                              <td>
                                <h4><span class="label label-primary">Beli</span></h4>
                              </td>
                              <td>
                                <div>
                                  <b>MP378654</b>
                                </div>
                                <div>
                                  <small>18 Agu 2016 12:49 WIB</small> 
                                </div>
                              </td>
                              <td>
                                <span>Galuh Muhamad Ramadhan</span>
                              </td>
                              <td>
                                <span>Belum Diproses</span>
                              </td>
                              <td>
                                <a href="#" class="btn btn-default btn-sm">
                                  Detail
                                </a>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                    </div>
                    <div class="tab-pane" id="tab_b">
                        <table class="table table-bordered clearfix">
                          <thead>
                            <th>Label</th>
                            <th>Detail Transaksi</th>
                            <th>Pembeli</th>
                            <th>Status Terakhir</th>
                            <th>Opsi</th>
                          </thead>
                          <tbody>
                            <tr>
                              <td>
                                <h4><span class="label label-success">Jual</span></h4>
                              </td>
                              <td>
                                <div>
                                  <b>MP378654</b>
                                </div>
                                <div>
                                  Rp 150,000
                                </div>
                                <div>
                                  <small>Batas waktu: 2 Hari</small> 
                                </div>
                              </td>
                              <td>
                                <span>Galuh Muhamad Ramadhan</span>
                              </td>
                              <td>
                                <span>Belum Diproses</span>
                              </td>
                              <td>
                                <a href="#" class="btn btn-default btn-sm">
                                  Dikirim
                                </a>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                    </div>
                    <div class="tab-pane" id="tab_c">
                      <h4>MP378654</h4>
                      <form class="form-horizontal">
                        <div class="form-group">
                          <label class="control-label col-sm-2" for="email">Waktu Terakhir:</label>
                          <div class="col-sm-10">
                            <span class="sell-detail-text">22 September 2016 01:59 WIB</span>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-2" for="pwd">Pembeli:</label>
                          <div class="col-sm-10"> 
                            <span class="sell-detail-text">Galuh Muhamad Ramadhan</span>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-2" for="email">Alamat:</label>
                          <div class="col-sm-10">
                            <span class="sell-detail-text">Jl. Raya Pandanrejo Batu Indonesia</span>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-2" for="pwd">No. Telepon:</label>
                          <div class="col-sm-10"> 
                            <span class="sell-detail-text">085642786198</span>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-2" for="pwd">No. Resi:</label>
                          <div class="col-sm-7"> 
                            <input type="text" style="padding: 5px 5px; position: relative; top: 3px;" id="resi-no" placeholder="Contoh: BDOP302730283615">
                          </div>
                          <div class="col-sm-3">
                            <button class="btn kb-button" style="margin: 3px;" type="button">Simpan</button>
                          </div>
                        </div>
                      </form>
                      <div class="row">
                        <div class="col-sm-2">
                          Status:
                        </div>
                        <div class="col-sm-10">
                          <input type="radio" name="order-status" value="0"> Belum Diproses<br>
                          <input type="radio" name="order-status" value="1"> Dibayar<br>
                          <input type="radio" name="order-status" value="2"> Other
                        </div>
                      </div>
                    </div>
            </div><!-- tab content -->

            </div><!-- end right-tabs -->  
        </div>
      </div>
    </div>
  </div>

</div>

<script type="text/javascript">
  var filter="buyer";
  var page = 0;
</script>>