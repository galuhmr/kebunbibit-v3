<?php

defined('BASEPATH') OR exit('No direct script access allowed');

?>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">  
<link rel="stylesheet" href="<?php echo base_url(); ?>public/css/penjual.css"> 
<link rel="stylesheet" href="<?php echo base_url(); ?>public/css/flexslider.css" type="" e="text/css" media="screen" />
<style>
@media only screen and (min-device-width: 1440px) {
.add-pro
{
    margin-left: 45px;
    margin-bottom: 15px;
    margin-top: -14px;cursor: pointer;    }
}
@media only screen and (max-device-width: 1440px) {
.add-pro { 
    margin-left: 75px;
    margin-bottom: 15px;
    margin-top: -14px;cursor: pointer;    }
}
</style>

  <div style="margin-top: 20px;" class="container white-bg">
    <div class="sub-category">
      <div class="row">
        <div class="col-lg-2">
          <div>
            <a href="<?php echo base_url() . kb_index(); ?>product/4/tanaman-hias-bunga">
              <h5>Tanaman Hias Bunga</h5>
            </a>  
            <ul>
              <li>
                <a href="<?php echo base_url() . kb_index(); ?>product/99/anthurium-bunga">Anthurium Bunga</a>
              </li>
              <li>
                <a href="<?php echo base_url() . kb_index(); ?>product/239/azalea">Azalea</a>
              </li>
              <li>
                <a href="<?php echo base_url() . kb_index(); ?>product/134/bakung-dan-lily">Bakung dan Lily</a>
              <li>
              <li>
                <a href="<?php echo base_url() . kb_index(); ?>product/144/bougenville">Bougenville</a>
              <li>
              <li>
                <a href="<?php echo base_url() . kb_index(); ?>product/241/camelia">Camelia</a>
              <li>
              <li>
                <a href="<?php echo base_url() . kb_index(); ?>product/242/chrysanthemum">Chrysanthemum</a>
              <li>
              <li>
                <a href="<?php echo base_url() . kb_index(); ?>product/123/disukai-kupu-kupu">Disukai Kupu-Kupu</a>
              <li>
              <li>
                <a href="<?php echo base_url() . kb_index(); ?>product/141/gerbera">Gerbera</a>
              <li>
              <li>
                <a href="<?php echo base_url() . kb_index(); ?>product/94/hibicus">Hibicus</a>
              <li>
              <li>
                <a href="<?php echo base_url() . kb_index(); ?>product/240/dahlia">Dahlia</a>
              <li>
              <li>
                <a href="<?php echo base_url() . kb_index(); ?>product/101/bunga-kalancoe">Kalanchoe</a>
              <li>
              <li>
                <a href="<?php echo base_url() . kb_index(); ?>product/114/tanaman-hias-bunga-lain">Tanaman Hias Bunga Lain</a>
              <li>
            </ul>
          </div>
          <div>
              <a href="<?php echo base_url() . kb_index(); ?>product/18/sayuran-dan-bumbu-dapur">
              <h5>Sayuran & Bumbu</h5>
            </a>
            <ul>
              <li>
                <a href="<?php echo base_url() . kb_index(); ?>product/149/tanaman-bumbu-dapur">Tanaman Bumbu Dapur</a>
              </li>
              <li>
                <a href="<?php echo base_url() . kb_index(); ?>product/148/tanaman-sayuran">Tanaman Sayuran</a>
              </li>
            </ul>
          </div>
          <div>
            <a href="<?php echo base_url() . kb_index(); ?>product/24/mawar">
              <h5>Mawar</h5>
            </a>
            <ul>
              <li>
                <a href="<?php echo base_url() . kb_index(); ?>product/24/mawar">Mawar</a>
              </li>
              <li>
                <a href="<?php echo base_url() . kb_index(); ?>product/25/mawar-tunggal">Mawar Tunggal</a>
              </li>
            </ul>
          </div>
          <img src="<?php echo image_url(); ?>public/img/menu/Kaktus.png">
          <div>
            <a href="<?php echo base_url() . kb_index(); ?>product/36/kaktus">
              <h5>Kaktus</h5>
            </a>
            <ul>
              <li>
                <a href="<?php echo base_url() . kb_index(); ?>product//">Own Roots Cactus</a>
              </li>
              <li>
                <a href="<?php echo base_url() . kb_index(); ?>product//">Grafted Cactus</a>
              </li>
            </ul>
          </div>   
        </div> <!-- ./col-lg-4 -->
        <div class="col-lg-2">
          <div>
            <a href="<?php echo base_url() . kb_index(); ?>product/32/tanaman-bonsai">
              <h5>Bonsai</h5>
            </a>
            <ul>
              <li>
                <a href="<?php echo base_url() . kb_index(); ?>product/49/mame-bonsai-5-15cm">Mame Bonsai (5-15 cm)</a>
              </li>
              <li>
                <a href="<?php echo base_url() . kb_index(); ?>product/170/ko-bonsai-16-30cm">Ko Bonsai (16-30 cm)</a>
              </li>
              <li>
                <a href="<?php echo base_url() . kb_index(); ?>product/137/chiu-bonsai-31-60cm">Chiu Bonsai (31-60 cm)</a>
              </li>
              <li>
                <a href="<?php echo base_url() . kb_index(); ?>product/48/dai-bonsai-61-90cm">Dai Bonsai (61-90 cm)</a>
              </li>
              <li>
                <a href="<?php echo base_url() . kb_index(); ?>product/47/bonsai-taman-100cm-up">Bonsai Taman 100cm up</a>
              </li>
              <li>
                <a href="<?php echo base_url() . kb_index(); ?>product/171/best-in-show-bonsai">Best in Show Bonsai</a>
              </li>
            </ul>
          </div>
          <img src="<?php echo image_url(); ?>public/img/menu/Pohon.png">
          <div>
            <a href="<?php echo base_url() . kb_index(); ?>product/29/pohon">
              <h5>Pohon</h5>
            </a>
            <ul>
              <li>
                <a href="<?php echo base_url() . kb_index(); ?>product/208/pohon-besar">Pohon Besar</a>
              </li>
              <li>
                <a href="<?php echo base_url() . kb_index(); ?>product//">Pohon Kecil</a>
              </li>
              <li>
                <a href="<?php echo base_url() . kb_index(); ?>product/44/pohon-bunga">Pohon Bunga</a>
              </li>
              <li>
                <a href="<?php echo base_url() . kb_index(); ?>product/43/pohon-peneduh">Pohon Peneduh</a>
              </li>
              <li>
                <a href="<?php echo base_url() . kb_index(); ?>product//">Pohon Cemara</a>
              </li>
              <li>
                <a href="<?php echo base_url() . kb_index(); ?>product//">Pohon Palem</a>
              </li>
            </ul>
          </div>
          <div>
            <a href="<?php echo base_url() . kb_index(); ?>product/19/tanaman-gantung">
              <h5>Tanaman Gantung</h5>
            </a>
            <ul>
              <li>
                <a href="<?php echo base_url() . kb_index(); ?>product/39/tanaman-gantung-bunga">Tanaman Gantung Bunga</a>
              </li>
              <li>
                <a href="<?php echo base_url() . kb_index(); ?>product/40/tanaman-gantung-daun">Tanaman Gantung Daun</a>
              </li>
            </ul>
          </div>
          <div>
            <a href="<?php echo base_url() . kb_index(); ?>product/125/penutup-tanah">
              <h5>Penutup Tanah</h5>
            </a>
            <ul>
              <li>
                <a href="<?php echo base_url() . kb_index(); ?>product/127/penutup-tanah-bunga">Penutup Tanah Bunga</a>
              </li>
              <li>
                <a href="<?php echo base_url() . kb_index(); ?>product/126/penutup-tanah-daun">Penutup Tanah Daun</a>
              </li>
            </ul>
          </div> 
        </div> <!-- ./col-lg-4 -->
        <div class="col-lg-2">
          <div>
            <a href="<?php echo base_url() . kb_index(); ?>product/15/tanaman-obat">
              <h5>Tanaman Obat</h5>
            </a>
            <ul>
              <li>
                <a href="<?php echo base_url() . kb_index(); ?>product/50/diabetes">Diabetes</a>
              </li>
              <li>
                <a href="<?php echo base_url() . kb_index(); ?>product/54/hipertensi">Hipertensi</a>
              </li>
              <li>
                <a href="<?php echo base_url() . kb_index(); ?>product/51/jantung-koroner">Jantung Koroner</a>
              </li>
              <li>
                <a href="<?php echo base_url() . kb_index(); ?>product/52/kanker-dan-tumor">Kanker dan Tumor</a>
              </li>
              <li>
                <a href="<?php echo base_url() . kb_index(); ?>product/55/kolesterol-tinggi">Kolesterol Tinggi</a>
              </li>
              <li>
                <a href="<?php echo base_url() . kb_index(); ?>product/63/maag-dan-nyeri-lambung">Maag dan Nyeri Lambung</a>
              </li>
              <li>
                <a href="<?php echo base_url() . kb_index(); ?>product/64/obat-kuat-dan-stamina-lelaki">Obat Kuat dan Stamina Lelaki</a>
              </li>
              <li>
                <a href="<?php echo base_url() . kb_index(); ?>product/56/penyakit-asma">Penyakit Asma</a>
              </li>
              <li>
                <a href="<?php echo base_url() . kb_index(); ?>product/58/penyakit-kewanitaan">Penyakit Kewanitaan</a>
              </li>
              <li>
                <a href="<?php echo base_url() . kb_index(); ?>product/61/penyakit-kulit-dan-kelamin">Penyakit Kulit dan Kelamin</a>
              </li>
              <li>
                <a href="<?php echo base_url() . kb_index(); ?>product/57/penyakit-liver-dan-hepatitis">Penyakit Liver dan Hepatitis</a>
              </li>
              <li>
                <a href="<?php echo base_url() . kb_index(); ?>product/62/rematik-dan-asam-urat">Rematik dan Asam Urat</a>
              </li>
              <li>
                <a href="<?php echo base_url() . kb_index(); ?>product/53/sakit-ginjal">Sakit Ginjal</a>
              </li>
              <li>
                <a href="<?php echo base_url() . kb_index(); ?>product/65/sakit-karena-guna-guna">Sakit karena Guna-Guna</a>
              </li>
              <li>
                <a href="<?php echo base_url() . kb_index(); ?>product/60/sakit-kepala-batuk-dan-flu">Sakit Kepala, Batuk dan Flu</a>
              </li>
              <li>
                <a href="<?php echo base_url() . kb_index(); ?>product/59/stroke">Stroke</a>
              </li>
            </ul>
          </div>
          <div>
            <a href="<?php echo base_url() . kb_index(); ?>product/138/tanaman-pengusir-serangga">
              <h5>Pengusir Hama</h5>
            </a>
            <ul>
              <li>
                <a href="<?php echo base_url() . kb_index(); ?>product/147/pengusir-nyamuk">Pengusir Nyamuk</a>
              </li>
              <li>
                <a href="<?php echo base_url() . kb_index(); ?>product/146/pengusir-tikus">Pengusir Tikus</a>
              </li>
            </ul>
          </div>
          <div>
            <a href="<?php echo base_url() . kb_index(); ?>product/174/herbal-kering">
              <h5>Herbal Kering</h5>
            </a>
          </div>   
          <div>
            <a href="<?php echo base_url() . kb_index(); ?>product/120/tanaman-indoor">
              <h5>Tanaman Indoor</h5>
            </a>
            <ul>
              <li>
                <a href="<?php echo base_url() . kb_index(); ?>product/150/tanaman-bunga-indoor">Tanaman Bunga Indoor</a>
              </li>
              <li>
                <a href="<?php echo base_url() . kb_index(); ?>product/151/tanaman-hias-indoor">Tanaman Hias Indoor</a>
              </li>
            </ul>
          </div>
        </div> <!-- ./col-lg-4 -->

        <div class="col-lg-2">
          <div>
            <a href="<?php echo base_url() . kb_index(); ?>product/16/tanaman-hias-daun">
              <h5>Tanaman Hias Daun</h5>
            </a>
            <ul>
              <li>
                <a href="<?php echo base_url() . kb_index(); ?>product/122/aglaonema">Aglaonema</a>
              </li>
              <li>
                <a href="<?php echo base_url() . kb_index(); ?>product/225/alocasia-talas">Alocasia / Talas</a>
              </li>
              <li>
                <a href="<?php echo base_url() . kb_index(); ?>product/100/anthurium-daun">Anthurium Daun</a>
              </li>
              <li>
                <a href="<?php echo base_url() . kb_index(); ?>product/235/bambu">Bambu</a>
              </li>
              <li>
                <a href="<?php echo base_url() . kb_index(); ?>product/224/begonia">Begonia</a>
              </li>
              <li>
                <a href="<?php echo base_url() . kb_index(); ?>product/142/bromelia">Bromelia</a>
              </li>
              <li>
                <a href="<?php echo base_url() . kb_index(); ?>product/107/calathea">Calathea</a>
              </li>
              <li>
                <a href="<?php echo base_url() . kb_index(); ?>product/227/dracena">Dracena</a>
              </li>
              <li>
                <a href="<?php echo base_url() . kb_index(); ?>product/143/coleus">Coleus</a>
              </li>
              <li>
                <a href="<?php echo base_url() . kb_index(); ?>product/226/cordyline">Cordyline</a>
              </li>
              <li>
                <a href="<?php echo base_url() . kb_index(); ?>product/135/tillandsia">Tillandsia</a>
              </li>
              <li>
                <a href="<?php echo base_url() . kb_index(); ?>product/136/pakis-dan-suplir">Pakis dan Suplir</a>
              </li>
              <li>
                <a href="<?php echo base_url() . kb_index(); ?>product/105/philodendron">Philodendron</a>
              </li>
              <li>
                <a href="<?php echo base_url() . kb_index(); ?>product/31/puring">Puring</a>
              </li>
              <li>
                <a href="<?php echo base_url() . kb_index(); ?>product/121/tanaman-lain">Tanaman Lain</a>
              </li>
            </ul>
          </div>
          <div>
            <a href="<?php echo base_url() . kb_index(); ?>product/91/tanaman-merambat">
              <h5>Tanaman Merambat</h5>
            </a>
            <ul>
              <li>
                <a href="<?php echo base_url() . kb_index(); ?>product/124/tanaman-merambat-buah">Tanaman Merambat Buah</a>
              </li>
              <li>
                <a href="<?php echo base_url() . kb_index(); ?>product/92/tanaman-merambat-bunga">Tanaman Merambat Bunga</a>
              </li>
              <li>
                <a href="<?php echo base_url() . kb_index(); ?>product/93/tanaman-merambat-daun">Tanaman Merambat Daun</a>
              </li>
            </ul>
          </div> 
          <div>
            <a href="<?php echo base_url() . kb_index(); ?>product/173/tanaman-karnivora">
              <h5>Tanaman Karnivora</h5>
            </a>
          </div>
          <div>
            <a href="<?php echo base_url() . kb_index(); ?>product/28/hasil-bumi">
              <h5>Hasil Bumi</h5>
            </a>
          </div>
          <div> 
           <a href="<?php echo base_url() . kb_index(); ?>product/21/biji-dan-umbi">
              <h5>Biji Dan Umbi</h5>
            </a> 
            <ul>
              <li>
                <a href="<?php echo base_url() . kb_index(); ?>product/69/biji-bunga">Biji Bunga</a>
              </li>
              <li>
                <a href="<?php echo base_url() . kb_index(); ?>product/92/biji-buah-buahan">Biji Buah-Buahan</a>
              </li>
              <li>
                <a href="<?php echo base_url() . kb_index(); ?>product/167/biji-tanaman-lain">Biji Tanaman Lain</a>
              </li>
            </ul>
          </div>  
        </div> <!-- ./col-lg-4 -->
        <div class="col-lg-2">
          <div>
            <a href="<?php echo base_url() . kb_index(); ?>product/14/tanaman-buah">
              <h5>Tanaman Buah</h5>
            </a>
            <ul>
              <li>
                <a href="<?php echo base_url() . kb_index(); ?>product/263/berbuah-1-2-tahun">Berbuah 1 - 2 tahun</a>
              </li>
              <li>
                <a href="<?php echo base_url() . kb_index(); ?>product/264/berbuah-3-5-tahun">Berbuah 3 - 5 tahun</a>
              </li>
              <li>
                <a href="<?php echo base_url() . kb_index(); ?>product/265/pernah-berbuah">Pernah Berbuah</a>
              </li>
              <li>
                <a href="<?php echo base_url() . kb_index(); ?>product/89/tabulampot">Tabulampot</a>
              </li>
              <li>
                <a href="<?php echo base_url() . kb_index(); ?>product/267/tanaman-buah-langka">Tanaman Buah Langka</a>
              </li>
            </ul>
          </div>
          <div>
            <a href="<?php echo base_url() . kb_index(); ?>product/28/tanaman-aquascape">
              <h5>Tanaman Aquascape</h5>
            </a>
            <ul>
              <li>
                <a href="<?php echo base_url() . kb_index(); ?>product/28/tanaman-aquascape">Tanaman Depan</a>
              </li>
              <li>
                <a href="<?php echo base_url() . kb_index(); ?>product/28/tanaman-aquascape">Tanaman Tengah</a>
              </li>
              <li>
                <a href="<?php echo base_url() . kb_index(); ?>product/28/tanaman-aquascape">Tanaman Belakang</a>
              </li>
              <li>
                <a href="<?php echo base_url() . kb_index(); ?>product/28/tanaman-aquascape">Pakis, Moss dan Anubis</a>
              </li>
            </ul>
          </div>
          <div>
            <a href="<?php echo base_url() . kb_index(); ?>product/102/anggrek-hibrida">
              <h5>Anggrek Hibrida</h5>
            </a>
            <ul>
              <li>
                <a href="<?php echo base_url() . kb_index(); ?>product/102/anggrek-hibrida">Cattleya Hibrida</a>
              </li>
              <li>
                <a href="<?php echo base_url() . kb_index(); ?>product/102/anggrek-hibrida">Dendrobium Hibrida</a>
              </li>
              <li>
                <a href="<?php echo base_url() . kb_index(); ?>product/102/anggrek-hibrida">Phalaenopsis Hibrida</a>
              </li>
              <li>
                <a href="<?php echo base_url() . kb_index(); ?>product/102/anggrek-hibrida">Vanda Hibrida</a>
              </li>
              <li>
                <a href="<?php echo base_url() . kb_index(); ?>product/102/anggrek-hibrida">Anggrek Hibrida Lain</a>
              </li>
            </ul>
          </div>
          <img src="<?php echo image_url(); ?>public/img/menu/Bunga-Harum.png">
          <div>
            <a href="<?php echo base_url() . kb_index(); ?>product/20/bunga-harum">
              <h5>Bunga Harum</h5>
            </a>
            <ul>
              <li>
                <a href="<?php echo base_url() . kb_index(); ?>product/41/aroma-kuat">Aroma Kuat</a>
              </li>
              <li>
                <a href="<?php echo base_url() . kb_index(); ?>product/42/aroma-lembut">Aroma Lembut</a>
              </li>
            </ul>
          </div>  
        </div> <!-- ./col-lg-4 -->
        <div class="col-lg-2">
          <div>
            <a href="<?php echo base_url() . kb_index(); ?>product/17/kaktus-dan-sukulen">
              <h5>Kaktus dan Sukulen</h5>
            </a>
            <ul>
              <li>
                <a href="<?php echo base_url() . kb_index(); ?>product/17/kaktus-dan-sukulen">Agave</a>
              </li>
              <li>
                <a href="<?php echo base_url() . kb_index(); ?>product/229/aloe">Aloe</a>
              </li>
              <li>
                <a href="<?php echo base_url() . kb_index(); ?>product/230/euphorbia">Euphorbia</a>
              </li>
              <li>
                <a href="<?php echo base_url() . kb_index(); ?>product/232/haworthia">Haworthia</a>
              </li>
              <li>
                <a href="<?php echo base_url() . kb_index(); ?>product/231/kalanchoe">Kalanchoe</a>
              </li>
            </ul>
          </div>
          <img src="<?php echo image_url(); ?>public/img/menu/Kaktus-sukulen.png">
          <div>
            <a href="<?php echo base_url() . kb_index(); ?>">
              <h5>Tanaman Purba</h5>
            </a>
            <ul>
              <li>
                <a href="<?php echo base_url() . kb_index(); ?>product">Encephalartos</a>
              </li>
              <li>
                <a href="<?php echo base_url() . kb_index(); ?>product">Cycads</a>
              </li>
            </ul>
          </div>
          <div>
            <a href="<?php echo base_url() . kb_index(); ?>product/103/anggrek-species">
              <h5>Anggrek Spesies</h5>
            </a>
            <ul>
              <li>
                <a href="<?php echo base_url() . kb_index(); ?>product/109/bulbophyllum">Bulbophyllum</a>
              </li>
              <li>
                <a href="<?php echo base_url() . kb_index(); ?>product/118/coelogyne">Coelogyne</a>
              </li>
              <li>
                <a href="<?php echo base_url() . kb_index(); ?>product/108/dendrobium">Dendrobium</a>
              </li>
              <li>
                <a href="<?php echo base_url() . kb_index(); ?>product/119/eria">Eria</a>
              </li>
              <li>
                <a href="<?php echo base_url() . kb_index(); ?>product/117/paphiopedilum">Paphiopedilum</a>
              </li>
              <li>
                <a href="<?php echo base_url() . kb_index(); ?>product/110/phalaenopsis">Phalaenopsis</a>
              </li>
              <li>
                <a href="<?php echo base_url() . kb_index(); ?>product/111/vanda">Vanda</a>
              </li>
              <li>
                <a href="<?php echo base_url() . kb_index(); ?>product/113/anggrek-species-lain">Anggrek Spesies Lain</a>
              </li>
            </ul>
          </div> 
          <div>
            <a href="<?php echo base_url() . kb_index(); ?>product/95/tanaman-air">
              <h5>Tanaman Air</h5>
            </a>
            <ul>
              <li>
                <a href="<?php echo base_url() . kb_index(); ?>product/96/tanaman-lotus">Lotus</a>
              </li>
              <li>
                <a href="<?php echo base_url() . kb_index(); ?>product/97/tanaman-teratai">Teratai</a>
              </li>
              <li>
                <a href="<?php echo base_url() . kb_index(); ?>product/98/tanaman-air-lain">Tanaman Air Lain</a>
              </li> 
            </ul>
          </div>
        </div> <!-- ./col-lg-4 -->
      </div> <!-- ./row -->
    </div>            
  </div> 


<div class="container add-pro">
  <img src="<?php echo image_url(); ?>public/img/Banner-Atas-BLT.png">
</div>

	<div class="container white-bg" id="produk-terbaru">
    <div class="kb-tab">
      <ul>
        <li class="active"><a href="#tab-satu" data-toggle="tab"><h3 class="kb-title" >Produk Terbaru</h3></a></li>
        <li><a href="#tab-dua" data-toggle="tab"><h3 class="kb-title">Produk Terlaris</h3></a></li>
        <li><a href="#tab-tiga" data-toggle="tab"><h3 class="kb-title">Rekomendasi</h3></a></li>
        </li>
      </ul>
    </div>
        <div class="tab-content">
          <div id="tab-satu" class="tab-pane active">
            <ul class="product-list">
              <?php 
                echo (count($products_newest) <= 0 ) ? "kosong": "";
                foreach($products_newest as $product):
                  echo show_product_list($product, $id_customer);
                endforeach; 
              ?>
            </ul>
          </div>
          <div id="tab-dua" class="tab-pane">
            <ul class="product-list">
              <?php 
                echo (count($best_seller) <= 0 ) ? "kosong": "";
                foreach($best_seller as $product):
                  echo show_product_list($product, $id_customer);
                endforeach; 
              ?>
            </ul>
          </div>
          <div id="tab-tiga" class="tab-pane">
            <ul class="product-list">
              <?php 
                echo (count($recommended) <= 0 ) ? "kosong": "";
                foreach($recommended as $product):
                  echo show_product_list($product, $id_customer);
                endforeach; 
              ?>
            </ul>
          </div>
        </div>
	</div>

  <div class="container white-bg">
    <div class="row">
      <div class="col-lg-6">
        <h3 class="kb-title">
          PENJUAL
        </h3>
      </div>
      <div class="col-lg-6 text-right">
        <div class="angle-box">
        </div>
      </div> <!-- ./col-lg-6 -->
    </div> <!-- ./row --> 

    <div id="container" class="cf">   
      <section class="slider">
        <div class="flexslider carousel">
          <ul class="slides">
           <?php foreach ($sellers as $seller): ?>
            <li>
              <?php if(strlen(trim($seller->img_path)) > 0): ?>
                  <img class="seller-photo seller-photo-slide" src="<?php echo image_url(); ?>public/uploads/<?php echo $seller->img_path; ?>">
                   <?php else: ?>
                   <img class="seller-photo seller-photo-slide" src="<?php echo image_url(); ?>public/img/penjual.png">
                <?php endif; ?>
              <h5>
                  <a href="<?php echo base_url() . kb_index() . 'profil/' . encrypt($seller->id_customer); ?>">
                    <p align="center">
                  <?php echo $seller->firstname . " " . $seller->lastname;?>
                  </p>
                  </a>

                </h5>
            </li>
             <?php endforeach; ?>
          </ul>
        </div>
      </section>  
  </div>

  </div>

  <?php foreach($categories as $category): ?>
    <?php if( count($products_group[$category->id_category]) > 0 ): ?>
      <div class="container white-bg">
        <div class="row">
          <div class="col-lg-6">
        		<h3 class="kb-title">
              <?php echo $category->name; ?>
            </h3>
          </div>
          <div class="col-lg-6 text-right">
        		<a class="see-all" href='<?= base_url() . kb_index() . "product/$category->id_category/$category->link_rewrite"?>'>
                Lihat Semua
            </a>
          </div> <!-- ./col-lg-6 -->
        </div> <!-- ./row -->
    		<ul class="product-list">
          <?php foreach($products_group[$category->id_category] as $product): ?>
      			<?php echo show_product_list($product, $id_customer); ?>
          <?php endforeach; ?> 
    		</ul>
    	</div>
    <?php endif; ?>  
  <?php endforeach; ?>
 
<!-- jQuery -->
  <script type="text/javascript">
    var SyntaxHighlighter = null;
  </script>
  <script src="<?php echo base_url(); ?>public/js/modernizr.js"></script> 
  <script src="<?php echo base_url(); ?>public/js/jquery-1.7.min.js"></script>
  <script>window.jQuery || document.write('<script src="js/libs/jquery-1.7.min.js">\x3C/script>')</script>
  <!-- FlexSlider -->
  <script defer src="<?php echo base_url(); ?>public/js/jquery.flexslider.js"></script> 
  <script type="text/javascript">

    (function() {

      // store the slider in a local variable
      var $window = $(window),
          flexslider = { vars:{} };

      // tiny helper function to add breakpoints
      function getGridSize() {
        return (window.innerWidth < 600) ? 2 :
               (window.innerWidth < 900) ? 2 : 7;
      }

      $(function() {
        if(SyntaxHighlighter != null) {
          SyntaxHighlighter.all();
        }
      });

      $window.load(function() {
        $('.flexslider').flexslider({
          animation: "slide",
          animationSpeed: 400,
          animationLoop: false,
          itemWidth: 210,
          itemMargin: 5,
          minItems: getGridSize(), // use function to pull in initial value
          maxItems: getGridSize(), // use function to pull in initial value
          start: function(slider){
            $('body').removeClass('loading');
            flexslider = slider;
          }
        });
      });

      // check grid size on resize event
      $window.resize(function() {
        var gridSize = getGridSize();

        flexslider.vars.minItems = gridSize;
        flexslider.vars.maxItems = gridSize;
      });
    }());
  </script> 