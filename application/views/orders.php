<?php
  defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container">
  <ol class="breadcrumb">
    <li><a href="#">Halaman Depan</a></li>
    <li><a href="#">Akun Saya</a></li>
    <li class="active">Transaksi</li>
  </ol>

  <div class="row">
      <div class="col-lg-3">
         <div class="panel panel-default">
            <div class="panel-heading panel-heading-kb">
               <h3 class="panel-title">Menu Saya</h3>
            </div>
            <div class="panel-body">
               <ul id="account-menu">
                  <?php $menus = account_menu(); ?>
                  <?php foreach($menus as $menu): ?>  
                    <li>
                       <a href="<?= $menu['link']; ?>"><?= $menu['name']; ?></a>
                    </li>
                  <?php endforeach; ?>
               </ul>
            </div><!-- .panel-body -->
         </div><!-- .panel -->
      </div><!-- .col-lg-3 -->
    <div class="col-lg-9">
      <div class="panel panel-default">
        <div class="panel-heading panel-heading-kb">
          <h3 class="panel-title">Transaksi</h3>
        </div>
        <div class="panel-body">
          <div class="col-lg-12 mb30" style="margin-bottom:20px;">
            <div class="col-lg-6">
              <a class="rdio-filter" href="#order_saya" data-id="buyer" style="color: rgb(255, 255, 255); background: rgb(139, 195, 74);">Pembelian</a>
            </div>
            <div class="col-lg-6">
              <a class="rdio-filter" href="#penjualan" data-id="seller">Penjualan</a>
            </div>
          </div>
          <div class="col-lg-12">
            <div id="tb-detail">
            </div>
            <div class="table-responsive" id="tb-order">
              <table class="table table-bordered mb30" id="table-transaksi">
                <thead>
                  <th style="width: 30%;">Kode Order</th>
                  <th>Tanggal</th>
                  <th>Total</th>
                </thead>
                <tbody>
                  
                </tbody>
              </table>
            </div>
            <div style="margin-bottom: 10px; width: 100%" class="text-center">
              <button style="display: none; margin-bottom: 25px;" id="load-more" class="kb-button">
                Lihat Lebih Banyak
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

</div>

<script type="text/javascript">
  var filter="buyer";
  var page = 0;
</script>>