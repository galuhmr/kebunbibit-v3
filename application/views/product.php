<link href="<?php echo base_url(); ?>public/css/bootstrap.min.css" rel="stylesheet"> 
<link href="<?php echo base_url(); ?>public/css/sidebar-menu.css" rel="stylesheet">
<style type="text/css">
    .li-cat:hover{
        color:#7ac144;
    }
</style>
<input id="category-active" type="hidden" value="<?php echo $category_active; ?>">
<div style="margin-top: 25px;" class="container white-bg">
    <div class="row">
    <div class="col-lg-3">
        <div class="span3">
            <section>
                <ul class="sidebar-menu">
                    <h3 class="kb-title">
                        kategori
                    </h3>
                    <?php foreach ($categories as $id => $category): ?>
                            <?php if($category->id_category == $category_active): ?>
                                    <li class="li-cat">
                                        <a style="color:#333 !important;font-weight: bold;"" 
                                          href= "<?php echo base_url() . kb_index(); ?>product/<?php echo $category->id_category . '/' . $category->link_rewrite ?>">
                                          <?php echo $category->name ?>
                                        </a>
                            <?php else: ?> 
                                    <li>
                                        <a class="li-cat" 
                                           href="<?php echo base_url(); ?>index.php/product/<?php echo $category->id_category . '/' . $category->link_rewrite?>">
                                           <?php echo $category->name ?>
                                        </a>
                            <?php endif; ?> 
                    <?php endforeach; ?>
                </ul>
            </section> 
        </div> 
    </div>
      <div class="col-lg-9">
            <h3 class="kb-title">
            <?php echo $category_title; ?>
        </h3>
        <div id="product-category-content">
        </div>
        <div style="margin-bottom: 10px; width: 100%" class="text-center">
            <img id="load-more-loading" 
                 style="margin: 10px 0px 25px 0px;" 
                 src="<?php echo image_url(); ?>public/img/load-more.gif" >
            <button style="display: none; margin-bottom: 25px;" 
                    id="load-more" class="kb-button">Lihat Lebih Banyak</button>
        </div><!-- .text-center --> 
      </div>
    </div>
</div>