<?php
   
   defined('BASEPATH') OR exit('No direct script access allowed');
   $user = $results[0];

   ?>
<div class="container">
   <ol class="breadcrumb">
      <li><a href="#">Halaman Depan</a></li>
      <li><a href="#">Akun Saya</a></li>
      <li class="active">Informasi Akun</li>
   </ol>
   <div class="row">
      <div class="col-lg-3">
         <div class="panel panel-default">
            <div class="panel-heading panel-heading-kb">
               <h3 class="panel-title">Menu Saya</h3>
            </div>
            <div class="panel-body">
               <ul id="account-menu">
                  <?php $menus = account_menu(); ?>
                  <?php foreach($menus as $menu): ?>  
                    <li>
                       <a href="<?= $menu['link']; ?>"><?= $menu['name']; ?></a>
                    </li>
                  <?php endforeach; ?>
               </ul>
            </div><!-- .panel-body -->
         </div><!-- .panel -->
      </div><!-- .col-lg-3 -->
      <div class="col-lg-9">
         <div class="panel panel-default">
            <div class="panel-heading panel-heading-kb">
               <h3 class="panel-title">Informasi Akun</h3>
            </div>
            <div class="panel-body">
               <form class="form-horizontal"
                  id="account-form"
                  action="<?php echo base_url() . kb_index(); ?>customer/update-customer/<?php echo $id_customer; ?>" 
                  method="POST" enctype="multipart/form-data">
                  <input type="hidden" id="img-path" name="img-path" value="<?php echo $user->img_path; ?>">
                  <div class="form-group">
                     <div class=" col-sm-12 text-center">
                        <label class="col-lg-12 alert alert-info"><i class="fa fa-info-circle"></i> Isi Data Diri, Nomor Handphone dan Alamat anda dengan lengkap .</label>
                        <div style="position: relative;">
                           <img id="profil-img" 
                              style="height: 200px; width: 200px;" 
                              src="<?php echo get_user_photo($user->img_path); ?>">
                        </div>
                        <div style="margin-top: 10px;" class="upload-section">
                           <button class="kb-button" type="button" id="upload-dp">
                           <i class="fa fa-upload"></i>&nbsp; Unggah Foto Profil
                           </button>
                           <input style="display: none;" type="file" id="uploaded_file_customer" name="uploaded_file_customer">
                        </div><!-- .upload-section -->
                     </div><!-- .col-sm-12 -->
                  </div><!-- .form-group -->
                  <div class="form-group">
                     <label class="control-label col-sm-3" for="firstname">Nama Depan:</label>
                     <div class="col-sm-9">
                        <input type="text" name="firstname" 
                           value="<?php echo $user->firstname; ?>" placeholder="Nama Depan">
                     </div><!-- .col-sm-9 -->
                  </div><!-- .form-group -->
                  <div class="form-group">
                     <label class="control-label col-sm-3" for="lastname">Nama Belakang:</label>
                     <div class="col-sm-9">
                        <input type="text"
                           value="<?php echo $user->lastname; ?>"
                           name="lastname" placeholder="Nama Belakang">
                     </div><!-- .col-sm-9 -->
                  </div><!-- .form-group -->
                  <div class="form-group">
                     <label class="control-label col-sm-3" for="email">Email:</label>
                     <div class="col-sm-9">
                        <input type="email"
                           value="<?php echo $user->email; ?>"
                           name="email" placeholder="Email">
                     </div><!-- .col-sm-9 -->
                  </div><!-- .form-group -->
                  <div class="form-group">
                     <label class="control-label col-sm-3" for="address">Alamat:</label>
                     <div class="col-sm-9"> 
                        <textarea name="address"
                           style="width: 100%; padding: 10px;" 
                           placeholder="Contoh: Jl. Diponegoro Gg. VI/22 RT.08 RW.07 Kel. Sisir Kec. Batu"><?php echo $user->address; ?></textarea>
                     </div><!-- .col-sm-9 -->
                  </div><!-- .form-group -->
                  <div class="form-group">
                     <label class="control-label col-sm-3" for="city">Kota:</label>
                     <div class="col-sm-9"> 
                        <input type="text" name="city" 
                           value="<?php echo $user->city; ?>"
                           placeholder="Contoh: Malang, Jawa Timur">
                     </div><!-- .col-sm-9 -->
                  </div><!-- .form-group -->
                  <div class="form-group">
                     <label class="control-label col-sm-3" for="phone">No Telepon:</label>
                     <div class="col-sm-9"> 
                        <input type="text" name="phone" 
                           value="<?php echo $user->phone; ?>"
                           placeholder="Contoh: 082435654321">
                     </div><!-- .col-sm-9 -->
                  </div><!-- .form-group -->
                  <div class="form-group">
                     <label class="control-label col-sm-3" for="old_password">Password Lama:</label>
                     <div class="col-sm-9">
                        <input type="password" name="old_password" placeholder="Password Lama">
                        <div id="old-pass-empty" style="margin-top: 5px;" class="error-message">
                           <i class="fa fa-times"></i> Password lama masih kosong
                        </div><!-- #old-pass-empty -->
                     </div><!-- .col-sm-9 -->
                  </div><!-- .form-group -->
                  <div class="form-group">
                     <label class="control-label col-sm-3" for="new_password">Password Baru:</label>
                     <div class="col-sm-9">
                        <input type="password" name="new_password" placeholder="Password Baru">
                        <div id="new-pass-empty" style="margin-top: 5px;" class="error-message">
                           <i class="fa fa-times"></i> Password baru masih kosong
                        </div><!-- #new-pass-empty -->
                     </div><!-- .col-sm-9 -->
                  </div><!-- .form-group -->
                  <div class="form-group">
                     <label class="control-label col-sm-3" for="confirm_password">Konfirmasi Password:</label>
                     <div class="col-sm-9">
                        <input type="password" name="confirm_password" placeholder="Konfirmasi Password">
                        <div id="confirm-pass-empty" style="margin-top: 5px;" class="error-message">
                           <i class="fa fa-times"></i> Konfirmasi password masih kosong
                        </div>
                        <div id="confirm-pass-wrong" style="margin-top: 5px;" class="error-message">
                           <i class="fa fa-times"></i> Konfirmasi password masih salah
                        </div>
                        <input type="hidden" name="current_password" value="<?php echo $user->passwd; ?>">
                     </div><!-- .col-sm-9 -->
                  </div><!-- .form-group -->
                  <div class="form-group">
                     <div class="col-sm-offset-3 col-sm-9">
                        <button id="save-profile" style="background: #7ac144; margin-right: 10px;" type="button" class="kb-button">Simpan</button>
                        <span>
                        <img id="edit-loading" src="<?php echo base_url(); ?>public/img/loading-small.gif">
                        <span id="old-pass-wrong" class="error-message">
                        <i class="fa fa-times"></i> Password lama anda masih salah
                        </span>
                        <span id="success-edit" class="success-message">
                        <i class="fa fa-check"></i> Informasi anda berhasil dirubah
                        </span>
                        </span>
                     </div><!-- .col-sm-offset-3 -->
                  </div><!-- .form-group -->
               </form>
            </div><!-- .panel-body -->
         </div><!-- .panel -->
      </div><!-- .col-lg-9 -->
   </div><!-- .row -->
</div><!-- .container -->