<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
?>
<input type="hidden" id="encrypt_id_chat" value="<?php echo $encrypt_id_chat; ?>">
<input type="hidden" id="id_user1" value="<?php echo $id_user1; ?>">
<input type="hidden" id="id_user2" value="<?php echo $id_user2; ?>">
<input type="hidden" id="is_starter" value="<?php echo $is_starter; ?>">
<div style="margin-top: 10px; margin-bottom: 30px;" class="container">
	<div class="row">
		<div class="col-lg-3">
			<ul class="product-list">
				<li>
					<img class="product-img" src="<?php echo base_url() . get_image_path2($product[0]); ?>">
					<div>
						<a class="product-name" href="#"><?php echo $product[0]->name; ?></a>
					</div>
					<h4 style="opacity: 0.3;"><?php echo format_to_rupiah($product[0]->price); ?></h4>
			        <div>
			          <?php echo $product[0]->description; ?>
			        </div>
			        <!-- <p>
			        	Tawaran: <b>Rp 5000</b>
			        </p> -->
	      		</li>
	      	</ul>
		</div>
		<div class="col-lg-5">
			<div id="message-box">
				<div class="message-display">
					<?php if(count($messages) > 0) { ?>
						<ul>
							<?php foreach($messages as $message): ?>
								<?php if ($message->id_user1 == $decrypt_id): ?>
									<li class="send-list">
										<div class="send">
											<div class="content">
												<?php echo $message->message; ?>
											</div>
											<div class="time">
												<?php echo format_date_message($message->date_add); ?>
											</div>
										</div>
									</li>
								<?php else: ?>
									<li class="read-list">
										<div class="read">
											<div class="content">
												<?php echo $message->message; ?>
											</div>
											<div class="time">
												<?php echo format_date_message($message->date_add); ?>
											</div>
										</div>	
									</li>
								<?php endif ?>	
							<?php endforeach; ?>	
						</ul>
					<?php } else { ?>
						<div class="placeholder">
							Silahkan chat dengan penjual
						</div>
					<?php } ?>
				</div>
				<div class="message-send">
					<input id="message-text" type="text" 
						   placeholder="Enter, kemudian refresh bila tidak muncul">
					<button id="btn-send">
						<i class="fa fa-paper-plane"></i>
					</button>
				</div>
			</div>
		</div>
		<div class="col-lg-4">
			<div id="offer-box">
				<!-- <p>
					Tawaran anda:
				</p>
				<input type="text" class="form-control">
				<button class="kb-button">Tawar</button>
				<hr /> -->
				<ul style="text-align: center;" id="seller-list">
			      <li>
			    	<p><?php echo $status; ?></p>
			    	<?php if(strlen(trim($user[0]->img_path)) > 0): ?>
				    	<img class="seller-photo" 
			        		 src="<?php echo base_url(); ?>public/uploads/<?php echo $user[0]->img_path; ?>">
			        <?php else: ?>
				    	<img class="seller-photo" 
			        		 src="<?php echo base_url(); ?>public/img/penjual.png">
			        <?php endif; ?>
				    		 
			        <h5>
			          <a href="#"><?php echo $user[0]->firstname . ' ' . $user[0]->lastname; ?></a>
			        </h5>
			        <button class="kb-button">Laporkan Pengguna</button>
			      </li>
			    </ul>  
			</div>
		</div>
	</div>
</div>