    <link rel="icon" href="https://kebunbibit.id/https://kebunbibit.id/public/img/kebunbibit.png">
    <style rel="stylesheet" type="text/css">
        .scroll{
  padding: 10px;
  overflow-y:auto;
  height: 500px;
}
.scroll::-webkit-scrollbar {
    width: 12px;
}
 
/* Track */
.scroll::-webkit-scrollbar-track {
    -webkit-box-shadow: inset 0 0 6px #27AE61; 
    -webkit-border-radius: 10px;
    border-radius: 10px;
}
 
/* Handle */
.scroll::-webkit-scrollbar-thumb {
    -webkit-border-radius: 10px;
    border-radius: 10px;
    background:#7ac144; 
    -webkit-box-shadow: inset 0 0 6px #27AE61; 
}
.scroll::-webkit-scrollbar-thumb:window-inactive {
    background: #27AE61; 
}
.faqHeader {
  font-size: 30px;
  margin: 20px;
}
.panel-heading [data-toggle="collapse"].collapsed:after {
  -webkit-transform: rotate(90deg);
  -moz-transform: rotate(90deg);
  -ms-transform: rotate(90deg);
  -o-transform: rotate(90deg);
  transform: rotate(90deg);
  color: rgba(77, 77, 77, 0.56);
}
.panel-group .panel {
  margin-bottom: 20px;
  border-radius: 4px;
}
.panel-default>.panel-heading+.panel-collapse>.panel-body {
  border-top-color: #ddd;
}
.panel-default>.panel-heading {
  color: #333;
  background: #f7f7f7;
  border-color: #ddd;
}
.panel-scroll {
    margin-bottom: 15px;
    border-radius: 4px;
}
    </style>
    <meta charset="utf-8">
    <title>FAQ Kebunbibit</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script type="text/javascript" src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

            <div class="container white-bg" style="margin-top:27px; border-top:0px solid white;">
                <h3 style="text-align:center;font-family: 'Open Sans', sans-serif;">Pertanyaan Umum (Penjual)</h3>
                <hr style="border-top:5px solid #7ac144;">
                <div class="col-md-9">
                    <div class="panel-group scroll panel-scroll" style="overflow-y:auto;" id="accordion">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a style="text-decoration:none;">1. Ketika saya ingin berjualan di pasar kebunbibit, apakah saya dipungut biaya?</a>
                                </h4>
                            </div>
                            <div class="panel-collapse collapse">
                                <div class="panel-body">
                                    Tidak, pasar kebunbibit tidak memungut biaya sepeser pun dari Penjual alias GRATIS.
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a style="text-decoration:none;">2. Apakah saya bisa menjual produk selain tanaman pada pasar kebunbibit?</a>
                                </h4>
                            </div>
                            <div class="panel-collapse collapse">
                                <div class="panel-body">
                                    Bisa, Anda bisa menjual produk non tanaman di pasar kebunbibit dengan syarat masih berhubungan dengan aktifitas berkebun dan produk yang Anda jual bukanlah produk terlarang yang tertera pada <a href="https://kebunbibit.id/syarat-panduan-produk">Seputar Produk.</a> 
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a style="text-decoration:none;">3. Apakah saya bisa menjual tanaman saya pada pasar kebunbibit hingga lebih dari satu?</a>
                                </h4>
                            </div>
                            <div class="panel-collapse collapse">
                                <div class="panel-body">
                                    Yups, Anda bisa menjual tanaman lebih dari satu item atau lebih, selama produk yang Anda jual bukanlah produk terlarang yang tertera pada aturan penggunaan.
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a style="text-decoration:none;">4. Apakah saya bisa memberi promo atau diskon pada produk saya?</a>
                                </h4>
                            </div>
                            <div class="panel-collapse collapse">
                                <div class="panel-body">
                                    Untuk saat ini, bila Anda ingin memberikan diskon atau promo belum bisa, sebab fitur yang dimiliki kebunbibit saat ini mengutamakan transaksi tawar-menawar yang tersedia pada button tawar.
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a style="text-decoration:none;">5. Apa yang harus saya lakukan jika Pembeli memesan produk yang sudah habis?</a>
                                </h4>
                            </div>
                            <div class="panel-collapse collapse">
                                <div class="panel-body">
                                    Anda harus sering meng-update produk yang Anda tampilkan di pasar Kebunbibit melalui menu “Produk Saya”. Anda juga bisa menginformasikan ketersediaan produk kepada Pembeli di fitur “tawar” yang telah kami sediakan. 
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a style="text-decoration:none;">6. Ketika saya meng-upload produk, apakah saya bisa melihat preview nya?</a>
                                </h4>
                            </div>
                            <div class="panel-collapse collapse">
                                <div class="panel-body">
                                    Karena sistem upload produk di pasar Kebunbibit adalah semudah Anda memposting update status di facebook maka Anda tidak bisa melihat “preview”nya terlebih dahulu.
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a style="text-decoration:none;">7. Bagaimana cara menghapus produk saya secara permanen?</a>
                                </h4>
                            </div>
                            <div class="panel-collapse collapse">
                                <div class="panel-body">
                                    Untuk menghapus produk secara permanen Anda bisa melakukannya sendiri pada informasi akun, lalu pilih menu “produk saya.” Kemudian silahkan pilih button “hapus”, maka produk Anda akan terhapus dari pasar kebunbibit.
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a style="text-decoration:none;">8. Apabila saya sedang kehabisan stok, apakah produk saya bisa dinonaktifkan sementara?</a>
                                </h4>
                            </div>
                            <div class="panel-collapse collapse">
                                <div class="panel-body">
                                    Yups, sangat bisa sekali. Anda bisa menghapus produk Anda melalui menu “produk saya.” Silahkan dipilih produk yang ingin dinonaktifkan, kemudian klik button “hapus.”
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a style="text-decoration:none;">9. Apabila saya salah mengisi informasi produk, apakah bisa saya edit?</a>
                                </h4>
                            </div>
                            <div class="panel-collapse collapse">
                                <div class="panel-body">
                                    Yups, Anda juga bisa mengedit informasi produk ataupun informasi akun termasuk alamat sesuai dengan tempat tinggal Anda sekarang pada menu - menu yang ada pada akun Anda.
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a style="text-decoration:none;">10. Saya tidak sengaja mengirimkan barang yang tidak sesuai pesanan, apa yang harus saya lakukan?</a>
                                </h4>
                            </div>
                            <div class="panel-collapse collapse">
                                <div class="panel-body">
                                    Nah, itu adalah kesalahan Anda sebagai Penjual. Maka Anda sendiri lah yang harus bertanggung jawab untuk mengganti dengan produk yang sesuai pesanan menggunakan biaya ongkos kirim dari Anda sebagai Penjual.
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a style="text-decoration:none;">11. Apakah saya bisa melihat detail penjualan saya?</a>
                                </h4>
                            </div>
                            <div class="panel-collapse collapse">
                                <div class="panel-body">
                                    Yups, Anda bisa melihat detail berapa kali transaksi Anda lakukan melalui fitur ... pada akun Anda.
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a style="text-decoration:none;">12. Bagaimana cara untuk mencegah pengiriman barang cacat/salah atau tidak lengkap pada pelanggan?</a>
                                </h4>
                            </div>
                            <div class="panel-collapse collapse">
                                <div class="panel-body">
                                    Kontrol kualitas Produk tanaman Anda. Produk harus dikontrol secara kualitas sebelum dikirim kepada kurir, jangan dikemas apabila tanaman telah layu, kondisi batang patah, atau daun-daunnya telah rontok.
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a style="text-decoration:none;">13. Bagaimana cara mengemas produk dengan baik dan benar?</a>
                                </h4>
                            </div>
                            <div class="panel-collapse collapse">
                                <div class="panel-body">
                                    Pastikan menggunakan pembungkus yang tahan suhu agar tanaman didalamnya tidak layu dan mati. Untuk penjelasan lengkapnya <a href="https://kebunbibit.id/syarat-panduan-produk">klik disini</a>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a style="text-decoration:none;">14. Apa yang harus saya lakukan apabila barang sudah saya kirim tapi belum juga sampai pada Pembeli?</a>
                                </h4>
                            </div>
                            <div class="panel-collapse collapse">
                                <div class="panel-body">
                                    Cek resinya terlebih dahulu apakah benar sudah proses pengiriman atau masih dalam tahap pengecekan, lalu konfirmasikan pada pihak pengiriman.
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a style="text-decoration:none;">15. Apabila saya membatalkan pesanan Pembeli, apakah saya mendapatkan sanksi dari pasar Kebunbibit?</a>
                                </h4>
                            </div>
                            <div class="panel-collapse collapse">
                                <div class="panel-body">
                                    Pihak Pasar Kebunbibit tidak akan memberikan sanksi pada Penjual. Namun, Anda harus sesegera mungkin menginformasikan pada Pembeli bahwa pesanannya telah Anda batalkan.
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a style="text-decoration:none;">16. Kenapa gambar produk saya ditolak oleh Kebunbibit?</a>
                                </h4>
                            </div>
                            <div class="panel-collapse collapse">
                                <div class="panel-body">
                                    Karena tidak memenuhi persayaratan pada poin ke-5 nomor 2.2 tentang Panduan Kualitas Gambar. Lebih lengkapnya <a href="https://kebunbibit.id/syarat-panduan-produk">klik disini</a>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a style="text-decoration:none;">17. Kenapa saya belum menerima uang pembayaran dari Kebunbibit?</a>
                                </h4>
                            </div>
                            <div class="panel-collapse collapse">
                                <div class="panel-body">
                                    Karena status order Pembeli belum dibayar atau Pembeli belum mengkonfirmasi email yang Kebunbibit kirimkan.
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a style="text-decoration:none;">18. Kapan saya harus mengirim pesanan? </a>
                                </h4>
                            </div>
                            <div class="panel-collapse collapse">
                                <div class="panel-body">
                                    Ketika Pembeli sudah membayar dan mengisi form pembayaran pada website Kebunbibit.
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a style="text-decoration:none;">19. Apakah laporan penjualan bisa saya unduh atau cetak?</a>
                                </h4>
                            </div>
                            <div class="panel-collapse collapse">
                                <div class="panel-body">
                                    Bisa, laporan penjualan pada sistem pasar Kebunbibit berupa PDF, Jadi bisa Penjual unduh atau cetak.
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a style="text-decoration:none;">20. Saya lupa password pada akun di Kebunbibit, apa yang harus saya lakukan?</a>
                                </h4>
                            </div>
                            <div class="panel-collapse collapse">
                                <div class="panel-body">
                                    Pada saat Anda masuk ke akun di Kebunbibit, akan muncul pop up untuk log in. Klik “lupa password” yang terletak di pojok kanan bawah.
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                    <div class="col-md-3">
                        <center><img src="https://kebunbibit.id/public/img/cms/FAQ.png" /></center>
                    </div>
                </div>
<script language="javascript" type="text/javascript">
(function () {
        $('.panel-collapse').hide();
        $('.panel-heading').hover(function () {
            $(this)
                .next()
                .slideDown()
                .siblings('.panel-collapse')
                .slideUp(200);
        });
    })();
</script>
</body>
</html>