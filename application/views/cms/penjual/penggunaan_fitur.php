    <title>Penggunaan Fitur</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <style type="text/css">
        #paragraph{
         text-align:justify; 
         line-height: 25px;
        }
        .fa-arrow-circle-right{
          margin-right:5px;
        }
@media (min-width: 992px){
        
    #image2{
        width:120px;
        height:auto;
        display:table-cell;
        margin:auto;
        vertical-align:middle;
    }
    #colmd2{
        margin-top:2%;
    }
    #list-style{
        list-style: none;
        text-align:justify;

    }
    #ul1{
        padding-left:15px;
    }
}
    #image{
        height:auto;
        display:table-cell;
        margin:auto;
        vertical-align:middle;
    }
    #image2{
        height:auto;
        display:table-cell;
        margin:auto;
        vertical-align:middle;
    }
    #list-style{
        list-style: none;
        text-align:justify;
    }
    </style>
    <div class="container white-bg" style="border-top:0px solid white; margin-top:27px;">
        <div class="col-md-3">
            <h3 style="background-color:#f7f7f7;text-align:center;line-height:60px;"> Penggunaan Fitur</h3>
            <ul class="nav">
                <li>
                    <a href="#" style="color:black;"><img src="https://kebunbibit.id/public/img/cms/arrow.png" style="margin-left:-20px;" /> 3.1 Syarat dan Ketentuan Layanan / Fitur</a>
                </li>
                <ul class="nav" style="margin-left:20px;">
                    <li class="active">
                        <a href="#home" style="color:black;"><img src="https://kebunbibit.id/public/img/cms/arrow-1.png" style="margin-left:-20px;" /> 3.1.1 Pedoman Penggunaan Layanan</a>
                    </li>
                    <li>
                        <a href="#home1" style="color:black;"><img src="https://kebunbibit.id/public/img/cms/arrow-1.png" style="margin-left:-20px;" /> 3.1.2 Kegiatan Terlarang</a>
                    </li>
                    <li>
                        <a href="#home2" style="color:black;"><img src="https://kebunbibit.id/public/img/cms/arrow-1.png" style="margin-left:-20px;" /> 3.1.3 Ketersediaan Layanan atau Fitur</a>
                    </li>
                    <li>
                        <a href="#home3" style="color:black;"><img src="https://kebunbibit.id/public/img/cms/arrow-1.png" style="margin-left:-20px;" /> 3.1.4 Kebunbibit Berhak Namun Tidak Berkewajiban Untuk Memantau Konten</a>
                    </li>
                </ul>
                <li>
                    <a href="#menu1" style="color:black;"><img src="https://kebunbibit.id/public/img/cms/arrow.png" style="margin-left:-20px;" /> 3.2 Tentang Akun Penjual</a>
                </li>
                <ul class="nav" style="margin-left:20px;">
                    <li class="active">
                        <a href="#menu2" style="color:black;"><img src="https://kebunbibit.id/public/img/cms/arrow-1.png" style="margin-left:-20px;" /> Produk Saya</a>
                    </li>
                    <li>
                        <a href="#menu9" style="color:black;"><img src="https://kebunbibit.id/public/img/cms/arrow-1.png" style="margin-left:-20px;" /> Transaksi </a>
                    </li>
                    <li>
                        <a href="#menu3" style="color:black;"><img src="https://kebunbibit.id/public/img/cms/arrow-1.png" style="margin-left:-20px;" /> Informasi Akun</a>
                    </li>
                    <li>
                        <a href="#menu4" style="color:black;"><img src="https://kebunbibit.id/public/img/cms/arrow-1.png" style="margin-left:-20px;" /> Kotak Masuk</a>
                    </li>
                </ul>
                <li>
                    <a href="#menu5" style="color:black;"><img src="https://kebunbibit.id/public/img/cms/arrow.png" style="margin-left:-20px;" /> 3.3 Tentang Fitur "Tawar Dulu"</a>
                </li>
                <li>
                    <a href="#menu6" style="color:black;"><img src="https://kebunbibit.id/public/img/cms/arrow.png" style="margin-left:-20px;" /> 3.4 Tentang Profil Penjual</a>
                </li>
            </ul>
        </div>
        <div class="tab-content">
            <div id="home" class="tab-pane fade in active">
                <h3 style="margin-top:35px;margin-bottom:0px;">Syarat dan Ketentuan Layanan / Fitur</h3>
                <hr style="border-top:5px solid #7ac144;margin-top:15px;">
                <p id="paragraph">
                    <b>3.1.1 Pedoman Penggunaan Layanan:</b> Anda setuju untuk mematuhi setiap dan semua pedoman, pemberitahuan, aturan operasi dan kebijakan dan instruksi yang berkaitan dengan penggunaan Layanan dan/atau akses ke Kebunbibit, serta setiap perubahan-nya, yang dikeluarkan oleh kami, dari waktu ke waktu. Kami berhak untuk merevisi pedoman, pemberitahuan, aturan operasi, kebijakan dan instruksi sewaktu-waktu dan Anda dianggap mengetahui dan tunduk oleh setiap perubahan tersebut di atas setelah ada-nya pemberitahuan atau publikasi atas perubahan tersebut di website Kebunbibit atau pemberitahuan melalui media lain.
                </p>
            </div>
            <div id="home1" class="tab-pane fade">
                <h3 style="margin-top:35px;margin-bottom:0px;">Kegiatan Terlarang</h3>
                <hr style="border-top:5px solid #7ac144;margin-top:15px;">
                <p id="paragraph"> <b> Anda setuju dan TIDAK akan melakukan hal - hal berikut ini: </b>
                    <br/>
                    <i class="fa fa-arrow-circle-right" aria-hidden="true"></i> Berpura-pura sebagai orang lain/entitas, atau memberikan keterangan yang salah, atau mengaku sebagai orang lain atau kelompok tertentu
                    <br/>
                    <i class="fa fa-arrow-circle-right" aria-hidden="true"></i> Menggunakan platform Kebunbibit atau Layanan untuk tujuan yang melanggar hukum (illegal)
                    <br/>
                    <i class="fa fa-arrow-circle-right" aria-hidden="true"></i> Berusaha untuk mendapatkan akses tidak sah atau mengganggu atau mengacaukan sistem komputer atau jaringan yang terhubung dengan platform Kebunbibit atau Layanan
                    <br/>
                    <i class="fa fa-arrow-circle-right" aria-hidden="true"></i> Mengumumkan (posting), mempromosikan atau mengirimkan Materi Terlarang apapun melalui Fitur atau Layanan
                    <br/>
                    <i class="fa fa-arrow-circle-right" aria-hidden="true"></i> Mengganggu pemanfaatan dan pemakaian dari platform Kebunbibit atau Layanan
                    <br/>
                    <i class="fa fa-arrow-circle-right" aria-hidden="true"></i> Menggunakan atau unggah atau meng-upload perangkat lunak atau material yang mengandung / dicurigai mengandung virus, komponen merusak, kode berbahaya atau komponen berbahaya dengan cara apapun yang dapat merusak data atau mengakibatkan kerusakan platform Kebunbibit atau mengganggu pengoperasian komputer pengguna lain atau perangkat mobile atau platform Kebunbibit atau Layanan dan
                    <br/>
                    <i class="fa fa-arrow-circle-right" aria-hidden="true"></i> Menggunakan platform Kebunbibit atau Layanan diluar dari aturan / kebijakan penggunaan setiap jaringan komputer yang terhubung, setiap standar Internet yang berlaku dan hukum yang berlaku lainnya.
                </p>
            </div>
            <div id="home2" class="tab-pane fade">
                <h3 style="margin-top:35px;margin-bottom:0px;">Ketersediaan Layanan atau Fitur</h3>
                <hr style="border-top:5px solid #7ac144;margin-top:15px;">
                <p id="paragraph">Kami dapat meningkatkan, memodifikasi, menghentikan sementara, menghentikan penyediaan, menghapus, baik secara keseluruhan atau sebagian dari layanan atau fitur, tanpa memberikan alasan & pemberitahuan sebelumnya, dan tidak bertanggung jawab jika peningkatan, modifikasi, suspensi atau penghapusan tersebut mencegah Anda mengakses layanan atau fitur Kebunbibit.</p>
            </div>
            <div id="home3" class="tab-pane fade">
                <h3 style="margin-top:35px;margin-bottom:0px;">Kebunbibit Berhak namun Tidak Berkewajiban Untuk Memantau Konten</h3>
                <hr style="border-top:5px solid #7ac144;margin-top:15px;">
                <p id="paragraph"> <b>Kami berhak, tetapi tidak wajib untuk: </b><br/>
                
                <i class="fa fa-arrow-circle-right" aria-hidden="true"></i> Memantau, menyaring atau mengontrol setiap kegiatan, isi atau materi pada website Kebunbibit melalui layanan dan fitur. Atas kebijakan kami sendiri, kami dapat menyelidiki setiap pelanggaran terhadap syarat dan ketentuan yang tercantum di sini dan dapat mengambil tindakan apapun yang dianggap sesuai atau tepat.
                <br/>
                <i class="fa fa-arrow-circle-right" aria-hidden="true"></i> Melaporkan kegiatan yang dicurigai sebagai pelanggaran terhadap hukum yang berlaku, undang-undang atau peraturan kepada pihak yang berwenang serta bekerja sama dengan pihak berwenang tersebut; dan/atau
                <br/>
                <i class="fa fa-arrow-circle-right" aria-hidden="true"></i> Meminta informasi dan data dari Anda sehubungan dengan penggunaan layanan, fitur dan/atau akses ke website Kebunbibit setiap saat, dan sebagai pelaksanaan hak kami jika Anda menolak untuk memberikan/mengungkapkan informasi/data tersebut, atau jika Anda memberikan informasi tidak akurat, menyesatkan, penipuan data dan/atau informasi atau jika kami memiliki alasan yang cukup mencurigai Anda telah menyediakan informasi tidak akurat, menyesatkan atau penipuan data dan/atau informasi.
                <br/>
                </p>
            </div>
            <div id="menu1" class="tab-pane fade">
                <h3 style="margin-top:35px;margin-bottom:0px;">Pada Akun Penjual Akan Muncul Tiga Menu:</h3>
                <hr style="border-top:5px solid #7ac144;margin-top:15px;">
            <div class="col-md-9">
                    <div class="col-md-3"  id="colmd2">
                        <img src="https://kebunbibit.id/public/img/cms/penjual-1.png" id="image" />
                    </div>
                    <div class="col-md-9">
                        <p>
                            <b>1.) Produk Saya </b>

                        </p>
                        <ul id="ul1">
                            <li id="list-style">
                                Di menu "produk saya" Anda bisa melihat berapa banyak produk yang Anda jual di pasar Kebunbibit. Selain itu, pada menu "produk saya" Penjual bisa mengedit informasi produknya sendiri melalui button "edit". Poin yang bisa diedit hanyalah harga produk dan deskripsi produk, untuk nama dan gambar tidak bisa diubah. Sehingga pastikan untuk mengisi nama produk dengan nama yang benar dan sesuai konten. Bila ingin menghapus produk, maka langsung saja menghapusnya dengan mengeklik button "hapus".
                            </li>
                        </ul>
                    </div>

            </div>
            <div class="col-md-9">
                    <div class="col-md-3"  id="colmd2">
                        <img src="/img/cms/penjual-4.png" id="image" />
                    </div>
                    <div class="col-md-9">
                        <p>
                            <b>2.) Transaksi </b>

                        </p>
                        <ul id="ul1">
                            <li id="list-style">
                                Di menu “transaksi” Penjual bisa melihat proses penjualan yang terjadi di pasar Kebunbibit secara rinci. Pada menu ini akan muncul nomor ID pemesanan, status pemesanan dan total pembayaran. Status pemesanan akan berganti muali dari “order baru”, “lunas” dan “order dikirim” dan “order diterima.” Sehingga Penjual bisa melihat status barang pesanannya mulai awal order baru hingga order diterima oleh Pembeli dan berapa kali transaksi yang terjadi di pasar Kebunbibit. Selain Penjual bisa melihat transaksi penjualannya melalui menu ini, Penjual juga akan mendapat notifikasi melalui email tentang status pemesannya.
                            </li>
                        </ul>
                    </div>

            </div>
            <div class="col-md-9">
                    <div class="col-md-3" id="colmd2">
                        <img src="https://kebunbibit.id/public/img/cms/penjual-2.png" id="image2" />
                    </div>
                    <div class="col-md-9">
                        <p>
                            <b>3.) Informasi Akun </b>

                        </p>
                        <ul id="ul1">
                            <li id="list-style">
                                Anda bisa mengedit dan mengubah informasi mengenai data diri Anda di menu "informasi akun". Apabila alamat dimana Anda tinggal sekarang tidak sesuai dengan alamat di KTP, Anda bisa mencantumkan alamat asli pada akun Anda. Untuk keamanan akun, mohon perhatikan poin <a href ="https://kebunbibit.id/penggunaan-fitur" style="text-decoration:none;">3.1.2 Tentang Kegiatan Terlarang</a>. Selain itu, Anda juga bisa mengubah password Anda dengan password yang baru, bila Anda lupa dengan password lama.
                            </li>
                        </ul>
                    </div>

            </div>
            <div class="col-md-9">
                    <div class="col-md-3"  id="colmd2">
                        <img src="https://kebunbibit.id/public/img/cms/penjual-3.png" id="image"/>
                    </div>
                    <div class="col-md-9">
                        <p>
                            <b>4.) Kotak Masuk</b>

                        </p>
                        <ul id="ul1">
                            <li id="list-style">
                                Pada menu "kotak masuk" Anda bisa melakukan komunikasi dengan Pembeli terutama untuk transaksi tawar menawar. Responlah dengan baik setiap pesan masuk pada produk Anda, pelayanan yang baik tentu akan meningkatkan tingkat penjualan Anda. Anda berhak untuk tidak merespon pesan masuk apabila konten yang Pembeli masukkan bukan berupa:
                                <ol>
                                    <li>
                                        Transaksi tawar menawar.
                                    </li>
                                    <li> 
                                        Pertanyaan lebih lanjut tentang produk yang dijual.
                                    </li>
                                    <li>
                                        Tips atau informasi media tanam yang paling tepat untuk produk yang Anda jual
                                    </li>
                                    <li>
                                        Informasi lain yang relevan dengan GARDENING.
                                    </li>
                                </ol>
                            </li>
                        </ul>
                        <p style="text-align:justify;">Apabila Anda mendapat pesan diluar ketentuan diatas, atau konten lain yang melanggar peraturan pada poin <a href ="https://kebunbibit.id/penggunaan-fitur" style="text-decoration:none;">3.1 Syarat dan Ketentuan Layanan / Fitur</a>, segera lapor melalui customer service kami di 0341 - 599399 atau melalui email ke <a href ="mailto:bantuan@kebunbibit.id" style="text-decoration:none;">bantuan@kebunbibit.id.</a></p>


                    </div>

            </div>
            </div>
            <div id="menu2" class="tab-pane fade">
                <h3 style="margin-top:35px;margin-bottom:0px;">Produk Saya</h3>
                <hr style="border-top:5px solid #7ac144;margin-top:15px;">
                <p id="paragraph">Di menu “produk saya” Anda bisa melihat berapa banyak produk yang Anda jual di pasar Kebunbibit. Selain itu, pada menu “produk saya” Penjual bisa mengedit informasi produknya sendiri melalui button “edit”. Poin yang bisa diedit hanyalah harga produk dan deskripsi produk, untuk nama dan gambar tidak bisa diubah. Sehingga pastikan untuk mengisi nama produk dengan nama yang benar dan sesuai konten. Bila ingin menghapus produk, maka langsung saja menghapusnya dengan mengeklik button “hapus.”</p>
            </div>
            <div id="menu9" class="tab-pane fade">
                <h3 style="margin-top:35px;margin-bottom:0px;">Transaksi</h3>
                <hr style="border-top:5px solid #7ac144;margin-top:15px;">
                <p id="paragraph">Di menu “transaksi” Penjual bisa melihat proses penjualan yang terjadi di pasar Kebunbibit secara rinci. Pada menu ini akan muncul nomor ID pemesanan, status pemesanan dan total pembayaran. Status pemesanan akan berganti muali dari “order baru”, “lunas” dan “order dikirim” dan “order diterima.” Sehingga Penjual bisa melihat status barang pesanannya mulai awal order baru hingga order diterima oleh Pembeli dan berapa kali transaksi yang terjadi di pasar Kebunbibit. Selain Penjual bisa melihat transaksi penjualannya melalui menu ini, Penjual juga akan mendapat notifikasi melalui email tentang status pemesannya.</p>
            </div>
            <div id="menu3" class="tab-pane fade">
                <h3 style="margin-top:35px;margin-bottom:0px;">Informasi Akun</h3>
                <hr style="border-top:5px solid #7ac144;margin-top:15px;">
                <p id="paragraph">Anda bisa mengedit dan mengubah informasi mengenai data diri Anda di menu “informasi akun.” Apabila alamat dimana Anda tinggal sekarang tidak sesuai dengan alamat di KTP, Anda bisa mencantumkan alamat asli pada akun Anda. Untuk keamanan akun, mohon perhatikan poin 3.1.2 tentang Kegiatan Terlarang. Selain itu, Anda juga bisa mengubah password Anda dengan password yang baru, bila Anda lupa dengan password lama. </p>
            </div>
            <div id="menu4" class="tab-pane fade">
                <h3 style="margin-top:35px;margin-bottom:0px;">Kotak Masuk</h3>
                <hr style="border-top:5px solid #7ac144;margin-top:15px;">
                <p id="paragraph">Pada menu “kotak masuk” Anda bisa melakukan komunikasi dengan Pembeli terutama untuk transaksi tawar menawar. Responlah dengan baik setiap pesan masuk pada produk Anda, pelayanan yang baik tentu akan meningkatkan tingkat penjualan Anda. Anda berhak untuk tidak merespon pesan masuk apabila konten yang Pembeli masukkan bukan berupa:</p>
                <p id="paragraph">1. Transaksi tawar menawar, </p>
                <p id="paragraph">2. Pertanyaan lebih lanjut tentang produk yang dijual, </p>
                <p id="paragraph">3. Tips atau informasi media tanam yang paling tepat untuk produk yang Anda jual, atau </p>
                <p id="paragraph">4. Informasi lain yang relevan dengan GARDENING. </p>
                <p id="paragraph"> Apabila Anda mendapat pesan diluar ketentuan diatas, atau konten lain yang melanggar peraturan pada poin 3.1 Syarat dan Ketentuan Layanan / Fitur, segera lapor melalui customer service kami di 0341 - 599399 atau melalui email ke bantuan@kebunbibit.id. </p>
            </div>
            <div id="menu5" class="tab-pane fade">
                <h3 style="margin-top:35px;margin-bottom:0px;">Tentang Fitur Tawar Dulu</h3>
                <hr style="border-top:5px solid #7ac144;margin-top:15px;">
                <div class="col-md-9">
                    <p style="text-align:justify;">
                    <b>3.3 Tentang Fitur Tawar </b>
                    </p>
                    <p style="text-align:justify;">
                    Pada fitur “tawar”, Anda bisa berkomunikasi lebih lanjut dengan Pembeli. Pastikan Anda meresponnya dengan baik, sebab kepercayaan Pembeli tergantung pada seberapa baiknya pelayanan Anda. Berikut ini adalah beberapa ketentuan dalam menggunakan fitur “tawar” di Kebunbibit:
                    </p>
                </div>
            <div class="col-md-9">
                    <div class="col-md-9" id="colmd9">
                            <ul>
                                <li id="li">
                                 Sistem Kebunbibit akan mengirim notifikasi melalui email Penjual apabila ada Pembeli yang merespon produk Anda mengenai penawaran harga.
                                 </li>
                            
                                <li id="li">Anda berhak untuk merespon Pembeli dengan ketentuan tidak melakukan hal hal pada poin <a href ="https://kebunbibit.id/penggunaan-fitur" style="text-decoration:none;">3.1.2  Tentang Kegiatan Terlarang</a>. Perhatikan juga poin <a href ="https://kebunbibit.id/penggunaan-fitur" style="text-decoration:none;">3.1.3 Tentang Ketersediaan Layanan atau Fitur</a>.</li>
                                <li id="li">
                                Konten yang bisa dimasukkan pada fitur "tawar" pada website Kebunbibit berisi tentang:
                                <ol>
                                    <li>
                                        Transaksi tawar menawar.
                                    </li>
                                    <li>
                                        Pertanyaan lebih lanjut tentang produk yang dijual.
                                    </li>
                                    <li>
                                        Tips atau informasi media tanam yang paling tepat untuk produk yang Anda jual.
                                    </li>
                                    <li>
                                        Informasi lain yang relevan dengan GARDENING.
                                    </li>
                                </ol>
                                </li>
                                <li id="li">
                                Selama Penjual beraktivitas di fitur "tawar" pada website Kebunbibit, Penjual dilarang keras menyampaikan setiap jenis konten apapun yang menyesatkan, memfitnah, atau mencemarkan nama baik, mengandung atau bersinggungan dengan unsur SARA, diskriminasi, dan/atau menyudutkan pihak lain.
                                </li>
                            </ul>
                    </div>
                    <div class="col-md-3" id="colmd3" style="padding-left:0px;">
                        <img src="https://kebunbibit.id/public/img/cms/tawar.png"/ id="tawar">
                    </div>
            </div>
            </div>
            <div id="menu6" class="tab-pane fade">
                <h3 style="margin-top:35px;margin-bottom:0px;">Tentang Profil Penjual</h3>
                <hr style="border-top:5px solid #7ac144;margin-top:15px;">
                <p id="paragraph">Pembeli tentu akan lebih tertarik dan yakin apabila profil Penjual jelas dan lengkap serta terpercaya. Maka, lengkapi profil Anda, termasuk alamat dimana Anda tinggal sekarang dan mengembangkan bisnis tanaman hias. Untuk melayani kepuasan Pembeli pada pasar Kebunbibit, kami menggolongkan Penjual menjadi tiga kriteria yaitu: </p>
                <p id="paragraph">
                    <h5><b>1. Penjual Biasa </b></h5>
                     Yaitu Penjual yang belum memverifikasi scan KTPnya, melalui email ke <a href ="mailto:verifikasi@kebunbibit.id" style="text-decoration:none;">verifikasi@kebunbibit.id.</a> Pada akun profil Penjual Biasa, tidak akan muncul tanda apapun alias hanya foto dan informasi Penjual saja.</p>
                <p id="paragraph">
                    <h5><b>2. Penjual Super</b> </h5>
                     Yaitu Penjual yang sudah memverifikasi scan KTPnya melalui email ke <a href ="mailto:verifikasi@kebunbibit.id" style="text-decoration:none;">verifikasi@kebunbibit.id.</a> Pada akun profil Penjual Super, akan muncul bunga berwarna biru atau blue flower yang menandakan akun Anda terpercaya.</p>
                <p id="paragraph">
                    <h5><b>3. Penjual Super Premium </b></h5>
                    Penjual Super Premium adalah Penjual yang sudah memverifikasi scan KTPnya serta membayar Rp. 30.000,- kepada Pasar Kebunbibit. Uang yang Anda bayarkan akan digunakan untuk mengirim dokumen perjanjian langsung ke alamat Penjual. Pada akun profil Penjual premium akan muncul bunga berwarna emas atau gold flower yang menandakan akun Anda sangat terpercaya. Untuk lebih jelasnya mengenai gold flower, silahkan hubungi customer service kami di 0341 - 599399 atau melalui email ke <a href ="mailto:bantuan@kebunbibit.id" style="text-decoration:none;">bantuan@kebunbibit.id.</a></p>
            </div>
        </div>
    </div>
    <script>
$(document)
    .ready(function()
    {
        $(".nav a")
            .click(function()
            {
                $(this)
                    .tab('show');
            });
    });

    </script>
