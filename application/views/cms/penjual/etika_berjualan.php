		<title>
			Etika Berjualan
		</title>
		<link rel="icon" href="https://kebunbibit.id/img/kebunbibit.png">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <link href="https://fonts.googleapis.com/css?family=Comfortaa" rel="stylesheet">
	    	<style type="text/css">
	    		.col-md-2{
	    			padding-right: 0px;
	    		}
	    		.col-md-3{
	    			margin-top: 25px;
	    		}
	    		.col-md-4{
	    			padding-left: 0px;
	    			padding-right: 0px;
	    			margin-top: 10px;
	    		}
	    		.col-md-9{
	    			margin-top: 25px;
	    		}
	    		.konten1{
		    	  font: 400 35px/1.3 'Comfortaa', cursive;
				  margin-top: 0px;
				  margin-bottom: 0px;
				  text-align:center;
		    	}
		    	.konten2{
					font-size: 17px;
					padding-top: 5px; 
					font-family: Arial, "Helvetica Neue", Helvetica, sans-serif;
					text-align: center;
				}
				.konten3{
					font-size: 16px;
					font-family: Arial, "Helvetica Neue", Helvetica, sans-serif;
				}
				.konten4{
					font-size: 16px;
					font-family: Arial, "Helvetica Neue", Helvetica, sans-serif;
					margin-left: 10px;
				}
				.gmbr1{
					width: 30px;
					height: 30px;
				}
                                p {
                                   text-align:justify;
                                 }
	    	</style>
	</head>
	<body>
		<div class="container white-bg" style = "border-top:0px solid white; margin-top:27px;">
			<h3 style="text-align:center;font-family: 'Open Sans', sans-serif;">Etika Berjualan</h3>
                        <hr style="border-top:5px solid #7ac144;">
			<div class="panel-body">
				<div class="col-md-5">
					<center><img src="img/pict.png"></center>
				</div>
				<div class="col-md-7">
					
					<p>
					<i class="fa fa-check-square-o" aria-hidden="true"></i>
					 Penjual wajib mengisi data pribadi secara lengkap dan jujur.
		            <br>
		            </p>
		            <p>
		            <i class="fa fa-check-square-o" aria-hidden="true"></i>
		             Penjual bertanggung jawab atas keamanan dari akun yang telah dibuat di Kebunbibit Marketplace termasuk alamat email dan
		            password
		            <br>
		            </p>
		            <p>
		            <i class="fa fa-check-square-o" aria-hidden="true"></i>
		             Penggunaan layanan apapun yang disediakan oleh pasar Kebunbibit berarti bahwa Penjual telah memahami dan menyetujui 
		            segala aturan yang diberlakukan oleh pasar Kebunbibit.
		            <br>
		            </p>
		            <p>
		            <i class="fa fa-check-square-o" aria-hidden="true"></i>
		             Selama berada dalam platform pasar Kebunbibit, Penjual dilarang keras menyampaikan setiap jenis konten apapun yang
		            menyesatkan, memfitnah, atau mencemarkan nama baik, mengandung atau bersinggungan dengan unsur SARA, diskriminasi,
		            dan/atau menyudutkan pihak lain.
		            <br>
		            </p>
		            <p>
		            <i class="fa fa-check-square-o" aria-hidden="true"></i>
		             Administrator pasar Kebunbibit berhak menyesuaikan dan/atau menghapus informasi produk, dan menonaktifkan akun
		            Pengguna apabila tidak sesuai dengan poin nomor 2. Tentang Syarat dan Panduan Produk.
		            <br>
		            </p>
		            <p>
		            <i class="fa fa-check-square-o" aria-hidden="true"></i>
		             Penjual bertanggung jawab atas segala resiko yang timbul di kemudian hari atas informasi yang diberikannya ke dalam website
		            pasar Kebunbibit, termasuk namun tidak terbatas pada hal-hal yang berkaitan dengan hak cipta, merek, desain industri, desain
		            tata letak industri dan hak paten atas suatu produk.
		            <br>
				</div>
				<div class="col-md-12">
					<p>
					<i class="fa fa-check-square-o" aria-hidden="true"></i>
					 Penjual akan mendapatkan beragam informasi promo terbaru dan penawaran istimewa. Namun, Pengguna dapat berhenti berlangganan (unsubscribe) jika tidak ingin menerima
		            informasi tersebut.
		            <br><br>
		            </p>
		            <p>
		            <i class="fa fa-check-square-o" aria-hidden="true"></i>
		             Penjual dilarang menggunakan kata-kata kasar yang tidak sesuai norma, saat berdiskusi di fitur tawar seperti yang tercantum pada poin 3.1 Tentang Syarat dan Ketentuan Layanan
		            / Fitur. Jika ditemukan pelanggaran, pasar Kebunbibit berhak memberikan sanksi seperti menonaktifkan sementara fitur pesan, dan membekukan atau menonaktifkan akun Penjual.
		            <br><br>
		            </p>
		            <p>
		            <i class="fa fa-check-square-o" aria-hidden="true"></i>
		             Penjual dengan ini menyatakan bahwa Penjual telah mengetahui seluruh peraturan perundang- undangan yang berlaku di wilayah Republik Indonesia dalam setiap transaksi di pasar
		            Kebunbibit, dan tidak akan melakukan tindakan apapun yang mungkin melanggar peraturan perundang-undangan yang berlaku di wilayah Republik Indonesia.
		            </p>
		        </div>
			</div>
		</div>
	</body>
</html>