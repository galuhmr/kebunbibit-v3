  <title>Syarat dan Panduan Produk</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <style type="text/css">
     .verticale{
        width: 2px;
        height:300px;
        background-color:rgba(0,0,0,0.3);
        float: left;
        margin-left: -30px;
      }

      @media (min-width: 992px) {
      }
  </style>

<div class="container white-bg" style = "border-top:0px solid white; margin-top:27px;">
<div class="col-md-4" >
<h1 style="background-color:#eee;text-align:center;line-height:60px;font-weight:700;color:#b2a9a9;">Kebunbibit.id</h1>
  <ul class="nav">
    <li class="active"><a href="#home" style="color:black;"><img src="img/arrow.png" style="margin-left:-13px;"/><font color="#83c132">2.1 Deskripsi Produk</font></a></li>
    <li><a href="#menu1" style="color:black;"><img src="img/arrow.png" style="margin-left:-13px;"/><font color="#83c132">2.2 Panduan Kualitas Gambar</font></a></li>
    <li><a href="#menu2" style="color:black;"><img src="img/arrow.png" style="margin-left:-13px;"/><font color="#83c132">2.3 Produk Larangan</font></a></li>
    <li><a href="#" style="color:black;"><img src="img/arrow.png" style="margin-left:-13px;"/><font color="#83c132">2.4 Harga Produk</font></a></li>
    	<ul class="nav" style="margin-left:20px;">
    		<li>
    		<a href="#menu3" style="color:black;"><img src="img/arrow-1.png" style="margin-left:-13px;"/><font color="#83c132"> 2.4.1 Harga Produk</font></a>
    		</li>
    		<li>
    		<a href="#menu4" style="color:black;"><img src="img/arrow-1.png" style="margin-left:-13px;"/><font color="#83c132"> 2.4.2 Ketentuan Harga Produk</font></a>
    		</li>
    		<li>
    		<a href="#menu5" style="color:black;"><img src="img/arrow-1.png" style="margin-left:-13px;"/><font color="#83c132"> 2.4.3 Harga Penawaran</font></a>
    	 </ul>
        <li>
        <a href="#" style="color:black;"><img src="img/arrow.png" style="margin-left:-13px;"/><font color="#83c132">2.5 Pengiriman Produk</font></a>
        </li>
        <ul class="nav" style="margin-left:20px;">
        <li>
        <a href="#menu7" style="color:black;"><img src="img/arrow-1.png" style="margin-left:-13px;"/><font color="#83c132"> 2.5.1 Alamat</font></a>
        </li>
        <li>
        <a href="#menu8" style="color:black;"><img src="img/arrow-1.png" style="margin-left:-13px;"/><font color="#83c132"> 2.5.2 Biaya Pengepakan dan Pengiriman</font></a>
        </li>
        <li>
        <a href="#menu9" style="color:black;"><img src="img/arrow-1.png" style="margin-left:-13px;"/><font color="#83c132"> 2.5.3 Standart Pengemasan Barang</font></a>
        </li>
        <li>
        <a href="#menu10" style="color:black;"><img src="img/arrow-1.png" style="margin-left:-13px;"/><font color="#83c132"> 2.5.4 Pelacakan</font></a>
        </li>
        <li>
        <a href="#menu11" style="color:black;"><img src="img/arrow-1.png" style="margin-left:-13px;"/><font color="#83c132"> 2.5.5 Jangka Waktu Pengiriman</font></a>
        </li>
        <li>
        <a href="#menu12" style="color:black;"><img src="img/arrow-1.png" style="margin-left:-13px;"/><font color="#83c132"> 2.5.6 Penerimaan</font></a>
        </li>
        <li>
        <a href="#menu14" style="color:black;"><img src="img/arrow-1.png" style="margin-left:-13px;"/><font color="#83c132"> 2.5.7 Kegagalan Pengiriman Produk</font></a>
        </li>
        </ul>
        </li>
  </ul>
</div>
<div class="col-md-8">
  <div class="tab-content">
    <div id="home" class="tab-pane fade in active">
      <h3>Deskripsi Produk</h3> <hr style="border-top:5px solid #7ac144;margin-top:15px;">
      <p style="text-align:justify;"> 
		<div class="col-md-7"><p style="text-align: justify;">Penjual harus memberikan deskripsi, harga, gambar, dan/atau informasi lainnya yang jelas dan benar mengenai setiap Produk yang ditawarkan untuk dijual kepada Pembeli melalui Kebunbibit. Sebagai penyedia pasar, Kebunbibit akan mengkomunikasikan kepada Penjual apabila terdapat informasi, deskripsi, harga, gambar yang tidak akurat mengenai Produk berdasarkan keluhan atau laporan dari Pembeli. Apabila Produk yang Pembeli pesan ternyata memiliki perbedaan dengan deskripsi, harga, gambar dan atau informasi lainnya yang diberikan oleh Penjual,
    </div>
    <div clas="col-md-5"><img src="img/deskripsi-produk.png">
    </div>
    <div class="col-md-12">
     Maka Pembeli harus mengkomunikasikan keberatan Pembeli kepada Kebunbibit melalui layanan Customer Service yang disediakan pada website Kebunbibit. Kebunbibit selanjutnya akan meneruskan keberatan Pembeli kepada Penjual yang bersangkutan. Untuk menghindari keraguan, tanggung jawab Kebunbibit terbatas pada layanan Kebunbibit melalui Customer Service untuk memfasilitasi keberatan Pembeli kepada Penjual yang bersangkutan untuk dimintakan pertanggungjawaban dan mencari solusi yang terbaik bagi Penjual dan Pembeli.</p>
     </div>


      </p>
    </div>
    <div id="menu1" class="tab-pane fade">
      <h3>Panduan Kualitas Gambar</h3><hr style="border-top:5px solid #7ac144; margin-top:15px;">
      <p>
    <div class="col-md-7">Hanya gambar yang jelas yang akan diterima oleh Kebunbibit, untuk lebih jelasnya demikian panduan untuk mengupload gambar dari produk Anda : <br><br>
      <ol>
        <li>Foto yang Anda upload untuk produk Anda haruslah foto yang paling baik dan jelas dilihat dari berbagai sisi.</li><br>
      <br><li>Anda bisa mengunggah lebih dari satu gambar.</li><br>
      <br><li>Ukuran minimal resolusi gambar untuk produk Anda adalah 500 x 500 pixel, sedangkan maksimal ukuran gambarnya adalah 1000 x 1000 pixel.</li><br>
      <br><li>Gambar memiliki resolusi minimum 72dpi yaitu: produk harus terlihat jelas, tajam dan tidak blur, dan tidak tertutupi apapun.</li><br>
      <br><li>Gambar produk harus sesuai dengan konten atau deskripsi produk.</li><br>
      <br><li>Anda tidak boleh mengunggah gambar produk yang sudah dibungkus.</li><br>
    </div>
    <div class="col-md-5"><img src="img/kualitas-gambar.png" style="padding:99px 15px 9px;">
    </div>
      </p>
    </div>
    <div id="menu2" class="tab-pane fade">
      <h3>Produk Larangan</h3><hr style="border-top: 5px solid #7ac144; margin-top:15px;">
      <p>
    <div class="col-md-12">2.3.1 Semua produk tanaman yang dilindungi oleh Peraturan Perundang - Undangan Negara Republik Indonesia dilarang untuk dijual di pasar Kebunbibit, diantaranya adalah:<br>
          <table class="table table-hover">
            <caption>Produk Larangan</caption>
              <tr>
                <td>Palem Raja/lndonesia (Caryota no)</td>
                <td>Palem Jawa (Ceratolobus glaucescens)</td>
              </tr>
              <tr>
                <td>Pinang merah Kalimantan (Cystostachys lakka)</td>
                <td>Pinang Merah Bangka (Cystostachys ronda)</td>
              </tr>
              <tr>
                <td>Palem Kipas Sumatera (Livistona spp.) (semua jenis dari genus Livistona)</td>
                <td>Palem Sumatera (Nenga gajah)</td>
              </tr>
              <tr>
                <td>Pinang Jawa (Pinanga javana)</td>
                <td>Bertan (Eugeissona utilis)</td>
              </tr>
              <tr>
                <td>Daun Payung (Johanneste ijsmaria altifrons)</td>
                <td>Korma Rawa (Phoenix paludosa)</td>
              </tr>
              <tr>
                <td>Manga (Pigafatta filaris)</td>
                <td>Anggrek Kebutan (Ascotentum miniatum)</td>
              </tr>
                <td>Anggrek Hitan (Coelegyne pandurata)</td>
                <td>Anggrek Koribas (Corybas fornicates)</td>
              <tr>
                <td>Anggrek Hartinah (Cymbidium hartinahiaum)</td>
                <td> Anggrek Karawai (Dendrobium catinecloesum)</td>
              </tr>
              <tr> 
                <td>Anggrek Albert (Dendrobium d’albertisi)</td>
                <td>Anggrek Stuberi (Dendrobium lasiantera)</td>
              </tr>
              <tr>
                <td>Anggrek Jaumrd (Dendrobium macrophyllum)</td>
                <td>Anggrek Karawai (Dendronium astrinoglossum)</td>
              </tr>
              <tr>
                <td>Anggrek Larat (Dendrobium phalaenopsis)</td>
                <td>Anggrek Raksasa Irian (Grammatophyllum papuanum)</td>
              </tr>
              <tr>
                <td>Anggrek Tebu (Grammatophyllum speciosum)</td>
                <td>Anggrek Ki Aksara (Macodes pitola)</td>
              </tr>
              <tr>
                <td>Anggrek Kasut Kumis (Paphiopedilum chamberlailianum)</td>
                <td>Anggrek Kasut Berbulu (Paphiopedilum glaucophyllum)</td>
              </tr>
              <tr>
                <td> Anggrek Kasut Pita (Paphiopedilum praestans)</td>
                <td>Anggrek Bulan Bintang (Paraphalaenopsis denevei)</td>
              </tr>
              <tr>
                <td>Anggrek Bulan Kaliman Tengah (Paraphalaenopsis laycocki)</td>
                <td>Anggrek Bulan Kaliman Barat (Paraphalaenopsis serpentilingua)</td>
              </tr>
              <tr>
                <td>Anggrek Bulan Ambon (Paraphalaenopsis amboinensis)</td>
                <td>Anggrek  Bulan Raksasa (Paraphalaenopsis gigantea)</td>
              </tr>
              <tr>
                <td>Anggrek Bulan Sumatra (Paraphalaenopsis sumatrana)</td>
                <td>Anggrek Kelip (Phalaenop sis violacose)</td>
              </tr>
              <tr>
                <td>Anggrek Jingga (Rananthera matutina)</td>
                <td>Anggrek Sendok (Spathoglottis zurea)</td>
              </tr>
              <tr>
                <td>Anggrek Mungil Minahasa (Vanda celebica)</td>
                <td>Anggrek Pensil (Vanda hokeriana)</td>
              </tr>
              <tr>
                <td>Anggrek Mini (Vanda pumila)</td>
                <td> Anggrek Sumatra (Vanda sumatrana)</td>
              </tr>  
          </table>
    </div>
      <div class="col-md-12">
          <br>2.3.2 Semua produk yang tertera dalam 2.3.1 boleh di jual di pasar kebunbibit asalkan hasil dari budidaya, bukan mengambil langsung dari hutan liar Indonesia.<br>
          <br>2.3.3 Bila produk 2.3.1 diambil langsung dari hutan liar, maka harus disertai dengan surat keterangan dari pihak Perhutani.<br>

          <br>2.3.4 Produk tanaman yang tergolong obat-obatan terlarang sebagaimana diatur dalam Undang-Undang No 35 Tahun 2009 Tentang Narkotika, dilarang untuk dijual di pasar Kebunbibit. Tanaman yang mengandung narkotika tersebut diantaranya adalah: (Tanaman Papaver Somniferum L, tanaman koka dan tanaman ganja)<br>

          <br>2.3.5 Kebunbibit tidak akan menampilkan produk yang tercantum pada 2.3.1 dan 2.3.4 serta berhak melakukan tindakan yang dianggap perlu terhadap akun Penjual yang bersangkutan.<br>
      </p>
      </div>
    </div>
    <div id="menuspecial" class="tab-pane fade">
      <h3>Harga Produk</h3><hr style="border-top:5px solid #7ac144;margin-top:15px;">
    </div>
    <div id="menu3" class="tab-pane fade">
      <h3>Harga Produk</h3><hr style="border-top:5px solid #7ac144; margin-top:15px;">
      <p>
    <p style="text-align: justify;">Harga produk yang tertera di setiap produk belum termasuk harga ongkos kirim. Harga Produk yang ditawarkan dan dicantumkan di Kebunbibit dapat berubah sewaktu-waktu tergantung kepada kebijakan Penjual. Penjual berhak untuk mengubah Harga Produk dan perubahan Harga tersebut akan tampil pada halaman penawaran Produk terkait, sehingga Pembeli dapat mengetahui Harga Produk yang diperbaharui sebelum melakukan pembayaran. Kebunbibit juga berhak untuk merubah harga produk, apabila ada promo dan diskon tertentu dari Kebunbibit. Harga yang harus dibayar oleh Pembeli merupakan Harga yang diperlihatkan pada saat Pembeli melakukan konfirmasi pembayaran Pesanan melalui website Kebunbibit.</p>
      </p>
    <br>
    <br>
     </div>
    <div id= "menu4" class="tab-pane fade">
    <b><h3>Ketentuan Harga Produk</h3></b><hr style="border-top: 5px solid #7ac144; margin-top:15px;">
    <p style="text-align: justify;">Harga yang Anda cantumkan pada saat meng-input produk adalah harga yang rasional dalam artian bisa diterima secara logis. Semisal bila Anda memasang harga Rp. 100,- maka harga tersebut bukanlah harga yang rasional sehingga sistem kami menghapus produk Anda atau membekukan atau bahkan menonaktifkan akun Penjual. Harga yang Anda masukkan dalam menginput produk harus di atas Rp.10.000,-, bila kurang dari itu maka sistem tidak bisa menerima produk Anda. Ketika memasukkan harga pada saat menginput produk, tidak usah menggunakan huruf Rp., titik ataupun koma cukup tuliskan angka saja. Sistem kami secara otomatis akan membuat harga produk Anda menjadi lengkap ketika produk Anda sudah berhasil diinput.</p><br>
    </div>
    <div id="menu5" class="tab-pane fade">
    <b><h3> Harga Penawaran</h3></b><hr style="border-top: 5px solid #7ac144; margin-top:15px;">
    <p style="text-align: justify;">Penjual mempunyai hak untuk menolak atau menerima harga penawaran yang diajukan oleh Pembeli melalui fitur “tawar”. Dari fitur “tawar”, Kebunbibit akan mencatat setiap transaksi yang sudah disepakati kedua belah pihak yaitu Penjual dan Pembeli. Harga yang dibayarkan oleh Penjual ten tu tidak sama jumlahnya dengan harga yang tercantum pada Produk awal, sebab Pembeli telah melakukan penawaran untuk Produk tersebut. Pembeli juga mempunyai hak untuk menambah produk pembelian semisal berupa media tanam atau produk lainnya yang dilakukan di fitur “tawar.” Pada akhirnya harga yang harus dibayarkan oleh Pembeli adalah harga final pada chat di fitur “tawar” yang harus Pembeli masukkan pada konfirmasi pembayaran.</p> 
    </p>
    </div>
    <div id="menu6" class="tab-pane fade">
      <h3>Pengiriman Produk</h3><hr style="border-top: 5px solid #7ac144; margin-top:15px;">
    </div>
    <div id="menu7" class="tab-pane fade">
      <h3>Alamat</h3><hr style="border-top: 5px solid #7ac144; margin-top:15px;">
      <p>Pengiriman Produk harus dikirim ke alamat yang Pembeli tentukan dalam Pesanan Anda, kecuali ada kesepakatan lain melalui fitur “tawar dulu.”</p>
    </div>
    <div id="menu8" class="tab-pane fade">
      <h3>Biaya pengiriman & pengepakan</h3><hr style="border-top: 5px solid #7ac144; margin-top:15px;">
      <p>
      Biaya pengiriman dan pengepakan sebagaimana tercantum dalam Pesanan sesuai dengan metode pengiriman yang dipilih oleh Pembeli baik melalui JNE, Tiki ataupun Pos Indonesia. Biaya pengiriman juga bisa dicek di laman website masing - masing layanan pemngiriman.
      </p>
    </div>
    <div id="menu9" class="tab-pane fade">
      <h3>Standar Pengemasan Barang </h3><hr style="border-top: 5px solid #7ac144; margin-top:15px;">
      <p>
    Penjual harus mengirim produk ke Pembeli apabila Pembeli sudah mengisi form konfirmasi pembayaran kurang lebih 2x24 jam dari waktu pembayaran. Pengepakan produk harus sesuai dengan standar Kebunbibit. Demikian panduan untuk mengemas produk tanaman bagi Penjual:
    <ul style="list-style-image: url('img/icon.gif');"> 
       <li>Sebagian daun,bunga dan buah dipangkas</li>
       <li>Akar dibungkus menggunakan plastik wrap, agar tidak berantakan</li>
       <li>Kemas serapi mungkin menggunakan kardus atau pembungkus standar, agar tanaman tidak rusak.</li>
       <li>Sertakan detail pengiriman yang dibungkus dalam plastik transparan agar tidak mengenai tanaman dan tidak terkena air.</li>
       <li>Segera kirimkan melalui jasa pengiriman yang dipilih oleh Pembeli secepat mungkin agar Pembeli puas dengan pelayanan Anda.</li>
       <li>Ingat, pengemasan yang rapi dan aman serta pengiriman yang cepat akan meningkatkan tingkat kepercayaan Pembeli kepada Anda.</li>
    </ul>
       
       <p>Adapun standar pengemasan untuk produk non-tanaman, adalah seperti berikut ini:</p>
    <ul style="list-style-image: url('img/icon.gif');">
       <li>Bila produk berupa obat – obatan dan pupuk dalam bentuk cairan, pastikan agar penutup botolnya rapat agar tidak tumpah</li>
       <li>Kemaslah serapi mungkin menggunakan kardus atau pembungkus standar, agar barang tidak rusak.</li>
       <li>Sertakan detail pengiriman yang dibungkus dalam plastik transparan agar tidak mengenai produk dan tidak terkena air.</li>
       <li>Segera kirimkan melalui jasa pengiriman yang dipilih oleh Pembeli secepat mungkin agar Pembeli puas dengan pelayanan Anda.</li>
       <li>Ingat, pengemasan yang rapi dan aman serta pengiriman yang cepat akan meningkatkan tingkat kepercayaan Pembeli kepada Anda.</li>
    </ul>
      </p>
    <iframe width="560" height="315" style="display:table-cell; margin:auto; vertical-align:middle;" src="https://www.youtube.com/embed/SsW33300OeQ" frameborder="0" allowfullscreen></iframe>
    </div>
  <div id="menu10" class="tab-pane fade">
      <h3>Pelacakan </h3><hr style="border-top:5px solid #7ac144;margin-top:15px;">
      <div class="col-md-6"><p>
      <p style="text-align: justify;">Kebunbibit hanya menyediakan layanan untuk memverifikasi apakah nomor resi valid atau tidak. Sedangkan pelacakan keberadaan pesanan dapat dilakukan oleh Penjual dan Pembeli menggunakan nomor resi pada website perusahaan penyedia jasa kurir/pengiriman yang bersangkutan.
      </div></p>
      <div class="col-md-6"><img src="img/pelacakan.png">
      </div>
  </div>
      <div id="menu11" class="tab-pane fade">
      <h3>Jangka waktu pengiriman </h3><hr style="border-top: 5px solid #7ac144; margin-top:15px;">
      <div class="col-md-7"><p style="text-align: justify;">Pengiriman produk tergantung pada ketersediaan dari produk. Penjual akan melakukan segala upaya yang wajar untuk memberikan Produk kepada Pembeli dalam jangka waktu pengiriman yang tertera pada halaman terkait yang digunakan oleh Produk terdaftar, namun dengan ini Pembeli mengakui bahwa ada kemungkinan sebuah Produk menjadi tidak tersedia pada saat Kebunbibit di update atau terjadi perubahan terbaru atas Pesanan atau Produk. Jangka waktu pengiriman yang diberikan merupakan perkiraan dan penundaan dapat saja terjadi. Jika pengiriman Produk Pembeli tertunda karena ketidaksediaan Produk, Penjual memberitahu Pembeli melalui fitur “tawar dulu” atau e-mail dan Produk Pembeli akan dikirim secepatnya ketika telah tersedia. Jangka waktu pengiriman bukan merupakan hakikat, dan Penjual tidak bertanggung jawab atas keterlambatan pengiriman.
      </p>
      </div>
      <div class="col-md-5"><img src="img/jangka-waktu.png" style="padding: 76px 12px; height: 285px;">
      </div>
    </div>
      <div id="menu12" class="tab-pane fade">
      <h3>Penerimaan</h3><hr style="border-top: 5px solid #7ac144; margin-top:15px;">
      <div class="col-md-8">
      <p style="text-align: justify;">
       Pengiriman Produk dilakukan oleh perusahaan penyedia jasa kurir/pengiriman yang ditunjuk oleh masing-masing Penjual, bukan oleh Kebunbibit. Kebunbibit bertanggung jawab untuk memfasilitasi laporan/keluhan dari Pembeli apabila Pembeli belum menerima Produk dalam jangka waktu yang diestimasikan. Dalam hal Pembeli belum menerima Produk dalam jangka waktu yang diestimasikan, maka Pembeli harus memberitahu Customer Service yang disediakan melalui website Kebunbibit perihal tersebut sejak tanggal estimasi Produk seharusnya diterima dalam jangka waktu yang wajar. Kebunbibit selanjutnya akan meneruskan keberatan Pembeli kepada Penjual dan/atau perusahaan penyedia jasa kurir/pengiriman yang bersangkutan untuk mencari solusi yang terbaik bagi Penjual dan Pembeli. Untuk menghindari keraguan, tanggung jawab Kebunbibit terbatas pada layanan Customer Service untuk memfasilitasi laporan/keluhan Pembeli kepada Penjual dan/atau perusahaan penyedia jasa kurir/pengiriman yang bersangkutan. Kebunbibit akan mengirim notifikasi terkait dengan penerimaan barang melalui email kepada Penjual. Apabila Pembeli tidak memberi jawaban email Kebunbibit dalam jangka waktu yang wajar sebagaimana yang disebutkan di atas, maka Kebunbibit menganggap Pembeli telah menerima Produk dengan baik. Setelah itu, Kebunbibit akan mengirim uang pembayaran melalui transfer ke nomor rekening bank yang digunakan oleh Penjual. 
       </p>
       </div>
      <div class="col-md-4"><img src="img/penerimaan.png" style="height: 182px;">
      </div>
    </div>
      <div id="menu13" class="tab-pane fade">
      <h3>Biaya pengiriman & pengepakan</h3><hr style="border-top: 5px solid #7ac144; margin-top:15px;">
      <p>
      Biaya pengiriman dan pengepakan sebagaimana tercantum dalam Pesanan sesuai dengan metode pengiriman yang dipilih oleh Pembeli baik melalui JNE, Tiki ataupun Pos Indonesia. Dalam 
      </p>
      </div>
      <div id="menu14" class="tab-pane fade">
      <h3>Kegagalan Pengiriman Produk</h3><hr style="border-top: 5px solid #7ac144; margin-top:15px;">
      <div class="col-md-7"><p style="text-align: justify;">
      Jika Penjual gagal mengirimkan Produk; dan/atau kurir gagal mengirimkan Produk; dan/atau barang hilang pada saat pengiriman, dan/atau Pembeli gagal menerima pengiriman Produk dikarenakan kesalahan dan/atau kelalaian-nya sendiri (selain karena alasan sebab apapun di luar kendali yang wajar Pembeli atau dengan alasan kesalahan Penjual), maka tanpa mengurangi hak Penjual untuk melakukan pengiriman ulang atau tindakan perbaikan lainnya, Penjual dapat saja memutuskan untuk mengakhiri atau membatalkan Pemesanan. Jika dalam hal ini, Pembeli telah membayarkan pembayaran atas Produk maka Kebunbibit akan mengembalikan pembayaran (refund) atas Pesanan tersebut kepada Pembeli.</p>
      </div>
      <div class="col-md-5"><img src="img/kegagalan-pengiriman.png" style="padding: 48px 17px;">
      </div>
      </div>
  </div>
</div>
</div>

<script>
$(document).ready(function(){
    $(".nav a").click(function(){
        $(this).tab('show');
    });
});
</script>