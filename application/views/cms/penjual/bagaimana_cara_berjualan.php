    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href='http://fonts.googleapis.com/css?family=Lobster+Two' rel='stylesheet' type='text/css'>
    	<style type="text/css">
    		.col-md-2{
    			padding-right: 0px;
    		}
    		.col-md-3{
    			margin-top: 25px;
    		}
    		.col-md-4{
    			padding-left: 0px;
    			padding-right: 0px;
    			margin-top: 10px;
    		}
    		.col-md-9{
    			margin-top: 25px;
    		}
    		.konten1{
	    	  font: 400 35px/1.3 'Lobster Two', Helvetica, sans-serif;
			  text-shadow: 1px 1px 0px #ededed, 4px 4px 0px rgba(0,0,0,0.15);
			  font-style:italic;
			  margin-top: 0px;
			  margin-bottom: 0px;
			  text-align:center;
	    	}
	    	.konten2{
				font-size: 17px;
				padding-top: 5px; 
				font-family: Arial, "Helvetica Neue", Helvetica, sans-serif;
				text-align: center;
			}
			.konten3{
				font-size: 16px;
				font-family: Arial, "Helvetica Neue", Helvetica, sans-serif;
			}
			.konten4{
				font-size: 16px;
				font-family: Arial, "Helvetica Neue", Helvetica, sans-serif;
				margin-left: 10px;
			}
			.gmbr1{
				width: 30px;
				height: 30px;
			}
    	</style>
	<div class="container white-bg" style = "border-top: 0px solid white; margin-top:27px;">
		<h3 style="text-align:center;font-family: 'Open Sans', sans-serif;">Bagaimana Cara Berjualan?</h3>
                     <hr style="border-top:5px solid #7ac144;">
		<div class="col-md-12" style="background-color:#7ac144;"><p class="konten2" style="color:white;">Berjualan di pasar Kebunbibit sangat mudah, semudah Anda update status di Facebook. Hanya dalam satu menit produk Anda sudah bisa ditampilkan di pasar Kebunbibit.</p></div>
		<div class="panel-body">
			<div class="col-md-3">
				<center><img src="https://kebunbibit.id/public/img/cms/Bagaimana-Cara-Berjualan_01.png"></center>
			</div>
			<div class="col-md-9">
				<p class="konten3"><img class="gmbr1" src="https://kebunbibit.id/public/img/cms/Icon.png">&nbsp<b>Cara Menginput Produk :</b></p>
				<p class="konten4">1. Klik icon "Plus" yang berada pada pojok kanan atas <br>
				2. Lalu Anda diarahkan untuk mengisi nama produk. Nama produk harus sesuai dengan apa yang Anda jual. <br>
				3. Setelah itu Anda diarahkan mengisi jenis kategori produk. Pemilihan kategori produk harus sesuai berdasarkan dengan produk jualan Anda. <br>
				4. Lalu Anda diwajibkan untuk mengisi harga dan deskripsi produk. Usahakan 
				untuk mengisi deskripsi semenarik mungkin, agar Pembeli tertarik dan membeli produk Anda. <br>
				5. Terakhir, input gambar asli produk yang Anda jual. Gambar produk harus jelas, tidak blur dan resolusi sesuai dengan ketentuan pasar kebunbibit. <br>
				6. Setelah semuanya sudah diisi dengan benar, klik "Posting" dan produk Anda siap di beli oleh pelanggan kebunbibit.</p>
			</div>
			<div class="col-md-2">
				<center><img src="https://kebunbibit.id/public/img/cms/Bagaimana-Cara-Berjualan_02.png"></center>
			</div>
			<div class="col-md-4">
				<p class="konten3"><img class="gmbr1" src="https://kebunbibit.id/public/img/cms/Icon.png">&nbsp<b>Cara Mengedit Informasi Produk :</b></p>
				<p class="konten4">1. Pada informasi akun Anda di Kebunbibit, akan muncul menu "Produk Saya" <br>
			2. Pilihlah produk yang ingin Anda ubah. <br>
			3. Klik button "Edit" di pop up yang muncul setelah Anda mengeklik produk yang dipilih sebelumnya. <br>
			4. Yang bisa Anda edit adalah harga dan deskripsi produk saja, untuk nama produk dan gambar tidak bisa Anda edit. <br>
			5. Setelah semuanya benar dan sesuai dengan keadaan serta kondisi produk Anda, klik "Selesai" dan perubahan yang telah dilakukan sudah tampil pada produk Anda.
			</p>
			</div>
			<div class="col-md-2">
				<center><img src="https://kebunbibit.id/public/img/cms/Bagaimana-Cara-Berjualan_03.png"></center>
			</div>
			<div class="col-md-4">
				<p class="konten3"><img class="gmbr1" src="https://kebunbibit.id/public/img/cms/Icon.png">&nbsp<b>Cara Menghapus Produk :</b></p>
				<p class="konten4">1. Pada informasi akun Anda di Kebunbibit, akan muncul menu "Produk Saya" <br>
			2. Pilihlah produk yang ingin Anda hapus. <br>
			3. Klik button "Hapus" di pop up yang muncul setelah Anda mengeklik produk yang telah Anda pilih sebelumnya. <br>
			4. Secara otomatis, produk Anda akan terhapus pada akun di website pasar kebunbibit. <br>
			</p>
			</div>
		</div>
	</div>