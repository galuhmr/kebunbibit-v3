    <style type="text/css">
@media (min-width: 992px) {
    .colmd2 {
        width: 20%;
    }
    .colmd9 {
        width: 80%;
    }
    .colmd4 {
        width: 29.333333%;
        margin-top: 1%;
    }
    .colmd4-1 {
        width: 29.333333%;
        margin-top: 3%;
    }
    .colmd8 {
        width: 70.666667%;
    }
    p {
        font-family: 'Open Sans', sans-serif;
    }
}
#li {
    position: relative;
    list-style-type: none;
    font-family: 'Open Sans', sans-serif;
}
#li::before {
    position: absolute;
    content: "";
    top: 50%;
    -webkit-transform: translateY(-50%);
    -moz-transform: translateY(-50%);
    -o-transform: translateY(-50%);
    -ms-transform: translateY(-50%);
    transform: translateY(-50%);
    margin-left: -20px;	
    width: 0;
    height: 0;
    border-top: 7px solid transparent;
    border-bottom: 7px solid transparent;
    border-left: 10px solid #E5E5E5;
}
#image {
    margin-left: auto;
    margin-right: auto;
    text-align: center;
    display: table-cell;
    vertical-align: middle;
}

    </style>

<div style="margin-top: 27px; border-top:0px solid white;" class="container white-bg">
        <h3 style="text-align:center;font-family: 'Open Sans', sans-serif;">Transaksi dan Pembayaran ?</h3>
        <hr style="border-top:5px solid #7ac144;">
        <div class="col-md-12" style="margin-bottom:30px;">
            <div class="col-md-4 colmd4 ">
                <img src="img/transaksi.png" id="image" />
            </div>
            <div class="col-md-8 colmd8 " style="text-align:justify;">
                <ul id="ul">
                    <li id="li">
                        Demi keamanan dan kenyamanan para Pengguna, setiap transaksi jual-beli di Kebunbibit diwajibkan untuk menggunakan Kebunbibit Payment System.</li>
                </ul>
                <ul id="ul">
                    <li id="li">
                    Untuk menjaga keamanan proses transaksi, Staff Customer Service Kebunbibit akan meminta nomor rekening Anda secara personal untuk proses penerimaan uang pembayaran apabila proses berjualan Anda telah sukses.</li>
                </ul>
                <ul id="ul">
                    <li id="li">Transaksi tawar menawar atau jual beli berlangsung pada fitur "tawar" di website kebunbibit dengan harga yang telah disepakati baik harga produk ataupun harga penawaran, selengkapnya lihat poin 2.4 tentang Harga Produk.</li>
                </ul>
                <ul id="ul">
                    <li id="li">
                    Penjual wajib mengirimkan barang dan mendaftarkan nomor resi pengiriman yang benar dan asli setelah status transaksi "Dibayar" atau Pembeli telah mengisi form pembayaran. Satu nomor resi hanya berlaku untuk satu nomor transaksi di pasar Kebunbibit.</li>
                </ul>
            </div>
        </div>
        <div class="col-md-12">
            <div class="col-md-8 colmd8" style="text-align:justify;">
                <ul id="ul">
                    <li id="li">
                        Sistem Kebunbibit secara otomatis mengecek status pengiriman barang melalui nomor resi yang diberikan Penjual. Seluruh dana akan dikembalikan ke Pembeli apabila nomor resi terdeteksi tidak valid dan Penjual tidak melakukan ubah resi valid dalam 1x24 jam. Jika Penjual memasukkan nomor resi tidak valid lebih dari satu kali maka Kebunbibit akan mengembalikan seluruh dana transaksi kepada Pembeli.
                    </li>
                </ul>
                <ul id="ul">
                    <li id="li">
                        Jika Penjual tidak mengirimkan barang dalam batas waktu pengiriman sejak pembayaran (2x24 jam kerja untuk biaya pengiriman reguler atau 2x24 jam untuk biaya pengiriman kilat), maka Penjual dianggap telah menolak pesanan. Sehingga, sistem secara otomatis memberikan serta mengembalikan seluruh dana (refund) ke Pembeli.
                    </li>
                </ul>
                <ul id="ul">
                    <li id="li">
                        Untuk Penjual yang berdomisili di wilayah Kota Batu, bisa menitipkan paket pesanan yang sudah siap dikirim (sudah dikemas) ke kantor Kebunbibit yang beralamat di: Jl. Puri Diponegoro no. 8 Kel. Sisir Kota Batu.
                    </li>
                </ul>
                <ul id="ul">
                    <li id="li">
                        Penjual yang berdomisili di Kota Batu juga bisa mengambil uang pembayaran secara cash di kantor Kebunbibit, setelah menyerahkan paket pesanannya.
                    </li>
                </ul>
            </div>
            <div class="col-md-4 colmd4-1">
                <img src="img/transaksi-1.png" id="image" />
            </div>
        </div>
    </div>