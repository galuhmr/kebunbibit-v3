<div style="margin-top: 25px;" class="container white-bg">
	<h1>
	 <h1>Cara Order di Kebunbibit</h1>
	</h1> 
<head> 
	<link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css"> 
	</head>
	<body>
		<div class="container" style="margin-top:0px;"> 
			<div class="example2">
				<div class="panel panel-default">
				  <div class="panel-heading" data-acc-link="demo5">Membeli Langsung</div>
				  <div class="panel-body" data-acc-content="demo5">
				    Pembelian produk di pasar Kebunbibit sangat mudah dan tanpa ribet. Hanya tinggal klik dan klik, maka produk Anda sudah ada pada keranjang belanja, dan siap dikirimkan secepatnya. Berikut ini langkah-langkahnya : </br>
						<p>1.      Cari produk yang Anda inginkan pada “Search Bar” </p>
						<p>2.      Apabila sudah menemukan produk yang Anda cari, klik “Beli” </p>
						<p>3.  Klik “Lanjut Pembayaran” jika Anda ingin melanjutkan ke pembayaran.  Atau klik “Belanja” jika Anda merasa ingin berbelanja lagi. </p>
						<p>4.    Tekan tombol “Keranjang Belanja” untuk melihat produk yang sudah masuk kedalam keranjang belanja. Pastikan produk yang ada dalam keranjang belanja Anda sudah benar. </p>
						<p>5.  Klik “Bayar” apabila sudah yakin dengan produk yang Anda beli. </p>
						<p>6.  Segera proses pembayaran Anda agar produk Anda sampai pintu rumah secepatnya. </p>
				  </div>
				</div>

				<div class="panel panel-default">
				  <div class="panel-heading" data-acc-link="demo6">Menawar Harga</div>
				  <div class="panel-body" data-acc-content="demo6">
				    Selain bisa membeli langsung, Anda juga bisa menawar harga produk di pasar Kebunbibit dengan menggunakan fitur “Tawar” di bawah gambar produk tanaman. Berikut ini tatacara untuk menawar: </br>
						<p>1. Cari produk yang Anda inginkan pada search bar. </p>
						<p>2. Apabila harga produk dirasa mahal, Anda bisa menggunakan fitur TAWAR. Di fitur ini Anda langsung berhubungan dengan penjual untuk melakukan tawar menawar  agar mendapatkan harga terendah. </p>
						<p>3. Klik 'Lanjut Pembayaran' bila Anda telah selesai berbelanja atau klik 'Belanja Lagi' bila ingin berbelanja lagi. </p>
						<p>4. Tekan tombol 'Keranjang Belanja' yang berada pada sebelah kanan atas, untuk melihat produk yang sudah masuk ke dalam keranjang belanja. Pastikan produk yang ada dalam keranjang belanja Anda benar. </p>
						<p>5. Klik 'bayar' apabila sudah yakin dengan produk yang Anda beli. Sesaat kemudian Anda akan menerima rincian pembelian melalui email. </p>
				  </div>
				</div>
			</div>
		</div> 
	</body> 
</div>