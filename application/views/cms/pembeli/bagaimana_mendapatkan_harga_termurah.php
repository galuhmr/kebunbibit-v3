<div style="margin-top: 27px; border-top: 0px white;" class="container white-bg">

			<h3 style="text-align:center;font-family: 'Open Sans', sans-serif;">Bagaimana Mendapatkan Harga Termurah?</h3>
				<hr style="border-top:5px solid #7ac144;">
			<div class="col-md-12">
				<div class="col-md-3 colmd2">
					<img src="img/coin.png" />
				</div>
				<div class="col-md-9 colmd9">
					<p style="text-align:justify;">
						Shopping tanaman hias online bagaikan belanja di pasar minggu, kini bukan hanya sebatas impian. Yup, karena saat ini pasar Kebunbibit hadir dengan fitur baru untuk melayani system menawar online. Sekarang transaksi tawar menawar tidak hanya bisa dilakukan oleh Penjual dan Pembeli langsung alias tidak perlu bertatap muka untuk menyepakati harga yang deal dan pas untuk Anda. Hanya dengan mengirimkan pesan ke Penjual, Anda bisa menawar harga tanaman yang Anda inginkan. Jangan khawatir akan tertipu dengan harga yang dipasarkan di produk tanaman. Sebab, harga tanaman sudah dipatok langsung oleh Penjual yang notabene adalah petani atau pehobi tanaman hias.
					</p>
					<p id="paragraph"><img src="img/Icon.png"/ style="width:20px;height:auto;">
						Berikut ini salah satu cara untuk mendapatkan harga termurah untuk berbelaja keperluan berkebun Anda di pasar Kebunbibit:
					</p>
					<ol style="font-family: 'Open Sans', sans-serif" id="ol">
						<li>
							 Klik button "tawar" <img src="img/tawar.jpg" style="width:70px;height:auto;"/> dan masukkan harga penawaran sesuai keinginan Anda.
						</li>
						<li>
							Alangkah baiknya bila Anda menentukan berapa batasan harga untuk tanaman yang diinginkan. Memiliki batasan yang jelas akan menghindari Anda membayar lebih dari yang diinginkan.
						</li>
						<li>
							Penjual akan merespon untuk menerimanya, menaikkan harga tawaran atau bahkan menolak.
						</li>
						<li>
							Proses ini dapat dilakukan berulang kali sampai Anda dan Penjual mendapatkan harga kesepakatan.
						</li>
					</ol>
				</div>
			</div>
		</div>