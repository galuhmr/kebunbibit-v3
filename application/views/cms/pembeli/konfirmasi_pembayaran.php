<html>
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
    <meta name="viewport" content="width=device-width, initial-scale=1"> 
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <link href="https://kebunbibit.id/public/css/bootstrap.min.css" rel="stylesheet">
     <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>public/css/ui.all.css" /> 
    <script src="<?php echo base_url(); ?>public/js/jquery-1.8.0.min.js"></script>
    <script src="<?php echo base_url(); ?>public/js/ui.core.js"></script>
    <script src="<?php echo base_url(); ?>public/js/ui.datepicker.js"></script>
    <script src="<?php echo base_url(); ?>public/js/ui.datepicker-id.js"></script>
    
    <link href='http://fonts.googleapis.com/css?family=Lobster+Two' rel='stylesheet' type='text/css'>
        <style type="text/css">
            .col-md-9{
                padding-left: 0px;
            }
            .col-md-4{
                padding-left: 0px;
                margin-top: 10px; 
            }
            .col-md-2{
                margin-top: 10px; 
            }
            .konten1{
              font: 400 35px/1.3 'Lobster Two', Helvetica, sans-serif;
              text-shadow: 1px 1px 0px #ededed, 4px 4px 0px rgba(0,0,0,0.15);
              font-style:italic;
              margin-top: 0px;
              margin-bottom: 0px;
              text-align:center;
            }
            .garis{
                width: 95%;
                height: 3px;
                background-color: #7ac144;
                margin-top: 0px;
                margin-bottom: 0px;
            }
            .konten2{
                font-size: 17px;
                font-family: Arial, "Helvetica Neue", Helvetica, sans-serif;
            }
            .konten3{
                font-size: 16px;
                font-family: Arial, "Helvetica Neue", Helvetica, sans-serif;
            }
            .konten4{
                font-size: 16px;
                font-family: Arial, "Helvetica Neue", Helvetica, sans-serif;
                margin-left: 50px;
            }
            .konten5{
                font-size: 16px;
                padding-top: 15px;
                font-family: Arial, "Helvetica Neue", Helvetica, sans-serif;
            }
        </style>
</head>
<body>
    <div class="container white-bg" style="border-top:0px solid white; margin-top:27px;">
        <h3 style="text-align:center;font-family: 'Open Sans', sans-serif;">Konfirmasi Pembayaran</h3>
        <hr style="border-top:5px solid #7ac144;">
        <div class="panel-body">
            <div class="col-md-12">
             <div style="border : 2px solid; margin-left: 175px; margin-right: 175px; padding-bottom: 35px; border-radius :15px;">
<div class="bs-example">

    <form action="post.php" method="POST" class="form-horizontal" style="margin-right: 258px; margin-top: 10px; margin-left: 180px; text-align: center;">
        <div class="form-group">
            <label class="control-label col-xs-3" for="inputNomerOrder">Nomer Order :</label>
            <div class="col-xs-9">
                <input type="nomerorder" class="form-control" id="inputNomerOrder" placeholder="Cth : MP123456">
            </div>
        </div> 
        <div class="form-group">
            <label class="control-label col-xs-3" id="InputBank" >Bank:</label>
            <div class="col-xs-9">
                <select class="form-control">
                    <option>(MANDIRI)/ PT Kebunbibit Penuh Bunga/ 1440015151621</option>
                    <option>(BCA)/ PT Kebunbibit Penuh Bunga/ 0113218611</option> 
                    <option>(BRI)/PT Kebunbibit Penuh /055101000296305</option>   
                </select>
            </div>
        </div>
            <div class="form-group">
                <label class="control-label col-xs-3" id="BuktiTransfer" >Bukti Transfer:</label>
                <div class="col-xs-9">
                     <input type="file">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-xs-3">Tanggal Transfer:</label>
                <div class="col-xs-9">
                 <div>
             <input class="form-control" name="tglterbit" id="tglterbit" type="text" /></input>
            </div>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-xs-3" >Nominal:</label>
            <div class="col-xs-9">
                <input class="form-control" name="InputNominal" id="InputNominal" type="text" placeholder="Cth: 900000" /></input>
            </div>
        </div>
        <br>
        <div class="form-group">
            <div class="col-xs-offset-3 col-xs-9">
                <input type="submit" class="btn btn-primary" value="Kirim Langsung">
                <input type="reset" class="btn btn-default" value="Reset">
            </div>
        </div>
    </form>
</div>
</div>
    </div>
    </div>
    </div>
     <script type="text/javascript">
        $(document).ready(function(){
            $("#tglterbit").datepicker({
                dateFormat : "dd/mm/yy",
                changeMonth : true,
                changeYear : true
            });
        });
    </script>
</body>
</html>