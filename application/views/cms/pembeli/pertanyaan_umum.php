    <link rel="icon" href="https://kebunbibit.id/img/kebunbibit.png">
    <style rel="stylesheet" type="text/css">
.scroll {
    padding: 10px;
    overflow-y: auto;
    height: 500px;
}
.scroll::-webkit-scrollbar {
    width: 12px;
}
/* Track */

.scroll::-webkit-scrollbar-track {
    -webkit-box-shadow: inset 0 0 6px #27AE61;
    -webkit-border-radius: 10px;
    border-radius: 10px;
}
/* Handle */

.scroll::-webkit-scrollbar-thumb {
    -webkit-border-radius: 10px;
    border-radius: 10px;
    background: #7ac144;
    -webkit-box-shadow: inset 0 0 6px #27AE61;
}
.scroll::-webkit-scrollbar-thumb:window-inactive {
    background: #27AE61;
}

.panel-group .panel {
    margin-bottom: 20px;
    border-radius: 4px;
}
.panel-default>.panel-heading+.panel-collapse>.panel-body {
    border-top-color: #ddd;
}
.panel-default>.panel-heading {
    color: #333;
    background: #f7f7f7;
    border-color: #ddd;
}


    </style>
    <meta charset="utf-8">
    <title>Pertanyaan Umum Pembeli</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script type="text/javascript" src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <div class="container white-bg" style="margin-top:27px; border-top:0px solid white;">
        <h3 style="text-align:center;font-family: 'Open Sans', sans-serif;"> Pertanyaan Umum (Pembeli)</h3>
        <hr style="border-top:5px solid #7ac144;">
        <div class="col-md-9">
            <div class="panel-group scroll" style="overflow-y:auto;" id="accordion">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
               <a style="text-decoration:none;">1. Apakah saya bisa melakukan pembelian grosir?</a>
              </h4>
                    </div>
                    <div class="panel-collapse collapse">
                        <div class="panel-body">
                            Jika Anda membeli barang dalam jumlah banyak, Anda bisa meminta diskon pada Penjual di fitur tawar yang telah kami sediakan ataupun sekaligus menanyakan ongkos kirimnya.
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
               <a style="text-decoration:none;">2. Bila saya tidak mempunyai rekening, apakah saya bisa COD dengan Penjual?</a>
              </h4>
                    </div>
                    <div class="panel-collapse collapse">
                        <div class="panel-body">
                            Untuk saat ini, hanya yang memiliki rekening yang bisa melakukan transaksi di pasar Kebunbibit.
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
               <a style="text-decoration:none;">3. Apakah kredit poin yang saya miliki bisa saya uangkan?</a>
              </h4>
                    </div>
                    <div class="panel-collapse collapse">
                        <div class="panel-body">
                            Kredit Poin yang Anda kumpulkan tidak bisa diuangkan tapi bisa Anda gunakan untuk berbelanja produk yang ada di pasar Kebunbibit.
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
               <a style="text-decoration:none;">4. Kira-kira berapa hari produk orderan saya bisa diterima?</a>
              </h4>
                    </div>
                    <div class="panel-collapse collapse">
                        <div class="panel-body">
                            Orderan Anda bisa diterima dalam jangka waktu sesuai dengan alamat Anda tinggal saat ini. Estimasi kedatangan belanjaan Anda bisa di cek secara rinci di <a href="http://jne.co.id/id/beranda" style="text-decoration:none;" >www.jne.co.id </a>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
               <a style="text-decoration:none;">5. Bagaimana melakukan cara pengecekan orderan saya?</a>
              </h4>
                    </div>
                    <div class="panel-collapse collapse">
                        <div class="panel-body">
                            Kami akan selalu mengirimi Anda notifikasion via email yang menginformasikan status orderan Anda.
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
               <a style="text-decoration:none;">6. Bagaimana cara membatalkan orderan saya?</a>
              </h4>
                    </div>
                    <div class="panel-collapse collapse">
                        <div class="panel-body">
                            Anda bisa membatalkan orderan dengan langsung menghubungi Penjualnya via tawar. Kami akan memberi notifikasi via email kepada Penjual untuk segera membatalkan orderan Anda sebelum pembayaran selesai. Akan tetapi bila Anda membatalakan order sesudah Anda membayar total belanjaan Anda, maka tidak diperbolehkan.
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
               <a style="text-decoration:none;">7. Bagaimana saya mengetahui apakah pembayaran saya berhasil atau gagal ?</a>
              </h4>
                    </div>
                    <div class="panel-collapse collapse">
                        <div class="panel-body">
                            Anda bisa memastikan pembayaran Anda berhasil atau tidak melalui Bank dimana Anda melakukan proses pembayaran. Sebab pihak bank memiliki sistem yang aman dan terpercaya sehingga bisa melacak transaksi pada Bank Anda.
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
               <a style="text-decoration:none;">8. Bagaimana cara merubah alamat pengiriman, nomor telepon atau penerima dalam orderan saya?</a>
              </h4>
                    </div>
                    <div class="panel-collapse collapse">
                        <div class="panel-body">
                            Anda bisa merubah alamat pengiriman dengan langsung memberi tahu Penjual di fitur tawar sebelum Penjual mengirimkan barang belanjaan Anda.
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
               <a style="text-decoration:none;">9. Dapatkah saya merubah metode pembayaran untuk orderan saya?</a>
              </h4>
                    </div>
                    <div class="panel-collapse collapse">
                        <div class="panel-body">
                            Yups, Anda bisa merubah metode pembayaran apabila Anda belum melakukan pembayaran untuk total belanjaan Anda.
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
               <a style="text-decoration:none;">10. Bisakah saya merubah produk dari  orderan saya setelah orderan saya diproses?</a>
              </h4>
                    </div>
                    <div class="panel-collapse collapse">
                        <div class="panel-body">
                            Anda tidak boleh mengubah atau membatalkan produk orderan apabila status order Anda sedang diproses.
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
               <a style="text-decoration:none;">11. Saya tidak sengaja memesan produk tanaman yang sama, bisakah saya membatalkan salah satunya?</a>
              </h4>
                    </div>
                    <div class="panel-collapse collapse">
                        <div class="panel-body">
                            Yups, Anda bisa membatalkan pesanan apabila Anda belum mentransfer uang yang harus Anda bayarkan.
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
               <a style="text-decoration:none;">12. Saya menerima orderan tanaman atau non tanaman dengan kondisi mati/rusak, apa yang harus saya lakukan?</a>
              </h4>
                    </div>
                    <div class="panel-collapse collapse">
                        <div class="panel-body">
                            Anda bisa langsung menanyakannya kepada Penjual melalui fitur tawar yang sudah disediakan.
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
               <a style="text-decoration:none;">13. Saya menerima orderan yang salah, apa yang harus saya lakukan?</a>
              </h4>
                    </div>
                    <div class="panel-collapse collapse">
                        <div class="panel-body">
                            Anda bisa langsung menanykannya kepada Penjual melalui fitur tawar yang sudah disediakan.
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
               <a style="text-decoration:none;">14. Saya memesan lebih dari satu item, tapi yang datang hanya satu. Bagaimana dengan produk pesanan saya yang lain?</a>
              </h4>
                    </div>
                    <div class="panel-collapse collapse">
                        <div class="panel-body">
                            Anda harus memastikan kepada Penjual melalui fitur tawar yang tersedia, apakah barang selanjutnya akan dikirim menyusul atau memang stoknya sedang tidak tersedia.
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
               <a style="text-decoration:none;">15. Bisakah saya menjadwalkan pengiriman untuk orderan saya?</a>
              </h4>
                    </div>
                    <div class="panel-collapse collapse">
                        <div class="panel-body">
                            Tentu bisa, apabila Anda membicarakannya terlebih dahulu dengan Penjual melalui fitur tawar.
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
               <a style="text-decoration:none;">16. Apakah pengiriman juga dilakukan pada hari sabtu, minggu, dan hari libur nasional lainnya?</a>
              </h4>
                    </div>
                    <div class="panel-collapse collapse">
                        <div class="panel-body">
                            Kami melibatkan pihak ketiga untuk proses pengiriman yaitu JNE, Tiki dan Pos Indonesia. Mereka bekerja sesuai dengan hari kerja, sehingga tidak memungkinkan kami mengirim pesanan Anda pada hari – hari tersebut.
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
               <a style="text-decoration:none;">17. Bagaimana jika saya tidak berada dirumah saat paket datang?</a>
              </h4>
                    </div>
                    <div class="panel-collapse collapse">
                        <div class="panel-body">
                            Pada alamat tujuan pengiriman Anda harus menyertakan nomor telepon, sehingga bila kurir datang dan tidak ada orang di rumah, Anda bisa langsung dihubungi oleh kurir untuk mengambil barang di pusat pengiriman terdekat.
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
               <a style="text-decoration:none;">18. Bagaimana apabila orderan saya belum juga saya terima dalam jangka waktu yang telah dijanjikan?</a>
              </h4>
                    </div>
                    <div class="panel-collapse collapse">
                        <div class="panel-body">
                            Anda bisa mengecek nomor resi di website metode pengiriman yang Anda pilih. Bila Anda memilih JNE, maka Anda bisa mengecek di website JNE. Atau menanyakan langsung ke Penjual melalui fitur tawar.
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
               <a style="text-decoration:none;">19. Nomor resi pesanan produk saya tidak bisa dilacak, apa yang harus saya lakukan?</a>
              </h4>
                    </div>
                    <div class="panel-collapse collapse">
                        <div class="panel-body">
                            Anda harus memastikannya kepada Penjual pada fitur tawar, apakah sudah betul nomor resinya dan kapan tepatnya barang Anda dikirim.
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <img src="https://kebunbibit.id/public/img/cms/FAQ.png" style="
        text-align:center;margin:auto;display: table-cell;vertical-align: middle;"/>
        </div>
    </div>
    <script language="javascript" type="text/javascript">
(function()
{

    $('.panel-collapse')
        .hide();
    $('.panel-heading')
        .hover(function()
        {
            $(this)
                .next()
                .slideDown()
                .siblings('.panel-collapse')
                .slideUp(200);
        });
})();

    </script>
