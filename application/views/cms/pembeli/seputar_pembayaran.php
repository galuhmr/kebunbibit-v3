    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

    <style type="text/css">


.nav>li>a:hover {
    text-decoration: none;
    background-color: #eee;
    border-top-right-radius: 50%;
    border-top-left-radius: 50%;

}
.panel-heading {
    font-color:white;
}
.panel-default>.panel-heading {
    color: #333;
    background-color:#7ac144;
    border-color: #7ac144;
}
.panel-body {
    padding: 15px;
    border: 1px solid #7ac144;
}

@media (max-width: 992px) {
    .nav-tabs>li.active>a,
    .nav-tabs>li.active>a:focus,
    .nav-tabs>li.active>a:hover {
        border-top-right-radius: 50%;
        border-top-left-radius: 50%;
    }
    #image_ordr {
        width: 100px;
        height: 100px;
    }
    .nav-tabs>li {
        margin-left: 4vw;
    }
    #gambar1{
        text-align:center;
        margin:auto;
        display: table-cell;
        vertical-align: middle;
    }
}
@media (min-width: 992px) {
    .nav-tabs {
        text-align: center;
    }
    .nav-tabs>li {
        margin-bottom: -1px;
        margin-left: 15px;
    }
    #image_ordr {
        width: 175px;
        height: 110px;
        padding:10px;
    }
    .nav-tabs>li.active>a,
    .nav-tabs>li.active>a:focus,
    .nav-tabs>li.active>a:hover {
        border-top-right-radius: 50%;
        border-top-left-radius: 50%;
    }
    .colmd2 {
    width: 14%;
}
    .colmd10 {
    width: 85.333333%;
    margin-top:3%;
}
}
@media (max-width: 480px) {
    #image_ordr {
        width: 30px;
        height: 20px;
    }
    #utama {
        padding-bottom: 30px;
    }
    .nav-tabs>li {
        margin-left: 0px;
    }
    #page_image {
        width: 35px;
        height: 35px;
    }
    #gambar1{
        text-align:center;
        margin:auto;
        display: table-cell;
        vertical-align: middle;
    }
}
@media (max-width: 359px) {
    #image_ordr {
        width: 20px;
        height: 20px;
    }
    #utama {
        padding-bottom: 30px;
    }
    #page_image {
        width: 30px;
        height: 30px;
    }
    #gambar1{
        text-align:center;
        margin:auto;
        display: table-cell;
        vertical-align: middle;
    }
}
#image_ordr {
    /* filter: url(filters.svg#grayscale); Firefox 3.5+ */
    
    filter: gray;
    /* IE5+ */
    
    -webkit-filter: grayscale(1);
    /* Webkit Nightlies & Chrome Canary */
    
    -webkit-transition: all .5s ease-in-out;
}
#image_ordr:hover {
    filter: none;
    -webkit-filter: grayscale(0);
}
#stylefont{
    color:white;
}

    </style>

<div style="margin-top: 27px; border-top: 0px solid white" class="container white-bg">
    <h3 style="text-align:center;font-family: 'Open Sans', sans-serif;">Seputar Pembayaran</h3>
    <hr style="border-top:5px solid #7ac144;">
    <div class="panel panel-default" style="margin-top:20px;text-align:justify;">
				  <div class="panel-heading"><b id="stylefont">Membeli Langsung</b></div>
				  <div class="panel-body">
				    Untuk menjamin keamanan dalam bertransaksi, kami membuat ketentuan agar transaksi di Kebunbibit berjalan dengan aman dan nyaman. Adapun ketentuan pembayaran adalah sebagai berikut :
						 <br>
						<ul>
							<li>
								Demi keamanan dan kenyamanan para Pengguna, setiap transaksi jual-beli di Kebunbibit Marketplace diwajibkan untuk menggunakan Kebunbibit Payment System.
							</li>
							<li>
								Pembeli wajib transfer sesuai dengan nominal total belanja dari transaksi dalam waktu 2x24 jam (dengan asumsi Pembeli telah mempelajari informasi barang yang telah dipesannya). Jika dalam waktu 2x24 jam barang dipesan tetapi Pembeli tidak mentransfer total pembayaran maka transaksi akan dibatalkan secara otomatis.

							</li>
							<li>
								Pembeli tidak dapat membatalkan transaksi setelah melunasi pembayaran.
							</li>
							<li>
								Pembeli harus mengisi form konfirmasi pembayaran setelah membayarkan nominal uanganya. Apabila Pembeli tidak mengisi form konfirmasi pembayaran, maka pengiriman tidak akan diproses.
							</li>
							<li>
								Jika Penjual tidak mengirimkan barang dalam batas waktu pengiriman sejak pembayaran (2x24 jam kerja untuk biaya pengiriman reguler atau 2x24 jam untuk biaya pengiriman kilat), maka Penjual dianggap telah menolak pesanan. Sehingga, sistem secara otomatis memberikan serta mengembalikan seluruh dana (refund) ke Pembeli.
							</li>
						</ul>
Sistem pembayaran pada pasar Kebunbibit bisa dilakukan via Indomaret, MasterCard, Visa, BCA, BRI dan Mandiri. Untuk pembayaran melalui kartu kredit, maka akan dikenakan biaya layanan sebesar 3% atau Rp. 7.500,- (di luar total belanja).

				  </div>
				</div>
            <ul class="nav nav-tabs">
                <li class="active">
                    <a data-toggle="tab"  href="#home"><img src="img/bca.png" id="image_ordr" class="img-responsive home" />
                    </a>
                </li>
                <li>
                    <a data-toggle="tab" href="#menu1"><img src="img/mandiri.png" id="image_ordr" class="img-responsive menu1" />
                    </a>
                </li>
                <li>
                    <a data-toggle="tab" href="#menu2"><img src="img/bri.png" id="image_ordr" class="img-responsive menu2" />
                    </a>
                </li>
                <li>
                    <a data-toggle="tab" href="#menu3"><img src="img/indomaret.png" id="image_ordr" class="img-responsive menu3" />
                    </a>
                </li>
                <li>
                    <a data-toggle="tab" href="#menu4"><img src="img/mastercard.png" id="image_ordr" class="img-responsive menu4" />
                    </a>
                </li>

            </ul>
            <div class="tab-content">
                <div id="home" class="tab-pane fade in active">
                  <div class="panel panel-default">
				  <div class="panel-heading"><b id="stylefont">Pembayaran Melalui ATM BCA</b></div>
				  <div class="panel-body">
				   <p>1.Masukkan kartu Anda, lalu ketik password.</p>
                    <p>2.Pilih tombol  “transaksi lain” lalu pilih “transfer” .</p>
                    <p>3.Nomor rekening Kebunbibit adalah 0113218611 a/n PT. Kebunbibit Penuh Bunga.</p>
                    <p>4.Masukkan nominal sesuai dengan jumlah yang harus Anda bayarkan.</p>
                    <p>5.Pastikan sekali lagi bahwa tujuan transfer dan nominal Anda benar.</p>
				  </div>
				</div>
                </div>
                <div id="menu1" class="tab-pane fade in active">
                  <div class="panel panel-default">
				  <div class="panel-heading"><b id="stylefont">Pembayaran Melalui ATM Mandiri</b></div>
				  <div class="panel-body">
                  <p>1.Masukkan kartu Anda, lalu ketik password.</p>
                    <p>2.Pilih tombol  “transaksi lain” lalu pilih “transfer” .</p>
                    <p>3.Nomor rekening Kebunbibit adalah 1440015151621  a/n PT. Kebunbibit Penuh Bunga.</p>
                    <p>4.Masukkan nominal sesuai dengan jumlah yang harus Anda bayarkan.</p>
                    <p>5.Pastikan sekali lagi bahwa tujuan transfer dan nominal Anda benar.</p>
				    
				  </div>
				</div>
                </div>
                <div id="menu2" class="tab-pane fade in active">
                  <div class="panel panel-default">
				  <div class="panel-heading"><b id="stylefont">Pembayaran Melalui ATM BRI</b></div>
				  <div class="panel-body">
				    <p>1.Masukkan kartu Anda, lalu ketik password.</p>
                    <p>2.Pilih tombol  “transaksi lain” lalu pilih “transfer” .</p>
                    <p>3.Nomor rekening Kebunbibit adalah 055101000296305  a/n PT. Kebunbibit Penuh Bunga.</p>
                    <p>4.Masukkan nominal sesuai dengan jumlah yang harus Anda bayarkan.</p>
                    <p>5.Pastikan sekali lagi bahwa tujuan transfer dan nominal Anda benar.</p>
				  </div>
				</div>
                </div>
                <div id="menu3" class="tab-pane fade in active">
                  <div class="panel panel-default">
				  <div class="panel-heading"><b id="stylefont">Pembayaran Melalui Indomaret</b></div>
				  <div class="panel-body">

                   <p>1.Pembayaran dengan Indomaret dapat dilakukan dengan menunjukkan nomor transaksi Anda pada gerai Indomaret pilihan Anda. </p>
                    <p>2.Anda akan dikenakan biaya pembayaran per transaksi sebesar 5000 (di luar total belanja) yang dibayarkan langsung saat pembayaran melalui gerai Indomaret. Ini merupakan ketentuan sepenuhnya dari Indomaret dan dapat berubah sewaktu-waktu tanpa pemberitahuan sebelumnya.</p>
                    <p>3.Metode pembayaran ini bergantung kepada jam operasional cabang gerai Indomaret pilihan Anda. Hal tersebut sepenuhnya berada di luar kuasa dan tanggung jawab Kebunbibit.id</p>
				    
				  </div>
				</div>
                </div>
                <div id="menu4" class="tab-pane fade in active">
                  <div class="panel panel-default">
				  <div class="panel-heading"><b id="stylefont">Pembayaran Melalui Mastercard</b></div>
				  <div class="panel-body">
                  <p>1.Kebunbibit hanya menerima pembayaran dengan kartu Visa/Mastercard yang dikeluarkan oleh bank dalam negeri.</p>
                    <p>2.Pembayaran dengan kartu Visa/Mastercard secara online, otorisasi akan dilakukan lewat payment gateway yang bekerjasama dengan PT Kebunbibit Penuh Bunga.</p>
                    <p>3.Kami menerima pembayaran dengan menggunakan kartu berlogo Visa dan Mastercard yang mendukung fasilitas keamanan 3-D Secure (3-domain Secure).</p>
                    <p>4.PT .Kebunbibit Penuh Bunga berhak membatalkan transaksi bila terdapat indikasi percobaan penyalahgunaan kartu dalam pembayaran.
                    </p>
                    <p>
                        Jika terjadi pengembalian barang kepada Penjual (barang di-retur) oleh Pembeli, pengembalian dana baru dapat diproses setelah 5 hari transaksi terjadi.
                    </p>
				    
				  </div>
				</div>
                </div>
            </div>
        <div class="panel panel-default" style="margin-top:20px;text-align:justify;">
            <div class="panel-heading"><b id="stylefont">Jaminan</b></div>
                  <div class="panel-body">
                  <div class="col-md-2 colmd2"><img src="img/jaminan.png" id="gambar1"/></div>
                  <div class="col-md-10 colmd10">
                    Kebunbibit akan mengembalikan 100% uang Pembeli ke saldo Bank yang digunakan untuk bertransaksi, jika Penjual tidak mengirim barang (3 hari kerja untuk biaya pengiriman reguler atau 3x24 jam (tidak termasuk hari besar) untuk biaya pengiriman kilat) setelah mengisi form konfimasi pembayaran.

                    </div>
                  </div>
                  </div>
                </div>
    </div>
<script language="javascript" type="text/javascript">
$(".home")
    .click(function()
    {
        $("#home")
            .fadeIn();
        $("#menu1")
            .fadeOut();
        $("#menu2")
            .fadeOut();
        $("#menu3")
            .fadeOut();
        $("#menu4")
            .fadeOut();
        
    });
$('#menu1')
    .hide();
$(".menu1")
    .click(function()
    {
        $("#home")
            .fadeOut();
        $("#menu1")
            .fadeIn();
        $("#menu2")
            .fadeOut();
        $("#menu3")
            .fadeOut();
        $("#menu4")
            .fadeOut();
    });
$('#menu2')
    .hide();
$(".menu2")
    .click(function()
    {
        $("#home")
            .fadeOut();
        $("#menu1")
            .fadeOut();
        $("#menu2")
            .fadeIn();
        $("#menu3")
            .fadeOut();
        $("#menu4")
            .fadeOut();
        
    });
$('#menu3')
    .hide();
$(".menu3")
    .click(function()
    {
        $("#home")
            .fadeOut();
        $("#menu1")
            .fadeOut();
        $("#menu2")
            .fadeOut();
        $("#menu3")
            .fadeIn();
        $("#menu4")
            .fadeOut();
        
    });
$('#menu4')
    .hide();
$(".menu4")
    .click(function()
    {
        $("#home")
            .fadeOut();
        $("#menu1")
            .fadeOut();
        $("#menu2")
            .fadeOut();
        $("#menu3")
            .fadeOut();
        $("#menu4")
            .fadeIn();
    });



</script>