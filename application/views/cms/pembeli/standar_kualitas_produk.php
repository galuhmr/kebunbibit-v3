<!DOCTYPE html>
<html>
<head>
	<title> Standar Kualitas Produk</title>		 
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<link href="https://fonts.googleapis.com/css?family=Oswald:700" rel="stylesheet">

	<style type="text/css">
	#photos {
	margin: 0 auto;
	max-width: 100%;
	position: relative;
	padding:0px;
}
#head1{
	padding:0px;
}
#photos a {
	text-decoration: none;
}

#photos img {
	position: relative;
	float: left;
	width: 100%;
}

#photos p {
	padding: 10% 0 0 10%;
	position: absolute;
	max-width: 100%;
	font-family: 'Oswald', cursive;
  color: #fff;
	font-size: 3vw;
	text-align: center;
}
@media (min-width: 992px){
		#image3{
			width:100%;
			height:auto;
			display:table-cell;
			margin:auto;
			vertical-align:middle;
		}
	}
@media (max-width: 992px){
		#image3{
			width:80%;
			height:auto;
			display:table-cell;
			margin:auto;
			vertical-align:middle;
		}
		#head1{
			width:60%;
		}
		#head2{
			width:40%;
			float:left;
		}
	}
	</style>
</head>
<body>
		<div class="container white-bg" style = "border-top:0px solid white;margin-top:27px;">
		  	    	<div class="col-md-12">	
	   					<div class="col-md-8" id="head1">
	   					<section id="photos">
					      <img src='https://kebunbibit.id/public/img/cms/header_03.png' >
      						<p>STANDAR KUALITAS PRODUK</p>
  						</section>
  					</div>
	   					<div class="col-md-4" id="head2">
	   					<img src="https://kebunbibit.id/public/img/cms/Standar-Kualitas-Produk_03.png"  class="img-responsive" id="image2" style="margin-top:7%">
	   					</div>
	   				</div>
	   				<div class="col-md-12">
	   					<p style="text-align:justify;padding-top:20px;">Para Penjual di pasar Kebunbibit notabene adalah petani atau pehobi tanaman hias yang memang sudah berpengalaman dengan aktivitas berkebun. Sehingga produk tanaman di pasar Kebunbibit adalah produk yang bisa dijamin kualitasnya. Beraneka tanaman dijual di Kebunbibit yang digolongkan sesuai dengan kategorinya. Produk tanaman di Kebunbibit antara lain adalah tanaman hias langka, tanaman karnivora, obat dan bumbu, buah serta sayuran.</p>
	   				</div>
	   				<div class="col-md-12">
	   					<div class="col-md-4">
							<img src="https://kebunbibit.id/public/img/cms/Standar-Kualitas-Produk_07.png" id="image3" style="height:auto;display:table-cell;margin:auto;vertical-align:middle;">
						</div>
					<div class="col-md-8">
					<p style=" text-align : justify;">Jangan khawatir bila tanaman Anda mendapat masalah, silahkan langsung menghubungi Penjual melalui fitur chatting yang telah kami sediakan. Pasar Kebunbibit juga menyediakan produk non tanaman yang sangat ampuh mengatasi permasalahan Anda dalam berkebun. Pupuk, obat obatan, hormon adalah produk andalan yang sudah dibuktikan sendiri oleh para pehobi tanaman hias. Berikan tanaman Anda perawatan maksimal dengan memberi obat agar terhindar dari serangan hama dan penyakit. Hasil panen berlimpah dan berkualitas bukan lagi mimpi buat Anda. Berikan tanaman buah atau sayuran Anda hormon untuk merangsang pertumbuhan bunga agar berbuah dengan sempurna. Ambil yang baik untuk pertumbuhan tanaman Anda!</p>

					<p style="text-align : justify;">Jadikan passion dan hobi berkebun Anda semakin mudah, dengan mengambil peralatan berkebun di pasar Kebunbibit. Kami menyediakan gardening tool dari bahan berkualitas tinggi dan tahan lama. Mulailah dari memilih alat yang baik untuk mengolah tanah, agar tanaman kesayangan Anda tumbuh dengan sempurna. Segala bentuk pot dan aksesoris tanaman hias berkualitas bagus, bisa Anda dapatkan di pasar Kebunbibit dari Penjualnya langsung. Jangan biarkan tanaman hias kesayangan Anda tampil biasa saja, tampilkan keindahnnya menggunakan pot yang menyempurnakan tampilannya.
					Selain itu untuk menjaga kualitas produk di pasar Kebunbibit, kami selalu melakukan kontrol pengawasan terhadap kualitas produk dan layanan penjual. Apabila Anda mendapatkan masalah, jangan sungkan untuk segera menghubungi kami di nomor 0341 - 599399 atau melalui email <a href ="mailto:bantuan@kebunbibit.id" style="text-decoration:none;">bantuan@kebunbibit.id.</a> Dengan senang hati Kebunbibit akan menyelesaikan permasalahan Anda.</p>
		</div>
	   	</div>

<!-- <div class="container" style="padding-right : 29px;padding-left :29px; margin-top :60px; margin-bottom: -50px;">
		<div class="kotak" style ="color: black; text-align : justify;">
				<p style="margin: -45px 0px 52px;">Para Penjual di pasar Kebunbibit notabene adalah petani atau pehobi tanaman hias yang memang sudah berpengalaman dengan aktivitas berkebun. Sehingga produk tanaman di pasar Kebunbibit adalah produk yang bisa dijamin kualitasnya. Beraneka tanaman dijual di Kebunbibit yang digolongkan sesuai dengan kategorinya. Produk tanaman di Kebunbibit antara lain adalah tanaman hias langka, tanaman karnivora, obat dan bumbu, buah serta sayuran.</p>
			</div>
	</div>

<div class="container white-bg">
	<div class="col-md-4">
		<img src="https://kebunbibit.id/public/img/cms/Standar-Kualitas-Produk_07.png" style="margin-top : 40px">
	</div>
		<div class="col-md-8">
			<p style="margin-top : 25px; text-align : justify;">Jangan khawatir bila tanaman Anda mendapat masalah, silahkan langsung menghubungi Penjual melalui fitur tawar yang telah kami sediakan. Pasar Kebunbibit juga menyediakan produk non tanaman yang sangat ampuh mengatasi permasalahan Anda dalam berkebun. Pupuk, obat obatan, hormon adalah produk andalan yang sudah dibuktikan sendiri oleh para pehobi tanaman hias. Berikan tanaman Anda perawatan maksimal dengan memberi obat agar terhindar dari serangan hama dan penyakit. Hasil panen berlimpah dan berkualitas bukan lagi mimpi buat Anda. Berikan tanaman buah atau sayuran Anda hormon untuk merangsang pertumbuhan bunga agar berbuah dengan sempurna. Ambil yang baik untuk pertumbuhan tanaman Anda!</p>

			<p style="text-align : justify;">Jadikan pacssion dan hobi berkebun Anda semakin mudah, dengan mengambil peralatan berkebun di pasar Kebunbibit. Kami menyediakan gardening tool dari bahan berkualitas tinggi dan tahan lama. Mulailah dari memilih alat yang baik untuk mengolah tanah, agar tanaman kesayangan Anda tumbuh dengan sempurna. Segala bentuk pot dan aksesoris tanaman hias berkualitas bagus, bisa Anda dapatkan di pasar Kebunbibit dari Penjualnya langsung. Jangan biarkan tanaman hias kesayangan Anda tampil biasa saja, tampilkan keindahnnya menggunakan pot yang menyempurnakan tampilannya.
			Selain itu untuk menjaga kualitas produk di pasar Kebunbibit, kami selalu melakukan kontrol terhadap kualitas produk dan layanan. Apabila Anda mendapatkan masalah, jangan sungkan untuk segera menghubungi kami di nomor 0341 - 599399 atau melalui email bantuan @kebunbibit.id. Dengan senang hati Kebunbibit akan mendengar permasalahan Anda.</p>
		</div>
</div>
</div> -->
	<script src="js/jquery-1.12.2.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
</div>
</body>
</html>