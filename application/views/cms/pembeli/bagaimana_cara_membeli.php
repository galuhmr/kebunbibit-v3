<html>
<head>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Lobster+Two' rel='stylesheet' type='text/css'>
	    <style type="text/css">
	    	.konten2{
	    		font-size: 17px;
	    		margin-left: 10px;
	    		font-family: Arial, "Helvetica Neue", Helvetica, sans-serif;
	    	}
	    	.konten3{
	    		margin-left: 40px; 
	    		margin-bottom: 0px;
	    		font-size: 16px;
	    		font-family: Arial, "Helvetica Neue", Helvetica, sans-serif;
	    		margin-bottom: 10px;
	    	}
	    	.garis{
	    		width: 90%;
	    		height: 3px;
	    		background-color: #27AE61;
	    		margin-top: 0px;
	    		margin-bottom: 0px;
	    	}
	    	.garis2{
	    		width: 90%;
	    		height: 3px;
	    		background-color: #27AE61;
	    		margin-top: 0px;
	    		margin-bottom: 0px;
	    	}
	    	.gmbr1{
	    		width: 30px;
	    		height: 30px;
	    	}
	    	.gmbr2{
	    		width: 300px;
    			height: 30px;
    			margin-left: 20px;
	    	}
	    	.gmbr3{
	    		width: 125px;
   				height: 30px;
   				margin-left: 20px;
	    	}
	    	.gmbr4{
	    		width: 90px;
    			height: 40px;
   				margin-left: 20px;
	    	}
	    	.gmbr5{
	    		width: 90px;
    			height: 30px;
   				margin-left: 20px;
	    	}
	    	@media(max-width: 360px){
	    		.gmbr2{
	    			margin-left: 0px;
	    			width: 85%;
	    		}
	    	}
	    	@media(max-width: 450px){
	    		.gmbr2{
	    			margin-left: 0px;
	    			width: 85%;
	    		}
	    	}
	    </style>
</head>
<body>
	<div class="container white-bg" style ="border-top: 0px solid white; margin-top:27px;">
		<h3 style="text-align:center;font-family: 'Open Sans', sans-serif;"> Bagaimana Cara Membeli? </h3>
		<hr style="border-top:5px solid #7ac144;">
		<div class="col-md-12">
			<div class="col-md-6">
				<img class="gmbr1" src="https://kebunbibit.id/public/img/cms/Icon.png"><span class="konten2"><b>Membeli Langsung</b></span>
			</div>
			<div class="col-md-12">
				<p class="konten3">Pembelian produk di pasar Kebunbibit sangat mudah dan tanpa ribet. Hanya tinggal klik dan klik, maka produk Anda sudah ada pada keranjang belanja, dan siap dikirimkan secepatnya. Berikut ini langkah-langkahnya :</p>
				<p class="konten3">1. Cari produk yang Anda inginkan pada "Search Bar"<img class="gmbr2" src="https://kebunbibit.id/public/img/cms/cari.jpg"></p>
				<p class="konten3">2. Apabila sudah menemukan produk yang Anda cari, klik "Beli"<img class="gmbr3" src="https://kebunbibit.id/public/img/cms/beli.jpg"></p>
				<p class="konten3">3. Tekan tombol "Keranjang Belanja" untuk melihat produk yang sudah masuk ke dalam keranjang belanja. Pastikan produk yang ada dalam keranjang belanja Anda sudah benar.<img class="gmbr4" src="https://kebunbibit.id/public/img/cms/keranjang.jpg"></p>
				<p class="konten3">4. Klik "Bayar" pada ikon keranjang belanja jika Anda ingin melanjutkan ke pembayaran.<img class="gmbr5" src="https://kebunbibit.id/public/img/cms/bayar.jpg"></p>
				<p class="konten3">5. Segera proses pembayaran Anda agar produk Anda sampai di pintu rumah secepatnya.</p>
			</div>
		</div>
		<hr style="border-top:5px solid #7ac144;">
		<div class="panel-body">
			<div class="col-md-6">
				<img class="gmbr1" src="https://kebunbibit.id/public/img/cms/Icon.png"><span class="konten2"><b>Menawar Harga</b></span>
			</div>
			<div class="col-md-12">
				<p class="konten3">Selain bisa membeli langsung, Anda juga bisa menawar harga produk di pasar Kebunbibit dengan menggunakan fitur "Tawar" di bawah gambar produk tanaman. Berikut ini tatacara untuk menawar :</p>
				<p class="konten3">1. Cari produk yang Anda inginkan pada "Search Bar"<img class="gmbr2" src="https://kebunbibit.id/public/img/cms/cari.jpg"></p>
				<p class="konten3">2. Apabila harga produk dirasa mahal, Anda bisa menggunakan fitur TAWAR. Di fitur ini Anda langsung berhubungan dengan Penjual untuk melakukan tawar menawar  agar mendapatkan harga terendah.<img class="gmbr3" src="https://kebunbibit.id/public/img/cms/beli.jpg"></p>
				<p class="konten3">3. Bila harga sudah disepakati, tekan tombol "Keranjang Belanja" yang berada pada sebelah kanan atas, untuk melihat produk yang sudah masuk ke dalam keranjang belanja. Pastikan produk yang ada dalam keranjang belanja Anda benar.<img class="gmbr4" src="https://kebunbibit.id/public/img/cms/keranjang.jpg"></p>
				<p class="konten3">4. Klik "bayar" apabila sudah yakin dengan produk yang Anda beli. Sesaat kemudian Anda akan menerima rincian pembelian melalui email.<img class="gmbr5" src="https://kebunbibit.id/public/img/cms/bayar.jpg"></p>
			</div>
		</div>
	</div>
</body>
</html>