<input type="hidden" id="id_user" value="<?php echo $id_user; ?>">
<div style="margin-top: 30px;" class="container">
 	<div class="row">
   		<div class="col-lg-3" style="padding: 0px;">
   	  		<div class="white-bg profil-box">
	     		<ul style="text-align: center;" id="seller-list">
					<li>
						<img class="seller-photo" 
							 src="<?php echo get_user_photo($user->img_path); ?>">
						<h4 class='c-seller_name' style="margin: 25px 0px;">
			      			<?php echo $user->firstname . ' ' . $user->lastname; ?>
			    		</h4>

			    		<!-- short bio user -->
			    		<!-- <div class="bio"></div> -->
			    		
			    		<div class="contact">
			      			Email: <?php echo $user->email; ?>
			    		</div><!-- .contact -->
			    		<div class="contact">
					      <?php if($address): ?> 
					       	No. Telp: <?php echo slength($address->phone) > 0 ? $address->phone : " -"; ?>
					      <?php else: ?> 
					       	No. Telp: -
					      <?php endif; ?> 
			    		</div><!-- .contact -->
			    		<button class="kb-button">Laporkan Pengguna</button>
					</li>
		 		</ul>
	  		</div><!-- .white-bg -->	 
   		</div><!-- .col-lg-3 -->
	    <div class="col-lg-9">
	   	 	<div class="white-bg" style="float: left; width: 100%;">
		  		<br>
		  		<ul id="profile-product" 
		  			class="product-list" style="padding-left: 15px; padding-bottom: 20px; float: left;">
		        </ul>
		        <div style="margin-bottom: 10px; width: 100%" class="text-center">
		     		<img id="load-more-loading" 
		     			 style="margin: 10px 0px 25px 0px;" 
		     			 src="<?php echo image_url(); ?>public/img/load-more.gif" >
		     		<button style="display: none; margin-bottom: 25px;" 
		     				id="load-more" class="kb-button">Lihat Lebih Banyak</button>
		    	</div><!-- .text-center -->	
		  	</div><!-- .white-bg -->   
	    </div><!-- .col-lg-9 -->
 	</div><!-- .row -->
</div><!-- .container -->
