<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
?>
	<div class="container">
		<ol class="breadcrumb">
		  <li><a href="#">Halaman Depan</a></li>
		  <li><a href="#">Akun Saya</a></li>
		  <li class="active">Kotak Masuk</li>
		</ol>
		
		<div class="row">
			<div class="col-lg-3">
				<div class="panel panel-default">
				  <div class="panel-heading panel-heading-kb">
				    <h3 class="panel-title">Menu Saya</h3>
				  </div>
				  <div class="panel-body">
				    <ul id="account-menu">
	                  <?php $menus = account_menu(); ?>
	                  <?php foreach($menus as $menu): ?>  
	                    <li>
	                       <a href="<?= $menu['link']; ?>"><?= $menu['name']; ?></a>
	                    </li>
	                  <?php endforeach; ?>
	               </ul>
				  </div>
				</div>
			</div>
			<div class="col-lg-9">
				<div class="panel panel-default">
				  <div class="panel-heading panel-heading-kb">
				    <h3 class="panel-title">Kotak Masuk</h3>
				  </div>
				  <div class="panel-body">
				    <div class="row">
				      <div class="col-lg-6">
				      	<div class="check-all-wrap">
					        <input style="position: relative; top: 2px;" type="checkbox" name="inbox-choose-all" value="all">
					        Pilih Semua
					    </div>    
				      </div>
				      <div class="col-lg-6">
				      	<button class="kb-button inbox-delete pull-right">
				      		<i class="fa fa-trash">&nbsp;Hapus</i>
				      	</button>
				      </div>
				    </div>
				  	<ul class="inbox-list-parent">
				  		<?php foreach($messages as $message): ?>
					  	  <li class="inbox-list">
					  		
					  		  	<?php 
					  		  	  $id_product = $message->id_product;
					  		  	  $owner = $message->owner;
					  		  	  $receiver = $message->receiver;
					  		  	  
					  		  	  if($message->status == "sell"):
					  		  		$url_chat = create_url_chat_seller($id_product, $owner, $receiver); 
					  		  	  else:
					  		  	  	$url_chat = create_url_chat_buyer($id_product, $owner, $receiver); 
					  		  	  endif;
					  		  	?>

							  	<?php if($message->is_read == 0): ?>
							  		<a href="<?php echo $url_chat; ?>" >
							  	<?php else: ?>
							  		<a href="<?php echo $url_chat; ?>" class="chat-read" >
							  	<?php endif; ?>
							  	
							  	<div class="inbox-table">
								   <div clas="row">
								     <div class="col-lg-4">
								     	<ul class="user-message">
								            <li>
								            	<input type="checkbox" class="inbox-choose" name="inbox-choose" 
								            		   value="<?php echo $message->chatcode; ?>">
								               <img class="seller-photo sm" 
								                 	src="<?php echo get_user_photo($message->img_path); ?>">
								            </li>
								            <li>
								               <div class="user-info">
								                  <div>
								                  	<?php echo $message->firstname . " " . $message->lastname; ?>
								                  </div>
								                  <div style="position: relative; top: 5px;">
								                     <?php echo format_message_time($message->date_upd); ?>
								                  </div>
								               </div>
								            </li>
								         </ul>
								     </div>
								    <div class="col-lg-8" style="padding: 13px 10px;">
								         <div class="row">
								            <div class="col-lg-8">
								               <div class="product-name">
								                  <?php echo $message->product_name; ?>
								               </div>
								               <div class="message">
								                  <i>Klik untuk melihat pesan</i>
								               </div>
								            </div>
								            <div class="col-lg-4">
								               <img class="product-img pull-right" 
								               		src="<?php echo get_product_image($message); ?>">
								            </div>
								         </div>
								      </div>
								   </div>
								</div>   

							  </a>
						  </li>
					 	<?php endforeach; ?>
					</ul>
				    
				  </div>
				</div>
			</div>
		</div>
	</div>
	