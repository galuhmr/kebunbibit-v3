<div  style="padding: 10px; margin-top: 27px;" class="container white-bg">
    <h3 class="kb-title">Konfirmasi Pembayaran</h3>
    <form action="<?php echo base_url() . '/' . kb_index(); ?>submit-payment-confirm" id="payment-confirm-form" class="form-horizontal">
      <div class="form-group">
        <label class="control-label col-sm-2" style="text-align: left;">Kode Order:</label>
        <div class="col-sm-5">
          <input type="text" class="form-control" id="orderno" placeholder="Contoh: MP09872">
            <div id="orderno-empty" style="margin-top: 5px; display: none;" class="error-message">
               <i class="fa fa-times"></i> Kode order wajib diisi
            </div><!-- #orderno-empty -->
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-sm-2" style="text-align: left;">Nama:</label>
        <div class="col-sm-5">
          <input type="text" class="form-control" id="customer-name" placeholder="Contoh: masukkan nama anda">
            <div id="customer-name-empty" style="margin-top: 5px; display: none;" class="error-message">
               <i class="fa fa-times"></i> Nama wajib diisi
            </div><!-- #orderno-empty -->
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-sm-2" style="text-align: left;">Email:</label>
        <div class="col-sm-5">
          <input type="email" class="form-control" id="customer-email" placeholder="Contoh: masukkan email anda">
            <div id="email-empty" style="margin-top: 5px; display: none;" class="error-message">
               <i class="fa fa-times"></i> Email wajib diisi
            </div><!-- #orderno-empty -->
            <div id="email-not-valid" style="margin-top: 5px; display: none;" class="error-message">
               <i class="fa fa-times"></i> Format email belum benar
            </div><!-- #orderno-empty -->
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-sm-2" style="text-align: left;">Bank:</label>
        <div class="col-sm-5"> 
          <select class="form-control" id="payment-confirm-bank" name="payment-confirm-bank">
            <option value="MANDIRI">MANDIRI - PT Kebunbibit Penuh Bunga - 1440015151621</option>
            <option value="BCA">BCA - PT Kebunbibit Penuh Bunga - 0113218611</option> 
            <option value="BRI">BRI - PT Kebunbibit Penuh - 055101000296305</option>   
          </select>
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-sm-2" style="text-align: left;">Tanggal Transfer:</label>
        <div class="col-sm-5"> 
          <input class="datepicker form-control" id="transfer-date" data-date-format="dd MM yyyy"> 
          <div id="transfer-date-empty" style="margin-top: 5px;display: none;" class="error-message">
              <i class="fa fa-times"></i> Tanggal transfer wajib diisi
          </div><!-- #transfer-date-empty -->
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-sm-2" style="text-align: left;">Nominal:</label>
        <div class="col-sm-5"> 
          <input type="text" class="form-control" id="transfer-nominal" placeholder="Contoh: 200065">
          <div id="transfer-nominal-empty" style="margin-top: 5px;display: none;" class="error-message">
              <i class="fa fa-times"></i> Nominal wajib diisi
          </div><!-- #transfer-nominal-empty -->
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-sm-2" style="text-align: left;">Bukti Transfer:</label>
        <div class="col-sm-5"> 
          <input type="file" style="position: relative;top: 7px;" id="transfer-photo" name="transfer-photo" >
          <div id="transfer-photo-wrap"></div>
          <div id="transfer-photo-empty" style="margin-top: 5px;display: none;" class="error-message">
              <i class="fa fa-times"></i> Bukti Transfer wajib di upload
          </div><!-- #transfer-nominal-empty -->
        </div>
      </div>
      <div class="form-group"> 
        <div class="col-sm-offset-2 col-sm-5 text-left">
          <button type="submit" id="submit-confirm" style="background: #7ac144;" class="kb-button">Kirim</button>
          <img id="submit-loading" style="display: none;" src="<?php echo base_url(); ?>public/img/loading-small.gif">
          <span id="orderno-wrong" style="display: none;" class="error-message">
            <i class="fa fa-times"></i> Pastikan kode order pesanan anda sudah benar
          </span>
          <span id="orderno-exist" style="display: none;" class="error-message">
            <i class="fa fa-times"></i> Kode order <span id="order-confirmed"></span> sudah pernah di konfirmasi
          </span>
          <span id="success-submit" style="display: none;" class="success-message">
            <i class="fa fa-check"></i> Konfirmasi pembayaran berhasil dikirim
          </span>
        </div>
      </div>
    </form>
</div>
