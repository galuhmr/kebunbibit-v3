<?php

defined('BASEPATH') OR exit('No direct script access allowed');

?>
<div style="margin-top: 25px;" class="container white-bg">
	<h3>Hasil pencarian <a href="<?php echo base_url();  ?>pencarian?q=<?php echo $q; ?>">"<span id="q"><?php echo $q; ?></span>"</a></h3>
	<ul id="profile-product" class="product-list" style="padding-left: 15px; padding-bottom: 20px; float: left;">
	       
	</ul>
	<div style="margin-bottom: 10px; width: 100%" class="text-center">
	   	<img id="load-more-loading" style="margin: 10px 0px 25px 0px;" src="<?php echo base_url(); ?>public/img/load-more.gif" >
	 	<button style="display: none; margin-bottom: 25px;" id="load-more" class="kb-button">Lihat Lebih Banyak</button>
	</div>
</div>