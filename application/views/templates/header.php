<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en-us">
  <head>
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta charset="UTF-8">
    <title>kebunbibit: Toko tanaman hias online</title>
    <link rel="icon" href="<?php echo image_url(); ?>img/kebunbibit.png">
    <link rel="stylesheet" href="<?php echo base_url(); ?>public/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>public/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>public/css/default.css">
    <!-- Open Sans -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <!-- Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro" rel="stylesheet">
    <script src="<?php echo base_url(); ?>public/js/float-panel.js"></script>
    <style type="text/css"> 
        .green {color:green;}
        pre {border: dashed 1px #dadecb; padding:8px 12px; background-color: #F6F6F6;}
        header {
            display: block;
            background:white;
        }
        img {vertical-align:middle;border:0;}
         
        .img-responsive {
            width:100%;
            height: auto;
            max-width:500px;
        } 
        /* Float Panel : class="float-panel" */
        .float-panel {position:fixed;width:100%;top:0;background:white;}
        .float-panel .content-area {margin:10px auto;}
        .float-panel .logo { position:absolute; left:0px; top:-4px; }
        .float-panel a {text-decoration:none;color:#444;}
        .float-panel .may-hide  {display:inline-block;opacity:1;transform:translate(0px, 0px);transition:all 0.15s ease 0.1s;}
        .float-panel .fa-gg {display:inline-block;font-size:35px; color:#5CE600; vertical-align:middle; transform:translate(-3px, 0px);transition:all 0.15s ease 0.1s;}
        .float-panel .logo-text {display:inline-block;transform:translate(0px, 0px);transition:all 0.15s ease 0.1s;}

        /* Sticky Float Panel: class="float-panel fixed" */
        .fixed {box-shadow:0 2px 6px rgba(0,0,0,0.2);}
        .fixed .may-hide  {opacity:0;transform:translate(0px, -20px);}  
    </style>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker3.css">
  </head>
  <body>
   <header>
  <div class="float-panel" data-top="0" data-scroll="200" style="z-index: 9;">
    <div id="topline">
      <div class="container">
        <div class="row">
          <div class="col-lg-6">
           <ul class="pull-left">
	    	    <li>
	            <i class="fa fa-phone"></i> 0341-599399
	          </li>
	          <li>
	            <i class="fa fa-envelope-o"></i> bantuan@kebunbibit.id
	          </li>
            <li>
              <i class="fa fa-mobile"></i> 08569020300 
            </li>
	         <ul>
          </div>
          <div class="col-lg-6">
          	<ul class="pull-right">
              <?php if(count($results) > 0) { ?>
    	    	    <li>
                  <span>Halo, </span>
    	            <a id="user-account" style="color: #FFC904; text-decoration: none !important;" href="#">
                    <?php echo $results[0]->firstname . " " . $results[0]->lastname; ?> 
                    &nbsp;
                    <i class="fa fa-angle-down"></i>
                  </a>
                  <div style="display: none;" id="user-menu">
                    <ul>
                      <li>
                        <a style="color: #333 !important;" href="<?= base_url() . kb_index() . 'profil/'.encrypt($results[0]->id_customer);?>">Lihat Produk Saya</a>
                      </li>
                      <li>
                        <a style="color: #333 !important;" href="<?php echo base_url() . kb_index();  ?>account/inbox">Kotak Masuk</a>
                      </li>
                      <li>
                        <a style="color: #333 !important;" href="<?php echo base_url() . kb_index(); ?>account">Informasi Akun</a>
                      </li>
                      <li>
                        <hr style="border-color: #c5c5c5;" />
                        <strong>
                          <a id="logout-btn" 
                             style="color: #333 !important;" 
                             href="<?php echo base_url() . kb_index();  ?>customer/logout">Keluar</a>
                        </strong>
                      </li>
                    </ul>
                  </div>
    	          </li>
    	        <?php } else { ?>
                <li>
                  <a id="show-register" href="#form-register">Daftar</a>
                </li>
                <li>
                  <span>|</span>
                </li>
                <li>
                  <a class="show-login" href="#form-login">Masuk</a>
                </li>
              <?php } ?>
            <ul>
          </div> <!-- ./col-lg-6 -->
    	</div> <!-- ./row -->
      </div> <!-- ./container -->
    </div>


 	<div id="header">
 	  <div class="container">
 	    <div class="row">
 		     <div class="col-lg-3">
     			<a href="<?php echo base_url(); ?>">
            <img src="<?php echo base_url(); ?>public/img/logo.jpg">
          </a>
 	      </div>
 	      <div class="col-lg-6">
          <div id="search">
            <input type="text" id="txt_search"  placeholder="Cari tanaman disini ..." onkeyup="showResult(this.value)">
            <button id="btn_search" class="kb-button">Cari</button>
            <div id="livesearch"></div>
          </div>
          <div id="recent-search">
            <ul>
              <?php foreach($top_searches as $top_search): ?>
                <li>
                  <a style="font-size: 12px;" href="<?php echo base_url() . 'pencarian?q=' . fix_top_search($top_search->product_name); ?>"><?php echo fix_top_search($top_search->product_name); ?></a>
                </li>
              <?php endforeach; ?>  
            </ul>
          </div>
 	      </div>
 	      <div class="col-lg-3">
          <ul class="pull-right" id="user-section">
            <li>
              <?php if(!isset($_SESSION["id_customer"])): ?>
                <button class="show-login">
                  <i class="fa fa-plus"></i>
                </button>
              <?php else: ?>
                <button id="show-add-product">
                  <i class="fa fa-plus"></i>
                </button>
              <?php endif; ?>
                
            </li>
						<li class="separator">
						</li>
            <li>
              <?php if(!isset($_SESSION["id_customer"])): ?>
                <a class="show-login" href="#form-login">
                  <i class="fa fa-inbox"></i>
                </a>
              <?php else: ?>
                <a href="<?php echo base_url() . kb_index(); ?>account/inbox">
                  <i class="fa fa-inbox"></i>
                </a>
                <div style="display: none;" id="inbox-badge" class="badge">
                  0
                </div>
              <?php endif; ?>  
            </li>
            <li>
              <?php if(!isset($_SESSION["id_customer"])): ?>
                <button class="show-login">
                  <i class="fa fa-shopping-cart"></i>
                </button>
              <?php else: ?>
                <button id="cart-list-button">
                  <i class="fa fa-shopping-cart"></i>
                </button>
                <div id="cart-badge" class="badge">
                  0
                </div>
              <?php endif; ?>
                
            </li>
          </ul>
 	      </div> <!-- ./col-lg-4 -->
 	    </div> <!-- ./row -->
 	  </div> <!-- ./container -->
 	</div>
  </div> 
  </header>

  <div id="nav">
    <div class="container">
      <ul class="top">
        <li class="top-list">
          <a href="https://kebunbibit.id/#produk-terbaru">Produk Terbaru</a>
        </li>
        <li class="top-list">
          <a href="<?php echo base_url() . kb_index(); ?>cara-order">Cara Order</a>
        </li>
        <li class="top-list">
          <a href="#">Kalender Event</a>
        </li>
        <li class="top-list">
          <a href="<?php echo base_url() . kb_index(); ?>csr">CSR</a>
        </li>
        <li class="top-list">
          <a href="https://friendster.id">Komunitas Berkebun</a>
        </li>
      </ul>
    </div> <!-- ./container -->
  </div>
<script>
function showResult(str) {
  if(str.length > 0){
    $("#livesearch").show();
  } else {
    $("#livesearch").hide();
  }

  var html  = '<ul class="search-list">';
      html += '<li>';
      html += '<i class="not-found">Pencarian sedang berlangsung<i>';
      html += '<img class="img-loading-small pull-right" src="<?php echo image_url(); ?>public/img/loading-small.gif">';
      html += '</li>';
      html += '</ul>';

  $("#livesearch").html(html);

  $.ajax({
    url: "<?php echo base_url() . kb_index(); ?>product/search",
    type: 'POST',
    data: {"product_name": str},
    success: function (data, status) {
      if(data != '')
        $("#livesearch").html(data);
      else
        $("#livesearch").html('');
    },
    error: function (xhr, ajaxOptions, thrownError) {
      console.log(xhr.status);
      console.log(xhr.responseText);
      console.log(thrownError);
    }
  });
}
</script>