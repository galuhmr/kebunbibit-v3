<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
?>
	
  <div style="display: none;" id="kb-modal">
  	
  	<!-- confirmation dialog -->
  	<div style="top: 10em; min-width: 435px; min-height: 120px; padding-top: 20px; display: none;" 
  		 id="confirmation-dialog" class="kb-modal-form">
  		<div class="modal-content-custom">
  			<p id="confirm-question"></p>
		   <button id="yes-confirm" class='kb-button confirm-btn'>Ya</button>
		   <button class='kb-button modal-close confirm-btn'>Tidak</button>
		</div><!-- .modal-content-custom -->
		<div class="modal-loading">
	  		<img src="<?php echo base_url() ?>public/img/loading.gif">
	  		<h5 class="loading-caption">Mohon tunggu sebentar</h5>
	  	</div><!-- .modal-loading -->
  	</div><!-- #confirmation-dialog -->

  	<!-- order info -->
  	<div style="width: 600px; height: 485px; display: none;" id="order-info" class="kb-modal-form">
  		<div class="modal-close">
  		    <i class="fa fa-times-circle"></i>
  		</div>
  		<div class="modal-content-custom">
  			<?php if($results): ?>
	  			<img src="<?php echo base_url(); ?>public/img/logo.jpg">
	  			<br><br>
	  			<?php $user = $results[0]; ?>
	  			<p>
		  			Terima kasih Bapak/Ibu 
		  			<?php echo $user->firstname . " " . $user->lastname; ?>  order 
		  			Anda telah kami terima, detail pesanan telah kami kirimkan 
		  			melalui kotak masuk dan email <span id="order-email"></span>
	  			</p>
	  			<p>
	  				Nomor order : <b id="reference"></b>.
	  			</p>
	  			<div class="col-lg-12">		
					<img style="margin-bottom: 10px;width: 202px;" 
						 class="bank-icon" 
						 src="https://kebunbibit.id/img/terimakasih_order.png">
	  			</div>
	  		<?php endif; ?>
		</div><!-- .modal-content-custom -->
		<div class="modal-loading">
	  		<img src="<?php echo base_url() ?>public/img/loading.gif">
	  		<h5 class="loading-caption">Mohon tunggu sebentar</h5>
	  	</div><!-- .modal-loading -->
  	</div><!-- #order-info -->

  	<!-- data cart -->
  	<div style="width: 600px; display: none;" id="cart-list" class="kb-modal-form">
  		<div class="modal-close">
  		 	<i class="fa fa-times-circle"></i>
  		</div>
  		<div class="modal-content-custom">
  			<div id="cart-data" style=" overflow-y: auto; height: 270px;"></div>
	  		<input type="hidden" class="form-control id_address_delivery">
	  		<input type="hidden" class="form-control cart-address">
			<input type="hidden" class="form-control cart-city">
			<input type="hidden" class="form-control cart-zipcode">
			<div style="margin-top: 30px;" class="row">
	  		  <div class="col-lg-6 text-left">
	  		    <h4>Total: </h4>
	  		  </div>
	  		  <div class="col-lg-6 text-right">
	  		  	<div class="cart-loading">
	 		     <img src="<?php echo base_url(); ?>/public/img/loading-small.gif">
	 		    </div>
	 		    <input type="hidden" id="cart-total-plain" value="">
	  		    <h4 class="cart-content" id="cart-total">0</h4>
	  		  </div><!-- .col-lg-6 -->
	  		</div><!-- .row -->
	  		<div class="text-right">
		  		<button style="width: 140px;" id="pay-button" class="kb-button yellow-bg" type="button">
					BAYAR
				</button>
			</div><!-- .text-right -->	
		</div><!-- .modal-content-custom -->
  	</div><!-- #cart-list -->

  	<!-- informasi cart baru saja ditambahkan -->
  	<div id="cart-added-info" style="display: none;" class="kb-modal-form">
  		<div class="modal-close">
  		 	<i class="fa fa-times-circle"></i>
  		</div>
  		<div class="modal-content-custom">
  		 	<div id="cart-success" class="alert alert-success" style="font-size: 12px;">
	  	 	 	Produk berhasil ditambahkan ke keranjang belanja
		 	</div>
		 	<img id="ac-product-img" style="height: 130px; width:130px;" src="<?php echo base_url() ?>public/img/no-image.png">
		 	<div>
  				<a id="ac-product-name" style="font-size: 16px;" class="product-name" href="#"></a>
  		 	</div>
  		 	<h4 id="ac-product-price"></h4>
  		 	<br>
  		 	<div class="seller-section">
            	<ul>
              		<li>
                		<img class="seller-photo" src="<?php echo base_url() ?>public/img/penjual.png">
              		</li>
              		<li>
                		<a id="ac-product-seller" href="#"></a>
              		</li>
            	</ul>
          	</div>
          <!--<div class="row">
            <div class="col-lg-6">
             <button id="continue-shopping" class="kb-button kb-cart-info-btn pull-left" type="button">
             	Belanja Lagi
             </button>
            </div>
            <div class="col-lg-6">
             <button class="kb-button kb-cart-info-btn pull-right" type="button">
             	Lanjut Pembayaran
             </button>
            </div>
          </div>-->
  		</div>
  		<div class="modal-loading">
	  		<img src="<?php echo base_url() ?>public/img/loading.gif">
	  		<h5 class="loading-caption">Proses sedang berlangsung</h5>
	  	</div><!-- .modal-loading -->
  	</div><!-- #cart-added-info -->

  	<!-- form input produk -->
  	<div style="width: 500px; bottom: 0px; display: none;" id="input-product" class="kb-modal-form">
  		<div class="modal-close">
  			<i class="fa fa-times-circle"></i>
  		</div> 
  		<div id="save-success" class="alert alert-success" style="font-size: 12px;">
	  	  <a style="margin: 3px;" href="#" class="close" data-hide="alert" aria-label="close">
	  	  	<i class="fa fa-times"></i>
	  	  </a>
		  Selamat, produk anda berhasil diposting!
		</div>
  		<div class="failed-verify alert alert-danger" style="font-size: 12px;">
	  	  <a style="margin: 3px;" href="#" class="close" data-hide="alert" aria-label="close">
	  	  	<i class="fa fa-times"></i>
	  	  </a>
		  <div class="failed-caption"></div><!-- .failed-caption -->
		</div><!-- .failed-verify -->
		<div class="modal-content-custom">
	  		<form id="imageform" 
				  action="<?php echo base_url(); ?>index.php/product/save" 
				  method="POST" enctype="multipart/form-data">
		  		  <input id="product-name" type="text" placeholder="Nama Produk">
			  	  <div style="margin: 10px 0px;">
			  		<select id="product-category" style="width: 100%;" class="form-control" name="category"></select>
				  </div>
			  	  <div style="margin-bottom: 6px;">
			  		<input id="product-price" type="text" placeholder="Harga Produk">
			  	  </div>
			  	  <textarea id="product-description" style="width: 100%; height: 100px; outline: none;"></textarea>
			  	  <input style="display: none;" type="file" id="uploaded_file" name="uploaded_file">
				  <div class="image-section">
				  	<input type="hidden" id="images" name="images">
				  	<div id="img-preview">
				  		<div class="li-special">
			        		<div class="img-add">	
			        			<img class="image-size" 
			        			     src="<?php echo base_url(); ?>public/img/add-image.png">
			        		</div><!-- .img-add -->	
						</div><!-- .li-special -->
				  	</div><!-- .img-preview -->
				  </div><!-- .image-section -->
			  	<button id="post-button" class="kb-button yellow-bg" type="button">
			  		Posting
			  	</button>
			</form>
		</div>	
		<div class="modal-loading">
	  		<img src="<?php echo base_url() ?>public/img/loading.gif">
	  		<h5 class="loading-caption">Proses sedang berlangsung</h5>
	  	</div>  	
  	</div>

  	<!-- form login -->
  	<div id="form-login" style="display: none;" class="kb-modal-form">
  		<div class="modal-close">
  			<i class="fa fa-times-circle"></i>
  		</div>
  		<img src="<?php echo base_url(); ?>public/img/logo.jpg">
  		<div class="modal-content-custom">
			<div id="login-display">	
				<h3>Sudah Daftar?</h3>
		  		<div class="failed-verify alert alert-danger" style="font-size: 12px;">
		  		  <a style="margin: 3px;" href="#" class="close" data-hide="alert" aria-label="close">
		  		  	<i class="fa fa-times"></i>
		  		  </a>
				  <div class="failed-caption"></div>
				</div>
		  		<div>
		  			<input id="user-email" type="text" placeholder="Masukan Email">
		  		</div>
		  		<div>
		  			<input id="user-password" type="password" placeholder="Password">
		  		</div>
		  		<button id="login-button" class="kb-button yellow-bg" type="submit">
		  			Masuk
		  		</button>
		  	</div>
		  	<div style="display: none;" id="forget-password-display">	
				<h3>Lupa Password?</h3>
				<div id="success-reset" class="alert alert-success" style="font-size: 12px;">
		  		  <a style="margin: 3px;" href="#" class="close" data-hide="alert" aria-label="close">
		  		  	<i class="fa fa-times"></i>
		  		  </a>
				  <div>Password berhasil direset, silahkan cek email anda.</div>
				</div>
		  		<div class="failed-verify alert alert-danger" style="font-size: 12px;">
		  		  <a style="margin: 3px;" href="#" class="close" data-hide="alert" aria-label="close">
		  		  	<i class="fa fa-times"></i>
		  		  </a>
				  <div class="failed-caption"></div>
				</div>
		  		<div>
		  			<input id="user-email-for-reset" type="text" placeholder="Masukan Email">
		  		</div>
		  		<button id="reset-password" class="kb-button yellow-bg" type="button">
		  			Reset Password
		  		</button>
		  	</div>	
	  		<div class="row">
	  			<div class="col-lg-6">
	  				<a id="to-register" class="pull-left" href="#">Daftar</a>
	  			</div>
	  			<div class="col-lg-6">
	  				<a id="forget-password-link" class="pull-right" href="#">Lupa Password</a>
	  				<a id="not-forget-password-link" style="display: none;" class="pull-right" href="#">Saya Nggak Jadi Lupa</a>
	  			</div>
	  		</div>
	  		<div class="social-media-login">
	  			<span>Masuk dengan: </span>
	  			<ul>
	  			  <li>
	  			  	<button id="login-facebook" class="fb">
	  			  		<i class="fa fa-facebook"></i>
	  			  	</button>
	  			  </li>
	  			  <li>
	  			  	<button id="login-google" class="google">
	  			  		<i class="fa fa-google"></i>
	  			  	</button>
	  			  </li>
	  			</ul>
	  		</div><!-- .social-media-plugin -->
	  	</div><!-- .modal-content-custom -->  	
	  	<div class="modal-loading">
	  		<img src="<?php echo base_url() ?>public/img/loading.gif">
	  		<h5 class="loading-caption">Verifikasi Pengguna</h5>
	  	</div><!-- .modal-loading -->
  	</div><!-- #form-login -->

  	<!-- form pendaftaran -->
  	<div id="form-register" style="display: none;" class="kb-modal-form">
  		<div class="modal-close">
  			<i class="fa fa-times-circle"></i>
  		</div>
  		<div class="modal-content-custom">
	  		<img src="<?php echo base_url(); ?>public/img/logo.jpg">
	  		<h3>Buat Akun</h3>
	  		<div id="register-success" class="alert alert-success" style="font-size: 12px;">
		  	  <a style="margin: 3px;" href="#" class="close" data-hide="alert" aria-label="close">
		  	  	<i class="fa fa-times"></i>
		  	  </a>
			  Silahkan cek email anda untuk mendapatkan password
			</div>
			<div class="failed-verify alert alert-danger" style="font-size: 12px;">
		  	  <a style="margin: 3px;" href="#" class="close" data-hide="alert" aria-label="close">
		  	  	<i class="fa fa-times"></i>
		  	  </a>
			  <div class="failed-caption"></div>
			</div>
	  		<div>
	  			<input id="nc-firstname" type="firstname" placeholder="Nama Depan">
	  		</div>
	  		<div>
	  			<input id="nc-lastname" type="lastname" placeholder="Nama Belakang">
	  		</div>
	  		<div>
	  			<input id="nc-email" type="email" placeholder="Email Anda">
	  		</div>
	  		<button id="register-button" class="kb-button yellow-bg" type="button">
	  			Daftar
	  		</button>
	  		<br>
	  		<br>
	  		<div class="row">
	  			<div class="col-lg-12">
	  				<span>Sudah punya akun? </span>
	  				<a id="to-login" href="#">Login disini</a>
	  			</div><!-- .col-lg-12 -->
	  		</div><!-- .row -->
	  	</div><!-- .modal-content-custom -->
	  	<div class="modal-loading">
	  		<img src="<?php echo base_url() ?>public/img/loading.gif">
	  		<h5 class="loading-caption">Registrasi sedang berlangsung</h5>
	  	</div><!-- .modal-loading -->
  	</div><!-- #form-register -->

  </div><!-- #kb-modal -->

  <div class="reason-list">
		<div class="container-fluid">
			<div class="container">
				<div class="row">
					<div class="box col-lg-3">
						<div class="img-wrap">
							<img src="<?php echo base_url(); ?>public/img/icon1.png">
						</div>
						<h5 class="description">
							Penawaran Harga
						</h5>
						<p>
						        Suka menawar barang sebelum dibeli? Hanya pasar Kebunbibitlah yang memberikan fasilitas tawar menawar, so...tunggu apa lagi, segera pilih produk keinginanmu dan tawar aja!
						</p>
					</div>
					<div class="box col-lg-3">
						<div class="img-wrap">
							<img src="<?php echo base_url(); ?>public/img/icon2.png">
						</div>
						<h5 class="description">
							Keamanan Pembayaran
 						</h5>
						<p>
							Sering kena tipu saat belanja online? Jangan khawatir, karena pasar Kebunbibit menjamin keamanan pembayaran dengan menggunakan Indomaret, MasterCard, Visa, BCA dan Mandiri yang aman dan terpercaya.						</p>
					</div>
					<div class="box col-lg-3">
						<div class="img-wrap">
							<img src="<?php echo base_url(); ?>public/img/icon3.png">
						</div>
						<h5 class="description">
							Kredit Poin
						</h5>
						<p>
							Siapa sih yang nggak suka diskon? Hanya dengan chat, tawar dan aktif bertanya di pasar Kebunbibit, dapat mendapatkan kredit poin yang bisa digunakan untuk berbelanja lebih hemat.													  
						</p>
					</div>
					<div class="box col-lg-3">
						<div class="img-wrap">
							<img 
							src="<?php echo base_url(); ?>public/img/icon4.png">
						</div>
						<h5 class="description">
							Jual Beli dalam 1 Menit
						</h5>
						<p>
							Lupakan cara berjualan di marketplace online yang lama dan ribet! Sudah saatnya berjualan tanaman dengan praktis. Cukup satu menit untuk menampilkan produk semudah update status di Facebook.
						</p>
					</div> <!-- .box -->
				</div> <!-- .row -->
			</div><!-- .container -->
		</div> <!-- .container-fluid -->
	</div> <!-- .reason-list -->

	<div id="footer" style="background: #FFF; " class="container-fluid">
		<div class="container">
			<div class="row">
				<div class="col-lg-3">
					<input id="kebunbibit" type="hidden" value="<?php echo user_login(); ?>">
					<h4>Tentang Kami</h4>
					<p style="text-align: justify;">
					Selamat datang! 
					</p>
					<p style="text-align: justify;">
					Setelah empat tahun berkecimpung di dunia tanaman online, kini hadir pasar Kebunbibit, suatu sistem terintegrasi antara Penjual dan Pembeli yang praktis, aman dan fleksibel.
					</p>
					<p style="text-align: justify;">
					Banyaknya penipuan berbelanja online di akun sosial media dan keamanan yang kurang terjamin serta minimnya kemampuan teknologi yang dimiliki petani alias Penjual adalah alasan kami untuk membuat pasar tanaman online.
					</p>
					<p style="text-align: justify;">
					Dengan banyak tambahan fitur baru, transaksi jual beli akan semakin cepat dan nyaman. Semoga pasar Kebunbibit dapat berguna bagi semua untuk memajukan dunia agribisnis.
					</p>
					<p style="text-align: justify;">
					Salam hangat,
					</p>
					<p style="text-align: justify;">
					Kebunbibit ALL STAR
					</p>
				</div>
				<div class="col-lg-3">
					<h4>Friendster</h4>
					<img style="position: relative;
    							right: 9px;" 
    					 src="<?php echo base_url(); ?>public/img/friendster.png">
				</div>
				<div class="col-lg-3">
					<h4>Twitter</h4>
					<img style="position: relative;
    							right: 13px;" 
    							src="<?php echo base_url(); ?>public/img/twitter.png">
				</div>
				<div class="col-lg-3">
					<h4>Hubungi Kami</h4>
					<ul class="contact-content">
						<li>
							<i class="fa fa-clock-o"></i>
						</li>
						<li>
							<strong>JAM OPERASIONAL 07.00-23.00 WIB</strong>
						</li>
					</ul>	
					<ul class="contact-content">
						<li>
							<i class="fa fa-phone"></i>
						</li>
						<li>
							<strong>LAYANAN PELANGGAN</strong>
							<br>
							0341 - 599399
					  	</li>
					</ul>
					<ul class="contact-content">
						<li>
							<i class="fa fa-mobile"></i>
						</li>
						<li>
							<strong>SMS CENTER</strong>
							<br>
							08569020300
					  	</li>
					</ul>	
					<ul class="contact-content">
						<li>
							<i class="fa fa-mobile"></i>
						</li>
						<li>
							<strong>BAGIAN PENJUALAN</strong>
							<br>
							085231512510 (Simpati)
							<br>
							087859308459 (XL)
							<br>
							085853844384(Indosat)
					  	</li>
					</ul>	
					<ul class="contact-content">
						<li>
							<i class="fa fa-mobile"></i>
						</li>
						<li>
							<strong>PIN BB</strong>
							<br>
							5E0BB686
					  	</li>
					</ul>
					<ul class="contact-content">
						<li>
							<i class="fa fa-print"></i>
						</li>
						<li>
							<strong>FAX</strong>
							<br>
							0341 - 3061399
					  	</li>
					</ul>	
				</div><!-- .col-lg-3 -->
			</div><!-- .row -->
			<div class="row">
				<div class="col-lg-3">
					<h4>Pembeli</h4>
					<ul>
						<li class="footer-list">
							<a href="<?php echo base_url(); ?>bagaimana-cara-membeli">Bagaimana Cara Membeli ?</a>
						</li>
						<li class="footer-list">
							<a href="<?php echo base_url(); ?>bagaimana-mendapatkan-harga-termurah">Bagaimana Mendapatkan Harga Termurah ?</a>
						</li>
						<li class="footer-list">
							<a href="<?php echo base_url(); ?>standar-kualitas-produk">Standar Kualitas Produk</a>
						</li>
						<li class="footer-list">
							<a href="<?php echo base_url(); ?>seputar-pembayaran">Seputar Pembayaran</a>
						</li>
						<li class="footer-list">
							<a href="<?php echo base_url(); ?>konfirmasi-pembayaran">Konfirmasi Pembayaran</a>
						</li>
						<li class="footer-list">
							<a href="<?php echo base_url(); ?>pertanyaan-umum-pembeli">Pertanyaan Umum (Pembeli)</a>
						</li>
					</ul>
				</div>
				<div class="col-lg-3">
					<h4>Penjual</h4>
					<ul>
						<li class="footer-list">
							<a href="<?php echo base_url(); ?>bagaimana-cara-berjualan">Bagaimana Cara Berjualan</a>
						</li>
						<li class="footer-list">
							<a href="<?php echo base_url(); ?>syarat-panduan-produk">Syarat Panduan Produk</a>
						</li>
						<li class="footer-list">
							<a href="<?php echo base_url(); ?>penggunaan-fitur">Penggunaan Fitur</a>
						</li>
						<li class="footer-list">
							<a href="<?php echo base_url(); ?>transaksi-dan-pembayaran">Transaksi dan Pembayaran</a>
						</li>
						<li class="footer-list">
							<a href="<?php echo base_url(); ?>etika-berjualan">Etika Berjualan</a>
						</li>
						<li class="footer-list">
							<a href="<?php echo base_url(); ?>pertanyaan-umum-penjual">Pertanyaan Umum (Penjual)</a>
						</li>
					</ul>
				</div>
				<div class="col-lg-3">
					<h4>Pembayaran</h4>
					<img class="bank" 
						 src="<?php echo base_url(); ?>public/img/bank1.png">
					<img class="bank" 
						 src="<?php echo base_url(); ?>public/img/bank2.png">
					<img class="bank" 
						 src="<?php echo base_url(); ?>public/img/bank3.png">
					<img class="bank" 
						 src="<?php echo base_url(); ?>public/img/bank4.png">
					
				</div>
				<div class="col-lg-3">
					<h4>Dapatkan Aplikasi Kami</h4>
					<div class="icon-playstore"></div>
				</div><!-- .col-lg-3 -->
			</div><!-- .row -->
		</div><!-- .container -->
		<div class="footer-caption">
			Copyright © 2012-2016 PT. Kebunbibit Penuh Bunga. All Rights Reserved
		</div>
	</div>

	<script> 
	  var base_url = "<?php echo base_url(); ?>";
	  var image_url = "<?php echo image_url(); ?>";
	  var kb_index = "<?php echo kb_index(); ?>";
	  var images = [];
	  var migration = -1;
      var minimum_price = 10000;

        // (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        // (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        // m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        // })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        // ga('create', 'UA-55803262-1', 'auto');
        // ga('send', 'pageview'); 

	</script>

	<script src="//apis.google.com/js/platform.js" async defer></script>
 	<script src="//apis.google.com/js/api:client.js"></script>
  	
	<?php 
		$js_files = array(
							"public/js/kb-helper.js", 
							"public/js/jquery.min.js", 
							"public/js/bootstrap.min.js",
							"public/js/modal.js",
							"public/js/login.js", 
							"public/js/product.js",
							"public/js/register.js",
							"public/js/cart.js",
							"public/js/order.js",
							"public/js/product-option.js",
						 	"public/js/kb-global.js",
						 	"public/js/kb-online.js",
						 );
		echo create_source_js($js_files); 
	?>
  	
 	<?php echo isset($extend_js) ? $extend_js : ""; ?>
 	
  </body>
</html>