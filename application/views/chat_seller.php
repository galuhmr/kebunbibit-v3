<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
?>
<style type="text/css">
textarea {
  resize: none; 
}
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;}
.tg .tg-yw4l{vertical-align:top}
</style>

<div style="margin-top: 10px; margin-bottom: 30px;" class="container">
<input type="hidden" id="chatcode" name="chatcode" value="<?php echo $chatcode; ?>">
<input type="hidden" id="chatcode_receiver" name="chatcode_receiver" value="<?php echo $chatcode_receiver; ?>">
<input type="hidden" id="status" name="status" value="sell">
<input type="hidden" id="id_product" name="id_product" value="<?php echo $id_product; ?>">
<input type="hidden" id="owner" name="owner" value="<?php echo $id_owner; ?>">
<input type="hidden" id="receiver" name="receiver" value="<?php echo $id_receiver; ?>">
	<div class="row">
		<div class="col-md-3">
			<ul class="product-list">
				<li style="float: none;">
					<img class="product-img" src="<?php echo get_product_image($product); ?>">
					<div>
						<a class="product-name" href="#"><?php echo $product->name; ?></a>
					</div>
					<h4 style="opacity: 0.3;"><?php echo format_to_rupiah($product->price); ?></h4>  
	      		</li>
	      		<div id="offer-box">  
				<ul style="text-align: center;" id="seller-list">
			      <li style="border: none; margin: 0px; float: none;">
			    	<p>Pembeli</p>
			    		<img class="seller-photo" 
			        		 src="<?php echo get_user_photo($receiver->img_path); ?>">
			        		 
			        <h5>
			          <a href="#"><?php echo $receiver->firstname . ' ' . $receiver->lastname ; ?></a>
			        </h5>
			        <!-- <div style="font-family: 'Source Sans Pro';">
			          <div>
			          	Email:
			          </div>
			          <div>
			          	No. Telp: 082143531162
			          </div>
			        </div>  -->
			      </li>
			    </ul>  
			</div> 
	      	</ul> 
		</div> 
			<div class="col-md-5"> 
	      			<ul class="description-top">
	      				<p style="text-align:justify;">
	      					<?php echo limit_string($product->description, 215); ?> 
	      				</p>
	      			</ul> 
	      			<div id="message-box">
				<div class="message-display" style="height: 431px;">
					<?php if(!$messages): ?>	
						<div class="placeholder">
							Silahkan chat dengan pembeli
						</div>
						<ul>
						  
						</ul>
					<?php else: ?>	
						<ul>
					      <?php foreach($messages as $message): ?> 
						    <?php if($message->status == "in"): ?>	
						       <li class="read-list">
						         <?php 
						            $bgsend = "";

						         	if($message->message_type == 1) {
						         		$bgsend = "message1";
						         	}
						         ?>
						         <div class="read <?php echo $bgsend; ?>">
						          <div class="content">
						          	<?php echo $message->message; ?>
						          </div>
						          <div class="time">
						           <?php echo format_message_time($message->date_add); ?>
						          </div>
						         </div> 
						       </li>
						    <?php else: ?>   
						       <li class="send-list">
						         <?php 
						            $bgsend = "";

						         	if($message->message_type == 1) {
						         		$bgsend = "message1";
						         	} else if($message->message_type == 2) {
						         		$bgsend = "message2";
						         	} else if($message->message_type == 3) {
						         		$bgsend = "message3";
						         	}
						         ?>
						         <div class="send <?php echo $bgsend; ?>">
						          <div class="content">
						          	<?php echo $message->message; ?>
						          </div>
						          <div class="time">
						           <?php echo format_message_time($message->date_add); ?> 
						           - <i class="is_read_sell chat-status-<?php echo $message->id_chat_detail; ?>"><?php echo $message->is_read == 1 ? "Dibaca" : "Diterima"; ?></i>
						          </div>
						         </div>
						       </li>
						    <?php endif; ?>   
						  <?php endforeach; ?>  
					    </ul>
					<?php endif; ?>
				</div>
				<div class="message-send" style="padding: 10px;">
					<?php if($is_bargain_exist): ?>
						<div style="display: none;" class="message-box">
							<table class="tg" style="width: 100%;">
							  <tr>
							    <td style="padding: 2px 5px; width: 340px;">
							    	<input type="text" id="chat-message" style="width: 100%; padding: 8px;" 
							    			  placeholder="Tulis pesan anda disini">
							    </td>
							    <td style="padding: 0px 5px;" class="tg-yw4l">
							    	<button id="btn-send" 
							    			style="position: relative; top: 2px;">
							    			<i class="fa fa-paper-plane"></i></button>
							    </td>
							  </tr>
							</table>
						</div>
						<div class="bargain-receiver">
							<table class="tg" style="width: 100%;">
							  <tr>
							    <td style="padding: 2px 5px; width: 270px;">
							    	<h5 class="caption">Pembeli menawar <?php echo $is_bargain_exist[0]->qty . " qty = " . format_to_rupiah($is_bargain_exist[0]->total); ?> </h5>
							    </td>
							    <td style="padding: 0px 5px;" class="tg-yw4l">
							    	<button id="bargain-accept" 
							    			class="bargain-button accept">
							    			<i class="fa fa-check"></i> Terima</button>
							    	<button id="bargain-reject" 
							    			class="bargain-button reject">
							    			<i class="fa fa-times"></i> Tolak</button>		
							    </td>
							  </tr>
							</table>
						</div>
						<input type="hidden" id="qty-bargain" name="qty-bargain" value="<?php echo $is_bargain_exist[0]->qty; ?>">
						<input type="hidden" id="price-bargain" name="price-bargain" 
							   value="<?php echo $is_bargain_exist[0]->total; ?>" >
						
					<?php else: ?>
						<div class="message-box">
							<table class="tg" style="width: 100%;">
							  <tr>
							    <td style="padding: 2px 5px; width: 340px;">
							    	<input type="text" id="chat-message" style="width: 100%; padding: 8px;" 
							    			  placeholder="Tulis pesan anda disini">
							    </td>
							    <td style="padding: 0px 5px;" class="tg-yw4l">
							    	<button id="btn-send" 
							    			style="position: relative; top: 2px;">
							    			<i class="fa fa-paper-plane"></i></button>
							    </td>
							  </tr>
							</table>
						</div>
						<div style="display: none;" class="bargain-receiver">
							<table class="tg" style="width: 100%;">
							  <tr>
							    <td style="padding: 2px 5px; width: 270px;">
							    	<h5 class="caption"></h5>
							    </td>
							    <td style="padding: 0px 5px;" class="tg-yw4l">
							    	<button id="bargain-accept" 
							    			class="bargain-button accept">
							    			<i class="fa fa-check"></i> Terima</button>
							    	<button id="bargain-reject" 
							    			class="bargain-button reject">
							    			<i class="fa fa-times"></i> Tolak</button>		
							    </td>
							  </tr>
							</table>
						</div>
						<input type="hidden" id="qty-bargain" name="qty-bargain" value="">
						<input type="hidden" id="price-bargain" name="price-bargain" value="" >
					<?php endif; ?>
				</div>
			</div>
			    </div>
			    <div class="col-md-4">
				<div id="produkserupa-box">
				  <h5 class="title">Produk yang Berkaitan</h5> 
				  <ul>
				  <?php foreach ($related_products as $key => $related_product) :
				    $path_img = get_product_image($related_product);
				  ?>
				    <li>
				      <a href='<?= base_url()."$related_product->link_rewrite.html";?>' target='_blank'>	
						<div class="row"> 
						  <div class="col-md-3">	
							<img class="product-display-img" src="<?php echo $path_img; ?>">	
						  </div>	
						  <div class="col-md-9">	
							<div class="product-info">	
								<h5><?= limit_string($related_product->product_name, 15) . 
										" - " . $related_product->firstname; ?></h5>
							  	<h5 class="price"><?= format_to_rupiah($product->price);?></h5>
						    </div>
						   </div>	
						</div>
					  </a>	
					</li>
				  <?php endforeach;?>
				  </ul>	
				</div>
		</div> 
		
	</div>
</div>
 