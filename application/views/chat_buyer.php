<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- <div class="container">
	<iframe class="kb-iframe" style="width: 100%; height: 50em;" src="http://192.168.1.41/chatting/index.php/chat/1234/p/1234/b/12/s/34"></iframe>
 </div>
 <script type="text/javascript">
 	$("textarea").resizable();
 </script> -->
 <style type="text/css">
textarea {
  resize: none; 
}
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;}
.tg .tg-yw4l{vertical-align:top}
</style>
<input type="hidden" id="chatcode" name="chatcode" value="<?php echo $chatcode; ?>">
<input type="hidden" id="chatcode_receiver" name="chatcode_receiver" value="<?php echo $chatcode_receiver; ?>">
<input type="hidden" id="status" name="status" value="buy">
<input type="hidden" id="id_product" name="id_product" value="<?php echo $id_product; ?>">
<input type="hidden" id="owner" name="owner" value="<?php echo $id_owner; ?>">
<input type="hidden" id="receiver" name="receiver" value="<?php echo $id_receiver; ?>">
<div style="margin-top: 10px; margin-bottom: 30px;" class="container">
	<div class="row">
		<div class="col-md-3">
			<ul class="product-list">
				<li style="float: none;">
					<img class="product-img" src="<?php echo get_product_image($product); ?>">
					<div>
						<a class="product-name" href="#">
							<?php echo $product->name; ?></a>
					</div>
					<h4 style="opacity: 0.3;"><?php echo format_to_rupiah($product->price); ?></h4>  
	      		</li>
	      		<div id="offer-box">  
				<ul style="text-align: center;" id="seller-list">
			      <li style="border: none; margin: 0px; float: none;">
			    	<p>Dikirim Oleh</p>
			    		<img class="seller-photo" 
			        		 src="<?php echo get_user_photo($receiver->img_path); ?>">
			        		 
			        <h5>
			          <a href="#">
			          	<?php echo $receiver->firstname . '&nbsp;' . $receiver->lastname ?>
			          </a>
			        </h5>
			       <!--  <div style="font-family: 'Source Sans Pro';">
			          <div>
			          	Email: <?php echo $seller[0]->email; ?>
			          </div>
			          <div>
			          	No. Telp: 082143531162
			          </div>
			        </div>  -->
			      </li>
			    </ul>  
			</div> 
	      	</ul> 
		</div> 
			<div class="col-md-5"> 
	      			<ul class="description-top">
	      				<p style="text-align:justify;">
	      					<?php echo limit_string($product->description, 215); ?> 
	      				</p>
	      			</ul> 
	      			<div id="message-box">
				<div class="message-display">
					<?php if(!$messages): ?>	
						<div class="placeholder">
							Silahkan chat dengan penjual
						</div>
						<ul>
						</ul>
					<?php else: ?>	
						<ul>
					      <?php foreach($messages as $message): ?> 
						    <?php if($message->status == "in"): ?>	
						       <li class="read-list">
						         <?php 
						            $bgsend = "";

						         	if($message->message_type == 1) {
						         		$bgsend = "message1";
						         	} else if($message->message_type == 2) {
						         		$bgsend = "message2";
						         	} else if($message->message_type == 3) {
						         		$bgsend = "message3";
						         	}
						         ?>
						         <div class="read <?php echo $bgsend; ?>">
						          <div class="content">
						           <?php echo $message->message; ?>
						          </div>
						          <div class="time">
						           08 September 2016 09:10
						          </div>
						         </div> 
						       </li>
						    <?php else: ?>   
						       <li class="send-list">
						         <?php 
						            $bgsend = "";

						         	if($message->message_type == 1) {
						         		$bgsend = "message1";
						         	}
						         ?>
						         <div class="send <?php echo $bgsend; ?>">
						          <div class="content">
						          	<?php echo $message->message; ?>
						          </div>
						          <div class="time">
						           08 September 2016 09:08 - <i class="is_read_buy chat-status-<?php echo $message->id_chat_detail; ?>"><?php echo $message->is_read == 1 ? "Dibaca" : "Diterima"; ?></i>
						          </div>
						         </div>
						       </li>
						    <?php endif; ?>   
						  <?php endforeach; ?>  
					    </ul>
					<?php endif; ?>	      
				</div>
				<div class="message-send">
					<table class="tg" style="width: 100%;">
					  <tr>
					    <td style="padding: 2px 5px; width: 340px;">
					    	<input type="text" id="chat-message" style="width: 100%; padding: 8px; border: 0px;" 
					    			  placeholder="Tulis pesan anda disini">
					    </td>
					    <td style="padding: 0px 5px;" class="tg-yw4l">
					    	<button id="btn-send" 
					    			style="position: relative; top: 2px;">
					    			<i class="fa fa-paper-plane"></i></button>
					    </td>
					  </tr>
					</table> 
				</div>
				<div class="message-bargain">
					<table class="tg" style="width: 100%;">
					  <tr>
					    <?php
					        $qty_bargain = 1;
					    	if($is_bargain_exist):
					    		$qty_bargain = $is_bargain_exist[0]->qty;
					    	    $product_price = $is_bargain_exist[0]->total + 0;
					    	endif;	 
					    ?>
					  	<td style="padding: 8px 5px 0px 5px;">
					    	<p align="justify">
					    		<input type="number" id="qty-bargain" maxlength="3" placeholder="qty" 
					    			   value="<?php echo $qty_bargain; ?>" 
					    			   style="width: 5em;" class="tawar" <?php if($is_bargain_exist){ echo "disabled"; } ?> >
					    	</p>
					    </td>
					    <td style="padding: 8px 5px 0px 5px;" class="tg-yw4l">
					    	<input type="hidden" id="for-multiply" value="<?php echo $product->price + 0; ?>">
					    	<input type="number" id="price-bargain" placeholder="Total harga" value="<?php echo $product->price  + 0; ?>" 
					    	class="tawar" <?php if($is_bargain_exist){ echo "disabled"; } ?> >
					    </td>
					    <td style="padding: 8px 5px 0px 5px;" class="tg-yw4l" rowspan="2">
					    	<button style="background: #969696 !important;" 
					    			id="btn-bargain" 
					    				<?php if($is_bargain_exist){ echo "disabled"; } ?> >
					    			<i class="fa fa-angle-double-down"></i>&nbsp;Tawar</button>
					    </td>
					   </tr>
					</table> 
				</div>
			</div>
			    </div>
			    <div class="col-md-4">
				<div id="produkserupa-box">
				  <h5 class="title">Produk yang Berkaitan</h5> 
				  <ul>
				  <?php foreach ($related_products as $related_product) :
				    $path_img = get_product_image($related_product);
				  ?>
				    <li>
				      <a href='<?= base_url()."$related_product->link_rewrite.html";?>' target='_blank'>	
						<div class="row"> 
						  <div class="col-md-3">	
							<img class="product-display-img" src="<?php echo $path_img; ?>">	
						  </div>	
						  <div class="col-md-9">	
							<div class="product-info">	
								<h5>
									<?= $related_product->product_name . 
										" - " . 
										$related_product->firstname; ?>
								</h5>
							  	<h5 class="price"><?= format_to_rupiah($product->price);?></h5>
						    </div>
						   </div>	
						</div>
					  </a>	
					</li>
				  <?php endforeach;?>
				  </ul>	
				</div>
		</div> 
		<div class="col-md-4">
	  <div style="    margin-top: 160px;margin-left: -50px;"> 
	  <ul>
	<div class="alert-box notice"> Mau beli harga murah? Gunakan fitur tawar, caranya gampang! cukup masukkan jumlah dan harga yang kamu inginkan dan KLIK tawar, penjual akan merespon tawaran kamu.</div>
		</ul>	
		</div>
		</div> 
		
	</div>
</div>
	</div>

 