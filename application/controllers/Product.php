<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CI_Controller {

	public function __construct() {
        parent::__construct();

        $this->load->model('category_model', 'category');
        $this->load->model('product_model', 'product');
        $this->load->model('productlang_model', 'productlang');
        $this->load->model('productshop_model', 'productshop');
        $this->load->model('kbmpseller_model', 'seller');
        $this->load->model('kbmpsellerproduct_model', 'sellerproduct');
		$this->load->model('image_model', 'image');
        $this->load->model('imagelang_model', 'imagelang');
        $this->load->model('imageshop_model', 'imageshop');
    }
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index() {}

	public function search() {
		$results = $this->product->find_name_like($_POST['product_name']);
		$is_exist = (count($results) > 0);

		$html = '<ul class="search-list">';

		if($is_exist) {
			foreach ($results as $result) {
				$html .= '<li>';
				$html .= "<a href='" . base_url() . kb_index() .  $result->link_rewrite . ".html'>";
				$html .= '<div class="content">';
				$html .= '<img src="' . get_product_image($result) . '">';
				$html .= '</div>';
				$html .= '<div class="content">';
				$html .= '<strong>' . $result->product_name . '</strong>';
				$html .= '<h5>' . format_to_rupiah($result->product_price) . '</h5>';
				$html .= '</div>';
				$html .= '</a>';
				$html .= '</li>';
			}
		} else {
			$html .= '<li>';
			$html .= '<i class="not-found">Pencarian tidak ditemukan<i>';	
			$html .= '</li>';	
		}	

		$html .= '</ul>';

		echo $html;
	}

	public function search_result() {
		if(isset($_GET['q']) && !empty($_GET['q'])) {
			$data['q'] = $_GET['q'];


			$js_files = array('public/js/search.js');
			$extend_js = create_source_js($js_files);
			
			$data['extend_js'] = $extend_js;
		
			load_view($this, 'search_result', $data);
		}
	}

	public function load_more_search($page) {
		$str = "";

		if($page > 0 && isset($_GET['q'])) {
			$name = $_GET['q'];
			$id_user = user_login();

			$page -= 1;
			$products = $this->product->find_name_like($name, 8, $page);
			
			foreach($products as $product) {
				$str .= show_product_list($product, $id_user);
			}
		}	

		echo $str;
	}

	public function load_more_category($page) {
		$limit = $_POST['limit'];
		$id_category = $_POST['category'];

		if($page > 0) {
			$id_user = user_login();
			
			$page -= 1;
			$offset = $page * $limit;

			$products = $this->product->products_in_category($id_category, $limit, $offset);
			
			if(!$products) {
				return "";
			}
			
			$str = '<ul class="product-list">';

        	foreach($products as $product) {
				$str .= show_product_list($product, $id_user);
			}

			$str .= "</ul>";

		}	

		echo $str;
	}

	public function update_product($id_product){
		$api = array();
			
		try {
			$this->product->update_price($id_product);
			$this->productshop->update_price($id_product);
			$this->productlang->update_description($id_product);
			
			$product = $this->product->find($id_product)[0];
				
			$api['formatted_price'] = format_to_rupiah($product->price);
			$api['price'] = $product->price;
			$api['status'] = 'success';
		} catch (Exception $e) {
			// var_dump($e->getMessage());
			$api['status'] = 'failed';
		}

		echo json_encode($api);
		
	}

	public function create_product_api($id_product) {
		$api = array();
		$data = array();

		$results = $this->product->find($id_product);

		foreach ($results as $result) {
			$result->img_product_path = get_image_path2($result);
		}

		$api['data'] = $data;
		$api['status'] = "failed"; 
		
		if($result) {
			$api['data'] = $results;
			$api['status'] = "success"; 
		}

		echo json_encode($api);
	}

	public function seller_products($id_seller, $page) {
		$str = "";

		if($page > 0) {
			$id_user = user_login();
			
			$page -= 1;
			$products = $this->product->products_in_seller($id_seller, $page);
			
			foreach($products as $product) {
				$str .= show_profile_product_list($product, $product->seller_id, $id_user);
			}

		}	

		echo $str;
	}
	
	public function delete($id_product) {
		$api = array();
		
		try {	
			$this->product->delete($id_product);
		
			$api['status'] = "success";
		} catch (Exception $e) {
			// var_dump($e->getMessage());
			$api['status'] = 'failed'; 
		}

		echo json_encode($api);
	}
	
	public function all() {
		$api = array();
		
		$results = $this->product->all();

		foreach($results as $result){
			$product = array();
			$product['id_product'] = $result->id_product;
			$product['id_customer'] = $result->id_customer;
			$product['firstname'] = $result->firstname;
			$product['lastname'] = $result->lastname;
			$product['name'] = $result->name;
			$product['price'] = format_to_rupiah($result->price);
			$product['description'] = htmlentities($result->description);
			$product['sc_path'] = $result->sc_path; // product image
	
			array_push($api, $product);
		}
		
		echo json_encode($api);
	}

	public function save() {
		$id_user = user_login();
		$images = json_decode($_POST['images']);

		$last_id_product = $this->product->insert_entry();

		$this->productlang->insert_entry($last_id_product);
		$this->productshop->insert_entry($last_id_product);

		$this->sellerproduct->insert_entry($last_id_product, $id_user);

		foreach($images as $key => $image) {
			$last_id_image = $this->image->insert_entry($last_id_product, $key, $image);
			$this->imagelang->insert_entry($last_id_image);
			$this->imageshop->insert_entry($last_id_image, $last_id_product);
		}
	}

	public function create_categories_api() {
		$api = $this->category->get();

		echo json_encode($api);
	}

	public function products_in_category($id_category = 4, $link_rewrite = ''){
		$data['category_title'] = str_replace("-", " ", $link_rewrite);
		$data['categories'] = $this->category->get();
		$data['category_active'] = $id_category;
		
		$js_files = array('public/js/product-category.js');
		$extend_js = create_source_js($js_files);
		
		$data['extend_js'] = $extend_js;

		load_view($this, 'product', $data);
	}

	public function product_detail($link_rewrite) {
		$product = $this->product->find_by_link_rewrite($link_rewrite);

		if($product){
			$product = $product[0];
		} else {
			redirect(base_url(), 'refresh');
		}

		$images = $this->product->get_images_product($link_rewrite);

		$url_deal = "#";
    	$current_user = user_login();
    	$is_login = ($current_user != null); 

	    if($is_login){
	      $url_deal = create_url_chat_buyer($product->product_id, $current_user, $product->seller_id);
	    }
	    
	    $js_files = array('public/js/product-detail.js');
		$extend_js = create_source_js($js_files);
		
		$data['extend_js'] = $extend_js;
		$data['is_mine'] = ($current_user == $product->seller_id);
		$data['is_login'] = $is_login;
		$data['url_deal'] = $url_deal;
		$data['product'] = $product;
		$data['images'] = $images;
        $data['categories'] = $this->category->get(); 

		load_view($this, 'product_detail', $data);
	}


    public function category_api() {
		$api = $this->category->get_categories();

		echo json_encode($api);
	}

	public function product_by_category_api($id_category, $offset) {
		$results = $this->product_model->get_products_by_category($id_category, 8, ($offset-1) * 8);
		
		echo json_encode($results); 
	}

	public function get_products_by_category($id=4, $nama =''){
		$id_customer = isset($_SESSION["id_customer"]) ? $_SESSION["id_customer"] : null;
		$decrypt_id = decrypt($id_customer);
		$model = $this->product_model->get_products_by_category($id,'all');

		$data['results'] = $this->customer->find($decrypt_id);
		$data['products'] = $model;
		$data['id_customer'] = $decrypt_id;
		$data['judul'] = str_replace("-", " ", $nama);
		$data['categories'] = $this->category->get_categories();
		$data['cat_active'] = $id;
		$this->load->view('templates/header', $data);
		$this->load->view('product', $data);
		$this->load->view('templates/footer');
	}

         /**
	 * Jangan dihapus !!
	 * Ini untuk mindah data dari ps_products ke ps_kb_mp_seller_products
	 */

	// public function migration_to_seller_product($i) {
	// 	$api = array();
	// 	$products = $this->product->find_by_offset($i);
	// 	$current_row = $this->product->get_rows();

	// 	if($products) {
	// 		foreach($products as $product) {
	// 			$api['status'] = "continue";

	// 			if(!$this->kbmpsellerproduct->is_exist($product->id_product)) {
	// 				$this->kbmpsellerproduct->insert_entry($product->id_product, '2195');
	// 			}
	// 		}
	// 	} else {
	// 		$api['status'] = "finish";
	// 	}

	// 	echo json_encode($api);
	// }
        

}
