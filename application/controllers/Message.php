<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Message extends CI_Controller {

	public function __construct() {
        parent::__construct();
        
        define("SELL", "sell");
		define("BUY", "buy");
		define("IN", "in");
		define("OUT", "out");

		$this->load->model('mpchat_model', 'chat');
        $this->load->model('mpchatdetail_model', 'chatdetail');
        $this->load->model('mpchatbargain_model', 'chatbargain');
        $this->load->model('product_model', 'product');
        $this->load->model('customer_model', 'customer');
    }
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index() {}

	public function check_is_read() {
		$chatcode = $_POST['chatcode'];
		$results = $this->chatdetail->find_is_read($chatcode);

		echo json_encode($results);
		
	}

	public function find_not_delivered() {
		$api = array();
		$chatcode = $_POST['chatcode'];
		
		$results = $this->chatdetail->find_not_delivered($chatcode);

		// format date_add ke format waktu indonesia
		foreach($results as $result):
			$result->date_add_format = format_message_time($result->date_add);
		endforeach;

		$api['message'] = array();
		$api['status'] = 'null';
		
		if($results) {
			$message = $results[0];
			$api['message'] = $message;
			$api['status'] = 'not null';
			
			// ambil id chat detail terbaru
			$this->chatdetail->change_to_delivered($message->id_chat_detail);
		}

		echo json_encode($api);
	}

	public function save_chat() {
		$api = array();

		try {
			$api['status'] = "success";
		
			$chatcode = $_POST['chatcode'];
			$chatcode_receiver = $_POST['chatcode_receiver'];
			$status = $_POST['status']; // buy : sell

			// chat pengirim
			$chat = $this->chat->find($chatcode); 
			
			// chat penerima
			$chat_receiver = $this->chat->find($chatcode_receiver); 

			// insert jika null
			if(!$chat){
				$this->chat->insert_entry($chatcode, $status, $chatcode_receiver);			
			}

			// insert jika null
			if(!$chat_receiver){
				$owner = $_POST['receiver'];
				$receiver = $_POST['owner'];

				$_POST['owner'] = $owner;
				$_POST['receiver'] = $receiver;
				
				if($status == BUY) {
					$this->chat->insert_entry($chatcode_receiver, SELL, $chatcode);			
				} else {
					$this->chat->insert_entry($chatcode_receiver, BUY, $chatcode);			
				}
			}

			$message_type  = $_POST['message_type'];
			
			/**
	 		 * Message Type:
	 		 * 0 = pesan biasa
	 		 * 1 = tawaran diajukan
	 		 * 2 = tawaran diterima
	 		 * 3 = tawaran ditolak
	 		 *
	 		 */
			switch ($message_type) {
			    case 0:
					$this->chat->update_entry($chatcode);
					$this->chatdetail->insert_entry($chatcode, OUT);
				
					$this->chat->update_entry($chatcode_receiver);
					$this->chatdetail->insert_entry($chatcode_receiver, IN);
			    
			        break;
			    case 1:
			    	$message_buyer = "Anda" . repair_message_bargain($_POST['message']);
					$message_seller = "Pembeli" . repair_message_bargain($_POST['message']);
					
					$_POST['message'] = $message_buyer;
					$this->chat->update_entry($chatcode);
					$this->chatdetail->insert_entry($chatcode, OUT);
					
					$_POST['message'] = $message_seller;
					$this->chat->update_entry($chatcode_receiver);
					$this->chatdetail->insert_entry($chatcode_receiver, IN);
				
			        break;
			    case 2:
			    	$_POST['message'] = "<i>Anda menerima tawaran pembeli</i>";
					$this->chat->update_entry($chatcode);
					$this->chatdetail->insert_entry($chatcode, OUT);
				
					$_POST['message'] = "<i>Selamat, tawaran anda diterima. Silahkan cek keranjang belanja anda.</i>";
					$this->chat->update_entry($chatcode_receiver);
					$this->chatdetail->insert_entry($chatcode_receiver, IN);
			
			        break;
			    case 3:
			    	$_POST['message'] = "<i>Anda menolak tawaran pembeli</i>";
					$this->chat->update_entry($chatcode);
					$this->chatdetail->insert_entry($chatcode, OUT);
				
					$_POST['message'] = "<i>Tawaran anda ditolak</i>";
					$this->chat->update_entry($chatcode_receiver);
					$this->chatdetail->insert_entry($chatcode_receiver, IN);
			
			        break;
			    default:
			        echo "Unrecognized";
			}

			/**
			* 1 = sudah dibaca
			* 0 = belum dibaca
			*
			*/
			$this->chat->change_to_read($chatcode, 1);
			$this->chat->change_to_read($chatcode_receiver, 0);

		} catch (Exception $e) {
			// var_dump($e->getMessage());
			$api['status'] = "failed";
		}	

		echo json_encode($api);
	}

	public function save_bargain() {
		$api = array();
		$api['status'] = "failed";
		
		try {
			$check = $this->chatbargain->find($_POST['chatcode_seller']);

			if(!$check) { // tunggu dulu tawaran diproses, baru tawar lagi 
				$this->chatbargain->insert_entry();
				$api['status'] = "success";
			}
		} catch (Exception $e) {
			var_dump($e->getMessage());
		}

		echo json_encode($api);
	}

	public function accept_bargain() {
		$api = array();
		
		try {
			$this->chatbargain->accept($this->current_bargain());

			$api['status'] = "success";
		} catch (Exception $e) {
			// var_dump($e->getMessage());
			$api['status'] = "failed";
		}

		echo json_encode($api);
	}

	public function reject_bargain() {
		$api = array();
			
		try {	
			$this->chatbargain->reject($this->current_bargain());

			$api['status'] = "success";
		} catch (Exception $e) {
			// var_dump($e->getMessage());
			$api['status'] = "failed";
		}

		echo json_encode($api);
	}

	private function current_bargain() {
		$chatcode_seller = $_POST['chatcode_seller'];
		
		$bargain = $this->chatbargain->find($chatcode_seller)[0];

		return $bargain->id_chat_bargain;	
	}

	public function total_new_inbox() {
		$api = array();
		$id_user = user_login();
		
		$total = count($this->chat->find_not_read($id_user));
		
		$api['total'] = $total;

		echo json_encode($api);
	}
	
	public function change_to_read($chatcode_receiver) {
		$this->chatdetail->change_to_read($chatcode_receiver, 1);
		$this->chat->change_to_read($_POST['chatcode'], 1);
		
		$api = array();
		$api['status'] = "success";

		echo json_encode($api);
	}

	public function find_bargain($chatcode){
		$api = array();
		$result = $this->chatbargain->find($chatcode);
		
		$api['bargain'] = array();
		$api['status'] = "null";
		
		if($result) {
			$bargain = $result[0];
			$bargain->total_format = format_to_rupiah($bargain->total);
			
			$api['bargain'] = $bargain;
			$api['status'] = "not null";
		}

		echo json_encode($api);
	}

	public function chat_buyer($id_chat, $id_product, $id_buyer, $id_seller){
		
		$id_product = decrypt($id_product);
		$id_owner = decrypt($id_buyer);
		$id_receiver = decrypt($id_seller);
		
		$chatcode = decrypt($id_chat) . $id_product;
		$chatcode_receiver = $id_receiver . $id_owner . $id_product;

		$this->chatdetail->change_all_to_delivered($chatcode);

		$product = $this->product->find($id_product);
		$receiver = $this->customer->find($id_receiver);
		
		if(!$product) {
			redirect(base_url(), 'refresh');
		}
		
		// tampilkan yang delivered
		$messages = $this->chatdetail->find($chatcode);
		
		// apakah masih ada tawaran yang belum diproses (ditolak/diterima)
		$is_bargain_exist = $this->chatbargain->find($chatcode_receiver);

		$js_files = array('public/js/message.js');
		$extend_js = create_source_js($js_files);
		
		$data['extend_js'] = $extend_js;
		$data['chatcode'] = $chatcode;
		$data['chatcode_receiver'] = $chatcode_receiver;
		$data['id_owner'] = $id_owner;
		$data['id_receiver'] = $id_receiver;
		$data['id_product'] = $id_product;
		$data['product'] = $product[0];
		$data['receiver'] = $receiver[0];
		$data['messages'] = $messages;
		$data['is_bargain_exist'] = $is_bargain_exist;
		$data['related_products'] = $this->product->get_related_products($product[0], 3);

		load_view($this, 'chat_buyer', $data, TRUE);
	}

	public function chat_seller($id_chat, $id_product, $id_seller, $id_buyer){
		$id_product = decrypt($id_product);
		$id_owner = decrypt($id_seller);
		$id_receiver = decrypt($id_buyer);

		$chatcode = decrypt($id_chat) . $id_product;
		$chatcode_receiver = $id_receiver . $id_owner . $id_product;

		$this->chatdetail->change_all_to_delivered($chatcode);
		
		$product = $this->product->find($id_product);
		$receiver = $this->customer->find($id_receiver);
		
		if(!$product) {
			redirect(base_url(), 'refresh');
		}
		
		// tampilkan yang delivered
		$messages = $this->chatdetail->find($chatcode);

		// apakah masih ada tawaran yang belum diproses (ditolak/diterima)
		$is_bargain_exist = $this->chatbargain->find($chatcode);

		$js_files = array('public/js/message.js');
		$extend_js = create_source_js($js_files);
		
		$data['extend_js'] = $extend_js;
		$data['chatcode'] = $chatcode;
		$data['chatcode_receiver'] = $chatcode_receiver;
		$data['id_owner'] = $id_owner;
		$data['id_receiver'] = $id_receiver;
		$data['id_product'] = $id_product;
		$data['product'] = $product[0];
		$data['receiver'] = $receiver[0];
		$data['messages'] = $messages;
		$data['is_bargain_exist'] = $is_bargain_exist;
		$data['related_products'] = $this->product->get_related_products($product[0], 3);

		load_view($this, 'chat_seller', $data, TRUE);
	}

	public function delete($chatcode) {
		$api = array();
		
		try {
			$this->chat->delete($chatcode);
			$this->chatdetail->delete($chatcode);
			$this->chatbargain->delete($chatcode);

			$api['status'] = "success";
		} catch (Exception $e) {
			// var_dump($e->getMessage());
			$api['status'] = "failed";
		}

		echo json_encode($api);
	}
}