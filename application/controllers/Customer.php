<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customer extends CI_Controller {

	public function __construct() {
        parent::__construct();
        
        $this->load->model('customer_model', 'customer');
        $this->load->model('address_model', 'address');
    }
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index() {}

	public function register() {
		$api = array();

	   	$customer = $this->customer->insert_entry();
	   
	    $this->address->insert_entry($customer['id_customer']);

	    /**
	     * password didapatkan setelah insert entry customer
	     * karena saat register tidak input password
	     * password berisi angka random
	     */
	    $subject = "Selamat Datang di Kebunbibit.id";
	    $body = $this->email_content_new_password($customer['password']);
	    
	    send_email($this, $subject, $body, $_POST['email']);
	   	
	    $api['password'] = "";
	    $api['status'] = 'success';

	    echo json_encode($api);
	}

	public function reset_password() {
		$is_email_exist = $this->customer->check_email();
		$api = array();

		if(!$is_email_exist) {
			$api['status'] = 'failed';
		} else {
			$new_password = $this->customer->reset_password();
			
			$subject = "Password Baru Anda di Kebunbibit.id";
	    	$body = $this->email_content_new_password($new_password);
	    
	    	send_email($this, $subject, $body, $_POST['email']);
	   	
			$api['status'] = 'success';
		}

		echo json_encode($api);
	}
	
	public function facebook_login() {
		$passwd = password_facebook();
		$_POST['password'] = $passwd;
		
		$available = $this->customer->is_available();

		// not available ?
		if(!$available){
			$is_email_exist = $this->customer->check_email();

			// validasi apakah email sudah ada
			if($is_email_exist) {
				$api = array();
				$api['status'] = "exist";

				echo json_encode($api);
			} else {
				$customer = $this->customer->insert_entry($passwd);
			   
			    $this->address->insert_entry($customer['id_customer']);
			    $this->login();
			}
		} else {
			$this->login();
		}
	}
	
	public function google_login() {
		$passwd = password_google();
		$_POST['password'] = $passwd;
		
		$available = $this->customer->is_available();

		if(!$available){
			$is_email_exist = $this->customer->check_email();

			// validasi apakah email sudah ada
			if($is_email_exist) {
				$api = array();
				$api['status'] = "exist";
			
				echo json_encode($api);
			} else {
				$customer = $this->customer->insert_entry($passwd);
			   
			    $this->address->insert_entry($customer['id_customer']);
			    
			    $this->login();
			}
		} else {
			$this->login();
		}
	}
	
	private function email_content_new_password($passwd) {
		$str = "";

		if(isset($_POST['firstname']) && isset($_POST['lastname'])){
			$str = "Hai, " . $_POST['firstname'] . " " . $_POST['lastname'] . 
				   " terima kasih telah mendaftar di kebunbibit.id, " .
				   " password anda adalah: <b>$passwd</b>";
		} else {
			$str = "Password anda berhasil direset, password terbaru anda adalah: <b>$passwd</b>";
		}


		return $str;
	}

	public function update_customer($id) {
		$api = array();

		try {
			if(empty($_POST['old_password'])){
				$this->apply_update_customer($id);
				$api['status'] = "success";
			} else {
				$_POST['password'] = $_POST['old_password'];
				$is_old_password_right = $this->customer->is_available();

				if($is_old_password_right){ // apakah password lama sudah benar			
					$this->apply_update_customer($id);
					$api['status'] = "success";
				} else {
					$api['status'] = "failed";
				}	
			}
		} catch (Exception $e) {
			// var_dump($e->getMessage());
			$api['status'] = "failed";
		}

		echo json_encode($api);		
	}

	private function apply_update_customer($id) {
		$this->look_and_save_address($id); // cek apakah alamat sudah ada, bila tidak ada, save !
		$this->customer->update_customer($id);
	}

	private function look_and_save_address($id_customer) {
		$customer = $this->customer->find($id_customer)[0];
		$is_address_exist = $this->address->find($id_customer);
	
		if(!$is_address_exist) {
			$_POST['firstname'] = $customer->firstname;
			$_POST['lastname'] = $customer->lastname;
			
			$this->address->insert_entry($id_customer);
		}
	}

	public function login() {
		$available = $this->customer->is_available();

		$api = array();
		
		if(!$available){
			$api['results'] = array();
			$api['status'] = "failed";
		} else {
			$api['results'] = $available;
			$api['status'] = "success";

			$id_customer = encrypt($available[0]->id_customer);
		
			$this->session->set_userdata('id_customer', $id_customer);
		}

		echo json_encode($api);
	}

	public function check_email() {
		$is_exist = $this->customer->check_email();

		$api = array();
		
		if($is_exist){
			$api['status'] = "exist";
		} else {
			$api['status'] = "not exist";
		}

		echo json_encode($api);
	}

	public function logout() {
		if (isset($_SESSION["id_customer"])) {
			$_POST['is_online'] = 0;
			$_POST['id_customer'] = user_login();
			
			$this->customer->change_status();
			
			$this->session->unset_userdata('id_customer');

		}

		redirect(base_url(), 'refresh');
	}

	public function get_address(){
		$id_user = user_login();
		$data = $this->customer->get_address($id_user);
		
		echo json_encode($data);
	}

	public function change_status() {
		$api = array();

		$this->customer->change_status();
		
		$api['status'] = 'success';

		echo json_encode($api);
	}
}
