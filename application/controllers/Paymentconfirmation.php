<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Paymentconfirmation extends CI_Controller {

	public function __construct() {
        parent::__construct();

    	$this->load->model('orders_model', 'orders');
        $this->load->model('paymentconfirmation_model', 'paymentconfirmation');
    }
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index() {}

	public function save() {
		$api = array();
		$is_order_exist = $this->orders->find("o.reference", $_POST['reference_number']);
		$is_confirm_exist = $this->paymentconfirmation->find($_POST['reference_number']);

		try {
			if($is_confirm_exist) {
				$api['status'] = "exist";
			} else if($is_order_exist) {
				$this->paymentconfirmation->insert_entry();
			
				$api['status'] = "success";
			} else {
				$api['status'] = "failed";
			}
		} catch (Exception $e) {
			// var_dump($e->getMessage());
			$api['status'] = 'failed'; 
		}	

		echo json_encode($api);
	}

}
