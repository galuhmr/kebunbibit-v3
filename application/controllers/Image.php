<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Image extends CI_Controller {

	public function __construct() {
        parent::__construct();
        
        define("BASE_PATH", getcwd());
		
		$this->load->model('mpchat_model', 'chat');
        $this->load->model('mpchatdetail_model', 'chatdetail');
        $this->load->model('mpchatbargain_model', 'chatbargain');
        $this->load->model('product_model', 'product');
        $this->load->model('customer_model', 'customer');
    }

	private function save_image($file){
		$id_user = user_login();
		$api = array();
		    
		if($file != null) {
			$errors     = array();
		    $maxsize    = 2097152;
		    $acceptable = array(
		        'image/jpeg',
		        'image/jpg',
		        'image/gif',
		        'image/png'
		    );

		    if(($file['size'] >= $maxsize) || ($file["size"] == 0)) {
		        array_push($errors, 'File too large. File must be less than 2 megabytes.');
		    }

		    if((!in_array($file['type'], $acceptable)) && (!empty($file["type"]))) {
		        array_push($errors, 'Invalid file type. Only JPG, GIF and PNG types are accepted.');
		    }

		    $extension = explode(".", $file['name']);

		    $filename = 'img_' . date('Y-m-d-H-s'). '_' . rand() . "_" . $id_user . ".". $extension[1];

		    $is_success = (count($errors) === 0);

		    if($is_success) {
		        move_uploaded_file($file['tmp_name'], BASE_PATH . '/public/uploads/' . $filename);

		        $api['image'] = $filename;
		    	$api['status'] = 'success';
		    } else {
		    	$api['debug'] = $errors;
				$api['status'] = 'failed';
		    }
		} else {
			$api['debug'] = "c";
			
			$api['status'] = 'failed';
		}

		echo json_encode($api);
	}

	public function upload(){
		if(isset($_FILES['file']) && !empty($_FILES['file'])) {
			$file = $_FILES['file'];

			$this->save_image($file);
		} else {
			$api = array();
			$api['debug'] = "a";
			$api['status'] = 'failed';
			
			echo json_encode($api);
		}
	}

	public function delete() {
		  $file = BASE_PATH . '/public/uploads/' . $_POST['image-src'];
		  $api = array();
		  
		  if (!unlink($file)) {
		  	$api['status'] = 'failed';
			
			echo json_encode($api);
		  } else {
			$api['status'] = 'success';

		  	echo json_encode($api);
		  }
	}

}