<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function __construct() {
        parent::__construct();

        $this->load->model('product_model', 'product');
        $this->load->model('mpcartdetail_model', 'cartdetail');
        $this->load->model('customer_model', 'customer');
        $this->load->model('category_model', 'category');
        
    }
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$products_group = array();
		
		$products_newest = $this->product->get_newest();
		
		$cart_key = user_login();

		// sellers in cart
		$sellers_in_cart = $this->cartdetail->get_sellers_in_cart($cart_key);
		$carts = array();

		foreach($sellers_in_cart as $seller) {
			$carts_seller = $this->cartdetail->get_carts_seller($seller->id_seller, $cart_key);
			
			$cart_data = array();
			$cart_data['data'] = $carts_seller;
			$cart_data['total'] = $this->cartdetail->get_total_cart($seller->id_seller, $cart_key);
			
			array_push($carts, $cart_data);
		}

		$sellers = $this->customer->find_random(12);
		
		$data['carts'] = $carts;
		$data['categories'] = $this->category->get(7);
		$data['recommended'] = $this->product->find_random(8);
		$data['best_seller'] = $this->product->find_random(8);
		$data['products_newest'] = $products_newest;
		$data['sellers'] = $sellers;

		foreach($data['categories'] as $category) {
			$sub_data = $this->product->get_products_by_category($category->id_category,4);
			$products_group[$category->id_category] = $sub_data;
		}

		$data['products_group'] = $products_group;
		
		load_view($this, 'welcome_message', $data);
	}


	public function check_html($url) {
		if (preg_match("/.html/i", $url)) {
		    redirect(base_url() . kb_index() . 'product/detail/' . str_replace('.html', '', $url), 'refresh');
		} else {
		    redirect(base_url(), 'refresh');
		}
	}

	public function cara_order() {
		load_view($this, 'cms/cara_order');
	}

	public function csr() {
		load_view($this, 'cms/csr');
	}
	
	public function bagaimana_cara_membeli() {
		load_view($this, 'cms/pembeli/bagaimana_cara_membeli');
	}

	public function bagaimana_mendapatkan_harga_termurah() {
		load_view($this, 'cms/pembeli/bagaimana_mendapatkan_harga_termurah');
	}

	public function standar_kualitas_produk() {
		load_view($this, 'cms/pembeli/standar_kualitas_produk');
	}

	public function seputar_pembayaran() {
		load_view($this, 'cms/pembeli/seputar_pembayaran');
	}

	public function konfirmasi_pembayaran() {
		$js_files = array('public/js/bootstrap-datepicker.min.js', 'public/js/payment-confirm.js');
		$extend_js = create_source_js($js_files);
		
		$data['extend_js'] = $extend_js;
		
		load_view($this, 'konfirmasi_pembayaran', $data);
	}

	public function pertanyaan_umum_pembeli() {
		load_view($this, 'cms/pembeli/pertanyaan_umum');
	}

	/** penjual **/

	public function bagaimana_cara_berjualan() {
		load_view($this, 'cms/penjual/bagaimana_cara_berjualan');
	}

	public function syarat_panduan_produk() {
		load_view($this, 'cms/penjual/syarat_panduan_produk');
	}

	public function penggunaan_fitur() {
		load_view($this, 'cms/penjual/penggunaan_fitur');
	}

	public function transaksi_dan_pembayaran() {
		load_view($this, 'cms/penjual/transaksi_dan_pembayaran');
	}

	public function etika_berjualan() {
		load_view($this, 'cms/penjual/etika_berjualan');
	}

	public function pertanyaan_umum_penjual() {
		load_view($this, 'cms/penjual/pertanyaan_umum');
	}

	public function test() {
	  $file = getcwd() . '/public/uploads/img_2016-09-18-11-50_320670456_2195.jpg';

	  if (!unlink($file)) {
	  	echo ("Error deleting $file");
	  } else {
	  	echo ("Deleted $file");
	  }
	}
}
