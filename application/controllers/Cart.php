<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Cart extends CI_Controller {

	public function __construct() {
        parent::__construct();

        $this->load->model('mpcart_model', 'cart');
        $this->load->model('mpcartdetail_model', 'cartdetail');
    }

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index() {}

	public function cart_data() {
		$cart_key = user_login();

		// penjual di dalam cart
		$sellers = $this->cartdetail->get_sellers_in_cart($cart_key);
		$api = array();

		foreach($sellers as $seller):
			$carts = array();

			// pengelompokan cart berdasarkan penjual
			$carts_seller = $this->cartdetail->get_carts_seller($seller->id_seller, $cart_key);
			
			foreach($carts_seller as $cart_seller):
				$cart_seller->format_price = format_to_rupiah($cart_seller->product_price);
				$cart_seller->img_path = get_product_image($cart_seller);
				
				array_push($carts, $cart_seller);
			endforeach;

			$api_content = array();
			$api_content['carts'] = $carts;
			$api_content['total'] = $this->cartdetail->get_total_cart($seller->id_seller, $cart_key);
			
			array_push($api, $api_content);
		endforeach;

		echo json_encode($api);
	}

	public function count() {
		$api = array();
		$cart_key = user_login();
		
		$api['count']	= $this->cartdetail->get_cart_count($cart_key);
		
		echo json_encode($api);
	}

	public function delete_product($id) {
		$api = array();

		try {
			$this->cartdetail->delete($id);
			
			$api['results'] = 'success';
		} catch (Exception $e) {
			// var_dump($e->getMessage());
		  	$api['results'] = 'failed';
		}

		echo json_encode($api);  
	}

	public function total_cart() {
		$api = array();

		$api['formatted'] = format_to_rupiah($_POST['total']);
		$api['plain'] = $_POST['total'];

		echo json_encode($api);
	}

	public function change_check_status(){
		$api = array();
		
		if(isset($_POST['cart_data'])) {
			$cart_key = user_login();
			$datas = $_POST['cart_data'];

			$cart = $this->cart->user_cart($cart_key);
			
			foreach($datas as $data){
				$cart_id = $cart[0]->cart_id;
				$id_seller = $data[0]; 
				$status_check = $data[1]; // 1 : 0 

				$this->cartdetail->change_check_status($cart_id, $id_seller, $status_check);
			}
		}
		
		$api['status'] = "success";

		echo json_encode($api);
	}

	public function format_price($price){
		$api = array();

		$api['formatted'] = format_to_rupiah($price);

		echo json_encode($api);
	}

	public function add() {
		$api = array();

		try { 
			$cart_key = user_login();
			
			$cart_id = $this->cart->save($cart_key);
			
			$this->cartdetail->insert_entry($cart_id);
			
			$api['status'] = 'success';
		} catch (Exception $e) {
		  	// var_dump($e->getMessage());
		    $api['status'] = 'failed';
		}

		echo json_encode($api);
	}
	
	public function add_from_bargain() {
		$api = array();

		try {
			// id pembeli, bukan id yang login saat ini 
			$cart_key = $_POST['id_buyer'];
			
			$cart_id = $this->cart->save($cart_key);
			
			$this->cartdetail->insert_entry($cart_id);
			
			$api['status'] = 'success';
		} catch (Exception $e) {
		    // var_dump($e->getMessage());
		    $api['status'] = 'failed';
		}

		echo json_encode($api);
	}

}