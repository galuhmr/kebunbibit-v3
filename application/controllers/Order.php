<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order extends CI_Controller {

	public function __construct() {
        parent::__construct();

        $this->load->model('mpcartdetail_model', 'cartdetail');
        $this->load->model('mpcart_model', 'cart');
        $this->load->model('customer_model', 'customer');
        $this->load->model('orders_model', 'orders');
      	$this->load->model('orderdetail_model', 'orderdetail');
        $this->load->model('orderhistory_model', 'orderhistory');
          
    }
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index() {}

	public function save_order(){
		$api = array();

		try {
			$id_buyer = user_login();

			$cart_key = $id_buyer;
			$cart_id = $this->cart->get_cart_id($cart_key);

			$customer = $this->customer->find($id_buyer)[0];

			// id order terakhir
			$id_order = $this->orders->insert_entry($cart_id, $id_buyer);
			
			// cari berdasarkan cart_id
			$cart_details = $this->cartdetail->find('cart_id', $cart_id);
			$save_hist=[];
			foreach($cart_details as $cart_detail) {
				$orderdata = array();
				$orderdata['id_seller'] = $cart_detail->id_seller;
				$orderdata['id_order'] = $id_order;
				$orderdata['order_status'] = 10; //10 = id order baru
				$orderdata['product_id'] = $cart_detail->product_id;
				$orderdata['product_name'] = $cart_detail->product_name;
				$orderdata['product_price'] = $cart_detail->product_price;
				$orderdata['product_qty'] = $cart_detail->qty;
				$orderdata['total_paid'] = $cart_detail->product_price;

				$this->orderdetail->insert_entry($orderdata);

				if(!isset($save_hist[$id_order.$cart_detail->id_seller])){
					$this->orderhistory->insert_entry($orderdata);
					$save_hist[$id_order.$cart_detail->id_seller] = 1;
				}

				$this->cartdetail->delete($cart_detail->cart_detail_id);
			}
			
			$api['email'] = $customer->email;
			$api['reference'] = order_reference($id_order);
			$api['status'] = 'success';

			$subject = "Order " . $api['reference'] . " di Kebunbibit.id";
			$body = $this->email_content($api['reference']);
			
			send_email($this, $subject, $body, $api['email']);
		} catch (Exception $e) {
			// var_dump($e->getMessage());
			$api['status'] = 'failed'; 
		}

		echo json_encode($api);
	}

	public function email_content($reference) {
		$order = $this->orders->find('o.reference', $reference);
		$customer = $this->customer->find($order->id_customer)[0];

		$str = "<html>
				<head>
				</head>
				<body>
					<div style=\"position: relative;
							margin: 0px auto;
							width: 820px;
							text-align: center;
							color: #525252;\">
						<div style='text-align:left;'>
							<img src='https://kebunbibit.id/public/img/logo.jpg'>
						</div>
						<h2 style=\"text-align: left; color: #797979;\">
							Tagihan Pembayaran: $order->reference
						</h2>
						<hr style='border: 0px; border-bottom: 1px solid #bfbfbf;'>
						<p>
							Hai, $order->firstname $order->lastname,
							terima kasih telah berbelanja di kebunbibit.id. Mohon segera lakukan pembayaran sebelum:
							<h4>" . get_time(2) . "</h4>
						</p>
						<div style=\"text-align: center;\">
							Lakukan pembayaran sebesar:
							<h1>" . format_to_rupiah($order->total_paid) . "</h1>
							<i>
							  Perbedaan nilai transfer akan menghambat proses verifikasi
							</i>
							<br>
							<br>
						</div>
						<div style='text-align: center; margin-bottom: 12px;'>
							Berikut adalah penjelasan tagihan pembayaran:
						</div>
						<table style=\"position: relative;
									  margin: 0px auto;
									  width: 500px;
									  border: 1px solid #bfbfbf;
									  color: #525252;\">
						  <tr>
						   <td style='padding: 7px;background-color: #7ac144; color: #FFF;' colspan='3'>
						     Kode Order: $order->reference
						   </td>
						  </tr>
						  <tr>
						    <td style=\"border: 1px solid #bfbfbf;
								 	   padding: 7px;\">
						      Waktu Transaksi
						    </td>
						    <td colspan='2' style=\"border: 1px solid #bfbfbf;
								 	   padding: 7px;\">
						      " . get_time() . "
						    </td>
						  </tr>
						  <tr>
						    <td style=\"border: 1px solid #bfbfbf;
								 	   padding: 7px;\">
						      Pembeli
						    </td>
						    <td colspan='2' style=\"border: 1px solid #bfbfbf;
								 	   padding: 7px;\">
						      $order->firstname $order->lastname
						    </td>
						  </tr>
						  <tr>
						    <td style=\"border: 1px solid #bfbfbf;
								 	   padding: 7px;\">
						      Metode Pembayaran
						    </td>
						    <td colspan='2' style=\"border: 1px solid #bfbfbf;
								 	   padding: 7px;\">
						      Transfer
						    </td>
						  </tr>";
						  
		foreach($this->orders->get_order_detail_seller($order->id_order) as $seller):
			$str .= "    <tr>
						   <td style=\"border: 1px solid #bfbfbf;
								 	   padding: 7px;background-color: #7ac144;\" colspan='3'>

						   </td>
						  </tr>
						  <tr>
						   <td style=\"border: 1px solid #bfbfbf;
								 	   padding: 7px;\" colspan='3'>
						   	Nama Penjual: $seller->firstname $seller->lastname
						   </td>
						  </tr>
						  <tr>
						   <td style=\"border: 1px solid #bfbfbf;
								 	   padding: 7px;\" colspan='3'>
						   	Kurir: JNE
						   </td>
						  </tr>";
			$str .= "	  <tr>
							   <td style=\"border: 1px solid #bfbfbf;
									 	   padding: 7px;\">
							    <b>Nama Produk</b>
							   </td>
							   <td style=\"border: 1px solid #bfbfbf;
									 	   padding: 7px;\">
							    <b>Qty</b>
							   </td>
							   <td style=\"border: 1px solid #bfbfbf;
									 	   padding: 7px;\">
							    <b>Harga</b>
							   </td>
						  </tr>";			  
			foreach($this->orders->get_products_by_seller($seller->id_order, $seller->id_seller) as $product):
				$str .= "	  <tr>
							   <td  style=\"border: 1px solid #bfbfbf;
									 	   padding: 7px;color: #7ac144;
										    text-transform: uppercase;
										    font-weight: bold;
										    font-family: 'Source Sans Pro';\">
							    $product->name
							   </td>
							   <td style=\"border: 1px solid #bfbfbf;
									 	   padding: 7px;\"> 
							    $product->product_quantity
							   </td>
							   <td style=\"border: 1px solid #bfbfbf;
									 	   padding: 7px;\">" . 
							    format_to_rupiah($product->product_price) . "
							   </td>
							  </tr>";
			endforeach;					  
		endforeach;					  

	$str .= "			  <tr>
						   <td style=\"border: 1px solid #bfbfbf;
								 	   padding: 7px;background-color: #7ac144;\" colspan='3'>

						   </td>
						  </tr>
						  <tr>
						   <td style=\"border: 1px solid #bfbfbf;
								 	   padding: 7px;\">
						    Total Harga
						   </td>
						   <td colspan='2' style=\"border: 1px solid #bfbfbf;
								 	   padding: 7px;\">" . 
						    format_to_rupiah($order->total_paid_tax_excl) . "
						   </td>
						  </tr>
						  <tr>
						   <td style=\"border: 1px solid #bfbfbf;
								 	   padding: 7px;\">
						    Diskon
						   </td>
						   <td colspan='2' style=\"border: 1px solid #bfbfbf;
								 	   padding: 7px;\">
						    Rp 0
						   </td>
						  </tr>
						  <tr>
						   <td style=\"border: 1px solid #bfbfbf;
								 	   padding: 7px;\">
						    <b>Total yang harus dibayar</b>
						   </td>
						   <td colspan='2' style='background-color: #7ac144; color: #FFF;padding: 7px;'>" . 
						    format_to_rupiah($order->total_paid) . "
						   </td>
						  </tr>
						</table>
						<p>
						 <b>Alamat tujuan pengiriman</b>
						 <div>$customer->firstname $customer->lastname</div>
						 <div>$customer->address</div>
						 <div>$customer->city</div>
						 <div>No Telp: $customer->phone</div>
						</p>
						<p>
							Pembayaran dapat dilakukan ke salah satu nomor rekening a/n PT Kebunbibit Penuh Bunga:
						</p>
						<ul style=\"list-style-type: none; 
									padding-left: 0px; 
									width: 300px; 
									position: relative;
									margin: 0px auto;\">
							<li style=\"display: block; 
										border-top: 1px solid #bfbfbf;
										text-align: left;
										padding: 15px 0px;\">
								<img style=\"width: 100px;
				    						height: 35px;\" src='https://kebunbibit.id/public/img/bca.png'>
								<div style=\"float: right;
											font-weight: bold;
											margin-top: 10px;\">
									0113218611
								</div>
							</li>
							<li style=\"display: block; 
										border-top: 1px solid #bfbfbf; 
										text-align: left;
										padding: 15px 0px;\">
								<img style=\"width: 100px;
				    						height: 35px;\" src='https://kebunbibit.id/public/img/mandiri.png'>
								<div style=\"float: right;
											font-weight: bold;
											margin-top: 10px;\">
									1440015151621
								</div>
							</li>
							<li style=\"display: block; 
										border-top: 1px solid #bfbfbf;
										border-bottom: 1px solid #bfbfbf; 
										text-align: left;
										padding: 15px 0px;\">
								<img style=\"width: 100px;
				    						height: 35px;\" src='https://kebunbibit.id/public/img/bri.png'>
								<div style=\"float: right;
											font-weight: bold;
											margin-top: 10px;\">
									055101000296305
								</div>
							</li>
						</ul>
						<br>
						<br>
						<div>
							<h4 style='text-align: left; margin: 5px 0px;'>Catatan:</h4>
							<ol style='padding-left: 15px; text-align: left;'>
							  <li>Untuk memudahkan proses verifikasi, mohon mentransfer dengan nominal sesuai dengan tagihan Bapak / ibu, contoh Rp 200.019 jangan dibulatkan menjadi Rp 201.000. </li>
							  <li>Apabila Bapak / Ibu memiliki pertanyaan silakan hubungi kami di 0341-599399.</li>
							  <li>Order ini akan dibatalkan secara otomatis apabila Bapak / Ibu tidak melakukan pembayaran dalam 2 hari.</li>
							</ol>
						</div>
						<p style='text-align: left;'>
						 Bapak / Ibu dapat melihat status terakhir dari order ini di link berikut: <a href='#'>www.kebunbibit.id/riwayat-pembelian</a>
						</p>
					</div>
				</body>
				</html>";

		return $str;		
	}

	public function get_orders_by_seller() {
		$results = $this->orders->get_by_seller($_POST["filter"], user_login(), $_POST["page"]);

		echo json_encode($results);
	}

	public function get_order_detail_seller() {
		$id_customer = isset($_SESSION["id_customer"]) ? decrypt($_SESSION["id_customer"]) : null;
		$order = $this->orders->find('o.reference', $_POST['id_order']);
		$data = [];
		$filter = $_POST['filter'];
		$total_seller = 0;
		$address = $this->customer->get_address($order->id_customer)[0];
		$str = "<table style=\"position: relative;
									  margin: 0px auto;
									  width: 100%;
									  color: #525252;\">
						  <tr>
						   <td style='padding: 7px;background-color: #7ac144; color: #FFF;' colspan='3'>
						     Kode Order: $order->reference
						   </td>
						  </tr>
						  <tr>
						    <td style=\"border: 1px solid #bfbfbf;
								 	   padding: 7px;\">
						      Waktu Transaksi
						    </td>
						    <td colspan='2' style=\"border: 1px solid #bfbfbf;
								 	   padding: 7px;\">
						      " . get_time() . "
						    </td>
						  </tr>";

				$str .= ($filter == "seller" ) ?		  "<tr>
						    <td style=\"border: 1px solid #bfbfbf;
								 	   padding: 7px;\">
						      Pembeli
						    </td>
						    <td colspan='2' style=\"border: 1px solid #bfbfbf;
								 	   padding: 7px;\">
						      $order->firstname $order->lastname
						    </td>
						  </tr>
						  <tr>
						    <td style=\"border: 1px solid #bfbfbf;
								 	   padding: 7px;\">
						      Alamat
						    </td>
						    <td colspan='2' style=\"border: 1px solid #bfbfbf;
								 	   padding: 7px;\">
						      " . $address->alamat. "
						    </td>
						  </tr>
						  <tr>
						    <td style=\"border: 1px solid #bfbfbf;
								 	   padding: 7px;\">
						      No Handphone
						    </td>
						    <td colspan='2' style=\"border: 1px solid #bfbfbf;
								 	   padding: 7px;\">
						      $address->phone_mobile
						    </td>
						  </tr>" : '';
			if($filter=="seller"){
				$str .= 	"<tr>
						    <td style=\"border: 1px solid #bfbfbf;
								 	   padding: 7px;\">
						      Input Nomer Resi
						    </td>
						    <td>
						      <input type='text' class='form-control' style='margin:2px;' placeholder='input no resi' id='txt-tracking-number'>
						    </td>
						    <td style=\"border-right: 1px solid #bfbfbf;padding: 7px;\"><a class='btn btn-success btn-resi' data-id_order='".$order->id_order."' style='text-decoration:none;color:white !important;'> Simpan </a>
						    <span id='success-edit' class='success-message' style='display:none;'>
                      <i class='fa fa-check'></i>Berhasil </span>
						    </td>
						  </tr>";
			}
		foreach($this->orders->get_order_detail_seller($order->id_order) as $seller):
                       
			if($filter == "buyer" || ($filter=="seller" && $seller->id_seller == $id_customer)){
			$str .= "    <tr>
						   <td style=\"padding: 10px;\" colspan='3'>

						   </td>
						  </tr>
						  <tr>
						   <td style=\"border: 1px solid #bfbfbf;
								 	   padding: 7px;color: #7ac144; font-weight: bold;\">
						   	Nama Penjual: $seller->firstname $seller->lastname
						   	</td>
						   	<td style='padding-left:5px;padding-top:5px;padding-right:5px;border: 1px solid #bfbfbf;' colspan='2'>
						   	<label style='margn-left:10px;' class='label label-info label-status'>".$this->orders->get_current_status($order->id_order,$seller->id_seller)."</label>
						   	<p class='pull-right' id='label-tracking-number'>No Resi: ".$this->orders->get_tracking_number($order->id_order,$seller->id_seller)."</p>
						   </td>
						  </tr>";
			foreach($this->orders->get_products_by_seller($seller->id_order, $seller->id_seller) as $product):
				$total_seller += (int)$product->product_price * (int)$product->product_quantity;
				$str .= "	  <tr>
							   <td  style=\"border: 1px solid #bfbfbf;
									 	   padding: 7px;
										    text-transform: capitalize;
										    font-family: 'Source Sans Pro';\">
							    $product->name
							   </td>
							   <td style=\"border: 1px solid #bfbfbf;
									 	   padding: 7px;\"> 
							    $product->product_quantity
							   </td>
							   <td style=\"border: 1px solid #bfbfbf;
									 	   padding: 7px;border-left:0px;\">" . 
							    format_to_rupiah($product->product_price) . "
							   </td>
							  </tr>";
			endforeach;
		}					  
		endforeach;	
	$tampil_total = ($filter == "seller" ) ? format_to_rupiah($total_seller) : format_to_rupiah($order->total_paid) ;
	$str .=				"<tr>
						   <td style=\"padding: 10px;\" colspan='3'>

						   </td>
						  </tr>
						  <tr>
						   <td style=\"border: 1px solid #bfbfbf;
								 	   padding: 7px;\">
						    Total Harga
						   </td>
						   <td colspan='2' style=\"border: 1px solid #bfbfbf;
								 	   padding: 7px;\">" . $tampil_total . "
						   </td>
						  </tr>
						  <tr>
						   <td style=\"border: 1px solid #bfbfbf;
								 	   padding: 7px;\">
						    Diskon
						   </td>
						   <td colspan='2' style=\"border: 1px solid #bfbfbf;
								 	   padding: 7px;\">
						    Rp 0
						   </td>
						  </tr>
						  <tr>
						   <td style=\"border: 1px solid #bfbfbf;
								 	   padding: 7px;\">
						    <b>Total</b>
						   </td>
						   <td colspan='2' style='border: 1px solid #bfbfbf;font-weight:bold;color:#7ac144;padding: 7px;'>" . $tampil_total. "
						   </td>
						  </tr>
						</table>";
		echo ($str);
	}

	public function change_tracking_number(){
		$id_customer = isset($_SESSION["id_customer"]) ? decrypt($_SESSION["id_customer"]) : null;
		
		$this->orders->update_tracking_number($_POST['id_order'], $id_customer, $_POST['tracking_number']);
		$arr = [
			"id_order"=> $_POST['id_order'],
			"order_status"=> 48,
			"id_seller"=> $id_customer
		];
		$this->orderhistory->insert_entry($arr);
	}

	public function orders_admin_api() {
		$orders = $this->orders->orders_admin_api();
		
		foreach($orders as $order):
			$order->total_paid = format_to_rupiah($order->total_paid);
		endforeach;

		$api = $orders;

		echo json_encode($api);
	}

}