<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Account extends CI_Controller {

	public function __construct() {
        parent::__construct();

        $this->load->model('customer_model', 'customer');
        $this->load->model('address_model', 'address');
        $this->load->model('mpchat_model', 'chat');
    }

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index() {	
		$js_files = array('public/js/account.js');
		$extend_js = create_source_js($js_files);
		
		$data['extend_js'] = $extend_js;

		load_view($this, 'account', $data);
	}

	public function profile($id_user) {
		$id_user = decrypt($id_user);
		
		$user = $this->customer->find($id_user)[0];
		$address = $this->address->find($id_user);

		if($address) {
			$address = $address[0];
		}

		$js_files = array('public/js/profil.js');
		$extend_js = create_source_js($js_files);
		
		$data['extend_js'] = $extend_js;
		$data['id_user'] = $id_user;
		$data['user'] = $user;
		$data['address'] = $address;
		
		load_view($this, 'profile', $data);
	}

	public function inbox() {
		$messages = $this->chat->get_inbox(user_login());

		$js_files = array('public/js/inbox.js');
		$extend_js = create_source_js($js_files);
		
		$data['extend_js'] = $extend_js;
		$data['messages'] = $messages;
		
		load_view($this, 'inbox', $data, TRUE);
	}

	public function order(){
		$js_files = array('public/js/transaction.js');
		$extend_js = create_source_js($js_files);
		
		$data['extend_js'] = $extend_js;

		load_view($this, 'orders', $data, TRUE);
	}

}