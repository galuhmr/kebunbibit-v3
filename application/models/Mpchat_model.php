<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Mpchat_model extends CI_Model {
    public $chatcode;
    public $id_product;
    public $last_message;
    public $owner;
    public $is_read;
    public $receiver;
    public $status;
    public $chatcode_receiver;
    public $date_add;
    public $date_upd;

	public function __construct() {
        parent::__construct();
    }

    public function find($chatcode) {
        $this->db->where('chatcode', $chatcode);
        $query = $this->db->get('ps_mp_chat');

        return $query->result();
    }
    
    public function find_not_read($id_user) {
        $sql = "SELECT * FROM ps_mp_chat mc " .
               "JOIN ps_kb_mp_seller_product kmsp ON kmsp.id_product = mc.id_product  " .
               "WHERE owner = '$id_user' AND is_read = '0' ";

        $query = $this->db->query($sql);

        return $query->result();
    }
    
    public function change_to_read($chatcode, $is_read) {
        $data = array( 'is_read' => $is_read );
         
        $this->db->where('chatcode', $chatcode); 
        $this->db->update('ps_mp_chat', $data);
    }

    public function insert_entry($chatcode, $status, $chatcode_receiver){
    	$this->chatcode = $chatcode;
        $this->id_product = $_POST['id_product'];
        $this->last_message = $_POST['message'];
        $this->owner = $_POST['owner'];
        $this->is_read = 0;
        $this->receiver = $_POST['receiver'];
        $this->status = $status;
        $this->chatcode_receiver = $chatcode_receiver;
        $this->date_add = date("Y-m-d H:i:s");
        $this->date_upd = date("Y-m-d H:i:s");

        $this->db->insert('ps_mp_chat', $this);

        return $this->db->insert_id();
    }

    public function update_entry($chatcode) {
        $data = array( 
                        'last_message' => $_POST['message'],
                        'date_upd' => date("Y-m-d H:i:s"),
                     );
         
        $this->db->where('chatcode', $chatcode);
        $this->db->update('ps_mp_chat', $data);
    }

    public function get_inbox($id_user){
        $sql = "SELECT mc.chatcode, mc.is_read, mc.last_message, mc.owner, mc.receiver, 
                c.firstname, c.lastname, c.img_path,  
                p.id_product, pl.name AS product_name, 
                p.price AS product_price, mc.date_upd, mc.status,
                IF (kbmsp.id_seller_product <= 9098, 'yes', 'no') as dir_kb, 
                (SELECT sc_path FROM ps_image pi WHERE id_product = p.id_product ORDER BY position ASC LIMIT 1) AS sc_path, 
                (SELECT id_image FROM ps_image pi WHERE id_product = p.id_product ORDER BY position ASC LIMIT 1) AS id_image 
                FROM ps_mp_chat mc 
                JOIN ps_product p ON p.id_product = mc.id_product 
                JOIN ps_product_lang pl ON p.id_product = pl.id_product 
                JOIN ps_customer c ON c.id_customer = mc.receiver
                JOIN ps_kb_mp_seller_product kbmsp ON kbmsp.id_product = p.id_product 
                WHERE mc.owner = '$id_user' ORDER BY mc.date_upd DESC ";

        $query = $this->db->query($sql);
        
        return $query->result();

    }
    
    public function delete($chatcode){
        $this->db->where('chatcode', $chatcode);
        $this->db->delete('ps_mp_chat');
    }
}