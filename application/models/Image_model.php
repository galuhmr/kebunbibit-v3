<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Image_model extends CI_Model {
	public $id_product;
	public $position;
	public $sc_path;

	public function __construct() {
        parent::__construct();
    }

    public function insert_entry($id_product, $position, $sc_path){
    	$this->id_product = $id_product;
		$this->position = $position;
		$this->sc_path = $sc_path;
	
		$this->db->insert('ps_image', $this);

		return $this->db->insert_id();
    }

    public function delete($id) {
	   $this->db->where('id_image', $id);
	   $this->db->delete('ps_image'); 
	}

}