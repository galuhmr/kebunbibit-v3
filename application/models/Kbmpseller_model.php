<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class KbMpSeller_model extends CI_Model {
	public $id_customer;
	public $approved;
	public $active;
	public $product_limit_wout_approval;
	public $approval_request_limit;
	public $notification_type;
	public $deleted;
	public $date_add;

	public function __construct() {
        parent::__construct();
    }

	public function delete($id) {
	   $this->db->where('id_seller', $id);
	   $this->db->delete('ps_kb_mp_seller'); 
	}

	public function insert_entry($id_customer){
		$retval = 0;

		$this->db->where('id_customer', $id_customer);
		$query = $this->db->get('ps_kb_mp_seller', 1);
		$results = $query->result();

		if(!$results) {
			$this->id_customer = $id_customer;
			$this->approved = 1;
			$this->active = 1;
			$this->product_limit_wout_approval = 1;
			$this->approval_request_limit = 3;
			$this->notification_type = 2;
			$this->deleted = 0;
			$this->date_add = date("Y-m-d H:i:s");

			$this->db->insert('ps_kb_mp_seller', $this);
			$retval = $this->db->insert_id();
		} else {
			$retval = $results[0]->id_seller;
		}

		return $retval;
	}

}