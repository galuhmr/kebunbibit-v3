<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Orders_model extends CI_Model {
	public $id_order;
	public $reference;
	public $id_shop_group;
	public $id_shop;
	public $id_carrier;
	public $id_lang;
	public $id_customer;
	public $id_cart;
	public $id_currency;
	public $id_address_delivery;
	public $id_address_invoice;
	public $current_state;
	public $secure_key;
	public $payment;
	public $conversion_rate;
	public $module;
	public $recyclable;
	public $gift;
	public $mobile_theme;
	public $total_discounts;
	public $total_discounts_tax_incl;
	public $total_paid;
	public $total_paid_tax_incl;
	public $total_paid_tax_excl;
	public $total_paid_real;
	public $total_products;
	public $total_products_wt;
	public $total_shipping;
	public $total_shipping_tax_incl;
	public $total_shipping_tax_excl;
	public $carrier_tax_rate;
	public $total_wrapping;
	public $total_wrapping_tax_incl;
	public $total_wrapping_tax_excl;
	public $round_mode;
	public $round_type;
	public $invoice_number;
	public $delivery_number;
	public $invoice_date;
	public $delivery_date;
	public $valid;
	public $date_add;
	public $date_upd;

	public function __construct() {
        parent::__construct();
    }

    public function get_last_id() {
    	$this->db->order_by('id_order', 'DESC');
    	$query = $this->db->get('ps_orders', 1);

    	if(!$query->result()) {
    		return 1;
    	}

		$res = $query->result();

    	return $res[0]->id_order + 1;
    }

    public function find($field, $reference) {
    	$sql = "SELECT o.id_order, o.reference, c.id_customer, c.firstname, c.lastname, total_paid, 	
    			total_paid_tax_excl
    			FROM ps_orders o 
    			JOIN ps_customer c ON c.id_customer = o.id_customer 
    			WHERE $field = '$reference' LIMIT 1";

    	$query = $this->db->query($sql);
    	$result = $query->result();

    	if(!$result) {
    		return $result;
    	}

    	return $result[0];			
    }

    public function get_order_detail_seller($id_order) {
    	$sql = " SELECT od.id_order, od.id_seller, c.firstname, c.lastname 
    			 FROM ps_order_detail od 
    			 JOIN  ps_customer c ON c.id_customer = od.id_seller 
    			 WHERE id_order = '$id_order' GROUP BY od.id_seller ";

		$query = $this->db->query($sql);
    	$result = $query->result();

    	return $result;
    }

    public function get_products_by_seller($id_order, $id_seller) {
    	$sql = " SELECT od.id_order, od.id_seller, od.product_id, pl.name, od.product_price, od.product_quantity 
    			 FROM ps_order_detail od 
    			 JOIN ps_product_lang pl ON pl.id_product = od.product_id 
    			 WHERE id_order = '$id_order' AND od.id_seller = '$id_seller'";
    	
    	$query = $this->db->query($sql);
    	$result = $query->result();
    	
    	return $result;
    }

    public function insert_entry($id_cart, $id_buyer){
    	$last_id = $this->get_last_id();

    	$this->id_order = $last_id;
    	$this->reference = order_reference($last_id);
		$this->id_shop_group = 1;
		$this->id_shop = 1;
		$this->id_carrier = 10;
		$this->id_lang = 1;
		$this->id_customer = $id_buyer;
		$this->id_cart = $id_cart;
		$this->id_currency = 1;
		$this->id_address_delivery = $_POST['id_address_delivery'];
		$this->id_address_invoice = 1;
		$this->current_state = 10;
		$this->secure_key = "9a50fbe66a62ac296fd6fa6dcc2e2d1a";
		$this->payment = "BCA";
		$this->conversion_rate = 1;
		$this->module = "bca";
		$this->recyclable = 0;
		$this->gift = 0;
		$this->mobile_theme = 0;
		$this->total_discounts = 0;
		$this->total_discounts_tax_incl = 0;
		$this->total_paid = $_POST['total_paid'] + randStr(2);
		$this->total_paid_tax_incl = $_POST['total_paid'] + randStr(2);
		$this->total_paid_tax_excl = $_POST['total_paid'];
		$this->total_paid_real = 0;
		$this->total_products = $_POST['total_paid'];
		$this->total_products_wt = $_POST['total_paid'];
		$this->total_shipping = 0;
		$this->total_shipping_tax_incl = 0;
		$this->total_shipping_tax_excl = 0;
		$this->carrier_tax_rate = 0;
		$this->total_wrapping = 0;
		$this->total_wrapping_tax_incl = 0;
		$this->total_wrapping_tax_excl = 0;
		$this->round_mode = 2;
		$this->round_type = 2;
		$this->invoice_number = 0;
		$this->delivery_number = 0;
		$this->invoice_date = "0000-00-00 00:00:00";
		$this->delivery_date = "0000-00-00 00:00:00";
		$this->valid = 0;
		$this->date_add = date("Y-m-d H:i:s");
		$this->date_upd = date("Y-m-d H:i:s");
	
		$this->db->insert('ps_orders', $this);

		return $this->db->insert_id();
    }

    public function get_by_seller($filter="all", $id, $offset = 0){
 		$limit = 2;
 		$offset = $offset * $limit;
 		
 		if($filter=="seller"){
 			$where = "AND pod.id_seller=$id";
 		} else {
 			$where = "AND po.id_customer=$id";
 		}
    	
    	$sql = "SELECT FORMAT(po.total_paid,0) AS total, po.id_customer, pod.id_seller as id_seller, 
    			po.id_order, po.reference, posl.name as status, 
    			DATE_FORMAT(po.date_add,'%d %b %Y') as date_add 
    			FROM ps_order_detail pod 
    			JOIN ps_orders po ON pod.id_order = po.id_order 
    			JOIN ps_order_state_lang posl ON posl.id_order_state=po.current_state 
    			WHERE posl.id_lang=1 $where GROUP BY po.id_order ORDER BY po.id_order DESC";
    	
    	$query = $this->db->query($sql);
    	$result = $query->result();

    	return $result;		
 	} 

    public function orders_admin_api(){
 		$sql = "SELECT o.id_order, o.reference, cl.name AS country_name, 
 				CONCAT(cu.firstname, ' ', cu.lastname) AS customer_name, 
				o.total_paid, o.payment, osl.name AS order_status, o.date_add AS order_date
				FROM ps_orders o JOIN ps_address a ON a.id_address = o.id_address_delivery 
				JOIN ps_country c ON a.id_country = c.id_country
				JOIN ps_country_lang cl ON cl.id_country = c.id_country
				JOIN ps_customer cu ON cu.id_customer = o.id_customer
				JOIN ps_order_state_lang osl ON osl.id_order_state = o.current_state
				ORDER BY o.date_add DESC LIMIT 1000 ";

		$query = $this->db->query($sql);
		$result = $query->result();

		return $result;		
 	}

 	public function get_current_status($id_order, $id_seller){
 		$return = "";
 		$sql = "SELECT pl.id_order_state, name as status, ph.id_order, ph.id_seller FROM ps_order_history ph JOIN ps_order_state_lang pl ON (ph.id_order_state=pl.id_order_state AND pl.id_lang=1) WHERE id_order=$id_order AND id_seller=$id_seller ORDER BY id_order_history DESC LIMIT 1";
 		$query = $this->db->query($sql);
 		if(count($query->result()) > 0)
 			$return = $query->result()[0]->status;
 		return $return;
 	}

 	public function get_tracking_number($id_order, $id_seller){
 		$return = " - ";
 		$sql = "SELECT tracking_number FROM ps_order_carrier WHERE id_seller=$id_seller AND id_order=$id_order";
 		$query = $this->db->query($sql);
 		if(count($query->result()) > 0)
 			$return = $query->result()[0]->tracking_number;
 		return $return;
 	}

 	public function update_tracking_number($id_order, $id_seller, $tracking_number){
 		if($this->get_tracking_number($id_order, $id_seller) == ""){
			$data = array(
			        'id_order' => $id_order,
			        'id_seller' => $id_seller,
			        'weight' => 1,
			        'tracking_number' => "$tracking_number",
			        'date_add' => "NOW()"
			);
			$this->db->insert('ps_order_carrier', $data);
			$this->db->query("UPDATE ps_orders SET current_state=48 WHERE id_order=$id_order");
 		} else {
	 		$sql = "UPDATE ps_order_carrier SET tracking_number='$tracking_number' , date_add=NOW() WHERE id_seller=$id_seller AND id_order=$id_order";
	 		$query = $this->db->query($sql);
 		}
 	}

}