<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Orderdetail_model extends CI_Model {
	public $id_seller;
	public $id_order;
	public $id_warehouse;
	public $id_shop;
	public $product_id;
	public $product_attribute_id;
	public $product_name;
	public $product_quantity;
	public $product_quantity_in_stock;
	public $product_quantity_refunded;
	public $product_quantity_return;
	public $product_quantity_reinjected;
	public $product_price;
	public $reduction_percent;
	public $reduction_amount;
	public $reduction_amount_tax_incl;
	public $reduction_amount_tax_excl;
	public $group_reduction;
	public $product_quantity_discount;
	public $product_reference;
	public $product_weight;
	public $id_tax_rules_group;
	public $tax_computation_method;
	public $tax_rate;
	public $ecotax;
	public $ecotax_tax_rate;
	public $discount_quantity_applied;
	public $download_nb;
	public $download_deadline;
	public $total_price_tax_incl;
	public $total_price_tax_excl;
	public $unit_price_tax_incl;
	public $unit_price_tax_excl;
	public $total_shipping_price_tax_incl;
	public $total_shipping_price_tax_excl;
	public $purchase_supplier_price;
	public $original_product_price;
	public $original_wholesale_price;

	public function __construct() {
        parent::__construct();
    }

    public function insert_entry($data) {
    	$this->id_seller = $data['id_seller'];
		$this->id_order = $data['id_order'];
		$this->id_warehouse = 0;
		$this->id_shop = 1;
		$this->product_id = $data['product_id'];
		$this->product_attribute_id = 0;
		$this->product_name = $data['product_name'];
		$this->product_quantity = $data['product_qty'];
		$this->product_quantity_in_stock = 1;
		$this->product_quantity_refunded = 0;
		$this->product_quantity_return = 0;
		$this->product_quantity_reinjected = 0;
		$this->product_price = $data['product_price'];
		$this->reduction_percent = 0;
		$this->reduction_amount = 0;
		$this->reduction_amount_tax_incl = 0;
		$this->reduction_amount_tax_excl = 0;
		$this->group_reduction = 0;
		$this->product_quantity_discount = 0;
		$this->product_reference = $data['product_name'];
		$this->product_weight = 0;
		$this->id_tax_rules_group = 0;
		$this->tax_computation_method = 0;
		$this->tax_rate = 0;
		$this->ecotax = 0;
		$this->ecotax_tax_rate = 0;
		$this->discount_quantity_applied = 0;
		$this->download_nb = 0;
		$this->download_deadline = "0000-00-00 00:00:00";
		$this->total_price_tax_incl = $data['total_paid'];
		$this->total_price_tax_excl = $data['total_paid'];
		$this->unit_price_tax_incl = $data['total_paid'];
		$this->unit_price_tax_excl = $data['total_paid'];
		$this->total_shipping_price_tax_incl = 0;
		$this->total_shipping_price_tax_excl = 0;
		$this->purchase_supplier_price = 0;
		$this->original_product_price = $data['total_paid'];
		$this->original_wholesale_price = 0;
    		
		$this->db->insert('ps_order_detail', $this);

		return $this->db->insert_id();
    }

}