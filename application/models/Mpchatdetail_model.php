<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Mpchatdetail_model extends CI_Model {
    public $chatcode;
    public $message;
    public $status;
    public $message_type;
    public $is_read;
    public $is_delivered;
    public $date_add;
    public $date_upd;

	public function __construct() {
        parent::__construct();
    }

    public function find($chatcode) {
        $this->db->order_by('id_chat_detail', 'ASC');
        $this->db->where('chatcode', $chatcode);
        $this->db->where('is_delivered', '1');
        $query = $this->db->get('ps_mp_chat_detail');

        return $query->result();
    }

    public function find_not_delivered($chatcode) {
        $this->db->order_by('date_add', 'ASC');
        $this->db->where('chatcode', $chatcode);
        $this->db->where('is_delivered', '0');
        $query = $this->db->get('ps_mp_chat_detail', 1);

        return $query->result();
    }

    public function find_is_read($chatcode) {
        $this->db->order_by('date_add', 'DESC');
        $this->db->where('status', "out");
        $this->db->where('chatcode', $chatcode);
        $this->db->where('is_read', '1');
        
        $query = $this->db->get('ps_mp_chat_detail');

        return $query->result();
    }

    public function insert_entry($chatcode, $status) {
        $this->chatcode = $chatcode;
        $this->message = $_POST['message'];
        $this->status = $status;
        $this->message_type = $_POST['message_type'];
        $this->is_read = 0;
        $this->is_delivered = 0;
        $this->date_add = date("Y-m-d H:i:s");
        $this->date_upd = date("Y-m-d H:i:s");

        $this->db->insert('ps_mp_chat_detail', $this);   
    }

    public function change_to_read($chatcode, $is_read) {
        $data = array( 'is_read' => $is_read );
         
        $this->db->where('chatcode', $chatcode);
        $this->db->update('ps_mp_chat_detail', $data);
    }    

    public function change_to_delivered($id_chat_detail) {
        $data = array( 'is_delivered' => '1' );
         
        $this->db->where('id_chat_detail', $id_chat_detail);
        $this->db->update('ps_mp_chat_detail', $data);
    }

    public function change_all_to_delivered($chatcode) {
        $data = array( 'is_delivered' => '1' );
         
        $this->db->where('chatcode', $chatcode);
        $this->db->update('ps_mp_chat_detail', $data);
    }

    public function delete($chatcode){
        $this->db->where('chatcode', $chatcode);
        $this->db->delete('ps_mp_chat_detail');
    }

}