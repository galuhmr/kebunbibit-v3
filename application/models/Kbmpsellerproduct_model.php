<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class KbMpSellerProduct_model extends CI_Model {
	public $id_seller;
	public $id_shop;
	public $id_product;
	public $approved;
	public $deleted;
	public $date_add;
	public $date_upd;

	public function __construct() {
        parent::__construct();
    }

	public function delete($id) {
	   $this->db->where('id_product', $id);
	   $this->db->delete('ps_kb_mp_seller_product'); 
	}

	public function insert_entry($id_product, $id_seller){
		$this->id_seller = $id_seller;
		$this->id_shop = 1;
		$this->id_product = $id_product;
		$this->approved = '0';
		$this->deleted = '0';
		$this->date_add = date("Y-m-d H:i:s");
		$this->date_upd = date("Y-m-d H:i:s");

		$this->db->insert('ps_kb_mp_seller_product', $this);
	}

}