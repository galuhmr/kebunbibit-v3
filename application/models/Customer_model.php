<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Customer_model extends CI_Model {
    public $id_shop_group;
    public $id_shop;
    public $id_gender;
    public $id_default_group;
    public $id_risk;
    public $firstname;
    public $lastname;
    public $email;
    public $passwd;
    public $last_passwd_gen;
    public $newsletter;
    public $optin;
    public $outstanding_allow_amount;
    public $show_public_prices;
    public $max_payment_days;
    public $secure_key;
    public $active;
    public $is_guest;
    public $deleted;
    public $date_add;
    public $date_upd;
    public $img_path;
    
    public function __construct() {
        parent::__construct();
    }

    public function is_available(){
    	$email = $_POST['email'];
    	$password = $_POST['password'];
        $filters = array('email' => $email, 'passwd' => hash_password($password));
        
    	$this->db->where($filters);

		$query = $this->db->get('ps_customer', 1);
	    
        return $query->result();
    }

    public function change_status() {
        $data = array( 
                      'is_online' => $_POST['is_online']
                     );

        if($_POST['is_online'] == 1) {
            $is_online = $this->is_online();

            if(!$is_online) { // update jika is_online masih 0
                $this->db->where('id_customer', $_POST['id_customer']);
                $this->db->update('ps_customer', $data);
            }
        } else {
            $this->db->where('id_customer', $_POST['id_customer']);
            $this->db->update('ps_customer', $data);
        }

    }

    private function is_online() {
        $this->db->where('id_customer', $_POST['id_customer']);
        $this->db->where('is_online', 1);
        $query = $this->db->get('ps_customer', 1);

        return $query->result();
    }

    public function check_email() {
        $email = $_POST['email'];
        
        $array = array('email' => $email);
        $this->db->where($array);

        $query = $this->db->get('ps_customer', 1);
        
        return $query->result();
    }

    public function insert_entry($passwd = null) {
        if($passwd == null) {
            $passwd = randStr(6);
        }

        $hash_password = hash_password($passwd);

        $this->id_shop_group = 1;
        $this->id_shop = 1;
        $this->id_gender = 0;
        $this->id_default_group = 3;
        $this->id_risk = 0;
        $this->firstname = $_POST['firstname'];
        $this->lastname = $_POST['lastname'];
        $this->email = $_POST['email'];
        $this->passwd = $hash_password;
        $this->last_passwd_gen = date("Y-m-d H:i:s");
        $this->newsletter = 0;
        $this->optin = 0;
        $this->outstanding_allow_amount = 0;
        $this->show_public_prices = 0;
        $this->max_payment_days = 0;
        $this->secure_key = "f4e90654b056cc3f097e572551f2caf2";
        $this->active = 1;
        $this->is_guest = 0;
        $this->deleted = 0;
        $this->date_add = date("Y-m-d H:i:s");
        $this->date_upd = date("Y-m-d H:i:s");
	    $this->img_path = isset($_POST['img_path']) ? $_POST['img_path'] : "";
	
        $this->db->insert('ps_customer', $this);

        $retval = array(); 
        $retval['password'] = $passwd;
        $retval['id_customer'] = $this->db->insert_id();

        return $retval;
    }

    public function find($id){
        $sql = "SELECT pc.*, address1 as address, city, phone FROM ps_customer pc 
                LEFT JOIN ps_address pa ON pc.id_customer = pa.id_customer 
                WHERE pc.id_customer = '$id' 
                ORDER BY pc.date_add DESC 
                LIMIT 1 ";

        $query = $this->db->query($sql);        
	    
        return $query->result();
    }

    public function reset_password() {
        $passwd = randStr(6);
        
        $hash_password = hash_password($passwd);
        
        $data = array( 
                      'passwd' => $hash_password
                     );

        $this->db->where('email', $_POST['email']);
        $this->db->update('ps_customer', $data);

        return $passwd;
    }

    public function update_customer($id_customer){
        $data = array( 
                      'firstname' => $_POST['firstname'],
                      'lastname' => $_POST['lastname'],
                      'email' => $_POST['email'],
                      'img_path' => $_POST['img-path'],
                      'date_upd' => date("Y-m-d H:i:s")
                     );

        $old_password = $_POST['old_password'];
        $new_password = $_POST['new_password'];
        $current_password = $_POST['current_password'];
        
        if(!empty($old_password) && !empty($new_password) && !empty($current_password)) {
            $data['passwd'] = hash_password($new_password); 
        }

        $this->db->where('id_customer', $id_customer);
        $this->db->update('ps_customer', $data);

        $this->update_customer_address($id_customer);
    }

    private function update_customer_address($id_customer){
        $data = array( 
                           'address1' => $_POST['address'],
                           'city' => $_POST['city'],
                           'phone' => $_POST['phone'],
                           'date_upd' => date("Y-m-d H:i:s")
                     );
             
        $this->db->where('id_customer', $id_customer);
        $this->db->update('ps_address', $data);
          
    }

    public function find_random($limit = 7){
        $sql = " SELECT
        	        pc.id_customer,
                    pc.firstname,
                    pc.lastname,
                    pc.img_path
                 FROM
                    ps_customer pc
                 JOIN ps_kb_mp_seller_product kbmsp ON kbmsp.id_seller = pc.id_customer
                 GROUP BY
                    kbmsp.id_seller
                 ORDER BY RAND() LIMIT $limit ";
        
        $query = $this->db->query($sql);
   
        return $query->result();
    }

    public function get_address($id_user){
        $sql = "SELECT
                    a.`id_address`,
                    a.`alias`,
                    a.`firstname` AS `firstname`,
                    a.`lastname` AS `lastname`,
                    `address1` AS address,
                    `postcode`,
                    `city`,
                    cl.`id_country` AS `country`,
                    cl.`name` AS negara,
                    a.id_customer,
                    a.phone,
                    a.phone_mobile,
                    CONCAT(address1, ' ', postcode, ' ', cl.name) as alamat
                FROM
                    `ps_address` a
                LEFT JOIN `ps_country_lang` cl ON (
                    cl.`id_country` = a.`id_country`
                    AND cl.`id_lang` = 1
                )
                LEFT JOIN `ps_customer` c ON a.id_customer = c.id_customer
                WHERE
                c.id_customer != 0
                AND c.id_customer = '$id_user'
                AND c.id_shop IN (1)
                AND a.`deleted` = 0
                ORDER BY a.`id_address` ASC LIMIT 1 ";
        
        $query = $this->db->query($sql);
        
        return $query->result();
    }
}