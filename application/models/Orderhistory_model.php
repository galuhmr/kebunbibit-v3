<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Orderhistory_model extends CI_Model {
	public $id_employee;
	public $id_order;
	public $id_order_state;
	public $id_seller;
	public $date_add;
	public $deleted;

	public function __construct() {
        parent::__construct();
    }

    public function insert_entry($data){
    	$this->id_employee = 0;
		$this->id_order = $data['id_order'];
		$this->id_order_state = $data['order_status'];
		$this->id_seller = $data['id_seller'];
		$this->date_add = date("Y-m-d H:i:s");
		$this->deleted = 0;
		
		$this->db->insert('ps_order_history', $this);

		return $this->db->insert_id();
    }

}