<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Category_model extends CI_Model {

	public function __construct() {
        parent::__construct();
    }

    public function get($limit='all'){
    	$where = "";
    	if($limit == "all"){
    		$limit = " ";
                $where = "AND id_parent = 3";
    	} else {
    		$limit = "limit $limit";
    		$where = "AND b.id_category in (21,14,4,16,22,15,95)";
    	}

    	$sql = "SELECT
					a.`id_category`,
					`name`,
					`description`,
					sa.`position` AS `position`,
					`active`,
					sa.position position,
					link_rewrite
				FROM
					`ps_category` a
				LEFT JOIN `ps_category_lang` b ON (
					b.`id_category` = a.`id_category`
					AND b.`id_lang` = 1
					AND b.`id_shop` = 1
				)
				LEFT JOIN `ps_category_shop` sa ON (
					a.`id_category` = sa.`id_category`
					AND sa.id_shop = 1
				)
				WHERE
					1
                                $where
				AND a.active=1
				OR (b.id_category IN (21))
				ORDER BY
					a.id_category = 269 ASC,
					NAME ASC
				$limit";
				
    	$query = $this->db->query($sql);
        
        return $query->result();
    }

}