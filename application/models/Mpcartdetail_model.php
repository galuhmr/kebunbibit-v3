<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Mpcartdetail_model extends CI_Model {
    public $cart_id;
    public $id_seller;
    public $product_id;
    public $product_name;
    public $product_price;
    public $qty;
    
    public function __construct() {
        parent::__construct();
    }
    
    public function get_sellers_in_cart($cart_key) {
        $sql  = "SELECT DISTINCT id_seller, is_check FROM ps_mp_cart_detail mcd ";
        $sql .= "JOIN ps_mp_cart mc ON mc.cart_id = mcd.cart_id ";
        $sql .= "WHERE cart_key = '$cart_key' ";

        $query = $this->db->query($sql);

        return $query->result();
    }

    public function change_check_status($cart_id, $id_seller, $is_check) {
        $data['is_check'] = $is_check;
         
        $this->db->where('cart_id', $cart_id);
        $this->db->where('id_seller', $id_seller);
        
        $this->db->update('ps_mp_cart_detail', $data);
    }

    public function delete($id){
        $this->db->where('cart_detail_id', $id);
        $this->db->delete('ps_mp_cart_detail');
    }

    // cart produk berdasarkan penjual
    public function get_carts_seller($id_seller, $cart_key) {
        $sql  = " SELECT ";
        $sql .= "    mcd.*, firstname, lastname, ( ";
        $sql .= "        SELECT ";
        $sql .= "            sc_path ";
        $sql .= "        FROM ";
        $sql .= "            ps_image pi ";
        $sql .= "        WHERE ";
        $sql .= "            id_product = product_id ";
        $sql .= "        ORDER BY ";
        $sql .= "            position ASC ";
        $sql .= "        LIMIT 1 ";
        $sql .= "    ) AS sc_path, ";
        $sql .= " (SELECT id_image FROM ps_image pi WHERE id_product = kbmsp.id_product 
                  ORDER BY position 
                  ASC LIMIT 1) AS id_image, ";
        $sql .= " IF (kbmsp.id_seller_product <= 9098, 'yes', 'no') as dir_kb ";          
        $sql .= " FROM ";
        $sql .= " ps_mp_cart_detail mcd ";
        $sql .= " JOIN ps_customer pc ON pc.id_customer = mcd.id_seller ";
        $sql .= " JOIN ps_mp_cart mc ON mc.cart_id = mcd.cart_id ";
        $sql .= " JOIN ps_kb_mp_seller_product kbmsp ON kbmsp.id_product = mcd.product_id ";
        $sql .= " WHERE ";
        $sql .= " mcd.id_seller = '$id_seller' ";
        $sql .= " AND mc.cart_key = '$cart_key' ";

        $query = $this->db->query($sql);

        return $query->result();
    }

    public function get_cart_count($cart_key){
        $sql = "SELECT COUNT(*) AS rows FROM ps_mp_cart_detail mcd
                JOIN ps_kb_mp_seller_product kbmsp ON kbmsp.id_product = mcd.product_id 
                JOIN ps_customer pc ON pc.id_customer = mcd.id_seller 
                JOIN ps_mp_cart mc ON mc.cart_id = mcd.cart_id WHERE mc.cart_key = '$cart_key'";
        
        $query = $this->db->query($sql);
        $result = $query->result();

        return $result[0]->rows;
    }

    public function get_total_cart($id_seller, $cart_key){
        $sql = "SELECT SUM(mcd.product_price) AS total FROM ps_mp_cart_detail mcd
                JOIN ps_mp_cart mc ON mc.cart_id = mcd.cart_id
                WHERE id_seller = '$id_seller' AND cart_key = '$cart_key'";

        $query = $this->db->query($sql);

        $result = $query->result();

        return $result[0]->total;
    }

    public function find($field, $cart_id) {
        $this->db->where($field, $cart_id);
        $this->db->where('is_check', '1');

        $query = $this->db->get('ps_mp_cart_detail');

        return $query->result();
    }

    public function insert_entry($cart_id){
        $this->db->where('cart_id', $cart_id);
        $this->db->where('id_seller', $_POST['id_seller']);
        $this->db->where('product_id', $_POST['product_id']);
        
        $query = $this->db->get('ps_mp_cart_detail');

        if(!$query->result()) {   
            $this->cart_id = $cart_id;
            $this->id_seller = $_POST['id_seller'];
            $this->product_id = $_POST['product_id'];
            $this->product_name = $_POST['product_name'];
            $this->product_price = $_POST['product_price'];
            $this->qty = isset($_POST['qty']) ? $_POST['qty'] : 1;
            $this->is_check = 1;

            $this->db->insert('ps_mp_cart_detail', $this);        
        } else {
            $this->db->where('cart_id', $cart_id);
            $this->db->where('id_seller', $_POST['id_seller']);
            $this->db->where('product_id', $_POST['product_id']);
            
            $this->db->set('qty', 'qty+1', FALSE);
            $this->db->set('product_price', 'product_price+' . $_POST['product_price'], FALSE);

            $this->db->update('ps_mp_cart_detail');
        }
    }

}