<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Mpchatbargain_model extends CI_Model {
	public $chatcode_seller;
    public $id_product;
    public $qty;
    public $total;
    public $buyer;
    public $is_accept;
    public $is_reject;
    public $date_add;
    public $date_upd;

	public function __construct() {
        parent::__construct();
    }

    public function insert_entry() {
        $this->chatcode_seller = $_POST['chatcode_seller'];
        $this->id_product = $_POST['id_product'];
        $this->qty = $_POST['qty_bargain'];
        $this->total = $_POST['total_bargain'];
        $this->buyer = $_POST['buyer'];
        $this->is_accept = 2;
        $this->is_reject = 2;
        $this->date_add = date("Y-m-d H:i:s");
        $this->date_upd = date("Y-m-d H:i:s");

        $this->db->insert('ps_mp_chat_bargain', $this);
    }

    public function reject($id_chat_bargain) {
        $data = array( 
                        'is_accept' => 0,
                        'is_reject' => 1,
                     );
         
        $this->db->where('id_chat_bargain', $id_chat_bargain);
        $this->db->update('ps_mp_chat_bargain', $data);
    }

    public function accept($id_chat_bargain) {
        $data = array( 
                        'is_accept' => 1,
                        'is_reject' => 0,
                     );
         
        $this->db->where('id_chat_bargain', $id_chat_bargain);
        $this->db->update('ps_mp_chat_bargain', $data);
    }

	public function find($chatcode_seller){
        $this->db->order_by('date_add', 'DESC');
        $this->db->where('chatcode_seller', $chatcode_seller);
        $this->db->where('is_accept', '2');
        $this->db->where('is_reject', '2');
        $query = $this->db->get('ps_mp_chat_bargain', 1);

        return $query->result();
    }

    public function delete($chatcode_seller){
        $this->db->where('chatcode_seller', $chatcode_seller);
        $this->db->delete('ps_mp_chat_bargain');
    }
}