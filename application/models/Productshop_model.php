<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Productshop_model extends CI_Model {
	
	public $id_product;
	public $id_shop;
	public $id_category_default;
	public $id_tax_rules_group;
	public $on_sale;
	public $online_only;
	public $ecotax;
	public $minimal_quantity;
	public $max_quantity;
	public $price;
	public $wholesale_price;
	public $unity;
	public $unit_price_ratio;
	public $additional_shipping_cost;
	public $customizable;
	public $uploadable_files;
	public $text_fields;
	public $active;
	public $redirect_type;
	public $id_product_redirected;
	public $available_for_order;
	public $available_date;
	public $condition;
	public $show_price;
	public $indexed;
	public $visibility;
	public $cache_default_attribute;
	public $advanced_stock_management;
	public $date_add;
	public $date_upd;
	public $pack_stock_type;
	
	public function __construct() {
        parent::__construct();
    }

	public function update_price($id_product) {
        $data = array( 
                        'price' => $_POST['price'],
                        'wholesale_price' => $_POST['price']
                     );
         
        $this->db->where('id_product', $id_product);
        $this->db->update('ps_product_shop', $data);
    }

	public function insert_entry($id_product) {
		$this->id_product = $id_product;
		$this->id_shop = 1;
		$this->id_category_default = $_POST['id_category'];
		$this->id_tax_rules_group = 0;
		$this->on_sale = 0;
		$this->online_only = 0;
		$this->ecotax = 0;
		$this->minimal_quantity = 1;
		$this->max_quantity = 0;
		$this->price = $_POST['price'];
		$this->wholesale_price = $_POST['price'];
		$this->unity = null;
		$this->unit_price_ratio = 0;
		$this->additional_shipping_cost = 0;
		$this->customizable = 0;
		$this->uploadable_files = 0;
		$this->text_fields = 0;
		$this->active = 1;
		$this->redirect_type = 404;
		$this->id_product_redirected = 0;
		$this->available_for_order = 1;
		$this->available_date = "0000-00-00";
		$this->condition = "new";
		$this->show_price = 1;
		$this->indexed = 1;
		$this->visibility = "both";
		$this->cache_default_attribute = 0;
		$this->advanced_stock_management = 0;
		$this->date_add = date("Y-m-d H:i:s");
		$this->date_upd = "0000-00-00 00:00:00";
		$this->pack_stock_type = 3;

		$this->db->insert('ps_product_shop', $this);

	}

}