<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Paymentconfirmation_model extends CI_Model {
    public $reference_number;
    public $customer_name;
    public $email;
    public $attachment;
    public $date_transfer;
    public $bank;
    public $nominal;

	public function __construct() {
        parent::__construct();
    }

    public function insert_entry() {
        $this->reference_number =  $_POST['reference_number'];
        $this->customer_name = $_POST['customer_name'];
        $this->email = $_POST['email'];
        $this->attachment = $_POST['transfer_photo'];
        $this->date_transfer =  $_POST['date_transfer'];
        $this->bank =  $_POST['bank'];
        $this->nominal =  $_POST['nominal'];
        $this->date_add = date("Y-m-d H:i:s");

        $this->db->insert('ps_payment_confirmation', $this);
    }

    public function find($reference_number) {
        $this->db->where('reference_number', $reference_number);
        $query = $this->db->get('ps_payment_confirmation', 1);    
        
        return $query->result();
    }

}