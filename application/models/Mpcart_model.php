<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Mpcart_model extends CI_Model {
    public $cart_key;
    public $total;
    public $date_add;
    public $date_upd;

	public function __construct() {
        parent::__construct();
    }

    public function user_cart($cart_key){
        $this->db->where('cart_key', $cart_key);
        $query = $this->db->get('ps_mp_cart', 1);

        return $query->result();
    }

    public function save($cart_key){
    	$retval = array();

        $this->db->where('cart_key', $cart_key);
        $query = $this->db->get('ps_mp_cart', 1);
	    
        $results = $query->result();

        if(!$results){
            $this->cart_key = $cart_key;
            $this->total = $_POST['total'];
            $this->date_add = date("Y-m-d H:i:s");
            $this->date_upd = date("Y-m-d H:i:s");

            $this->db->insert('ps_mp_cart', $this);

            return $this->db->insert_id();        
        } else {
            $this->update_entry($cart_key, $_POST['total']);
        }

        return $results[0]->cart_id;

    }

    public function update_entry($cart_key, $total) {
        $data['date_upd'] = date("Y-m-d H:i:s");

        $this->db->where('cart_key', $cart_key);
        $this->db->set('total', 'total+' . $total, FALSE);
        $this->db->update('ps_mp_cart', $data);
    }

    public function get_cart_id($cart_key) {
        $this->db->where('cart_key', $cart_key);
 
        $query = $this->db->get('ps_mp_cart');

        $res = $query->result();

        return $res[0]->cart_id;
    }

    public function delete($cart_id){
        $tables = array('ps_mp_cart', 'ps_mp_cart_detail');
        $this->db->where('cart_id', $cart_id);
        $this->db->delete($tables);
    }

}