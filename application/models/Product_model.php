<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Product_model extends CI_Model {
    public $id_supplier;
    public $id_shop_default;
    public $id_tax_rules_group;
    public $on_sale;
    public $ecotax;
    public $quantity;
    public $minimal_quantity;
    public $max_quantity;
    public $price;
    public $wholesale_price;
    public $unit_price_ratio;
    public $additional_shipping_cost;
    public $width;
    public $height;
    public $depth;
    public $weight;
    public $out_of_stock;
    public $customizable;
    public $uploadable_files;
    public $text_fields;
    public $active;
    public $redirect_type;
    public $id_product_redirected;
    public $available_for_order;
    public $available_date;
    public $condition;
    public $show_price;
    public $indexed;
    public $visibility;
    public $cache_is_pack;
    public $cache_has_attachments;
    public $is_virtual;
    public $date_add;
    public $date_upd;
    public $advanced_stock_management;

	public function __construct() {
        parent::__construct();
    }

    public function get_results(){
    	$sql   = "SELECT pp.id_product, pc.id_customer, pc.firstname, pc.lastname, name, price, description, "; 
        $sql  .= "(SELECT sc_path FROM ps_image pi "; 
        $sql  .= "WHERE id_product = pp.id_product ORDER BY position ASC LIMIT 1) AS sc_path ";
        $sql  .= "FROM ps_product pp JOIN ps_product_lang pl ON pl.id_product = pp.id_product ";
        $sql  .= "JOIN ps_kb_mp_seller_product pkmsp ON pkmsp.id_product = pp.id_product ";
        $sql  .= "JOIN ps_kb _mp_seller pkms ON pkmsp.id_seller = pkms.id_seller ";
        $sql  .= "JOIN ps_customer pc ON pc.id_customer = pkms.id_customer ";
        $sql  .= "WHERE (SELECT id_image FROM ps_image pi ";
        $sql  .= "WHERE id_product = pp.id_product ORDER BY position ASC LIMIT 1) IS NOT NULL ";
        $sql  .= "ORDER BY pp.date_add DESC "; 
        $sql  .= "LIMIT 8 OFFSET 0";

        $query = $this->db->query($sql);

        return $query->result();
    }

    public function get_top_search() {
        $sql = "SELECT od.product_id, LOWER(pl.name) AS product_name, count(od.product_id) AS count
                FROM ps_order_detail od 
                JOIN ps_product p ON od.product_id = p.id_product
                JOIN ps_product_lang pl on p.id_product = pl.id_product
                WHERE od.product_id <> 87 
                GROUP BY od.product_id
                ORDER BY count(od.product_id) DESC LIMIT 5 ";

        $query = $this->db->query($sql);

        return $query->result();     
    }

    public function products_in_seller($id_seller, $page){
        $limit = 6;
        $offset = $page * $limit;

        $sql = "SELECT
                    kbmsp.id_product as product_id, 
                   IF (kbmsp.date_add < '" . KB_DATE_ADD . "', 'yes', 'no') as dir_kb,
                    kbmsp.id_seller as seller_id, 
                    (SELECT sc_path FROM ps_image pi WHERE id_product = kbmsp.id_product ORDER BY position ASC LIMIT 1) AS sc_path, 
                    (SELECT id_image FROM ps_image pi WHERE id_product = kbmsp.id_product ORDER BY position ASC LIMIT 1) AS id_image, 
                    ppl.name as product_name, 
                    pps.price as product_price,
                    ppl.description_short as description,  
                    ppl.link_rewrite, 
                    pc.firstname, 
                    pc.lastname, 
                    pc.img_path 
                FROM 
                    ps_kb_mp_seller_product kbmsp JOIN ps_product pp ON kbmsp.id_product = pp.id_product 
                    JOIN ps_product_lang ppl ON ppl.id_product = pp.id_product 
                    JOIN ps_product_shop pps ON pps.id_product = pp.id_product 
                    JOIN ps_customer pc ON pc.id_customer = kbmsp.id_seller 
                WHERE
                    kbmsp.deleted != '1' AND pp.active=1 AND kbmsp.id_seller = $id_seller
                ORDER BY kbmsp.date_add DESC
                LIMIT $limit OFFSET $offset ";

        $query = $this->db->query($sql);

        return $query->result();       
    }

    public function get_newest(){ // product terbaru
        $sql = "SELECT
                    kbmsp.id_product as product_id,
                    IF (kbmsp.date_add < '" . KB_DATE_ADD . "', 'yes', 'no') as dir_kb, 
                    kbmsp.id_seller as seller_id,
                    (SELECT sc_path FROM ps_image pi WHERE id_product = kbmsp.id_product ORDER BY position ASC LIMIT 1) AS sc_path,
                    (SELECT id_image FROM ps_image pi WHERE id_product = kbmsp.id_product ORDER BY position ASC LIMIT 1) AS id_image,
                    ppl.name as product_name,
                    pps.price as product_price,
                    ppl.description_short AS description,
                    ppl.link_rewrite,
                    pc.firstname,
                    pc.lastname,
                    pc.img_path
                FROM
                    ps_kb_mp_seller_product kbmsp JOIN ps_product pp ON kbmsp.id_product = pp.id_product
                    JOIN ps_product_lang ppl ON ppl.id_product = pp.id_product
                    JOIN ps_product_shop pps ON pps.id_product = pp.id_product
                    JOIN ps_customer pc ON pc.id_customer = kbmsp.id_seller
                WHERE
                    kbmsp.deleted != '1' AND pp.active=1
                ORDER BY kbmsp.id_seller_product DESC
                LIMIT 8";
        $query = $query = $this->db->query($sql);
        return $query->result();
    }
    
    public function delete($id_product) {
        $data = array( 
                        'active' => '0',
                     );
         
        $this->db->where('id_product', $id_product);
        $this->db->update('ps_product', $data);
    }

    public function find_name_like($product_name, $limit = 6, $offset = 0) {
        if(isset($product_name) AND $product_name != "" AND $product_name != NULL) {
            $page = $offset * $limit;
            $sql   = "SELECT
                    kbmsp.id_product as product_id,
                    IF (kbmsp.date_add < '" . KB_DATE_ADD . "', 'yes', 'no') as dir_kb, 
                    kbmsp.id_seller as seller_id,
                    (SELECT sc_path FROM ps_image pi WHERE id_product = kbmsp.id_product ORDER BY position ASC LIMIT 1) AS sc_path,
                    (SELECT id_image FROM ps_image pi WHERE id_product = kbmsp.id_product ORDER BY position ASC LIMIT 1) AS id_image,
                    ppl.name as product_name,
                    pps.price as product_price,
                    ppl.description_short as description, 
                    pc.firstname,
                    pc.lastname,
                    ppl.link_rewrite,
                    pc.img_path
                FROM
                    ps_kb_mp_seller_product kbmsp JOIN ps_product pp ON kbmsp.id_product = pp.id_product
                    JOIN ps_product_lang ppl ON ppl.id_product = pp.id_product
                    JOIN ps_product_shop pps ON pps.id_product = pp.id_product
                    JOIN ps_customer pc ON pc.id_customer = kbmsp.id_seller
                WHERE
                    kbmsp.deleted != '1' 
                AND 
                    pp.active=1
                AND ppl.name like '%$product_name%'
                ORDER BY ppl.name ASC
                LIMIT $limit OFFSET $page";

            $query = $query = $this->db->query($sql);
            return $query->result();
        }
        
        return array();
    }

    public function products_in_category($id_category, $limit = 8, $offset = 0) {
        if($limit == 'all'){
            $limit = " ";
        } else {
            $limit = "LIMIT $limit OFFSET $offset";
        }

        $sql   = "SELECT pp.id_product AS product_id, pc.id_customer AS seller_id, 
                pc.firstname, pc.lastname, pc.img_path, pl.name AS product_name,
                pl.link_rewrite, 
                pps.price AS product_price, description_short AS description,
                IF (pkmsp.id_seller_product <= 9098, 'yes', 'no') AS dir_kb,
                    (SELECT sc_path FROM ps_image pi
                WHERE id_product = pp.id_product ORDER BY position ASC LIMIT 1) AS sc_path,
                (SELECT id_image FROM ps_image pi
                WHERE id_product = pp.id_product ORDER BY position ASC LIMIT 1) AS id_image  
                FROM ps_category pcg JOIN ps_product pp ON pp.id_category_default = pcg.id_category
                JOIN ps_product_lang pl ON pl.id_product = pp.id_product 
                JOIN ps_product_shop pps ON pps.id_product = pp.id_product 
                JOIN ps_kb_mp_seller_product pkmsp ON pkmsp.id_product = pp.id_product 
                JOIN ps_customer pc ON pc.id_customer = pkmsp.id_seller 
                WHERE (SELECT id_image FROM ps_image pi 
                WHERE id_product = pp.id_product ORDER BY position ASC LIMIT 1) IS NOT NULL 
                AND pkmsp.deleted != '1'
                AND pp.active=1
                AND ( pp.id_category_default = '$id_category' OR pcg.id_category = '$id_category')
                group by pp.id_product
                ORDER BY pp.date_add DESC  
                $limit ";

                // OR pp.id_product IN (SELECT id_product FROM ps_category_product WHERE id_category=$id_category)
                
        $query = $this->db->query($sql);

        return $query->result();
    }
    
    public function get_products_by_category($id_category, $limit = 8, $offset = 0) {
        if($limit == 'all'){
            $limit = " ";
        } else {
            $limit = "LIMIT $limit OFFSET $offset";
        }
        $sql   = "SELECT pp.id_product AS product_id, pc.id_customer AS seller_id, 
                pc.firstname, pc.lastname, pc.img_path, pl.name AS product_name,
                pl.link_rewrite, 
                pps.price AS product_price, description_short AS description,
                IF (pkmsp.id_seller_product <= 9098, 'yes', 'no') AS dir_kb,
                    (SELECT sc_path FROM ps_image pi
                WHERE id_product = pp.id_product ORDER BY position ASC LIMIT 1) AS sc_path,
                (SELECT id_image FROM ps_image pi
                WHERE id_product = pp.id_product ORDER BY position ASC LIMIT 1) AS id_image  
                FROM ps_category pcg JOIN ps_product pp ON pp.id_category_default = pcg.id_category
                JOIN ps_product_lang pl ON pl.id_product = pp.id_product 
                JOIN ps_product_shop pps ON pps.id_product = pp.id_product 
                JOIN ps_kb_mp_seller_product pkmsp ON pkmsp.id_product = pp.id_product 
                JOIN ps_customer pc ON pc.id_customer = pkmsp.id_seller 
                WHERE (SELECT id_image FROM ps_image pi 
                WHERE id_product = pp.id_product ORDER BY position ASC LIMIT 1) IS NOT NULL 
                AND pkmsp.deleted != '1'
                AND pp.active=1
                AND ( pp.id_category_default = '$id_category' OR pcg.id_category = '$id_category')
                OR pp.id_product IN (SELECT id_product FROM ps_category_product WHERE id_category=$id_category)
                group by pp.id_product
                ORDER BY pp.date_add DESC    
                $limit ";

        $query = $this->db->query($sql);

        return $query->result();
    }
     
    public function insert_entry() {
        $this->id_supplier = 0;
        $this->id_category_default = $_POST['id_category'];
        $this->id_shop_default = 1;
        $this->id_tax_rules_group = 0;
        $this->on_sale = 0;
        $this->ecotax = 0;
        $this->quantity = 0;
        $this->minimal_quantity = 1;
        $this->max_quantity = 0;
        $this->price = $_POST['price'];
        $this->wholesale_price = $_POST['price'];
        $this->unit_price_ratio = 0;
        $this->additional_shipping_cost = 0;
        $this->width = 0;
        $this->height = 0;
        $this->depth = 0;
        $this->weight = 0;
        $this->out_of_stock = 1;
        $this->customizable = 0;
        $this->uploadable_files = 0;
        $this->text_fields = 0;
        $this->active = 1;
        $this->redirect_type = 404;
        $this->id_product_redirected = 0;
        $this->available_for_order = 0;
        $this->available_date = date("Y-m-d");
        $this->condition = "new";
        $this->show_price = 1;
        $this->indexed = 0;
        $this->visibility = "both";
        $this->cache_is_pack = 0;
        $this->cache_has_attachments = 0;
        $this->is_virtual = 0;
        $this->date_add = date("Y-m-d H:i:s");
        $this->date_upd = date("Y-m-d H:i:s");
        $this->advanced_stock_management = 0;

        $this->db->insert('ps_product', $this);

        return $this->db->insert_id();
    }

    public function is_mine($id_cutomer, $id_product) {
        $sql  = "SELECT 1 FROM ps_kb_mp_seller_product kmsp ";
        $sql .= "JOIN ps_kb_mp_seller kms ON kms.id_seller = kmsp.id_seller ";
        $sql .= "WHERE kmsp.id_product = '$id_product' AND kms.id_customer = '$id_cutomer' ;";

        $query = $query = $this->db->query($sql);

        if($query->result()) {
            return 'Y';
        }

        return 'N';
    }

    public function find($id) {
        if(isset($id)){
            $sql   = "  SELECT
                            kbmsp.id_product as product_id,
                            IF (kbmsp.date_add < '" . KB_DATE_ADD . "', 'yes', 'no') as dir_kb, 
                            kbmsp.id_seller as seller_id,
                            (SELECT sc_path FROM ps_image pi WHERE id_product = kbmsp.id_product 
                             ORDER BY position ASC LIMIT 1) AS sc_path,
                            (SELECT id_image FROM ps_image pi WHERE id_product = kbmsp.id_product 
                             ORDER BY position ASC LIMIT 1) AS id_image,
                            ppl.name as name,
                            pps.price as price,
                            ppl.description_short as description,
                            pc.firstname,
                            pc.lastname,
                            pc.img_path,
                            pp.id_category_default as id_category
                        FROM
                            ps_kb_mp_seller_product kbmsp JOIN ps_product pp ON kbmsp.id_product = pp.id_product
                            JOIN ps_product_lang ppl ON ppl.id_product = pp.id_product
                            JOIN ps_product_shop pps ON pps.id_product = pp.id_product
                            JOIN ps_customer pc ON pc.id_customer = kbmsp.id_seller
                        WHERE
                            kbmsp.deleted != '1' 
                        AND 
                            pp.active=1
                        AND ppl.id_product = '$id'
                        ORDER BY ppl.name ASC
                        LIMIT 1 ";

            $query = $this->db->query($sql);

            return $query->result();
        }
        
        return array();    
    }
    
    public function update_price($id_product) {
        $data = array( 
                        'price' => $_POST['price'],
                        'wholesale_price' => $_POST['price']
                     );
         
        $this->db->where('id_product', $id_product);
        $this->db->update('ps_product', $data);
    }

    public function get_images_product($link_rewrite) {
        $sql = "SELECT pi.*, kmsp.id_seller_product,
                IF (kmsp.date_add < '" . KB_DATE_ADD . "', 'yes', 'no') as dir_kb
                     FROM ps_image pi 
                JOIN ps_kb_mp_seller_product kmsp ON pi.id_product = kmsp.id_product
                JOIN ps_product_lang  ppl ON ppl.id_product = pi.id_product
                WHERE ppl.link_rewrite = '$link_rewrite' ORDER BY position ASC LIMIT 3";

        $query = $this->db->query($sql);

        return $query->result();        
    }

    public function find_by_link_rewrite($link_rewrite) {
        $sql = "SELECT
                    kbmsp.id_product as product_id,
                    IF (kbmsp.date_add < '" . KB_DATE_ADD . "', 'yes', 'no') as dir_kb, 
                    kbmsp.id_seller as seller_id,
                    (SELECT sc_path FROM ps_image pi WHERE id_product = kbmsp.id_product ORDER BY position ASC LIMIT 1) AS sc_path,
                    (SELECT id_image FROM ps_image pi WHERE id_product = kbmsp.id_product ORDER BY position ASC LIMIT 1) AS id_image,
                    ppl.name as name,
                    pps.price as price,
                    ppl.description_short as description,
                    pc.firstname,
                    pc.lastname,
                    pc.img_path
                FROM
                    ps_kb_mp_seller_product kbmsp JOIN ps_product pp ON kbmsp.id_product = pp.id_product
                    JOIN ps_product_lang ppl ON ppl.id_product = pp.id_product
                    JOIN ps_product_shop pps ON pps.id_product = pp.id_product
                    JOIN ps_customer pc ON pc.id_customer = kbmsp.id_seller
                WHERE
                    kbmsp.deleted != '1' 
                AND 
                    pp.active=1
                AND ppl.link_rewrite = '$link_rewrite'
                ORDER BY ppl.name ASC
                LIMIT 1";

        $query = $this->db->query($sql);

        return $query->result();        
    }

    public function find_by_offset($i) {
        $this->db->order_by('date_add', 'ASC');
        $query = $this->db->get('ps_product', 1, $i);
        
        return $query->result();
    }

    public function get_rows() {
        return $this->db->count_all_results('ps_product');  
    }

    public function find_random($limit=4) {
        $sql   = "SELECT
                    kbmsp.id_product as product_id,
                    IF (kbmsp.date_add < '" . KB_DATE_ADD . "', 'yes', 'no') as dir_kb,
                    kbmsp.id_seller as seller_id,
                    (SELECT sc_path FROM ps_image pi WHERE id_product = kbmsp.id_product ORDER BY position ASC LIMIT 1) AS sc_path,
                    (SELECT id_image FROM ps_image pi WHERE id_product = kbmsp.id_product ORDER BY position ASC LIMIT 1) AS id_image,
                    ppl.name as product_name,
                    pps.price as product_price,
                    ppl.description_short as description, 
                    ppl.link_rewrite,
                    pc.firstname,
                    pc.lastname,
                    pc.img_path   
                FROM
                    ps_kb_mp_seller_product kbmsp JOIN ps_product pp ON kbmsp.id_product = pp.id_product
                    JOIN ps_product_lang ppl ON ppl.id_product = pp.id_product
                    JOIN ps_product_shop pps ON pps.id_product = pp.id_product
                    JOIN ps_customer pc ON pc.id_customer = kbmsp.id_seller
                WHERE
                    kbmsp.deleted != '1' 
                AND 
                    pp.active=1
                ORDER BY RAND()
                LIMIT $limit";

        $query = $query = $this->db->query($sql);
        return $query->result();
    }

    public function get_related_products($products, $limit = 8) {
        if($limit == 'all'){
            $limit = " ";
        } else {
            $limit = "LIMIT $limit";
        }
        
        $product_id = $products->product_id;
        $id_category = $products->id_category;
        $product_name = $products->name;

        $sql   = "  SELECT pp.id_product AS product_id, pc.id_customer AS seller_id, 
                    pc.firstname, pc.lastname, pc.img_path, pl.name AS product_name,
                    pl.link_rewrite, 
                    pps.price AS product_price, description_short AS description,
                    IF (pkmsp.date_add < '" . KB_DATE_ADD . "', 'yes', 'no') as dir_kb,
                    (SELECT sc_path FROM ps_image pi
                        WHERE id_product = pp.id_product ORDER BY position ASC LIMIT 1) AS sc_path,
                    (SELECT id_image FROM ps_image pi
                        WHERE id_product = pp.id_product ORDER BY position ASC LIMIT 1) AS id_image  
                    FROM ps_category pcg JOIN ps_category_product pcgp ON pcg.id_category = pcgp.id_category 
                    JOIN ps_product pp ON pp.id_product = pcgp.id_product
                    JOIN ps_product_lang pl ON pl.id_product = pp.id_product 
                    JOIN ps_product_shop pps ON pps.id_product = pp.id_product 
                    JOIN ps_kb_mp_seller_product pkmsp ON pkmsp.id_product = pp.id_product 
                    JOIN ps_customer pc ON pc.id_customer = pkmsp.id_seller 
                    WHERE ( SELECT id_image FROM ps_image pi 
                            WHERE id_product = pp.id_product ORDER BY position ASC LIMIT 1 ) IS NOT NULL 
                    AND pkmsp.deleted != 0
                    AND pp.active=1
                    AND ( 
                          pl.name like '%$product_name%' OR 
                          pp.id_category_default = '$products->id_category' OR pcg.id_category = '$id_category'
                        )
                    AND 
                    pp.id_product != $product_id
                    group by pp.id_product
                    ORDER BY pp.date_add DESC  
                    $limit ";

        $query = $this->db->query($sql);

        return $query->result();
    }
}