<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class ImageShop_model extends CI_Model {
	public $id_image;
	public $id_shop;	
	public $id_product;	

	public function __construct() {
        parent::__construct();
    }

    public function insert_entry($id_image, $id_product){
    	$this->id_image = $id_image;
		$this->id_shop = 1;
		$this->id_product = $id_product;
	
		$this->db->insert('ps_image_shop', $this);
    }

    public function delete($id) {
	   $this->db->where('id_image', $id);
	   $this->db->delete('ps_image_shop'); 
	}
}