<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class ImageLang_model extends CI_Model {
	public $id_lang;
	public $legend;

	public function __construct() {
        parent::__construct();
    }

    public function insert_entry($id_image){
    	$this->id_image = $id_image;
		$this->id_lang = 1;
	
		$this->db->insert('ps_image_lang', $this);
    }

    public function delete($id) {
	   $this->db->where('id_image', $id);
	   $this->db->delete('ps_image_lang'); 
	}

}