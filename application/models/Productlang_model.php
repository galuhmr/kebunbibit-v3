<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class ProductLang_model extends CI_Model {

	public $id_product;
	public $id_shop;
	public $id_lang;
	public $description_short;
	public $link_rewrite;
	public $name;

	public function __construct() {
        parent::__construct();
    }

	public function delete($id) {
	   $this->db->where('id_product', $id);
	   $this->db->delete('ps_product_lang'); 
	}

	
    public function update_description($id_product) {
        $data = array( 'description_short' => $_POST['description'] );
         
        $this->db->where('id_product', $id_product);
        $this->db->update('ps_product_lang', $data);
    }

    public function insert_entry($id_product){
    	$this->id_product = $id_product;
		$this->id_shop = 1;
		$this->id_lang = 1;
		$this->description_short = $_POST['description'];
		$this->link_rewrite = format_url($_POST['name']);
		$this->name = $_POST['name'];

		$this->db->insert('ps_product_lang', $this);
    }

}