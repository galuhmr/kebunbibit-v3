<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Address_model extends CI_Model {
	public $id_country;
	public $id_state;
	public $id_customer;
	public $id_manufacturer;
	public $id_supplier;
	public $id_warehouse;
	public $alias;
	public $firstname;
	public $lastname;
	public $address1;
	public $city;
	public $phone;
	public $date_add;
	public $date_upd;

	public function __construct() {
        parent::__construct();
    }
    
    public function find($id_customer) {
    	$this->db->where('id_customer', $id_customer);
    	$query = $this->db->get('ps_address', 1);
    	
    	return $query->result();
    }

    public function insert_entry($id_customer){
		$this->id_country = 111;
		$this->id_state = 1;
		$this->id_customer = $id_customer;
		$this->id_manufacturer = 0;
		$this->id_supplier = 0;
		$this->id_warehouse = 0;
		$this->alias = "-";
		$this->firstname = $_POST['firstname'];
		$this->lastname = $_POST['lastname'];
		$this->address1 = isset($_POST['address']) ? $_POST['address'] : "-";
		$this->city = isset($_POST['city']) ? $_POST['city'] : "-";
		$this->phone = isset($_POST['phone']) ? $_POST['phone'] : "-";
		$this->date_add = date("Y-m-d H:i:s");
		$this->date_upd = date("Y-m-d H:i:s");
		
		$this->db->insert('ps_address', $this);

		return $this->db->insert_id();
    }


}