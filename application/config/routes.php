<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'welcome';
$route['cara-order'] = 'welcome/cara_order';
$route['csr'] = 'welcome/csr';
$route['submit-payment-confirm']['POST'] = 'paymentconfirmation/save';
$route['pencarian'] = 'product/search_result';
$route['bagaimana-cara-membeli'] = 'welcome/bagaimana_cara_membeli';
$route['bagaimana-mendapatkan-harga-termurah'] = 'welcome/bagaimana_mendapatkan_harga_termurah';
$route['standar-kualitas-produk'] = 'welcome/standar_kualitas_produk';
$route['seputar-pembayaran'] = 'welcome/seputar_pembayaran';
$route['konfirmasi-pembayaran'] = 'welcome/konfirmasi_pembayaran';
$route['pertanyaan-umum-pembeli'] = 'welcome/pertanyaan_umum_pembeli';
/** penjual **/
$route['bagaimana-cara-berjualan'] = 'welcome/bagaimana_cara_berjualan';
$route['syarat-panduan-produk'] = 'welcome/syarat_panduan_produk';
$route['penggunaan-fitur'] = 'welcome/penggunaan_fitur';
$route['transaksi-dan-pembayaran'] = 'welcome/transaksi_dan_pembayaran';
$route['etika-berjualan'] = 'welcome/etika_berjualan';
$route['pertanyaan-umum-penjual'] = 'welcome/pertanyaan_umum_penjual';
$route['test'] = 'welcome/test';
$route['product_api/(:num)/(:num)'] = 'product/product_by_category_api/$1/$2';
$route['product_api_by_id/(:num)'] = 'product/product_api_by_id/$1';
$route['product/update_price/(:num)']['POST'] = 'product/update_price/$1';
$route['order/save-order']['POST'] = 'order/save_order';
$route['order/get_orders_by_seller']['POST'] = 'order/get_orders_by_seller';
$route['cart/count']['POST'] = 'cart/count';
$route['cart/change_check_status']['POST'] = 'cart/change_check_status';
$route['cart/format_price/(:any)'] = 'cart/format_price/$1';
$route['cart/cart_data']['POST'] = 'cart/cart_data';
$route['cart/total_cart']['POST'] = 'cart/total_cart';
$route['cart/delete_product/(:num)'] = 'cart/delete_product/$1';
$route['cart/add']['POST'] = 'cart/add';
$route['cart/add-from-bargain']['POST'] = 'cart/add_from_bargain';
$route['customer/logout'] = 'customer/logout';
$route['customer/get_address']['POST'] = 'customer/get_address';
$route['customer/reset-password']['POST'] = 'customer/reset_password';
$route['customer/login']['POST'] = 'customer/login';
$route['customer/facebook_login']['POST'] = 'customer/facebook_login';
$route['customer/google_login']['POST'] = 'customer/google_login';
$route['customer/update-customer/(:num)']['POST'] = 'customer/update_customer/$1';
$route['customer/register']['POST'] = 'customer/register';
$route['customer/check_email']['POST'] = 'customer/check_email';
$route['customer/change-status']['POST'] = 'customer/change_status';
$route['account'] = 'account';
$route['product/seller_products/(:num)/(:num)'] = 'product/seller_products/$1/$2';
$route['product/load_more_search/(:num)']['POST'] = 'product/load_more_search/$1';
$route['product/load_more_category/(:num)']['POST'] = 'product/load_more_category/$1';
$route['profil/(:any)'] = 'account/profile/$1';
$route['product/my_products/(:num)'] = 'product/my_products/$1';
$route['account/inbox'] = 'account/inbox';
$route['account/order'] = 'account/order';
$route['product/search']['POST'] = 'product/search';
$route['product/(:num)/(:any)'] = 'product/products_in_category/$1/$2';
$route['product/create-categories-api']['POST'] = 'product/create_categories_api';
$route['image/upload']['POST'] = 'image/upload';
$route['image/delete']['POST'] = 'image/delete';
$route['product/save']['POST'] = 'product/save';
$route['product/delete/(:num)']['POST'] = 'product/delete/$1';
$route['product/detail/(:any)'] = 'product/product_detail/$1';
$route['product/migration/(:num)']['POST'] = 'product/migration_to_seller_product/$1';
$route['checkout/purchase'] = 'checkout/purchase';
$route['message/delete/(:num)']['POST'] = 'message/delete/$1';
$route['message/check_inbox']['POST'] = 'deal/check_inbox';
$route['message/check-is-read']['POST'] = 'message/check_is_read';
$route['message/find-bargain/(:num)']['POST'] = 'message/find-bargain/$1';
$route['message/find-not-delivered']['POST'] = 'message/find-not-delivered';
$route['message/change-to-read/(:any)']['POST'] = 'message/change_to_read/$1';
$route['deal/api/(:any)'] = 'deal/api/$1';
$route['message/save-chat']['POST'] = 'message/save_chat';
$route['message/save-bargain']['POST'] = 'message/save_bargain';
$route['message/accept-bargain']['POST'] = 'message/accept_bargain';
$route['message/reject-bargain']['POST'] = 'message/reject_bargain';
$route['deal/(:any)/read/chat'] = 'deal/read_chat/$1';
$route['deal/(:any)'] = 'deal/chat/$1';
$route['message/(:num)/p/(:num)/b/(:num)/s/(:num)'] = 'message/chat_buyer/$1/$2/$3/$4';
$route['message/(:num)/p/(:num)/s/(:num)/b/(:num)'] = 'message/chat_seller/$1/$2/$3/$4';
$route['product/all'] = 'product/all';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
$route['(:any)'] = 'welcome/check_html/$1';