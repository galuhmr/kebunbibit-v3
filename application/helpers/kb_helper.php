<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

define('ACTIVATE_EMAIL', FALSE); 
define('ACTIVATE_INDEX_PHP', FALSE); 

/**
 * Hash password user baru.
 *
 * @param   string  $password
 * @return  string
 */
function hash_password($password){
	$COOKIE_KEY = "HYJKhTidrrs8COghD0crYkoIrMQwg7tQAS8Hd6zw8h9GtSNpnn6Moy3v";
	$retval = "";

	if(isset($password)) {
		$retval = md5($COOKIE_KEY . $password);
	}

  return $retval;
}

/**
 * Mendapatkan panjang karakter
 *
 * @param   string  $str
 * @return  int
 */
function slength($str) {
  return strlen(trim($str));
}

function account_menu() {
  // produk saya
  $menu1 = array();
  $menu1['name'] = "Produk Saya";
  $menu1['link'] = base_url() . kb_index() . "profil/".encrypt(user_login());
  
  // informasi akun
  $menu2 = array();
  $menu2['name'] = "Informasi Akun";
  $menu2['link'] = base_url() . kb_index() . "account";
  
  // kotak masuk
  $menu3 = array();
  $menu3['name'] = "Kotak Masuk";
  $menu3['link'] = base_url() . kb_index() . "account/inbox";
  
  // transaksi
  $menu4 = array();
  $menu4['name'] = "Transaksi";
  $menu4['link'] = base_url() . kb_index() . "account/order";
  
  return array($menu1, $menu2, $menu3, $menu4);
}

/**
 * mendapatkan id_user yang login saat ini
 *
 * @return  int
 */
function user_login() {
    $id_customer = isset($_SESSION["id_customer"]) ? $_SESSION["id_customer"] : null;
    $retval = null;

    if($id_customer != null){
      $retval = decrypt($id_customer);
    }

    return $retval;
}

/**
 * membatasi karakter yang ditampilkan
 *
 * @param   string $str
 * @param   int $limit
 * @return  int
 */
function limit_string($str, $limit = 30) {
	$retval = $str;

	if (strlen($str) > $limit) { $retval = substr($str, 0, $limit) . ' ...'; }

	return $retval;
}

/**
 * terkadang ada htaccess yang tidak support di beberapa server,
 * set ACTIVATE_INDEX_PHP ke true
 * untuk menampilkan index.php
 *
 * @return  string
 */
function kb_index() {
  if(ACTIVATE_INDEX_PHP){
    return "index.php/";
  }

  return "";
}

/**
 * jangan pake http / https
 * https://google.github.io/styleguide/htmlcssguide.xml?showone=Protocol#Protocol
 *
 * @return  string
 */
function create_source_js($js_files) {
  $retval = "";

  foreach($js_files as $js_file):
    $retval .= '<script src="//localhost/~galuh/kebunbibit/' . $js_file . '"></script>';
  endforeach;

  return $retval;
}

/**
 * untuk mengirim email
 *
 * @param   object  $obj
 * @param   string  $subject
 * @param   string  $body
 * @param   string  $target_email
 * @return  void
 */
function send_email($obj, $subject, $body, $target_email) {
    if(ACTIVATE_EMAIL){
      $config['mailtype'] = 'html';

      $obj->load->library('email');
      $obj->email->initialize($config);
      $obj->email->from('no-reply@kebunbibit.id', 'Kebunbibit');
      $obj->email->to($target_email);

      $obj->email->subject($subject);
      $obj->email->message($body);

      $obj->email->send();
    }  
}

/**
 * load view dengan header footer
 *
 * @param   object  $obj
 * @param   string  $view
 * @param   array   $data
 * @param   bool    $session_not_null Jika TRUE ketika session kosong langsung redirect ke halaman utama
 * @return  void
 */
function load_view($obj, $view, $data = array(), $session_not_null = FALSE) {
  if($session_not_null){  
    if(!isset($_SESSION["id_customer"])) {
       redirect(base_url(), 'refresh');
    }
  }  

  $obj->load->model('customer_model', 'customer');
  $obj->load->model('product_model', 'product');
  
  $id_customer = isset($_SESSION["id_customer"]) ? $_SESSION["id_customer"] : null;
  $decrypt_id = decrypt($id_customer);
  $results = $obj->customer->find($decrypt_id);
  $top_searches = $obj->product->get_top_search();

  $data['results'] = $results;
  $data['id_customer'] = $decrypt_id;
  $data['top_searches'] = $top_searches;

  $obj->load->view('templates/header', $data);
  $obj->load->view($view, $data);
  $obj->load->view('templates/footer');
}

function fix_top_search($str) {
  $parts = preg_split('/\s+/', $str);

  if(count($parts) >= 3) {
    return $parts[0] . ' ' . $parts[1];
  } else if(count($parts) == 2) {
    return $parts[0] . ' ' . $parts[1];
  }

  return $str;
}

/**
 * format string agar url friendly
 * biasanya untuk link_rewrite
 *
 * @param   string  $str
 * @param   string  $sep
 * @return  string
 */
function format_url($str, $sep='-') {
  $res = strtolower($str);
  $res = preg_replace('/[^[:alnum:]]/', ' ', $res);
	$res = preg_replace('/[[:space:]]+/', $sep, $res);
	
	return trim($res, $sep);
}

/**
 * ngeformat total tawar ke bentuk rupiah
 *
 * @param   string  $str
 * @return  string
 */
function repair_message_bargain($str){
  $pieces = explode("k3bunb1b1tb4rg4in", $str);

  return $pieces[0] . format_to_rupiah($pieces[1]);
}

/**
 * ambil gambar dari cdn
 *
 * @return  string
 */
function image_url() {
  return "https://kebunbibitcdn-ptkebunbibitpenu.netdna-ssl.com/";
}

/**
 * membuat reference dari id order
 *
 * @param   string   $id_order
 * @return  string
 */
function order_reference($id_order){
  if(strlen($id_order) == 2) {
    return "MP0" . $id_order;
  } else if(strlen($id_order) == 1){
    return "MP00" . $id_order;
  }

  return "MP" . $id_order;
}

/**
 * mengambil lokasi foto profil user
 *
 * @param   string   $path
 * @return  string
 */
function get_user_photo($path) {
  if(strlen(trim($path)) == 0) {
    return image_url() . "/public/img/penjual.png";
  }

  if(strpos($path, 'googleusercontent.com')){
    return $path;
  } else if(strpos($path, 'scontent.xx.fbcdn.net')) {
    return $path;
  }

  return image_url() . "/public/uploads/" .$path;
}

/**
 * mengambil lokasi gambar produk
 *
 * @param   string   $product
 * @return  string
 */
function get_product_image($product){
  $src_img = "public/uploads/".$product->sc_path;
  
  if($product->dir_kb == "yes"){
    $src_img = "img/p/".implode("/",str_split("$product->id_image"))."/$product->id_image-tonytheme_product_small_2x.jpg";
  }

  return image_url() . $src_img;

}

/**
 * encrypt buatan sendiri
 *
 * @param   string   $id
 * @return  string
 */
function encrypt($id) {
	$prefix = randStr();
	$suffix = randStr();
	
	return $prefix . $id . $suffix;
}

/**
 * decrypt buatan sendiri
 *
 * @param   string   $str
 * @return  string
 */
function decrypt($str) {
	$str1 = substr($str, 10);
	$str2 = substr($str1, -10);

	return str_replace($str2, '', $str1);
}

/**
 * random karakter
 *
 * @param   int      $length panjang karakter
 * @return  string
 */
function randStr($length = 10) {
    $characters = '0123456789';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

/**
 * format ke bentuk rupiah
 *
 * @param   int  $val
 * @return  string
 */
function format_to_rupiah($val) {
    return 'Rp ' . number_format($val);
} 

/**
 * format waktu ke indonesia
 *
 * @param   datetime  $datetime
 * @return  date
 */
function format_message_time($datetime) {
  $date = date_create($datetime);

  return date_format($date, 'd F Y H:i');
}

/**
 * no description
 *
 * @param   datetime  $datetime
 * @return  date
 */
function format_to_indo($str){
   $res = format_hari($str);

   return format_bulan($res);
}

/**
 * terjemahkan hari
 * ke dalam bahasa indonesia
 *
 * @param   string  $str
 * @return  string
 */
function format_hari($str) {
  $day = array("Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday");
  $hari = array("Senin", "Selasa", "Rabu", "Kamis", "Jum'at", "Sabtu", "Minggu");
  $retval = "";
  $i = 0;

  while(count(explode($day[$i], $str)) <= 1){
    $i++;
  }

  $retval = str_replace($day[$i], $hari[$i], $str);
  

  return $retval;
}

/**
 * terjemahkan bulan
 * ke dalam bahasa indonesia
 *
 * @param   string  $str
 * @return  string
 */
function format_bulan($str) {
  $month = array("January", "February", "March", "April", "May", "June", "July", "August", "September", 
                 "October", "November", "December");
  $bulan = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", 
                 "Oktober", "November", "Desember");
  $retval = "";
  $i = 0;

  while(count(explode($month[$i], $str)) <= 1){
    $i++;
  }

  $retval = str_replace($month[$i], $bulan[$i], $str);
  
  return $retval;
}

/**
 * mendapatkan waktu 2 hari berikutnya, 3 hari, dst
 * (tergantung inputan)
 *
 * @param   int     $i jumlah hari yang diinginkan
 * @return  string
 */
function get_time($i = 0) {
  $date = date('l, d F Y', strtotime("+$i day", time()));
  $time = date('h:i', strtotime("+$i day", time()));
  
  return format_to_indo($date . ' Pukul ' . $time . ' WIB');
}

function create_url_chat_buyer($id_product, $id_buyer, $id_seller) {
  $url_deal  = base_url() . kb_index() . 'message/';
  $url_deal .= encrypt($id_buyer . $id_seller);
  $url_deal .= '/p/' . encrypt($id_product);
  $url_deal .= '/b/' . encrypt($id_buyer);
  $url_deal .= '/s/' . encrypt($id_seller);
  
  return $url_deal;   
}

function create_url_chat_seller($id_product, $id_seller, $id_buyer) {
  $url_deal  = base_url() . kb_index() . 'message/';
  $url_deal .= encrypt($id_seller . $id_buyer);
  $url_deal .= '/p/' . encrypt($id_product);
  $url_deal .= '/s/' . encrypt($id_seller);
  $url_deal .= '/b/' . encrypt($id_buyer);
  
  return $url_deal;   
}

function show_product_list($product, $id_customer){
	$src_img = image_url() . "public/uploads/".$product->sc_path;
  $url_deal = "#";
  
  if(isset($_SESSION["id_customer"])){
    $url_deal = create_url_chat_buyer($product->product_id, $id_customer, $product->seller_id);
  }

  if($product->dir_kb == "yes"){
    $src_img = "https://kebunbibit.id/img/p/".implode("/", str_split("$product->id_image"))."/$product->id_image-tonytheme_product_small_2x.jpg";
  }
$html =  "<li>";
  if($id_customer == $product->seller_id):
    $html =  "<li class='li-edit'>";
    $html .= "<div class='read-product'>";
  endif;
  $html .= "			<img class='product-img' src='$src_img'>
			<div>
				<a class='product-name' href='" . base_url() . kb_index() . $product->link_rewrite . ".html'>" . limit_string($product->product_name) ."</a>
			</div>";
          if($id_customer != $product->seller_id):
              $html .= " <h4 class='c-product_formatted_price'>". format_to_rupiah($product->product_price)."</h4>";
              if(!isset($_SESSION["id_customer"])){  
                $html .= "<a class='kb-button kb-deal show-login' 
                   href='$url_deal'>
                   <i class='fa fa-angle-double-down'></i> Tawar Dulu
                </a>";
              } else {
                $html .= "<a class='kb-button kb-deal' 
                   href='$url_deal'>
                   <i class='fa fa-angle-double-down'></i> Tawar Dulu
                  </a>";
              }
              $html .= "
              <input type='hidden' class='c-id_seller' value='$product->seller_id'>
              <input type='hidden' class='c-product_id' value='$product->product_id'>
              <input type='hidden' class='c-product_name' value='$product->product_name'>
              <input type='hidden' class='c-product_price' value='$product->product_price'>";

              if(!isset($_SESSION["id_customer"])){  
                $html .= "<button class='kb-button show-login' style='background-color: #7ac144;'>
                          <i class='fa fa-shopping-cart'></i> Beli Langsung</button>";
              } else {
                $html .= "<button class='kb-button kb-cart'><i class='fa fa-shopping-cart'></i> Beli Langsung</button>";
              }  

          else:
            $html .= " <h4 class='c-product_formatted_price-edit'>". format_to_rupiah($product->product_price)."</h4>";
              // $html .= "<div class='my-product-caption'>
              //   <h5>Ini produk Anda</h5>
              // </div>";
            $html .= "
              <input type='hidden' class='c-id_seller-edit' value='$product->seller_id'>
              <input type='hidden' class='c-product_id-edit' value='$product->product_id'>
              <input type='hidden' class='c-product_name-edit' value='$product->product_name'>
              <input type='hidden' class='c-product_price-edit' value='$product->product_price'>";
            $html .= "<button style='background-color: #b9b9b9;' class='kb-button kb-update'>
                         <i class='fa fa-pencil-square-o'></i> Ubah
                      </button>";
            $html .= "<button style='background-color: #d9534f; color: #FFF !important;' class='kb-button kb-delete'>
                          <i class='fa fa-trash-o'></i> Hapus
                      </button>"; 
          endif;  
          $html .= "<div class='seller-section'>
            <ul>
              <li>";
          
          if(strlen(trim($product->img_path)) > 0) {
            $html .= "<img class='seller-photo' src='".image_url()."public/uploads/$product->img_path'>";
          } else {
            $html .= "<img class='seller-photo' src='".image_url()."public/img/penjual.png'>";
          }
          
          $html .= "</li>
              <li>
                <a class='c-seller_name' href='" . base_url() . kb_index() . "profil/" . encrypt($product->seller_id) . "'>$product->firstname $product->lastname</a>
              </li>
            </ul>
          </div>";

        if($id_customer == $product->seller_id):
          $html .= "</div>";
          $html .= "<div style='display: none;' class='update-product'>
                    <div style='float: left'>
                     <input type='text' class='edit-product-price' value='" . ($product->product_price + 0) . "'>
                     <textarea class='edit-desc'>" . $product->description . "</textarea>
                     <button class='kb-button apply-edit'>Lanjutkan</button>
                     <button class='kb-button cancel-edit'>Batal</button>
                    </div>
                  </div>
                  <div style='display: none;' class='delete-product'>
                    <p>
                      Anda yakin ingin menghapus produk ini?
                    </p>
                     <button class='kb-button yes-delete'>Ya</button>
                     <button class='kb-button no-delete'>Tidak</button>
                  </div>
                  <div style='display: none;' class='validate-product'>
                    <div style='display: none;' class='progressbar'>
                      <img src='" . image_url() . 'public/img/loading.gif' . "' >
                      <p style='margin-top: 26px;font-family: 'Source Sans Pro', sans-serif;'>
                        Tunggu Sebentar
                      </p> 
                    </div>
                    <div style='display: none;' class='message'>
                      <div style='display: none;' class='error'>
                         <div class='alert alert-danger'>
                           Harga minimum 10,000
                         </div>
                         <button class='kb-button try-again-edit'>Coba Lagi</button>
                      </div>
                      <div style='display: none;' class='success'>
                         <div class='alert alert-success'>
                           
                         </div>
                         <button class='kb-button done-edit'>Selesai</button>
                      </div>
                    </div>
                  </div>
                  ";
        endif;
        
        $html .= "</li>";

    return $html;    
}

function show_profile_product_list($product, $id_seller, $id_user){
	$src_img = image_url() . "public/uploads/".$product->sc_path;
  $url_deal = "#";
  
  if(isset($_SESSION["id_customer"])){
    $url_deal = create_url_chat_buyer($product->product_id, $id_user, $product->seller_id);
  }

  if($product->dir_kb == "yes"){
    $src_img = "https://kebunbibit.id/img/p/".implode("/", str_split("$product->id_image"))."/$product->id_image-tonytheme_product_small_2x.jpg";
  }
$html =  "<li>";
  if($id_user == $product->seller_id):
    $html =  "<li class='li-edit'>";
    $html .= "<div class='read-product'>";
  endif;
  $html .= "      <img class='product-img' src='$src_img'>
      <div>
        <a class='product-name' href='" . base_url() . kb_index() . $product->link_rewrite . ".html'>" . limit_string($product->product_name) ."</a>
      </div>";
          if($id_user != $product->seller_id):
              $html .= " <h4 class='c-product_formatted_price'>". format_to_rupiah($product->product_price)."</h4>";
              if(!isset($_SESSION["id_customer"])){  
                $html .= "<a class='kb-button kb-deal show-login' 
                   href='$url_deal'>
                   <i class='fa fa-angle-double-down'></i> Tawar Dulu
                </a>";
              } else {
                $html .= "<a class='kb-button kb-deal' 
                   href='$url_deal'>
                   <i class='fa fa-angle-double-down'></i> Tawar Dulu
                  </a>";
              }
              $html .= "
              <input type='hidden' class='c-id_seller' value='$product->seller_id'>
              <input type='hidden' class='c-product_id' value='$product->product_id'>
              <input type='hidden' class='c-product_name' value='$product->product_name'>
              <input type='hidden' class='c-product_price' value='$product->product_price'>";

              if(!isset($_SESSION["id_customer"])){  
                $html .= "<button class='kb-button show-login' style='background-color: #7ac144;'>
                          <i class='fa fa-shopping-cart'></i> Beli Langsung</button>";
              } else {
                $html .= "<button class='kb-button kb-cart'><i class='fa fa-shopping-cart'></i> Beli Langsung</button>";
              }  

          else:
            $html .= " <h4 class='c-product_formatted_price-edit'>". format_to_rupiah($product->product_price)."</h4>";
              // $html .= "<div class='my-product-caption'>
              //   <h5>Ini produk Anda</h5>
              // </div>";
            $html .= "
              <input type='hidden' class='c-id_seller-edit' value='$product->seller_id'>
              <input type='hidden' class='c-product_id-edit' value='$product->product_id'>
              <input type='hidden' class='c-product_name-edit' value='$product->product_name'>
              <input type='hidden' class='c-product_price-edit' value='$product->product_price'>";
            $html .= "<button style='background-color: #b9b9b9;' class='kb-button kb-update'>
                         <i class='fa fa-pencil-square-o'></i> Ubah
                      </button>";
            $html .= "<button style='background-color: #d9534f; color: #FFF !important;' class='kb-button kb-delete'>
                         <i class='fa fa-trash-o'></i> Hapus
                      </button>"; 
          endif;  
          $html .= "<div  style='display: none;' class='seller-section'>
            <ul>
              <li>";
          
          if(strlen(trim($product->img_path)) > 0) {
            $html .= "<img class='seller-photo' src='".image_url()."public/uploads/$product->img_path'>";
          } else {
            $html .= "<img class='seller-photo' src='".image_url()."public/img/penjual.png'>";
          }
          
          $html .= "</li>
              <li>
                <a class='c-seller_name' href='" . base_url() . kb_index() . "profil/" . encrypt($product->seller_id) . "'>$product->firstname $product->lastname</a>
              </li>
            </ul>
          </div>";

        if($id_user == $product->seller_id):
          $html .= "</div>";
          $html .= "<div style='display: none; height: 362px;' class='update-product'>
                    <div style='float: left'>
                     <input type='text' class='edit-product-price' value='" . ($product->product_price + 0) . "'>
                     <textarea style='height: 18.5em;' class='edit-desc'>" . $product->description . "</textarea>
                     <button class='kb-button apply-edit'>Lanjutkan</button>
                     <button class='kb-button cancel-edit'>Batal</button>
                    </div>
                  </div>
                  <div style='display: none;' class='delete-product'>
                    <p>
                      Anda yakin ingin menghapus produk ini?
                    </p>
                     <button class='kb-button yes-delete'>Ya</button>
                     <button class='kb-button no-delete'>Tidak</button>
                  </div>
                  <div style='display: none;' class='validate-product'>
                    <div style='display: none;' class='progressbar'>
                      <img src='" . image_url() . 'public/img/loading.gif' . "' >
                      <p style='margin-top: 26px;font-family: 'Source Sans Pro', sans-serif;'>
                        Tunggu Sebentar
                      </p> 
                    </div>
                    <div style='display: none;' class='message'>
                      <div style='display: none;' class='error'>
                         <div class='alert alert-danger'>
                           Harga minimum 10,000
                         </div>
                         <button class='kb-button try-again-edit'>Coba Lagi</button>
                      </div>
                      <div style='display: none;' class='success'>
                         <div class='alert alert-success'>
                           
                         </div>
                         <button class='kb-button done-edit'>Selesai</button>
                      </div>
                    </div>
                  </div>
                  ";
        endif;
        
        $html .= "</li>";

    return $html;  
}

function password_facebook() {
  return "|Uv-[N1]6c_fl2R";
}

function password_google() {
  return ",%^)oi6#mh>\/Yd";
}