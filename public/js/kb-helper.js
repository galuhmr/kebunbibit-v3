/**
 * Helper untuk JavaScript
 * 
 */

KbHelper = {
    isEmpty: function(str) {
    	return (str.length === 0 || !str.trim());
	},
	isEmailValid: function(email) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    },
    refresh: function() {
        location.reload();
    }   
}