var account = new Account();
account.init();

function Account() {

    this.init = function() {
        hideAll();

        saveButtonListener();
        uploadListener();
    }

    function hideAll() {
        $("#old-pass-empty").hide();
        $("#new-pass-empty").hide();
        $("#confirm-pass-empty").hide();
        $("#edit-loading").hide();
        $("#old-pass-wrong").hide();
        $("#success-edit").hide();
        $("#confirm-pass-wrong").hide();
    }

    function hideOldPassEmpty() {
        $("#old-pass-empty").hide();
    }

    function hideNewPassEmpty() {
        $("#new-pass-empty").hide();
    }

    function hideConfirmPassEmpty() {
        $("#confirm-pass-empty").hide();
    }

    function hideEditLoading() {
        $("#edit-loading").hide();
    }

    function hideOldPassWrong() {
        $("#old-pass-wrong").hide();
    }

    function hideSuccessEdit() {
        $("#success-edit").hide();
    }

    function hideConfirmPassWrong() {
        $("#confirm-pass-wrong").hide();
    }

    /** show **/
    function showOldPassEmpty() {
        hideAll();
        $("#old-pass-empty").show();
    }

    function showNewPassEmpty() {
        hideAll();
        $("#new-pass-empty").show();
    }

    function showConfirmPassEmpty() {
        hideAll();
        $("#confirm-pass-empty").show();
    }

    function showEditLoading() {
        hideAll();
        $("#edit-loading").show();
    }

    function showOldPassWrong() {
        hideAll();
        $("#old-pass-wrong").show();
    }

    function showSuccessEdit() {
        hideAll();
        $("#success-edit").show();
    }

    function showConfirmPassWrong() {
        hideAll();
        $("#confirm-pass-wrong").show();
    }

    // simpan perubahan data customer dengan validasi
    function saveButtonListener() {
        $("#save-profile").click(function() {
            var old_password = $( "input[name='old_password']" ).val();
            var new_password = $( "input[name='new_password']" ).val();
            var confirm_password = $( "input[name='confirm_password']" ).val();

            if (old_password.length == 0 && confirm_password.length > 0) {
                showOldPassEmpty();
            } else if (old_password.length == 0 && new_password.length > 0) {
                showOldPassEmpty();
            } else if (new_password.length == 0 && confirm_password.length > 0) {
                showNewPassEmpty();
            } else if (confirm_password.length == 0 && new_password.length > 0) {
                showConfirmPassEmpty();
            } else if (new_password.length == 0 && old_password.length > 0) {
                showNewPassEmpty();
            } else if (new_password != confirm_password) {
                showConfirmPassWrong();
            } else {
                editProfile();
            }
        });
    }

    function uploadListener() {
        $("#upload-dp").click(function() {
            $("#uploaded_file_customer").click();
        });

        if (document.getElementById('uploaded_file_customer') != undefined) {
            document.getElementById('uploaded_file_customer').addEventListener('change',
                saveImageCustomer, false);
        }
    }

    // simpan foto profil
    function saveImageCustomer() {
        var _url = base_url + kb_index + "image/upload";
        var file_data = $('#uploaded_file_customer').prop('files')[0];
        var form_data = new FormData();

        if (file_data !== undefined) {
            form_data.append('file', file_data);

            $("#profil-img").attr("src", image_url + "public/img/loading-img.gif");

            $.ajax({
                type: "POST",
                url: _url, // point to server-side PHP script 
                dataType: 'json', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,
                success: function(data) {
                    if (data.status !== 'failed') {
                        $("#profil-img").attr("src", image_url + "public/uploads/" + data.image);
                        $("#img-path").val(data.image);
                    }
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });
        }
    }

    function editProfile() {
        var _url = $("#account-form").attr('action');
        var form = $('#account-form');
        var _data = form.serialize();

        showEditLoading();

        $.ajax({
            type: "POST", // HTTP method POST or GET
            dataType: "json",
            url: _url, //Where to make Ajax calls
            data: _data, //Form variables
            success: function(data) {
                if (data.status == 'failed') {
                    showOldPassWrong();
                } else {
                    showSuccessEdit();
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });
    }

}