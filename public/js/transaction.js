var transaction = new Transaction();
transaction.init();

function Transaction() { 
    
  this.init = function() {
      showOrder();
      hideAll();
      showTransaction("buyer");
      $('.rdio-filter').click(function(){
      el = $(this);
      filter = el.data('id');
      $('.rdio-filter').css("background","");
      $('.rdio-filter').css("color","#FFF");
      el.css("background","#8BC34A");
      el.attr("active");
      $( "#table-transaksi > tbody" ).html("");
      showTransaction(filter);
    });
  }

  function hideLoading() {
    $("#load-more-loading").remove();
    // $("#load-more").show();
  }

  function showLoading() {
    var loading = '<tr id="load-more-loading">' +
                  ' <td colspan="3" align="center">' +
                  '  Sedang memuat data ...' +
                  ' </td>' +
                  '</tr>';
    $( "#table-transaksi > tbody").html(loading);
                  
    $("#load-more").hide();
  }

  function hideAll() {
    $("#load-more").hide();
    $("#load-more-loading").hide();
  }

  function showOrder() {
    $("#tb-detail").hide();
    $("#tb-order").show();
  }  

  function showDetail() {
    $("#tb-detail").show();
    $("#tb-order").hide();
  }

  function loadMoreListener() {
    $("#load-more").click(function() {
      showTransaction(filter);
    });
  }

  function showDetailProccess(reference)
  {
    showLoading();
    showDetail();
    $.ajax({
      url: base_url + "index.php/order/get_order_detail_seller",
      type: 'POST',
      data: {"id_order":reference, "filter": filter},
      success: function(data) {
        hideLoading();
        $("div#tb-detail").html(data,1000);
        klikBtnResi();
      },error: function(e){
        alert(e.status);
      }
    });
  }

  function klikDetail(){
    $('.reference').click(function(){
      var el = $(this);
      reference = el.data('reference');
      showDetailProccess(reference);
    });
  }
  

  function show(data){
    str = "<tr>";
    str += "<td><a class='reference' href='javascript:void(0)' data-reference='"+data.reference+"'>"+data.reference+"</a></td>"; 
    str += "<td>"+data.date_add+"</td>"; 
    str += "<td> Rp "+data.total+"</td>"; 
    str += "</tr>";
    return str;
  }

  function showTransaction(filter="all")
  {
    showLoading();
    loadMoreListener();
    str="";
    
    $.ajax({
      url: base_url + "index.php/order/get_orders_by_seller",
      type: 'POST',
      data: {"filter": filter, "page": page},
      dataType: "JSON",
      success: function(data) {
        isi = $( "#table-transaksi > tbody").html();
        hideLoading();
        if(data.length > 0){
          if(data.length == 0) hideAll();
          str="";
          $.each(data, function(key, val) {
            str += show(val);
          }); 
        } else {
          console.log(isi.length);
          str="<i>tidak ada transaksi</i>";
          hideAll();          
        }
          showOrder();
          $("#table-transaksi >tbody").html(str);
          klikDetail();
          page = page+1;
      },
      error: function(xhr, ajaxOptions, thrownError) {
          console.log(xhr.status);
          console.log(xhr.responseText);
          console.log(thrownError);
      }
    });
  }

  function klikBtnResi(){
    $('.btn-resi').click(function(){
      var el = $(this);
      el.attr("disabled", "disabled");
      id_order = el.data('id_order');
      $.ajax({
        url: base_url + "index.php/order/change_tracking_number",
        type: 'POST',
        data: {"id_order":id_order, "tracking_number": $('#txt-tracking-number').val()},
        success: function(data) {
          el.removeAttr("disabled");
          $('#success-edit').show();
          $('.label-status').html("sudah kirim JNE");
          $('#label-tracking-number').html('No Resi:  '+$('#txt-tracking-number').val());
        },error: function(e){
          alert(e.status);
        }
      });
    });
  }
}