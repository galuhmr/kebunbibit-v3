var modal = new Modal();
modal.init();

function Modal() {
    var nextTask = null;

    this.init = function() {
        $("#kb-modal").hide();
        $(".kb-modal-form").hide();

        cartListButtonListener();
        addCartListener();
        loginListener();
        registerListener();
        addProductListener();

        modalCloseListener();

        $("#yes-confirm").click(function(){
            executeNextTask();
        });

        
    };

    function modalCloseListener() {
        $(".modal-close").click(function() {
            hideModalForm();
        });
    }

    function hideModalForm() {
        $("#kb-modal").fadeOut(100, function() {
            $(".kb-modal-form").fadeOut();
        });
    }

    function showModal(element) {
        $("#kb-modal").fadeIn(100, function() {
            element.fadeIn();
        });
    }

    function cartListButtonListener() {
        $("#cart-list-button").click(function() {
            showModal($("#cart-list"));
        });
    }

    function addCartListener() {
        $(".kb-cart").click(function() {
            showModal($("#cart-added-info"));
        });
    }

    function addProductListener() {
        $("#show-add-product").click(function() {
            showModal($("#input-product"));
        });
    }

    function loginListener() {
        $(".show-login").click(function() {
            showModal($("#form-login"));
        });
    }

    function registerListener() {
        $("#show-register").click(function() {
            showModal($("#form-register"));
        });
    }

    this.showConfirm = function(question) {
        $("#confirm-question").html(question);
        showModal($("#confirmation-dialog"));
        modalCloseListener();
        
    }

    this.showLoading = function() {
        $(".modal-content-custom").hide();
        $(".modal-loading").show();
    }

    this.hideLoading = function() {
        $(".modal-content-custom").show();
        $(".modal-loading").hide();
    }

    this.setNextTask = function(fn){
        nextTask = null;
        nextTask = fn;
    }

    function executeNextTask() {
        nextTask();
    }

}