var message = new Message();
message.init();

function Message() {

    this.init = function() {
        $("#inbox-badge").hide();
        
        sendButtonListener();
        qtyBargainChangeListener();
        bargainButtonListener();
        bargainAcceptButtonListener();
        bargainRejectButtonListener();
        
        scrollToBottom();
        localAutoRefresh();
    }

    function localAutoRefresh() {
        setInterval(function() {
            loadMessage();
        }, 1000);
        setInterval(function() {
            checkBargain();
        }, 1000);
        setInterval(function() {
            changeToRead();
        }, 1000);
        setInterval(function() {
            checkIsRead();
        }, 1000);
    }

    function checkIsRead() {
        var _url = base_url + kb_index + 'message/check-is-read';
        var _chatcode = $("#chatcode").val();

        $.ajax({
                type: "POST", // HTTP method POST or GET
                url: _url, //Where to make Ajax calls
                dataType: "json", // Data type, HTML, json etc.
                data: {"chatcode": _chatcode}, //Form variables
                success: function(data) {
                    $.each(data, function(key, message) {
                        if(message.is_read == 1) {
                            var caption = $(".chat-status-" + message.id_chat_detail).html();
                        
                            if(caption == "Diterima") {
                                $(".chat-status-" + message.id_chat_detail).html("Dibaca");
                            }
                        }
                    });
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
        });
    }

    function checkBargain() {
        var chatcode = $("#chatcode").val();
        var chatcode_receiver = $("#chatcode_receiver").val();
        var _url = base_url + kb_index + "message/find_bargain/" + chatcode;
        var status = $("#status").val();

        if (status == "buy") {
            _url = base_url + kb_index + "message/find_bargain/" + chatcode_receiver;
            
            $.ajax({
                type: "POST", // HTTP method POST or GET
                url: _url, //Where to make Ajax calls
                dataType: "json", // Data type, HTML, json etc.
                data: {}, //Form variables
                success: function(data) {
                    if (data.status != "null") { // ada tawaran yang belum diproses
                        $("#qty-bargain").prop("disabled", true);
                        $("#price-bargain").prop("disabled", true);
                        $("#btn-bargain").prop("disabled", true);
                    } else {
                        $("#qty-bargain").prop("disabled", false);
                        $("#price-bargain").prop("disabled", false);
                        $("#btn-bargain").prop("disabled", false);
                    }
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });
        } else {
            $.ajax({
                type: "POST", // HTTP method POST or GET
                url: _url, //Where to make Ajax calls
                dataType: "json", // Data type, HTML, json etc.
                data: {}, //Form variables
                success: function(data) {
                    if (data.status != "null") { // ada tawaran yang belum diproses
                        var str = "Pembeli menawar " + data.bargain.qty + " qty = " + data.bargain.total_format;

                        $("#qty-bargain").val(data.bargain.qty);
                        $("#price-bargain").val(data.bargain.total);
                        $(".message-send .bargain-receiver .caption").html(str);
                        
                        showBargainReceiver();
                    } else {
                        hideBargainReceiver();
                    }
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }

            });
        }

    }

    function hideBargainReceiver() {
        $(".message-send .bargain-receiver").hide();
        $(".message-send .message-box").show();
    }

    function showBargainReceiver() {
        $(".message-send .bargain-receiver").show();
        $(".message-send .message-box").hide();
    }

    function changeToRead() {
        if ($("#chatcode_receiver") != undefined) {
            var chatcode = $("#chatcode").val();
            var chatcode_receiver = $("#chatcode_receiver").val();
            var _url = base_url + kb_index + "message/change_to_read/" + chatcode_receiver;
            var _data = {"chatcode": chatcode};

            $.ajax({
                type: "POST", // HTTP method POST or GET
                url: _url, //Where to make Ajax calls
                dataType: "json", // Data type, HTML, json etc.
                data: _data, //Form variables
                success: function(data) {},
                error: function(xhr, ajaxOptions, thrownError) {
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });
        }
    }

    function loadMessage() {
        var _url = base_url + kb_index + "message/find_not_delivered";
        var chatcode = $("#chatcode").val();
        var _data = {"chatcode": chatcode};

        $.ajax({
            type: "POST", // HTTP method POST or GET
            url: _url, //Where to make Ajax calls
            dataType: "json", // Data type, HTML, json etc.
            data: _data, //Form variables
            success: function(data) {
                var str = "";
                
                if (data.status != "null") {
                    var bgsend = ""; // css class

                    if (data.message.message_type == 1) {
                        bgsend = "message1";
                    } else if (data.message.message_type == 2) {
                        bgsend = "message2";
                    } else if (data.message.message_type == 3) {
                        bgsend = "message3";
                    }

                    if (data.message.status == "in") {
                        str += '<li class="read-list">';
                        str += '<div class="read ' + bgsend + '">';
                        str += '<div class="content">';
                        str += data.message.message
                        str += '</div>';
                        str += '<div class="time">';
                        str += data.message.date_add_format;
                        str += '</div>';
                        str += '</div>';
                        str += '</li>';
                    } else {
                        str += '<li class="send-list">';
                        str += '<div class="send ' + bgsend + '">';
                        str += '<div class="content">';
                        str += data.message.message
                        str += '</div>';
                        str += '<div class="time">';
                        if (data.message.is_read == 1) {
                            str += data.message.date_add_format + ' - <i class="chat-status-' + data.message.id_chat_detail + '">Dibaca</i>';
                        } else {
                            str += data.message.date_add_format + ' - <i class="chat-status-' + data.message.id_chat_detail + '">Diterima</i>';
                        }
                        str += '</div>';
                        str += '</div>';
                        str += '</li>';
                    }

                    if ($(".message-display .placeholder").show()) {
                        $(".message-display .placeholder").hide();
                    }
                    
                    removeLoadingSend();
                    
                    $(".message-display ul").append(str);
                    
                    scrollToBottom();
                }

            },
            error: function(xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }

        });
    }

    function sendButtonListener() {
        $("#chat-message").keypress(function(event) {
            if (event.which == 13) {
                savePlainChat(); // pesan bukan hasil dari generate
            }
        });

        $("#btn-send").click(function() {
            savePlainChat(); // pesan bukan hasil dari generate
        });
    }

    function bargainButtonListener() {
        $("#btn-bargain").click(function() {
            saveBargain();
        });
    }

    function bargainAcceptButtonListener() {
        $("#bargain-accept").click(function() {
            acceptBargain();
        });
    }

    function bargainRejectButtonListener() {
        $("#bargain-reject").click(function() {
            rejectBargain();
        });
    }

    function qtyBargainChangeListener() {
        $("#qty-bargain").change(function() {
            var qty = $("#qty-bargain").val();
            var price = $("#for-multiply").val();

            var total_bargain = qty * price;
            $("#price-bargain").val(total_bargain);
        });
    }

    function scrollToBottom() {
        $(".message-display").animate({
            scrollTop: $(".message-display ul").height()
        }, "fast");
    }

    function showLoadingSend() {
        var str = '<li class="send-list loading">';
        str += '<div class="send">';
        str += '<div class="content">';
        str += '<img src="' + image_url + 'public/img/message-loading.gif">';
        str += '</div>';
        str += '<div class="time">';
        str += 'Sedang mengirim pesan';
        str += '</div>';
        str += '</div> ';
        str += '</li>';

        scrollToBottom();

        if ($(".message-display .placeholder").show()) {
            $(".message-display .placeholder").hide();
        }

        $(".message-display ul").append(str);
    }

    function removeLoadingSend() {
        $(".send-list.loading").eq(0).remove();
    }

    function savePlainChat() {
        var message = "<span>" + $("#chat-message").val() + "</span>";

        saveChat(message, 0);
    }

    function saveChat(message, message_type) {
        var _url = base_url + kb_index + "message/save-chat";
        var chatcode = $("#chatcode").val();
        var chatcode_receiver = $("#chatcode_receiver").val();
        var status = $("#status").val();
        var id_product = $("#id_product").val();
        var owner = $("#owner").val();
        var receiver = $("#receiver").val();
        var is_null = (message == '<span></span>');
        
        $("#chat-message").val("");

        if(!is_null) {
            showLoadingSend();
            $.ajax({
                type: "POST", // HTTP method POST or GET
                url: _url, //Where to make Ajax calls
                dataType: "json", // Data type, HTML, json etc.
                data: {
                    "chatcode": chatcode,
                    "chatcode_receiver": chatcode_receiver,
                    "message": message,
                    "status": status,
                    "id_product": id_product,
                    "owner": owner,
                    "receiver": receiver,
                    "message_type": message_type
                }, //Form variables
                success: function(data) {},
                error: function(xhr, ajaxOptions, thrownError) {
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }

            });
        }
    }

    function saveBargain() {
        var _url = base_url + kb_index + "message/save-bargain";
        var chatcode_seller = $("#chatcode_receiver").val();
        var id_product = $("#id_product").val();
        var qty_bargain = $("#qty-bargain").val();
        var total_bargain = $("#price-bargain").val();
        var buyer = $("#owner").val();
        var _data = { 
                        "chatcode_seller": chatcode_seller,
                        "id_product": id_product,
                        "qty_bargain": qty_bargain,
                        "total_bargain": total_bargain,
                        "buyer": buyer
                    };

        $("#qty-bargain").prop("disabled", true);
        $("#price-bargain").prop("disabled", true);
        $("#btn-bargain").prop("disabled", true);

        $.ajax({
            type: "POST", // HTTP method POST or GET
            url: _url, //Where to make Ajax calls
            dataType: "json", // Data type, HTML, json etc.
            data: _data, //Form variables
            success: function(data) {
                if(data.status == 'success'){    
                    var message = " menawar: <br> " + qty_bargain + " qty = k3bunb1b1tb4rg4in" + total_bargain;
                    
                    saveChat(message, 1);
                }    
            },
            error: function(xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }

        });

    }

    function rejectBargain() {
        var _url = base_url + kb_index + "message/reject-bargain";
        var chatcode_seller = $("#chatcode").val();
        var _data = { "chatcode_seller": chatcode_seller };

        $.ajax({
            type: "POST", // HTTP method POST or GET
            url: _url, //Where to make Ajax calls
            dataType: "json", // Data type, HTML, json etc.
            data: _data, //Form variables
            success: function(data) {
                saveChat("tolak", 3);
                hideBargainReceiver();
            },
            error: function(xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }

        });
    }

    function acceptBargain() {
        var _url = base_url + kb_index + "message/accept-bargain";
        var chatcode_seller = $("#chatcode").val();
        var _data = { "chatcode_seller": chatcode_seller };

        $.ajax({
            type: "POST", // HTTP method POST or GET
            url: _url, //Where to make Ajax calls
            dataType: "json", // Data type, HTML, json etc.
            data: _data, //Form variables
            success: function(data) {
                saveChat("terima", 2);
                addToCartFromBargain();
                hideBargainReceiver();
            },
            error: function(xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }

        });
    }


    function addToCartFromBargain() {
        var _url = base_url + kb_index + "cart/add-from-bargain";
        var qty_bargain = $("#qty-bargain").val();
        var total_bargain = $("#price-bargain").val();
        var id_seller = $("#owner").val();
        var id_buyer = $("#receiver").val();
        var product_id = $("#id_product").val();
        var product_name = $(".product-name").html();

        var _data = {
                        "total": total_bargain,
                        "id_seller": id_seller,
                        "id_buyer": id_buyer,
                        "product_id": product_id,
                        "product_name": product_name,
                        "product_price": total_bargain,
                        "qty": qty_bargain
                    };
        
        $.ajax({
            type: "POST", // HTTP method POST or GET
            dataType: "json",
            url: _url, //Where to make Ajax calls
            data: _data, //Form variables
            success: function(data) {},
            error: function(xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });
    }

}