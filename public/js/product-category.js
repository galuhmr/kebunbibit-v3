var product_category = new ProductCategory();
product_category.init();

function ProductCategory() {
    var i = 1;

    this.init = function() {
        loadProduct(i);
        
        loadMoreListener();
    }

    function hideLoading() {
        $("#load-more-loading").hide();
        $("#load-more").show();
    }

    function showLoading() {
        $("#load-more-loading").show();
        $("#load-more").hide();
    }

    function hideAll() {
        $("#load-more").hide();
        $("#load-more-loading").hide();
    }

    function loadMoreListener() {
        $("#load-more").click(function() {
            loadProduct(++i);
        });
    }

    function loadProduct(page) {
        var _url = base_url + kb_index + "product/load_more_category/" + page;
        var _category = $("#category-active").val();

        showLoading();

        $.ajax({
            type: "POST", // HTTP method POST or GET
            url: _url, //Where to make Ajax calls
            dataType: "html", // Data type, HTML, json etc.
            data: {"category": _category, "limit": 6}, //Form variables
            success: function(data) {
                if (data.length == 0 && page == 1) {
                    $("#product-category-content").append("<div class='text-center no-product'><i>Tidak ada produk di kategori ini</i></div>");
                    hideAll();
                } else if (data.length > 0) {
                    $("#product-category-content").append(data);
                    hideLoading();
                } else {
                    hideAll();
                }
                product_option.init();
                cart.init();
            },
            error: function(xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
            }

        });

    }
   
}