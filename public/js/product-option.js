var product_option = new ProductOption();
product_option.init();

function ProductOption() {
    var current_index = 0;
    var product_id = 0;
    var is_product_detail = ($("#Z4ARdvivjhAjpMhnEUFF").html() != undefined);

    this.init = function() {
        editProductListener();
        applyUpdateListener();
        tryAgainListener();
        doneEditListener();
        deleteProductListener();
    };

    function turnOnEdit(i) {
        $("#kb-modal").show();
        $(".li-edit").eq(i).addClass("li-on-top");
    }

    function turnOffEdit(i) {
        $("#kb-modal").hide();
        $(".li-edit").eq(i).removeClass("li-on-top");
    }

    function editProductListener() {
        $(".kb-update").click(function() {
            current_index = $(".kb-update").index(this);
            product_id = $(".c-product_id-edit").eq(current_index).val();
          
            turnOnEdit(current_index);
            startEditProduct(current_index);
        });

        $(".cancel-edit").click(function() {
            $(".read-product").eq(current_index).show();
            $(".update-product").eq(current_index).hide();
            
            turnOffEdit(current_index);
        }); 
    }

    function startEditProduct(i) {
        $(".update-product").hide();
        $(".read-product").show();
        $(".read-product").eq(current_index).hide();
        $(".update-product").eq(current_index).show();
    }

    function applyUpdateListener() {
        validatePrice();

        $(".apply-edit").click(function() {
            updateProduct(current_index);
        });
    }

    function deleteProductListener() {
        $(".kb-delete").click(function() {
            current_index = $(".kb-delete").index(this);
            product_id = $(".c-product_id-edit").eq(current_index).val();

            $('.read-product').eq(current_index).hide();
            $('.delete-product').eq(current_index).show();
            turnOnEdit(current_index);
        });

        $(".yes-delete").click(function() {
            deleteProduct();
        });

        $(".no-delete").click(function() {
            $('.delete-product').eq(current_index).hide();
            $('.read-product').eq(current_index).show();
            turnOffEdit(current_index);
        });
    }

    function deleteProduct() {
        var _url = base_url + kb_index + 'product/delete/' + product_id;

        $('.delete-product').eq(current_index).hide();
        
        showUpdateLoading();
        
        $.ajax({
            type: "POST",
            url: _url, // point to server-side PHP script 
            dataType: 'json', // what to expect back from the PHP script, if anything
            data: '',
            success: function(data) {
                KbHelper.refresh();
            },
            error: function(xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });
    }


    function isValid() {
        var newprice = $(".edit-product-price").eq(current_index).val();
        var trim = newprice.trim();

        if (trim.length == 0) {
            hideUpdateLoading();
            showErrorMessage("Harga tidak boleh kosong");
            return false;
        } else if (newprice <= minimum_price) {
            hideUpdateLoading();
            showErrorMessage("Harga minimum 10,000!");
            return false;
        }

        return true;
    }

    // hanya untuk angka
    function validatePrice() {
        $(".edit-product-price").keydown(function(e) {
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                // Allow: Ctrl+A
                (e.keyCode == 65 && e.ctrlKey === true) ||
                // Allow: Ctrl+C
                (e.keyCode == 67 && e.ctrlKey === true) ||
                // Allow: Ctrl+X
                (e.keyCode == 88 && e.ctrlKey === true) ||
                // Allow: home, end, left, right
                (e.keyCode >= 35 && e.keyCode <= 39)) {
                // let it happen, don't do anything
                return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        });
    }

    function doneEditListener() {
        $(".done-edit").click(function() {
            $(".validate-product").eq(current_index).hide();
            $(".read-product").eq(current_index).show();
            turnOffEdit(current_index);
        });
    }

    function tryAgainListener() {
        $(".try-again-edit").click(function() {
            $(".validate-product").eq(current_index).hide();
            $(".update-product").eq(current_index).show();
        });
    }

    function showUpdateLoading() {
        var i = current_index;

        $(".update-product").eq(i).hide();
        $(".validate-product .message").eq(i).hide();
        $(".validate-product .progressbar").eq(i).show();
        $(".validate-product").eq(i).show();
    }

    function hideUpdateLoading() {
        $(".validate-product .progressbar").eq(current_index).hide();
    }

    function showErrorMessage(str) {
        hideMessage();
        $(".validate-product .message").eq(current_index).show();
        $(".validate-product .message .error .alert-danger").eq(current_index).html(str);
        $(".validate-product .message .error").eq(current_index).show();
    }

    function hideMessage() {
        $(".validate-product .message").hide();
        $(".validate-product .message .success").hide();
        $(".validate-product .message .error").hide();
    }

    function showSuccessMessage(str) {
        hideMessage();
        $(".validate-product .message").eq(current_index).show();
        $(".validate-product .message .success .alert-success").eq(current_index).html(str);
        $(".validate-product .message .success").eq(current_index).show();
    }

    function updateProduct(i) {
        var newprice = $(".edit-product-price").eq(i).val();
        var _url = base_url + kb_index + "product/update_product/" + product_id;
        var description = $(".edit-desc").eq(i).val();
        var _data = { "price": newprice, "description": description};
        
        showUpdateLoading();

        if (isValid()) {
            $.ajax({
                type: "POST",
                url: _url, // point to server-side PHP script 
                dataType: 'json', // what to expect back from the PHP script, if anything
                data: _data,
                success: function(data) {
                    if(is_product_detail) {
                        KbHelper.refresh();
                    } else {
                        hideUpdateLoading();
                        showSuccessMessage("Produk berhasil dirubah");
                        $('.c-product_formatted_price-edit').eq(i).html(data.formatted_price);
                        $('.c-product_price-edit').eq(i).val(data.price);
                    }
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });
        }
    }
}