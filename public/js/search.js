var search = new Search();
search.init();

function Search() {
    var i = 1;

    this.init = function() {
        var q = $("#q").html();

        loadProduct(i);
        loadMoreListener();
    }

    function hideLoading() {
        $("#load-more-loading").hide();
        $("#load-more").show();
    }

    function showLoading() {
        $("#load-more-loading").show();
        $("#load-more").hide();
    }

    function hideAll() {
        $("#load-more").hide();
        $("#load-more-loading").hide();
    }

    function loadMoreListener() {
        $("#load-more").click(function() {
            loadProduct(++i);
        });
    }

    function loadProduct(page) {
        var q = $("#q").html();
        var _url = base_url + kb_index + "product/load_more_search/" + page;
        var _data = {
            "q": q
        }
        
        showLoading();

        $.ajax({
            type: "GET", // HTTP method POST or GET
            url: _url, //Where to make Ajax calls
            dataType: "html", // Data type, HTML, json etc.
            data: _data, //Form variables
            success: function(data) {
                
                if (data.length == 0 && page == 1) {
                    $("#profile-product").append("<div class='text-center no-product'><i>Produk tidak ditemukan</i></div>");
                    hideAll();
                } else if (data.length > 0) {
                    $("#profile-product").append(data);
                    hideLoading();
                } else {
                    hideAll();
                }

                cart.init();
                product_option.init();
            },
            error: function(xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }

        });

    }

}