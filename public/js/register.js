var register = new Register();
register.init();

function Register() {

    this.init = function() {
        setFailedCaption("Format email yang anda masukkan salah.");
        
        failedVerify();
        hideSuccess();
        hideFailedVerify()
        closeFailedListener();
        
        registerListener();
        toLoginListener();
    }

    function toLoginListener() {
        $("#to-login").click(function(){
            $("#form-login").show();
            $("#form-register").hide();
        });
    }

    function closeFailedListener() {
        $(".close").click(function() {
            $(".failed-verify").hide();
            hideSuccess();
        });
    }

    function hideSuccess() {
        $("#register-success").hide();
    }

    function showSuccess() {
        $("#register-success").show();
    }

    function hideFailedVerify() {
        $(".failed-verify").hide();
    }

    function failedVerify() {
        $(".failed-verify").show();
    }

    function setFailedCaption(str) {
        $(".failed-caption").html(str);
    }

    function registerListener() {
        $("#nc-email").keypress(function(event){
            if (event.which == 13) {
                registerCustomer();
            }
        });

        $("#register-button").click(function() {
            registerCustomer();
        });
    }

    function validateEmail(email) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }

    function showLoading() {
        $(".modal-content-custom").hide();
        $(".modal-loading").show();
    }

    function hideLoading() {
        $(".modal-content-custom").show();
        $(".modal-loading").hide();
    }

    function clearForm() {
        $("#nc-firstname").val('');
        $("#nc-lastname").val('');
        $("#nc-email").val('');
    }

    function registerCustomer() {
        var firstname = $("#nc-firstname").val();
        var lastname = $("#nc-lastname").val();
        var email = $("#nc-email").val();
        var str_error = "";

        if (firstname == null || firstname.length <= 0) {
            str_error = "<b>Gagal !</b> nama depan masih kosong.";
        } else if (lastname == null || lastname.length <= 0) {
            str_error = "<b>Gagal !</b> nama belakang masih kosong.";
        } else if (email == null || email.length <= 0) {
            str_error = "<b>Gagal !</b> email masih kosong.";
        } else if (!validateEmail(email)) {
            str_error = "Format email yang anda masukkan salah.";
        }

        if (str_error.length > 0) {
            setFailedCaption(str_error);
            failedVerify();
        } else {
            _data = {
                "firstname": firstname,
                "lastname": lastname,
                "email": email
            };

            validateRegisterCustomer(_data);
        }
    }

    function validateRegisterCustomer(data_register) {
        var email = $("#nc-email").val();
        var _url = base_url + kb_index + "customer/check_email";
        var _data = { "email": email };

        showLoading();

        $.ajax({
            type: "POST", // HTTP method POST or GET
            dataType: "json",
            url: _url, //Where to make Ajax calls
            data: _data, //Form variables
            success: function(data) {
                if (data.status === 'exist') {
                    hideLoading();
                    setFailedCaption("Email " + email + " sudah terdaftar.");
                    failedVerify();
                } else {
                    _url = base_url + kb_index + "customer/register";
                    _data = data_register;
                    startRegister(_url, _data);
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });
    }    

    function startRegister(_url, _data) {
        $.ajax({
            type: "POST", // HTTP method POST or GET
            dataType: "json",
            url: _url, //Where to make Ajax calls
            data: _data, //Form variables
            success: function(data) {
                if (data.status === 'failed') {
                    hideLoading();
                    setFailedCaption("Gagal.");
                    failedVerify();
                } else {
                    hideLoading();
                    hideFailedVerify();
                    showSuccess();
                    clearForm();
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });
    }

}