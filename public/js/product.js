var product = new Product();
product.init();

function Product() {
    var index_image = 1;

    this.init = function() {
        setCategories();
        postButtonListener();
        validatePrice();
        initSuccess();
        imageAddListener();
        uploadFileListener();
    }

    function initSuccess() {
        $("#save-success").hide();
        $(".close").click(function() {
            $("#save-success").hide();
        });
    }

    function showSuccess() {
        $("#save-success").show();
    }

    function removeImageListener() {
        $(".remove-img-btn").click(function() {
            var i = $(".remove-img-btn").index(this);
            
            if((i >= 0) && (i < images.length)){
                deleteImage(images[i]);
            
                images.splice(i, 1);

                var json = JSON.stringify(images);

                $("#images").val(json);

                $("#img-preview .li").eq(i).remove();
            }
        });
    }

    function deleteImage($image) {
        var _url = base_url + kb_index + "image/delete";

        $.ajax({
            type: "POST", // HTTP method POST or GET
            url: _url, //Where to make Ajax calls
            dataType: "json", // Data type, HTML, json etc.
            data: {"image-src": $image}, //Form variables
            success: function(data) {
                console.log(data.status);
            },
            error: function(xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });
    }

    function setCategories() {
        var _url = base_url + kb_index + "product/create-categories-api";

        $.ajax({
            type: "POST", // HTTP method POST or GET
            url: _url, //Where to make Ajax calls
            dataType: "json", // Data type, HTML, json etc.
            data: {}, //Form variables
            success: function(data) {
                var str = "";

                $.each(data, function(key, category) {
                    if(category.name != null) {
                        str += '<option value="' + category.id_category + '">' + category.name + '</option>';
                    }
                });

                $("#product-category").html(str);

            },
            error: function(xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });

    }

    // hanya untuk angka
    function validatePrice() {
        $("#product-price").keydown(function(e) {
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                // Allow: Ctrl+A
                (e.keyCode == 65 && e.ctrlKey === true) ||
                // Allow: Ctrl+C
                (e.keyCode == 67 && e.ctrlKey === true) ||
                // Allow: Ctrl+X
                (e.keyCode == 88 && e.ctrlKey === true) ||
                // Allow: home, end, left, right
                (e.keyCode >= 35 && e.keyCode <= 39)) {
                // let it happen, don't do anything
                return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        });
    }

    function imageAddListener() {
        $(".img-add").click(function() {
            $("#uploaded_file").click();
        });
    }

    function postButtonListener() {
        $("#post-button").click(function() {
            saveData();
        });
    }

    function setFailedCaption(str) {
        $(".failed-caption").html(str);
    }

    function failedVerify() {
        $(".failed-verify").show();
    }

    function showLoading() {
        $(".modal-content-custom").hide();
        $(".modal-loading").show();
    }

    function hideLoading() {
        $(".modal-content-custom").show();
        $(".modal-loading").hide();
    }

    function uploadFileListener() {
        document.getElementById('uploaded_file').addEventListener('change', saveImages, false);
    }


    function saveImages() {
        var _url = base_url + kb_index + "image/upload";
        var file_data = $('#uploaded_file').prop('files')[0];
        var form_data = new FormData();

        if (file_data !== undefined) {
            form_data.append('file', file_data);

            index_image += 1;

            var str = "";

            str += '<div class="li">';
            str += '<button type="button" class="remove-img-btn">';
            str += '<i class="fa fa-times-circle"></i></button>';
            str += '<img id="preview-' + index_image + '" class="image-size image-to-upload" src="' + image_url + 'public/img/loading-img.gif">';
            str += '</div>';

            $("#img-preview").prepend(str);

            $.ajax({
                type: "POST",
                url: _url, // point to server-side PHP script 
                dataType: 'json', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,
                success: function(data) {
                    if (data.status !== 'failed') {
                        images.push(data.image);
                        images.reverse();

                        $("#images").val(JSON.stringify(images));

                        $("#uploaded_file").val("");

                        $.each(images, function (index, image) {
                            $(".image-to-upload").eq(index).attr("src", base_url + 'public/uploads/' + image);
                            removeImageListener();
                        });
                    }
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });
        }
    }

    function saveData() {
        var _url = $("#imageform").attr('action');
        var name = $("#product-name").val();
        var price = $("#product-price").val();
        var id_category = $("#product-category").val();
        var images = $("#images").val();
        var description = $("#product-description").val();
        var str_error = "";

        if (images.length <= 2) {
            images = [];
        }

        var array_image = images;

        if (name == null || name.length <= 0) {
            str_error = "<b>Gagal !</b> nama produk masih kosong.";
        } else if (id_category == null || id_category.length <= 0) {
            str_error = "<b>Gagal !</b> kategori masih kosong.";
        } else if (price == null || price.length <= 0 || parseInt(price) <= 10000) {
            str_error = "<b>Gagal !</b> harga masih kosong / kurang dari 10.000.";
        } else if (price == 0) {
            str_error = "<b>Gagal !</b> harga tidak boleh 0.";
        } else if (array_image.length == 0) {
            str_error = "<b>Gagal !</b> gambar masih kosong.";
        }

        if (str_error.length > 0) {
            setFailedCaption(str_error);
            failedVerify();
        } else {
            showLoading();

            var _data = { 
                            "name": name,
                            "price": price,
                            "description": description,
                            "id_category": id_category,
                            "images": images
                        }    

            $.ajax({
                type: "POST", // HTTP method POST or GET
                url: _url, //Where to make Ajax calls
                data: _data, //Form variables
                success: function(data) {
                    hideLoading();
                    clearForm();
                    showSuccess();
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });
        }

    }

    function clearForm() {
        images = [];
        var json = JSON.stringify(images);

        $("#img-preview .li").remove();

        $("#product-name").val('');
        $("#product-price").val('');
        $("#product-description").val('');
        $("#images").val(json);
        $("#uploaded_file").val("");
    }

}