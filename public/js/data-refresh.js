var data_refresh = new DataRefresh();
data_refresh.init();

$(document).ready(function(){

    $(window).on('beforeunload', function(){
        return 'Are you sure you want to leave?';
    });
    
});

function DataRefresh() {

    this.init = function() {
        setInterval(function() {
            checkInbox();
        }, 1000);
        setInterval(function() {
            setCartCount();
        }, 1000);
        window.onbeforeunload = function() {
            return "Bye now!";
        };
    }

    function checkInbox() {
        var _url = base_url + kb_index + "message/check_inbox";

        $.ajax({
            type: "POST", // HTTP method POST or GET
            url: _url, //Where to make Ajax calls
            dataType: "json", // Data type, HTML, json etc.
            data: "", //Form variab les
            success: function(data) {
                $("#inbox-badge").html(data.inbox);
                console.log(data.inbox);
                if (data.inbox > 0) {
                    $("#inbox-badge").show();
                } else {
                    $("#inbox-badge").hide();
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }

        });
    }

    function setCartCount() {
        var _url = base_url + kb_index + 'cart/count';

        $("#cart-badge").hide();

        $.ajax({
            type: "POST", // HTTP method POST or GET
            dataType: "json",
            url: _url, //Where to make Ajax calls
            data: '', //Form variables
            success: function(data) {
                if (data.result > 0) {
                    $("#cart-badge").html(data.result);
                    $("#cart-badge").show();
                } else {
                    $("#cart-badge").html("0");
                    $("#cart-badge").hide();
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });
    }

}