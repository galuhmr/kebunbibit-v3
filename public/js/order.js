var order = new Order();
order.init();

function Order() {

    this.init = function() {
        $("#kb-modal").hide();
        $("#order-info").hide();
    
        orderButtonListener();
        closeOrderListener();
    
        hideOrderInfo();
    }

    function showOrderInfo() {
        $("#cart-list").hide('fast', function() {
            $("#order-info").show();
        });
    }

    function hideOrderInfo() {
        $("#kb-modal").hide("fast", function() {
            $("#order-info").hide();
        });
    }

    function closeOrderListener() {
        $("#close-order-info").click(function() {
            hideOrderInfo();
        });
    }

    function orderButtonListener() {
        $("#pay-button").click(function() {
            saveOrder();
        });
    }

    function showLoading() {
        showOrderInfo();
        $(".modal-content-custom").hide();
        $(".modal-loading").show();
    }

    function hideLoading() {
        $(".modal-content-custom").show();
        $(".modal-loading").hide();
    }

    function saveOrder() {
        var total_paid = $("#cart-total-plain").val();
        var _url = base_url + kb_index + 'order/save-order';
        var address = $('.cart-address').val();
        var zipcode = $('.cart-zipcode').val();
        var city = $('.cart-city').val();
        var id_address_delivery = $('.id_address_delivery').val();

        var is_address_valid = (address.length >= 10 && city.length > 2);
                    
        if (is_address_valid) {
            
            showLoading();

            $.ajax({
                type: "POST", // HTTP method POST or GET
                dataType: "json",
                url: _url, //Where to make Ajax calls
                data: {
                    "total_paid": total_paid,
                    "id_address_delivery": id_address_delivery
                }, //Form variables
                success: function(data) {
                    $("#order-email").html(data.email);
                    $("#reference").html(data.reference);
                    hideLoading();
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });
        } else {
            window.location.href = base_url + kb_index + "account";
        }
    }

}