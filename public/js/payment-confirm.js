var payment_confirm = new PaymentConfirm();
payment_confirm.init();

function PaymentConfirm() {

    this.init = function() {
        uploadFileListener();
        submitConfirmListener();
        $('.datepicker').datepicker();
    }

    function uploadFileListener() {
        document.getElementById('transfer-photo').addEventListener('change', saveImages, false);
    }

    function submitConfirmListener() {
        $("#submit-confirm").click(function(e){
            var orderno = $("#orderno").val();
            var customer_name = $("#customer-name").val();
            var customer_email = $("#customer-email").val();
            var bank = $("#payment-confirm-bank").val();
            var transfer_date = $("#transfer-date").val();
            var transfer_nominal = $("#transfer-nominal").val();
            var transfer_photo = $("#transfer-photo").val();

            e.preventDefault();
            
            $(".error-message").hide();

            if(orderno.trim().length == 0) {    
                $("#orderno-empty").show();
            } else if(customer_name.trim().length == 0) {
                $("#customer-name-empty").show();
            } else if(customer_email.trim().length == 0) {
                $("#email-empty").show();
            } else if(!isEmailValid(customer_email)) {
                $("#email-not-valid").show();
            } else if(transfer_date.trim().length == 0) {
                $("#transfer-date-empty").show();
            } else if(transfer_nominal.trim().length == 0) {
                $("#transfer-nominal-empty").show();
            } else if(transfer_photo.trim().length == 0) {
                $("#transfer-photo-empty").show();
            } else {
                 var payment_confirm_data = {};

                 payment_confirm_data.reference_number = orderno;
                 payment_confirm_data.customer_name = customer_name;
                 payment_confirm_data.email = customer_email;
                 payment_confirm_data.transfer_photo = $(".preview-transfer").attr("src");
                 payment_confirm_data.date_transfer = transfer_date;
                 payment_confirm_data.bank = bank;
                 payment_confirm_data.nominal = transfer_nominal;

                 doSubmit(payment_confirm_data);    
            }
        });
    }

    function isEmailValid($email) {
       return KbHelper.isEmailValid($email);
    }

    function doSubmit(payment_confirm_data) {
        var _url = $("#payment-confirm-form").attr('action');
        var _data = $("#payment-confirm-form").serialize();

        _data = {
             "reference_number": payment_confirm_data.reference_number,
             "customer_name": payment_confirm_data.customer_name,
             "email": payment_confirm_data.email,
             "transfer_photo": payment_confirm_data.transfer_photo,
             "date_transfer": payment_confirm_data.date_transfer,
             "bank": payment_confirm_data.bank,
             "nominal": payment_confirm_data.nominal
        }

        $("#submit-loading").show();

        $.ajax({
            type: "POST", // HTTP method POST or GET
            url: _url, //Where to make Ajax calls
            dataType: "json", // Data type, HTML, json etc.
            data: _data, //Form variables
            success: function(data) {
                $("#submit-loading").hide();

                $(".error-message").hide();
                $(".success-message").hide();

                if(data.status == 'exist') {
                    $("#orderno-exist").show();
                } else if(data.status == 'failed') {
                    $("#orderno-wrong").show();
                    $("#success-submit").hide();
                } else {
                    $("#orderno-wrong").hide();
                    $("#success-submit").show();
                    KbHelper.refresh();
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });

    }

    function errorMessage() {}

    function saveImages() {
        var filename = $('#transfer-photo').val();
        var form_data = new FormData();

        if(filename.trim().length > 0) {
            var _url = base_url + kb_index + "image/upload";
            var file_data = $('#transfer-photo').prop('files')[0];

            if (file_data !== undefined) {
                form_data.append('file', file_data);

                var loading_image = '<img class="preview-transfer" src="' + image_url + 'public/img/loading-img.gif">';
                
                $("#transfer-photo-wrap").html(loading_image );

                $.ajax({
                    type: "POST",
                    url: _url, // point to server-side PHP script 
                    dataType: 'json', // what to expect back from the PHP script, if anything
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: form_data,
                    success: function(data) {
                        if (data.status !== 'failed') {
                            $(".preview-transfer").attr("src", base_url + "public/uploads/" + data.image);
                        } else {
                           $("#transfer-photo-wrap").empty(); 
                        }
                    },
                    error: function(xhr, ajaxOptions, thrownError) {
                        console.log(xhr.status);
                        console.log(xhr.responseText);
                        console.log(thrownError);
                    }
                });
            }    
        } else {
           $("#transfer-photo-wrap").empty(); 
        }    
    }

}