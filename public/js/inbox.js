var inbox = new Inbox();
inbox.init();

function Inbox() {

    this.init = function() {
        chooseAllListener();
        deleteListener();
    }

    function chooseAllListener() {
        $("input[name='inbox-choose-all'").change(function() {
            $("input[name='inbox-choose']").prop( "checked", this.checked );
        });
    }

    function deleteListener() {
        $(".inbox-delete").click(function() {
            modal.showConfirm("Anda yakin ingin menghapus?");
            modal.setNextTask(startDelete); // jika yes
        });
    }

    function startDelete(){
         $("input[name='inbox-choose']").each(function() {
            var i = $("input[name='inbox-choose']").index(this);
            var data = null;

            if (this.checked) {
                chatcode = $("input[name='inbox-choose']").eq(i).val(); 
                processDelete(chatcode);
            }
        });
    }

    function processDelete(chatcode) {
        var _url = base_url + kb_index + 'message/delete/' + chatcode;
        
        modal.showLoading();
        
        $.ajax({
            type: "POST", // HTTP method POST or GET
            dataType: "json",
            url: _url, //Where to make Ajax calls
            data: {}, //Form variables
            success: function(data) {
                if(data.status == 'success') {
                    KbHelper.refresh();
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });
    }

}