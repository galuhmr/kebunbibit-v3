$( document ).ready(function() {
	var isRunning = true;

	$("#logout-btn").click(function(e){
		isRunning = false;
	});

    $( window ).bind('load', function() {
    	setInterval(function() {
            changeToOnline();
        }, 3000);
    });

    $(window).bind('unload', function(){
    	changeToOffline();
    });

    function changeToOnline() {
        var _url = base_url + kb_index + "customer/change-status";
        var id_customer = $("#kebunbibit").val();
        
        if(id_customer.length > 0) {
		    if(isRunning) {    
		        $.ajax({
		            type: "POST", // HTTP method POST or GET
		            url: _url, //Where to make Ajax calls
		            dataType: "json", // Data type, HTML, json etc.
		            data: { "is_online": 1, "id_customer": id_customer }, //Form variables
		            success: function(data) {
		                // console.log(data);
		            },
		            error: function(xhr, ajaxOptions, thrownError) {
		                console.log(xhr.status);
		                console.log(xhr.responseText);
		                console.log(thrownError);
		            }
		        });
		    }    
	    }    
    }

    function changeToOffline() {
        var _url = base_url + kb_index + "customer/change-status";
        var id_customer = $("#kebunbibit").val();

        isRunning = false;

        if(id_customer.length > 0) {
	        $.ajax({
	            type: "POST", // HTTP method POST or GET
	            url: _url, //Where to make Ajax calls
	            async: false,
	            dataType: "json", // Data type, HTML, json etc.
	            data: {"is_online": 0, "id_customer": 2195 }, //Form variables
	            success: function(data) {
	                
	            },
	            error: function(xhr, ajaxOptions, thrownError) {
	                console.log(xhr.status);
	                console.log(xhr.responseText);
	                console.log(thrownError);
	            }
	        });
	    }   
    }

});