var kb_global = new KbGlobal();
kb_global.init();

function KbGlobal() {

    this.init = function() {
        setInterval(function() {
            countNewInbox(); // jumlah pesan baru
        }, 1000);
        setInterval(function() {
            cart.updateCartCount();
        }, 1000);
 
        searchListener();
    }

    function searchListener() {
        $("#txt_search").keypress(function(event) {
            if (event.which == 13) {
                var q = $("#txt_search").val();
                location.href = base_url + kb_index + 'pencarian?q=' + q;
            }
        });

        $("#btn_search").click(function() {
            var q = $("#txt_search").val();
            location.href = base_url + kb_index + 'pencarian?q=' + q;
        });
     }

    function countNewInbox() {
        var _url = base_url + kb_index + "message/total_new_inbox";

        $.ajax({
            type: "POST", // HTTP method POST or GET
            url: _url, //Where to make Ajax calls
            dataType: "json", // Data type, HTML, json etc.
            data: "", //Form variables
            success: function(inbox) {
                $("#inbox-badge").html(inbox.total);

                if (inbox.total > 0) {
                    $("#inbox-badge").show();
                } else {
                    $("#inbox-badge").hide();
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });
    }

}