var cart = new Cart();
cart.init();

function Cart() {
    var cart_data = [];

    this.init = function() {
        hideCartLoading();
        
        $("#cart-badge").hide();
        
        addToCartListener();
        checkboxListener();
        deleteCartListener();
        cartListButtonListener();
        
        updateCartData();
    };

    this.updateCartCount = function() {
        var _url = base_url + kb_index + 'cart/count';

        $.ajax({
            type: "POST", // HTTP method POST or GET
            dataType: "json",
            url: _url, //Where to make Ajax calls
            data: {}, //Form variables
            success: function(data) {
                $("#cart-badge").html(data.count);
                
                if(data.count > 0){
                    $("#cart-badge").show();
                } else {
                    $("#cart-badge").hide();
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });
    }

    function cartListButtonListener() {
        $("#cart-list-button").click(function() {
            updateCartData();
        });
    }

    function updateCartData() {
        if ($("#cart-badge") != undefined) {

            var _url_delete = base_url + kb_index + 'cart/delete_product/';
            var _url = base_url + kb_index + 'cart/cart_data';

            $.ajax({
                type: "POST", // HTTP method POST or GET
                dataType: "json",
                url: _url, //Where to make Ajax calls
                data: {}, //Form variables
                success: function(data) {
                    var str = '';

                    $.each(data, function(key, val) {
                        var seller = val.carts[0];
                        var carts_seller = val;
                        var is_seller_selected = (seller.is_check == 1);

                        str += '<table class="table table-bordered cart-table">';
                        str += '  	<tr>';
                        str += '  	 <td style="width: 10%;">';

                        if (is_seller_selected) { // yes, seller is selected
                            str += '  	   <input type="checkbox" class="rowcart" value="' + seller.id_seller + '"  checked>';
                        } else {
                            str += '  	   <input type="checkbox" class="rowcart" value="' + seller.id_seller + '">';
                        }

                        str += '  	 </td>';
                        str += '  	 <td class="text-left" colspan="4">';
                        str += '  	   <b>';
                        str += seller.firstname + ' ' + seller.lastname;
                        str += '	   </b>';
                        str += '	   <input class="product-price-hidden" ';
                        str += '			  type="hidden" ';
                        str += '			  value="' + carts_seller.total + '">';
                        str += '  	 </td>';
                        str += '  	</tr>';

                        $.each(carts_seller.carts, function(key, product) {
                            str += '	  <tr class="cart-detail-item">';
                            str += '	  	  <td style="width: 10%;">';
                            str += '	  	  </td>';
                            str += '	  	  <td style="width: 10%;">';
                            str += '	  	   <img style="height: 50px; width:50px;" ';
                            str += '	  	  	    src="' + product.img_path + '">';
                            str += '	  	  </td>';
                            str += '	  	  <td style="width: 40%; text-align: left;">';
                            str += product.product_name + '<p>' + product.qty + ' pcs</p>';
                            str += '	  	  </td>';
                            str += '	  	  <td style="width: 30%; text-align: left;">';
                            str += product.format_price;
                            str += '	  	  </td>';
                            str += '	  	  <td style="width: 10%;">';
                            str += '	  	 	<a class="delete-cart-button" ';
                            str += '	  	 	   href="' + _url_delete + product.cart_detail_id + '">';
                            str += '	  	 		<i class="fa fa-trash delete-cart-row"></i>';
                            str += '	  	 	</a>';
                            str += '	  	  </td>';
                            str += '	  </tr>';
                        });

                        str += '</table>';

                    });

                    if (!KbHelper.isEmpty(str)) {
                        str += '<div class="col-md-12" id="view-address"></div>';
                        getAddress();
                    }

                    $("#cart-data").html(str);
                    
                    checkboxListener();
                    deleteCartListener();
                    processChange();
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });

        }
    }

    function hideCartLoading() {
        $(".cart-loading").hide();
        $(".cart-content").show();
        $("#pay-button").show();
    }

    function showCartLoading() {
        $(".cart-loading").show();
        $(".cart-content").hide();
        $("#pay-button").hide();
    }

    function showLoading() {
        $(".modal-content-custom").hide();
        $(".modal-loading").show();
    }

    function hideLoading() {
        $(".modal-content-custom").show();
        $(".modal-loading").hide();
    }

    function addToCartListener() {
        $('.kb-cart').click(function() {
            var i = $(".kb-cart").index(this);
            addToCart(i);
        });
    }

    function deleteCartListener() {
        $('.delete-cart-button').click(function(e) {
            e.preventDefault();
            
            var i = $(".delete-cart-button").index(this);
            var _url = $(".delete-cart-button").eq(i).attr('href');

            $.ajax({
                type: "GET", // HTTP method POST or GET
                dataType: "json",
                url: _url, //Where to make Ajax calls
                data: "", //Form variables
                success: function(data) {
                    $(".cart-detail-item").eq(i).remove();

                    updateCartData();
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                }
            });
        });
    }

    function pushToCartData(arr) {
        cart_data.push(arr);
    }

    function changeCheckStatus() {
        var _url = base_url + kb_index + 'cart/change_check_status';
        var _cart_data = cart_data;

        $.ajax({
            type: "POST", // HTTP method POST or GET
            dataType: "json",
            url: _url, //Where to make Ajax calls
            data: {
                "cart_data": _cart_data
            }, //Form variables
            success: function(data) {},
            error: function(xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });
    }

    function checkboxListener() {
        $('.rowcart[type=checkbox]').click(function() {
            processChange();
        });
    }

    function processChange() {
        var _url = base_url + kb_index + "cart/total_cart";
        var _data;
        var total = 0;
        
        showCartLoading();

        $('.rowcart[type=checkbox]').each(function() {
            var i = $(".rowcart[type=checkbox]").index(this);
            var data = [];

            if (this.checked) {
                var price = $(".product-price-hidden").eq(i).val();

                total += Number(price);

                data = [];
                data[0] = $(".rowcart[type=checkbox]").eq(i).val(); // id_seller
                data[1] = 1; // check_status
            } else {
                data = [];
                data[0] = $(".rowcart[type=checkbox]").eq(i).val(); // id_seller
                data[1] = 0; // check_status
            }

            pushToCartData(data);
            
        });

        _data = {"total": total};

        $.ajax({
            type: "POST", // HTTP method POST or GET
            dataType: "json",
            url: _url, //Where to make Ajax calls
            data: _data, //Form variables
            success: function(total) {
                var total_plain = total.plain; // dalam bentuk angka
                var total = total.formatted; // dalam betuk Rp.

                hideCartLoading();
                if (total == "Rp 0") {
                    total = "(Kosong)";
                    $("#pay-button").hide();
                } else {
                    $("#pay-button").show();
                }

               
                $("#cart-total-plain").val(total_plain);
                $("#cart-total").html(total);
                
                changeCheckStatus();

            },
            error: function(xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });
    }

    function addToCart(i) {
        var total = $('.c-product_price').eq(i).val();
        var id_seller = $('.c-id_seller').eq(i).val();
        var seller_name = $('.c-seller_name').eq(i).html();
        var product_id = $('.c-product_id').eq(i).val();
        var product_name = $('.c-product_name').eq(i).val();
        var product_price = $('.c-product_price').eq(i).val();
        var product_formatted_price = $('.c-product_formatted_price').eq(i).html();
        var product_image = $('.product-img').eq(i).attr('src');

        var _data = {
                        "total": total, 
                        "id_seller": id_seller,
                        "product_id": product_id,
                        "product_name": product_name,
                        "product_price": product_price
                    };

        var _url = base_url + kb_index + "cart/add";

        showLoading();

        if ($('.c-seller_name').eq(i) === undefined) {
            seller_name = $('.c-seller_name').html();
        }

        $("#ac-product-img").attr("src", product_image);
        $("#ac-product-name").html(product_name);
        $("#ac-product-price").html(product_formatted_price);
        $("#ac-product-seller").html(seller_name);

        $.ajax({
            type: "POST", // HTTP method POST or GET
            dataType: "json",
            url: _url, //Where to make Ajax calls
            data: _data, //Form variables
            success: function(data) {
                hideLoading();
            },
            error: function(xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });
    }

    function getAddress() {
        var str = "";
        var _url = base_url + kb_index + "customer/get_address";
        
        $.ajax({
            type: "POST", // HTTP method POST or GET
            dataType: "json",
            url: _url, //Where to make Ajax calls
            success: function(data) {
                $.each(data, function(key, addr) {
                    $('.cart-address').val(addr.address);
                    $('.cart-zipcode').val(addr.postcode);
                    $('.cart-city').val(addr.city);
                    $('.id_address_delivery').val(addr.id_address);
                    
                    str =   '<div class="col-md-3">Alamat</div>' +
                            '<div class="col-md-9" style="text-align:left;">' +
                                addr.address + '<br/>' + addr.city + ' ' +
                                addr.postcode + ' ' +
                                '<a href="' + base_url + kb_index + 'account">' +
                                    '<i class="fa fa-pencil"></i> edit alamat' +
                                '</a>' +
                            '</div>';

                    var is_address_valid = (addr.address.length >= 10 && addr.city.length > 2);
                    
                    if(!is_address_valid) { // alamatnya bener nggak?
                        str = "Alamat tidak lengkap " +
                              '<a href="' + base_url + kb_index + 'account">' +
                              '<i class="fa fa-pencil"></i> edit alamat</a>';
                    }

                    $("#view-address").html(str);
                    
                });
            },
            error: function(xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });
    }
}