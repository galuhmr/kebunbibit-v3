var product_detail = new ProductDetail();
product_detail.init();

function ProductDetail() {
    var current_index = 0;
    var last_index = 0;

    this.init = function() {
        imageClickListener();
    };

    function setActive(i) {
        $('.img-small-click').eq(i).fadeTo( "fast" , 0.5);
    }

    function setDefault(i){
        $('.img-small-click').eq(i).fadeTo( "fast" , 1);
    }

    function imageClickListener() {
        setActive(current_index);

        $('.img-small-click').click(function(){
            current_index = $('.img-small-click').index(this);
            var _zoom_img = $('.img-small-click').eq(current_index).attr('src'); 

            setDefault(last_index);
            setActive(current_index);

            $("#zoom-img").attr('src', _zoom_img);

            last_index = current_index;
        });
    }

}