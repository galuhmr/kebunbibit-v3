var profil = new Profil();
profil.init();

function Profil() {
    var i = 1;

    this.init = function() {
        loadProduct(i);
        
        loadMoreListener();
        scrollListener();
    }

    function hideLoading() {
        $("#load-more-loading").hide();
        $("#load-more").show();
    }

    function showLoading() {
        $("#load-more-loading").show();
        $("#load-more").hide();
    }

    function hideAll() {
        $("#load-more").hide();
        $("#load-more-loading").hide();
    }

    function loadMoreListener() {
        $("#load-more").click(function() {
            loadProduct(++i);
        });
    }

    function scrollListener() {
        $(window).scroll(function() {
            var scrollTop = $(window).scrollTop();
            if (scrollTop > 200) {
                if (scrollTop > ($("#profile-product").height() - 50)) {
                    $(".profil-box").removeClass("fixed");
                    $(".profil-box").css({
                        top: $("#profile-product").height() - 350,
                        position: 'relative'
                    });

                } else {
                    $(".profil-box").css({
                        top: 20,
                        position: 'fixed'
                    });
                }
            } else {
                $(".profil-box").css({
                    top: 0,
                    position: 'static'
                });
            }

        });
    }

    function loadProduct(page) {
        var id_seller = $("#id_user").val();
        var _url = base_url + kb_index + "product/seller_products/" + id_seller + "/" + page;

        showLoading();

        $.ajax({
            type: "GET", // HTTP method POST or GET
            url: _url, //Where to make Ajax calls
            dataType: "html", // Data type, HTML, json etc.
            data: "", //Form variables
            success: function(data) {
                if (data.length == 0 && page == 1) {
                    $("#profile-product").append("<div class='text-center no-product'><i>Tidak ada produk yang dijual</i></div>");
                    hideAll();
                } else if (data.length > 0) {
                    $("#profile-product").append(data);
                    hideLoading();
                } else {
                    hideAll();
                }
                product_option.init();
                cart.init();
            },
            error: function(xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
            }

        });

    }
   
}