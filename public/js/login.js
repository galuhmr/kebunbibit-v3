var login = new Login();
login.init();

function Login() {

    this.init = function() {
        hideLoading();
        
        initFailed();
        userMenuToggle();
    
        loginListener();
        googleLoginListener();
        forgetPasswordListener();
        resetPasswordListener();
        $("#success-reset").hide();
        toRegisterListener();
    }

    function userMenuToggle() {
        $("#user-menu").hide();
        $("#user-account").click(function() {
            $("#user-menu").toggle();
        });
    }

    function toRegisterListener() {
        $("#to-register").click(function(){
            $("#form-login").hide();
            $("#form-register").show();
        });
    }

    function forgetPasswordListener() {
        $("#forget-password-link").click(function() {
            $("#forget-password-link").hide();
            $("#not-forget-password-link").show();
            $("#login-display").hide();
            $("#forget-password-display").show();
        });

        $("#not-forget-password-link").click(function() {
            $("#forget-password-link").show();
            $("#not-forget-password-link").hide();
            $("#login-display").show();
            $("#forget-password-display").hide();
        });
    }

    function resetPasswordListener() {
        $("#user-email-for-reset").keypress(function(event) {
            if (event.which == 13) {
                doResetPassword();
            }
        });

        $("#reset-password").click(function(){
            doResetPassword();
        });
    }

    function doResetPassword() {
        var email = $("#user-email-for-reset").val();
        var _url = base_url + kb_index + "customer/reset-password";
        var _data = {"email": email};

        $("#success-reset").hide();
        initFailed();

        if (email.length == 0) {
            setFailedCaption("Email masih kosong.");
            failedVerify();

            return;
        }

        if (!KbHelper.isEmailValid(email)) {
            setFailedCaption("Format email yang anda masukkan salah.");
            failedVerify();

            return;
        }

        showLoading();

        $.ajax({
            type: "POST", // HTTP method POST or GET
            dataType: "json",
            url: _url, //Where to make Ajax calls
            data: _data, //Form variables
            success: function(reset) {
                if (reset.status === 'failed') {
                    hideLoading();
                    setFailedCaption("Email tidak terdaftar.");
                    failedVerify();
                } else {
                    hideLoading();
                    $("#success-reset").show();
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });
    }

    function setFailedCaption(str) {
        $(".failed-caption").html(str);
    }

    function initFailed() {
        $(".failed-verify").hide();
        $(".close").click(function() {
            $(".failed-verify").hide();
        });
    }

    function failedVerify() {
        $(".failed-verify").show();
    }

    function loginListener() {
        $("#user-password").keypress(function(event) {
            if (event.which == 13) {
                $("#login-button").click();
            }
        });

        $("#login-button").click(function() {
            var email = $("#user-email").val();
            var password = $("#user-password").val();

            if (email.length == 0) {
                setFailedCaption("Email masih kosong.");
                failedVerify();

                return;
            }

            if (!KbHelper.isEmailValid(email)) {
                setFailedCaption("Format email yang anda masukkan salah.");
                failedVerify();

                return;
            }

            if (password.length == 0) {
                setFailedCaption("Password masih kosong.");
                failedVerify();

                return;
            }

            showLoading();
            startLogin();

        });
    }

    function showLoading() {
        $(".modal-content-custom").hide();
        $(".modal-loading").show();
    }

    function hideLoading() {
        $(".modal-content-custom").show();
        $(".modal-loading").hide();
    }

    function startLogin() {
        var email = $("#user-email").val();
        var password = $("#user-password").val();
        var _url = base_url + kb_index + "customer/login";
        var _data = {"email": email, "password": password};

        $.ajax({
            type: "POST", // HTTP method POST or GET
            dataType: "json",
            url: _url, //Where to make Ajax calls
            data: _data, //Form variables
            success: function(login) {
                if (login.status === 'failed') {
                    console.log(login.results);
                    hideLoading();
                    setFailedCaption("Email atau password yang anda masukkan salah.");
                    failedVerify();
                } else {
                    KbHelper.refresh();
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });
    }

    /**
     * Google
     */
    function googleLoginListener() {
        var googleUser = {};
        var startApp = function() {
            gapi.load('auth2', function() {
                // Retrieve the singleton for the GoogleAuth library and set up the client.
                auth2 = gapi.auth2.init({
                    client_id: '419879806369-die6ju7ljlrc7acb291gksmlnsagl1n9.apps.googleusercontent.com',
                    cookiepolicy: 'single_host_origin',
                    // Request scopes in addition to 'profile' and 'email'
                    //scope: 'additional_scope'
                });
                attachSignin(document.getElementById('login-google'));
            });
        };

        function attachSignin(element) {
            auth2.attachClickHandler(element, {},
                function(googleUser) {
                    var profile = googleUser.getBasicProfile();
                    // console.log('ID: ' + profile.getId()); // Do not send to your backend! Use an ID token instead.
                    var firstname = profile.getName();
                    var lastname = " ";
                    var img_path = profile.getImageUrl() + "?sz=250";
                    var email = profile.getEmail();

                    googleLogin(email, firstname, lastname, img_path);
                },
                function(error) {
                    alert(JSON.stringify(error, undefined, 2));
                });
        }

        startApp();
    }

    function googleLogin(email, firstname, lastname, img_path) {
        var _url = base_url + kb_index + "customer/google_login";
        var _data = {
                        "email": email,
                        "firstname": firstname,
                        "lastname": lastname,
                        "img_path": img_path 
                    };

        showLoading();
        
        $.ajax({
            type: "POST", // HTTP method POST or GET
            dataType: "json",
            url: _url, //Where to make Ajax calls
            data: _data, //Form variables
            success: function(login) {
                if (login.status === 'failed') {
                    hideLoading();
                    setFailedCaption("Login menggunakan google gagal.");
                    failedVerify();
                } else if (login.status === 'exist') {
                    hideLoading();
                    setFailedCaption("Email sudah digunakan");
                    failedVerify();
                } else {
                    KbHelper.refresh();
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });
    }

    /**
     * Facebook
     */

    function statusChangeCallback(response) {
        console.log('statusChangeCallback');
        console.log(response.status);
        // The response object is returned with a status field that lets the
        // app know the current login status of the person.
        // Full docs on the response object can be found in the documentation
        // for FB.getLoginStatus().
        if (response.status === 'connected') {
            var accessToken = response.authResponse.accessToken;
            console.log("Token: " + accessToken);

            // Logged into your app and Facebook.
            executeFacebookAPI();
        } else if (response.status === 'not_authorized') {
            // The person is logged into Facebook, but not your app.
            console.log('Please log into this app.');
        } else {
            // The person is not logged into Facebook, so we're not sure if
            // they are logged into this app or not.
            console.log('Please log into facebook.');

        }
    }

    function facebookLogin(email, firstname, lastname, img_path) {
        var _url = base_url + kb_index + "customer/facebook_login";
        
        showLoading();

        $.ajax({
            type: "POST", // HTTP method POST or GET
            dataType: "json",
            url: _url, //Where to make Ajax calls
            data: {
                "email": email,
                "firstname": firstname,
                "lastname": lastname,
                "img_path": img_path
            }, //Form variables
            success: function(login) {
                if (login.status === 'failed') {
                    hideLoading();
                    setFailedCaption("Login menggunakan facebook gagal.");
                    failedVerify();
                } else if (login.status === 'exist') {
                    hideLoading();
                    setFailedCaption("Email sudah digunakan");
                    failedVerify();
                } else {
                    KbHelper.refresh();
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });
    }

    window.fbAsyncInit = function() {
        FB.init({
            appId: '900768870028674',
            cookie: true, // enable cookies to allow the server to access 
            // the session
            xfbml: true, // parse social plugins on this page
            version: 'v2.5' // use graph api version 2.5
        });

        // Now that we've initialized the JavaScript SDK, we call 
        // FB.getLoginStatus().  This function gets the state of the
        // person visiting this page and can return one of three states to
        // the callback you provide.  They can be:
        //
        // 1. Logged into your app ('connected')
        // 2. Logged into Facebook, but not your app ('not_authorized')
        // 3. Not logged into Facebook and can't tell if they are logged into
        //    your app or not.
        //
        // These three cases are handled in the callback function.

        $("#login-facebook").click(function() {
            FB.login(function(response) {
                statusChangeCallback(response);
            });
        });

    };

    // Load the SDK asynchronously
    (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

    // Here we run a very simple test of the Graph API after login is
    // successful.  See statusChangeCallback() for when this call is made.
    function executeFacebookAPI() {
        console.log('Welcome!  Fetching your information.... ');
        FB.api('/me?fields=id,email,first_name,last_name', function(response) {
            var email = response.id;
            var first_name = response.first_name;
            var last_name = response.last_name;

            if (response.email !== undefined) {
                email = response.email;
                FB.api('/me/picture?height=250',
                    function(response) {
                        if (response && !response.error) {
                            console.log(response.data.url);
                            facebookLogin(email, first_name, last_name, response.data.url);
                        }
                    }
                );
            } else {
                hideLoading();
                setFailedCaption("Email tidak ditemukan di akun Facebook anda.");
                failedVerify();
            }

        });
    }

}